#include "GameSettings.h"
#include "Input/KeyBindings.h"
#include "Graphics/Frame.h"

GameSettings::GameSettings(BinaryReaderPtr reader)
{
    /*
    reader.readUnsignedInt(&_clientWidth);
    reader.readUnsignedInt(&_clientHeight);
    reader.readBoolean(&_isMusicMuted);
    reader.readBoolean(&_areSoundEffectsMuted);
    reader.readBoolean(&_shouldMuteInBackground);

    uint8_t textSpeedUInt8;
    reader.readUnsignedChar(&textSpeedUint8_t);
    _textSpeed = static_cast<TextSpeed>(textSpeedUint8_t);

    reader.readBoolean(&_shouldUseMouse);
    reader.readBoolean(&_isPostLatinEnabled);
    reader.readFloat(&_musicVolume);
    _frameHandle = reader.readString();
    _textboxFrameHandle = reader.readString();
*/
    _keyBindings = new KeyBindings(reader);
}

GameSettings::~GameSettings()
{
    if (_keyBindings != nullptr)
    {
        delete _keyBindings;
        _keyBindings = nullptr;
    }
}

// Member access functions (nothing too special beyond this point)
const KeyBindings& GameSettings::getKeyBindings() const
{
    return *_keyBindings;
}

FramePtr GameSettings::getFrame() const
{
    return Frame::get(0);
}

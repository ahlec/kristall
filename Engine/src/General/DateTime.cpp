#include "General/DateTime.h"

template<>
String toString<DateTime>(DateTime value)
{
    String str(toString(value.dayOfWeek));
    str += ", ";
    str += toString(value.day);
    str += " ";
    str += toString(value.month);
    str += " ";
    str += toString(value.year);
    str += " at ";
    if (value.hour < 10)
    {
        str += "0";
    }
    str += toString(value.hour);
    str += ":";
    if (value.minute < 10)
    {
        str += "0";
    }
    str += toString(value.minute);
    str += ":";
    if (value.second < 10)
    {
        str += "0";
    }
    str += toString(value.second);
    str += " (";
    str += toString(value.timeOfDay);
    if (value.isTwilight)
    {
        str += ", Twilight";
    }
    str += ")";
    return str;
};

// Class functions
DateTime::DateTime(int32_t ticks) : _ticks(ticks)
{
    std::time_t timetTicks(ticks);
    std::tm* currentTime(localtime(&timetTicks));

    hour = currentTime->tm_hour;
    minute = currentTime->tm_min;
    second = currentTime->tm_sec;
    timeOfDay = TimesOfDay::get(hour);
    isTwilight = TimesOfDay::isTwilight(hour);

    day = currentTime->tm_mday;
    dayOfWeek = DaysOfWeek::get(currentTime->tm_wday);
    month = Months::get(currentTime->tm_mon);
    year = currentTime->tm_year + 1900; // tm_year stores 'years since 1900'
}

DateTime DateTime::now()
{
    return DateTime(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));
}

int32_t DateTime::getTicks() const
{
    return _ticks;
}

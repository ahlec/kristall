#include "General/TypeEffectiveness.h"

TypeEffectiveness operator*(TypeEffectiveness a, TypeEffectiveness b)
{
    // Zero
    if (a == TypeEffectiveness::Zero || b == TypeEffectiveness::Zero)
    {
        return TypeEffectiveness::Zero;
    }

    // One
    if (a == TypeEffectiveness::One)
    {
        return b;
    }
    if (b == TypeEffectiveness::One)
    {
        return a;
    }

    // Double, Half, Quadruple, Quarter (shouldn't have Zero or One now)
    if (a == TypeEffectiveness::Quarter)
    {
        if (b == TypeEffectiveness::Double)
        {
            return TypeEffectiveness::Half;
        }
        else if (b == TypeEffectiveness::Quadruple)
        {
            return TypeEffectiveness::One;
        }
        else
        {
            return TypeEffectiveness::Quarter;
        }
    }

    if (a == TypeEffectiveness::Half)
    {
        if (b == TypeEffectiveness::Half)
        {
            return TypeEffectiveness::Quarter;
        }
        else if (b == TypeEffectiveness::Double)
        {
            return TypeEffectiveness::One;
        }
        else
        {
            return TypeEffectiveness::Quarter;
        }
    }

    if (a == TypeEffectiveness::Double)
    {
        if (b == TypeEffectiveness::Half)
        {
            return TypeEffectiveness::One;
        }
        else if (b == TypeEffectiveness::Quarter)
        {
            return TypeEffectiveness::Half;
        }
        else
        {
            // If 2*2, then 4; if 2*4, then 4 (max is 4)
            return TypeEffectiveness::Quadruple;
        }
    }

    if (a == TypeEffectiveness::Quadruple)
    {
        if (b == TypeEffectiveness::Half)
        {
            return TypeEffectiveness::Double;
        }
        else if (b == TypeEffectiveness::Quarter)
        {
            return TypeEffectiveness::One;
        }
        else
        {
            return TypeEffectiveness::Quadruple;
        }
    }

    // Shouldn't get here, but fallback and no effectiveness if not processed
    return TypeEffectiveness::Zero;
}

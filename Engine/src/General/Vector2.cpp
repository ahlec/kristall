#include "General/Vector2.h"

Vector2::Vector2() : x(0), y(0)
{
}

Vector2::Vector2(int32_t pX, int32_t pY) : x(pX), y(pY)
{
}

bool Vector2::operator==(const Vector2& other) const
{
    return (x == other.x &&
            y == other.y);
}

bool Vector2::operator!=(const Vector2& other) const
{
    return !(*this == other);
}

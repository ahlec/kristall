#include "General/Strings.h"

namespace Strings
{
    std::string trimRight(const std::string& str)
    {
        std::string returnString = str;
        size_t indexFound = returnString.find_last_not_of(" \t\f\v\n\r");
        if (indexFound != std::string::npos)
        {
            returnString.erase(indexFound + 1);
        }
        else
        {
            returnString.clear();
        }

        return returnString;
    }

}

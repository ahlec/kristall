#include "General/TimeOfDay.h"

template<>
String toString<TimeOfDay>(TimeOfDay value)
{
    switch (value)
    {
        case TimeOfDay::Morning:
            {
                return "Morning";
            }
        case TimeOfDay::Day:
            {
                return "Day";
            }
        case TimeOfDay::Night:
            {
                return "Night";
            }
    }

    return "<Unrecognized TimeOfDay>";
};

namespace TimesOfDay
{
    TimeOfDay getNow()
    {
        std::time_t currentTimeT(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));
        std::tm* currentTime(localtime(&currentTimeT));

        if (currentTime->tm_hour >= 4 && currentTime->tm_hour < 10)
        {
            return TimeOfDay::Morning;
        }
        else if (currentTime->tm_hour >= 10 && currentTime->tm_hour < 20)
        {
            return TimeOfDay::Day;
        }
        else
        {
            return TimeOfDay::Night;
        }
    }

    TimeOfDay get(uint8_t hour)
    {
        if (hour >= 4 && hour < 10)
        {
            return TimeOfDay::Morning;
        }
        else if (hour >= 10 && hour < 20)
        {
            return TimeOfDay::Day;
        }
        else
        {
            return TimeOfDay::Night;
        }
    }

    bool isNowTwilight()
    {
        std::time_t currentTimeT(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));
        std::tm* currentTime(localtime(&currentTimeT));

        return (currentTime->tm_hour >= 17 && currentTime->tm_hour < 20);
    }

    bool isTwilight(uint8_t hour)
    {
        return (hour >= 17 && hour < 20);
    }
}

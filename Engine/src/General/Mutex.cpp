#include "General/Mutex.h"
#include "Errors/ExecutionException.h"

Mutex::Mutex() : _mutex(SDL_CreateMutex())
{
}

Mutex::~Mutex()
{
    if (_mutex != nullptr)
    {
        SDL_DestroyMutex(_mutex);
        _mutex = nullptr;
    }
}

void Mutex::unlock()
{
    if (_mutex == nullptr)
    {
        throw ExecutionException("Cannot unlock a mutex that has already been destroyed!");
    }
    SDL_UnlockMutex(_mutex);
}

void Mutex::lock()
{
    if (_mutex == nullptr)
    {
        throw ExecutionException("Cannot lock a mutex that has already been destroyed!");
    }
    SDL_LockMutex(_mutex);
}

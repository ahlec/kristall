#include "General/Direction.h"
#include "General/Math.h"

template<>
String toString<Direction>(Direction value)
{
    switch (value)
    {
        case Direction::North:
            {
                return String(L"North");
            }
        case Direction::East:
            {
                return String(L"East");
            }
        case Direction::South:
            {
                return String(L"South");
            }
        case Direction::West:
            {
                return String(L"West");
            }
    }

    return "<Unrecognized Direction: " + toString(static_cast<uint32_t>(value)) + ">";
};

namespace Directions
{
    Direction complement(Direction direction)
    {
        switch (direction)
        {
            case Direction::North:
                {
                    return Direction::South;
                }
            case Direction::East:
                {
                    return Direction::West;
                }
            case Direction::South:
                {
                    return Direction::North;
                }
            case Direction::West:
                {
                    return Direction::East;
                }
        }
    }

    Direction calculate(int32_t initialX, int32_t initialY,
                        int32_t finalX, int32_t finalY)
    {
        if (initialX == finalX && initialY == finalY)
        {
            return Direction::South;
        }

        if (std::abs(finalX - initialX) >= std::abs(finalY - initialY))
        {
            return (finalX - initialX > 0 ? Direction::East : Direction::West);
        }
        else
        {
            return (finalY - initialY > 0 ? Direction::South : Direction::North);
        }
    }

    void applyDirectionToTileLocation(Direction direction, int32_t& tileX, int32_t& tileY)
    {
        switch (direction)
        {
            case Direction::North:
                {
                    --tileY;
                    break;
                }
            case Direction::East:
                {
                    ++tileX;
                    break;
                }
            case Direction::South:
                {
                    ++tileY;
                    break;
                }
            case Direction::West:
                {
                    --tileX;
                    break;
                }
        }
    }
};

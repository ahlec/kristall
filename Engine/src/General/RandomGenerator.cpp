#include "General/RandomGenerator.h"

RandomGenerator::RandomGenerator(int32_t minValue, int32_t maxValue) :
    _engine(), _distribution(minValue, maxValue)
{
}

int32_t RandomGenerator::next()
{
    return _distribution(_engine);
}

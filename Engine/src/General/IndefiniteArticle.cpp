#include "General/IndefiniteArticle.h"

template<>
String toString<IndefiniteArticle>(IndefiniteArticle value)
{
    switch (value)
    {
        case IndefiniteArticle::A:
        {
            return L"A";
        }
        case IndefiniteArticle::An:
        {
            return L"An";
        }
    }

    return "<Unrecognized IndefiniteArticle: " + toString(static_cast<int32_t>(value)) + ">";
};

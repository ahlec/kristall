#include "General/MutexScopeLock.h"

MutexScopeLock::MutexScopeLock(Mutex* mutex) : m_mutex(mutex)
{
    if (m_mutex != nullptr)
    {
        m_mutex->lock();
    }
}

MutexScopeLock::~MutexScopeLock()
{
    if (m_mutex != nullptr)
    {
        m_mutex->unlock();
    }
}

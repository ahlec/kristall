#include "General/Math.h"

namespace Math
{

    int32_t clamp(int32_t value, int32_t minValue, int32_t maxValue)
    {
        if (value <= minValue)
        {
            return minValue;
        }

        if (value >= maxValue)
        {
            return maxValue;
        }

        return value;
    }

}

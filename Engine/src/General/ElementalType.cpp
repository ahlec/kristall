#include "General/ElementalType.h"

#define EFFECT_RAW(x, y) if (defendingType == (x)) { return (y); }
#define EFFECT_DOUBLE(x) EFFECT_RAW(x, TypeEffectiveness::Double)
#define EFFECT_HALF(x) EFFECT_RAW(x, TypeEffectiveness::Half)
#define EFFECT_ZERO(x) EFFECT_RAW(x, TypeEffectiveness::Zero)

namespace ElementalTypes
{
    TypeEffectiveness getEffectiveness(ElementalType attackingType, ElementalType defendingType)
    {
        EFFECT_ZERO(ElementalType::Indeterminate)

        switch (attackingType)
        {
            case ElementalType::Normal:
                {
                    EFFECT_HALF(ElementalType::Rock)
                    EFFECT_HALF(ElementalType::Steel)
                    EFFECT_ZERO(ElementalType::Ghost)
                    break;
                }
            case ElementalType::Fire:
                {
                    EFFECT_HALF(ElementalType::Fire)
                    EFFECT_HALF(ElementalType::Water)
                    EFFECT_DOUBLE(ElementalType::Grass)
                    EFFECT_DOUBLE(ElementalType::Ice)
                    EFFECT_DOUBLE(ElementalType::Bug)
                    EFFECT_HALF(ElementalType::Rock)
                    EFFECT_HALF(ElementalType::Dragon)
                    EFFECT_DOUBLE(ElementalType::Steel)
                    break;
                }
            case ElementalType::Water:
                {
                    EFFECT_DOUBLE(ElementalType::Fire)
                    EFFECT_HALF(ElementalType::Water)
                    EFFECT_HALF(ElementalType::Grass)
                    EFFECT_DOUBLE(ElementalType::Ground)
                    EFFECT_DOUBLE(ElementalType::Rock)
                    EFFECT_HALF(ElementalType::Dragon)
                    break;
                }
            case ElementalType::Grass:
                {
                    EFFECT_HALF(ElementalType::Fire)
                    EFFECT_DOUBLE(ElementalType::Water)
                    EFFECT_HALF(ElementalType::Grass)
                    EFFECT_HALF(ElementalType::Poison)
                    EFFECT_DOUBLE(ElementalType::Ground)
                    EFFECT_HALF(ElementalType::Flying)
                    EFFECT_HALF(ElementalType::Bug)
                    EFFECT_DOUBLE(ElementalType::Rock)
                    EFFECT_HALF(ElementalType::Dragon)
                    EFFECT_HALF(ElementalType::Steel)
                    break;
                }
            case ElementalType::Electric:
                {
                    EFFECT_DOUBLE(ElementalType::Water)
                    EFFECT_HALF(ElementalType::Grass)
                    EFFECT_HALF(ElementalType::Electric)
                    EFFECT_ZERO(ElementalType::Ground)
                    EFFECT_DOUBLE(ElementalType::Flying)
                    EFFECT_HALF(ElementalType::Dragon)
                    break;
                }
            case ElementalType::Ice:
                {
                    EFFECT_HALF(ElementalType::Fire)
                    EFFECT_HALF(ElementalType::Water)
                    EFFECT_DOUBLE(ElementalType::Grass)
                    EFFECT_HALF(ElementalType::Ice)
                    EFFECT_DOUBLE(ElementalType::Ground)
                    EFFECT_DOUBLE(ElementalType::Flying)
                    EFFECT_DOUBLE(ElementalType::Dragon)
                    EFFECT_HALF(ElementalType::Steel)
                    break;
                }
            case ElementalType::Fighting:
                {
                    EFFECT_DOUBLE(ElementalType::Normal)
                    EFFECT_DOUBLE(ElementalType::Ice)
                    EFFECT_HALF(ElementalType::Poison)
                    EFFECT_HALF(ElementalType::Flying)
                    EFFECT_HALF(ElementalType::Psychic)
                    EFFECT_HALF(ElementalType::Bug)
                    EFFECT_DOUBLE(ElementalType::Rock)
                    EFFECT_ZERO(ElementalType::Ghost)
                    EFFECT_DOUBLE(ElementalType::Dark)
                    EFFECT_DOUBLE(ElementalType::Steel)
                    EFFECT_HALF(ElementalType::Fairy)
                    break;
                }
            case ElementalType::Poison:
                {
                    EFFECT_DOUBLE(ElementalType::Grass)
                    EFFECT_HALF(ElementalType::Poison)
                    EFFECT_HALF(ElementalType::Ground)
                    EFFECT_HALF(ElementalType::Rock)
                    EFFECT_HALF(ElementalType::Ghost)
                    EFFECT_ZERO(ElementalType::Steel)
                    EFFECT_DOUBLE(ElementalType::Fairy)
                    break;
                }
            case ElementalType::Ground:
                {
                    EFFECT_DOUBLE(ElementalType::Fire)
                    EFFECT_HALF(ElementalType::Grass)
                    EFFECT_DOUBLE(ElementalType::Electric)
                    EFFECT_DOUBLE(ElementalType::Poison)
                    EFFECT_ZERO(ElementalType::Flying)
                    EFFECT_HALF(ElementalType::Bug)
                    EFFECT_DOUBLE(ElementalType::Rock)
                    EFFECT_DOUBLE(ElementalType::Steel)
                    break;
                }
            case ElementalType::Flying:
                {
                    EFFECT_DOUBLE(ElementalType::Grass)
                    EFFECT_HALF(ElementalType::Electric)
                    EFFECT_DOUBLE(ElementalType::Fighting)
                    EFFECT_DOUBLE(ElementalType::Bug)
                    EFFECT_HALF(ElementalType::Rock)
                    EFFECT_HALF(ElementalType::Steel)
                    break;
                }
            case ElementalType::Psychic:
                {
                    EFFECT_DOUBLE(ElementalType::Fighting)
                    EFFECT_DOUBLE(ElementalType::Poison)
                    EFFECT_HALF(ElementalType::Psychic)
                    EFFECT_ZERO(ElementalType::Dark)
                    EFFECT_HALF(ElementalType::Steel)
                    break;
                }
            case ElementalType::Bug:
                {
                    EFFECT_HALF(ElementalType::Fire)
                    EFFECT_DOUBLE(ElementalType::Grass)
                    EFFECT_HALF(ElementalType::Fighting)
                    EFFECT_HALF(ElementalType::Poison)
                    EFFECT_HALF(ElementalType::Flying)
                    EFFECT_DOUBLE(ElementalType::Psychic)
                    EFFECT_HALF(ElementalType::Ghost)
                    EFFECT_DOUBLE(ElementalType::Dark)
                    EFFECT_HALF(ElementalType::Steel)
                    EFFECT_HALF(ElementalType::Fairy)
                    break;
                }
            case ElementalType::Rock:
                {
                    EFFECT_DOUBLE(ElementalType::Fire)
                    EFFECT_DOUBLE(ElementalType::Ice)
                    EFFECT_HALF(ElementalType::Fighting)
                    EFFECT_HALF(ElementalType::Ground)
                    EFFECT_DOUBLE(ElementalType::Flying)
                    EFFECT_DOUBLE(ElementalType::Bug)
                    EFFECT_HALF(ElementalType::Steel)
                    break;
                }
            case ElementalType::Ghost:
                {
                    EFFECT_ZERO(ElementalType::Normal)
                    EFFECT_DOUBLE(ElementalType::Psychic)
                    EFFECT_DOUBLE(ElementalType::Ghost)
                    EFFECT_HALF(ElementalType::Dark)
                    break;
                }
            case ElementalType::Dragon:
                {
                    EFFECT_DOUBLE(ElementalType::Dragon)
                    EFFECT_HALF(ElementalType::Steel)
                    EFFECT_ZERO(ElementalType::Fairy)
                    break;
                }
            case ElementalType::Dark:
                {
                    EFFECT_HALF(ElementalType::Fighting)
                    EFFECT_DOUBLE(ElementalType::Psychic)
                    EFFECT_DOUBLE(ElementalType::Ghost)
                    EFFECT_HALF(ElementalType::Dark)
                    EFFECT_HALF(ElementalType::Fairy)
                    break;
                }
            case ElementalType::Steel:
                {
                    EFFECT_HALF(ElementalType::Fire)
                    EFFECT_HALF(ElementalType::Water)
                    EFFECT_HALF(ElementalType::Electric)
                    EFFECT_DOUBLE(ElementalType::Ice)
                    EFFECT_DOUBLE(ElementalType::Rock)
                    EFFECT_HALF(ElementalType::Steel)
                    EFFECT_DOUBLE(ElementalType::Fairy)
                    break;
                }
            case ElementalType::Fairy:
                {
                    EFFECT_HALF(ElementalType::Fire)
                    EFFECT_DOUBLE(ElementalType::Fighting)
                    EFFECT_HALF(ElementalType::Poison)
                    EFFECT_DOUBLE(ElementalType::Dragon)
                    EFFECT_DOUBLE(ElementalType::Dark)
                    EFFECT_HALF(ElementalType::Steel)
                    break;
                }
            default:
                {
                    return TypeEffectiveness::Zero;
                }
        }

        return TypeEffectiveness::One;
    }
};

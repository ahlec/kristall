#include "General/Numbers.h"
#include <sstream>
#include "General/Math.h"
#include "General/Strings.h"

const int BILLION = 1000000000;
const int MILLION = 1000000;
const int THOUSAND = 1000;

const std::string englishDigits[] = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
const std::string englishTeens[] = { "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen",
            "seventeen", "eighteen", "nineteen" };
const std::string englishTens[] = { "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

std::string convertIntToLonghand(int number)
{
    float remaining = (number < 0 ? -number : number);
    bool andUsed = (remaining < 100);
    std::stringstream english;

    if (number < 0)
    {
        english << "negative ";
    }

    if (remaining >= BILLION)
    {
        int billions = static_cast<int>(std::floor(remaining / BILLION));
        remaining -= billions * BILLION;
        english << englishDigits[billions] << " billion ";
    }

    if (remaining >= MILLION)
    {
        int millions = static_cast<int>(std::floor(remaining / MILLION));
        remaining -= millions * MILLION;

        if (millions >= 100)
        {
            int hundreds = static_cast<int>(std::floor(millions / 100.0));
            english << englishDigits[hundreds] << " hundred ";
            millions -= hundreds * 100;
        }

        if (millions >= 20)
        {
            int tens = static_cast<int>(std::floor(millions / 10.0));
            english << englishTens[tens - 1] << " "; // intentionally subtracting 1 here
            millions -= tens * 10;
        }

        if (millions >= 10)
        {
            english << englishTeens[millions % 10] << " ";
        }
        else if (millions > 0)
        {
            english << englishDigits[millions] << " ";
        }

        english << "million ";
    }

    if (remaining >= THOUSAND)
    {
        int thousands = static_cast<int>(std::floor(remaining / THOUSAND));
        remaining -= thousands * THOUSAND;

        if (thousands >= 100)
        {
            int hundreds = static_cast<int>(std::floor(thousands / 100));
            english << englishDigits[hundreds] << " hundred ";
            thousands -= hundreds * 100;
        }

        if (thousands >= 20)
        {
            int tens = static_cast<int>(std::floor(thousands / 10));
            english << englishTens[tens - 1] << " "; // intentionally subtracting 1 here
            thousands -= tens * 10;
        }

        if (thousands >= 10)
        {
            english << englishTeens[thousands % 10] << " ";
        }
        else
        {
            english << englishDigits[thousands] << " ";
        }
        english << "thousand ";
    }

    if (remaining >= 100)
    {
        int hundreds = static_cast<int>(std::floor(remaining / 100));
        english << englishDigits[hundreds] << " hundred ";
        remaining -= hundreds * 100;
    }

    if (remaining >= 20)
    {
        if (!andUsed)
        {
            english << "and ";
            andUsed = true;
        }

        int tens = static_cast<int>(remaining / 10);
        english << englishTens[tens - 1] << " "; // intentionally subtracting 1 here
        remaining -= tens * 10;
    }

    if (remaining >= 10)
    {
        if (!andUsed)
        {
            english << "and ";
        }
        english << englishTeens[static_cast<int>(remaining) % 10];
    }
    else if (remaining > 0)
    {
        if (!andUsed)
        {
            english << "and ";
        }
        english << englishDigits[static_cast<int>(remaining)];
    }

    return Strings::trimRight(english.str());
}

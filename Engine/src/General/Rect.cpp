#include "General/Rect.h"
#include "General/Math.h"

Rect::Rect() : x(0), y(0), width(0), height(0)
{
}

Rect::Rect(int32_t pX, int32_t pY, int32_t pWidth, int32_t pHeight) : x(pX), y(pY),
    width(pWidth), height(pHeight)
{
}

bool Rect::intersects(const Rect& other) const
{
    return (std::max(x, other.x) < std::min(x + width, other.x + other.width) &&
            std::max(y, other.y) < std::min(y + height, other.y + other.height));
}

bool Rect::contains(int32_t pX, int32_t pY) const
{
    return (pX >= x && pX < x + width && pY >= y && pY < y + height);
}

bool Rect::contains(Vector2 point) const
{
    return (point.x >= x && point.x < x + width && point.y >= y && point.y < y + height);
}

bool Rect::operator==(const Rect& other) const
{
    return (x == other.x &&
            y == other.y &&
            width == other.width &&
            height == other.height);
}

bool Rect::operator!=(const Rect& other) const
{
    return !(*this == other);
}

template<>
String toString<Rect>(Rect value)
{
    return String("{x: " + toString(value.x) + ", y: " + toString(value.y) +
        ", w: " + toString(value.width) + ", h: " + toString(value.height) + "}");
};

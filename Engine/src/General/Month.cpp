#include "General/Month.h"
#include "Errors/ExecutionException.h"


template<>
String toString<Month>(Month value)
{
    switch (value)
    {
        case Month::January:
            {
                return "January";
            }
        case Month::February:
            {
                return "February";
            }
        case Month::March:
            {
                return "March";
            }
        case Month::April:
            {
                return "April";
            }
        case Month::May:
            {
                return "May";
            }
        case Month::June:
            {
                return "June";
            }
        case Month::July:
            {
                return "July";
            }
        case Month::August:
            {
                return "August";
            }
        case Month::September:
            {
                return "September";
            }
        case Month::October:
            {
                return "October";
            }
        case Month::November:
            {
                return "November";
            }
        case Month::December:
            {
                return "December";
            }
    }

    return "<Unrecognized Month: " + toString(static_cast<int32_t>(value)) + ">";
};

namespace Months
{
    Month current()
    {
        std::time_t currentTimeT(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));
        std::tm* currentTime(localtime(&currentTimeT));

        return static_cast<Month>(currentTime->tm_mon);
    }

    Month get(uint8_t month)
    {
        if (month > 11)
        {
            ExecutionException error("Attempted to get the month from an invalid int8.");
            error.addData("month", month);
            throw error;
        }

        return static_cast<Month>(month);
    }
}

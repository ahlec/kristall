#include "General/DayOfWeek.h"
#include "Errors/ExecutionException.h"

template<>
String toString<DayOfWeek>(DayOfWeek value)
{
    switch (value)
    {
        case DayOfWeek::Sunday:
            {
                return "Sunday";
            }
        case DayOfWeek::Monday:
            {
                return "Monday";
            }
        case DayOfWeek::Tuesday:
            {
                return "Tuesday";
            }
        case DayOfWeek::Wednesday:
            {
                return "Wednesday";
            }
        case DayOfWeek::Thursday:
            {
                return "Thursday";
            }
        case DayOfWeek::Friday:
            {
                return "Friday";
            }
        case DayOfWeek::Saturday:
            {
                return "Saturday";
            }
    }

    return "<Unknown>";
};

namespace DaysOfWeek
{
    DayOfWeek today()
    {
        std::time_t currentTimeT(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));
        std::tm* currentTime(localtime(&currentTimeT));

        if (currentTime->tm_wday == 0)
        {
            return DayOfWeek::Sunday;
        }
        else if (currentTime->tm_wday == 1)
        {
            return DayOfWeek::Monday;
        }
        else if (currentTime->tm_wday == 2)
        {
            return DayOfWeek::Tuesday;
        }
        else if (currentTime->tm_wday == 3)
        {
            return DayOfWeek::Wednesday;
        }
        else if (currentTime->tm_wday == 4)
        {
            return DayOfWeek::Thursday;
        }
        else if (currentTime->tm_wday == 5)
        {
            return DayOfWeek::Friday;
        }
        else
        {
            return DayOfWeek::Saturday;
        }
    }

    DayOfWeek get(uint8_t day)
    {
        if (day > 6)
        {
            ExecutionException error("Attempted to get a DayOfWeek from an invalid int8.");
            error.addData("day", toString(day));
            throw error;
        }

        return static_cast<DayOfWeek>(day);
    }
}


#include "DataTypes.h"
#include "unicode/numfmt.h"
#include <cstring>

static NumberFormat* s_numberFormatter(nullptr);
icu::NumberFormat* getICUNumberFormatter()
{
    if (s_numberFormatter == nullptr)
    {
        UErrorCode errorCode(U_ZERO_ERROR);
        s_numberFormatter = NumberFormat::createInstance(errorCode);
        s_numberFormatter->setGroupingUsed(false);
    }

    return s_numberFormatter;
};

template<>
String toString<String>(String value)
{
    return value;
};
template<>
String toString<const std::string&>(const std::string& value)
{
    return String(value.c_str(), value.size());
};
template<>
String toString<bool>(bool value)
{
    return (value ? L"true" : L"false");
};
template<>
String toString<int8_t>(int8_t value)
{
    String str;
    getICUNumberFormatter()->format(value, str);
    return str;
};
template<>
String toString<uint8_t>(uint8_t value)
{
    String str;
    getICUNumberFormatter()->format(value, str);
    return str;
};
template<>
String toString<int16_t>(int16_t value)
{
    String str;
    getICUNumberFormatter()->format(value, str);
    return str;
};
template<>
String toString<uint16_t>(uint16_t value)
{
    String str;
    getICUNumberFormatter()->format(value, str);
    return str;
};
template<>
String toString<int32_t>(int32_t value)
{
    String str;
    getICUNumberFormatter()->format(value, str);
    return str;
};
template<>
String toString<uint32_t>(uint32_t value)
{
    String str;
    getICUNumberFormatter()->format(static_cast<int64_t>(value), str);
    return str;
};
template<>
String toString<int64_t>(int64_t value)
{
    String str;
    getICUNumberFormatter()->format(value, str);
    return str;
};
template<>
String toString<uint64_t>(uint64_t value)
{
    // format() only accepts an int64_t; therefore, if we have something passed to this like MAX_UINT64,
    // the function isn't going to output the correct value. However, I don't foresee there ever being a
    // scenario where we're working even remotely close to the upper limit of an in64_t, MUCH LESS the
    // upper limit of a uint64_t. This sacrifice shouldn't be noticable through the course of the program.
    String str;
    getICUNumberFormatter()->format((int64_t)value, str);
    return str;
};
template<>
String toString<float>(float value)
{
    String str;
    getICUNumberFormatter()->format(value, str);
    return str;
}
template<>
String toString<char>(char value)
{
    return String(value);
};

// We want to have this function here to avoid importing the entirety of the
// <cstring> library into all of the files. We want to avoid importing anything
// other than the absolute base necessities. Additionally, this function is only
// used by the toString template specification for char*, which is not common
// enough that we would incur a performance penalty by having a middle-tier wrapper
// around strlen.
size_t getCStrLength(const char* value)
{
    return strlen(value);
};

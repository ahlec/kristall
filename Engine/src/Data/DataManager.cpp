#include <cstdio>
#include <cstring>
#include "Data/DataManager.h"
#include "Data/BinaryReader.h"
#include "General/MutexScopeLock.h"
#if DEBUG
#include <iostream>
#endif

FILE* DataManager::_file = nullptr;
Mutex* s_mutex = new Mutex();
bool DataManager::_isReaderOpen = false;
static std::vector< std::vector<FileEntry> > s_fileEntries;

void DataManager::initialize()
{
    MutexScopeLock lock(s_mutex);

    if (_file != nullptr)
    {
        ExecutionException error("Already initialized the DataManager.");
        throw error;
    }

    _file = fopen("Kristall.bin", "rb+");
    if (_file == nullptr)
    {
        ExecutionException error("Could not open `Kristall.bin`.");
        throw error;
    }

    // Read in all of the header blocks
    uint16_t fileIndex;
    uint16_t numberFiles;
    for (uint8_t fileTypeIndex = 0; fileTypeIndex < static_cast<uint8_t>(FileType::COUNT); ++fileTypeIndex)
    {
        std::vector<FileEntry> files;
        if (!IS_SPECIAL_FILE_TYPE(static_cast<FileType>(fileTypeIndex)))
        {
            // Read the number of files
            if (fread(&numberFiles, sizeof(uint16_t), 1, _file) != 1)
            {
                if (feof(_file) != 0)
                {
                    ExecutionException error("Read beyond the bounds of `Kristall.bin`.");
                    error.addData("function", "readHeaderBlock");
                    throw error;
                }
                else
                {
                    ExecutionException error("Encountered an error when reading from `Kristall.bin`.");
                    error.addData("Error text", strerror(errno));
                    error.addData("function", "readHeaderBlock");
                    throw error;
                }
            }
            files.reserve(numberFiles);

            // Read the FileEntries
            FileEntry* entries = new FileEntry[numberFiles];
            if (fread(entries, sizeof(FileEntry), numberFiles, _file) != numberFiles)
            {
                if (feof(_file) != 0)
                {
                    ExecutionException error("Read beyond the bounds of `Kristall.bin`.");
                    error.addData("function", "readHeaderBlock");
                    throw error;
                }
                else
                {
                    ExecutionException error("Encountered an error when reading from `Kristall.bin`.");
                    error.addData("Error text", strerror(errno));
                    error.addData("function", "readHeaderBlock");
                    throw error;
                }
            }

            for (int index = 0; index < numberFiles; ++index)
            {
                files.push_back(entries[index]);
            }

            delete[] entries;
        }
        s_fileEntries.push_back(files);
    }
}

void DataManager::deinitialize()
{
    if (_file != nullptr)
    {
        fclose(_file);
        _file = nullptr;

        s_fileEntries.clear();
    }
    else
    {
        ExecutionException error("Already deinitialized the DataManager.");
        throw error;
    }
}

void DataManager::onBinaryReaderClosed()
{
    _isReaderOpen = false;
    s_mutex->unlock();
}

uint16_t DataManager::getNumberFiles(FileType fileType)
{
    #if DEBUG
    if (fileType == FileType::COUNT)
    {
        ExecutionException error("FileType::COUNT is not a valid FileType!");
        throw error;
    }
    #endif

    if (IS_SPECIAL_FILE_TYPE(fileType))
    {
        return 1;
    }

    return s_fileEntries[static_cast<uint8_t>(fileType)].size();
}


// External (Non-`Kristall.bin`) files
class ExternalFileFunctor : public Functor<void>
{
public:
    ExternalFileFunctor(FILE* filePointer) : _pointer(filePointer)
    {
    }

    virtual void operator()() final
    {
        if (_pointer != nullptr)
        {
            fclose(_pointer);
            _pointer = nullptr;
        }
    }

private:
    FILE* _pointer;
};

BinaryReaderPtr DataManager::openFileReader(FileType fileType, uint16_t fileNo)
{
    #if DEBUG
    if (fileType == FileType::COUNT)
    {
        ExecutionException error("FileType::COUNT is not a valid FileType!");
        throw error;
    }
    #endif

    /** -------- PlayerGame ---------- **/
    if (fileType == FileType::PlayerGame)
    {
        FILE* playerGameFile = fopen("playerGame.bin", "rb");

        fseek(playerGameFile, 0, SEEK_END);
        uint32_t playerGameFileSize = ftell(playerGameFile);
        fseek(playerGameFile, 0, SEEK_SET);

        FunctionPtr<void> onPlayerGameReaderClosed(new ExternalFileFunctor(playerGameFile));
        return BinaryReaderPtr(new BinaryReader(playerGameFile, FileType::PlayerGame,
                                                0, playerGameFileSize, onPlayerGameReaderClosed));
    }

    /** -------- GameSettings ---------- **/
    if (fileType == FileType::GameSettings)
    {
        FILE* gameSettingsFile = fopen("settings.bin", "rb");

        fseek(gameSettingsFile, 0, SEEK_END);
        uint32_t fileSize = ftell(gameSettingsFile);
        fseek(gameSettingsFile, 0, SEEK_SET);

        FunctionPtr<void> onGameSettingsReaderClosed(new ExternalFileFunctor(gameSettingsFile));
        return BinaryReaderPtr(new BinaryReader(gameSettingsFile, FileType::GameSettings,
                                                0, fileSize, onGameSettingsReaderClosed));
    }

    /** -------- General ---------- **/
    #if DEBUG
    if (IS_SPECIAL_FILE_TYPE(fileType))
    {
        ExecutionException error("Did not handle the processing of a special FileType!");
        error.addData("fileType", toString(fileType));
        throw error;
    }
    #endif

    s_mutex->lock(); // We do this manually because we need to have the mutex locked outside of the scope of this function.

    if (_isReaderOpen)
    {
        ExecutionException error("Attempted to open a BinaryReader for `Kristall.bin` while another BinaryReader is still open.");
        error.addData("fileType", toString(fileType));
        error.addData("fileNo", toString(fileNo));
        s_mutex->unlock();
        throw error;
    }

    if (fileNo >= s_fileEntries[static_cast<uint8_t>(fileType)].size())
    {
        ExecutionException error("Attempted to read an invalid file from `Kristall.bin`.");
        error.addData("fileType", toString(fileType));
        error.addData("fileNo", toString(fileNo));
        error.addData("nFiles", toString(s_fileEntries[static_cast<uint8_t>(fileType)].size()));
        s_mutex->unlock();
        throw error;
    }

    FileEntry& fileEntry = s_fileEntries[static_cast<uint8_t>(fileType)][fileNo];
    if (fseek(_file, fileEntry.offset, SEEK_SET) != 0)
    {
        ExecutionException error("Could not seek to the appropriate file in `Kristall.bin`.");
        error.addData("fileType", toString(fileType));
        error.addData("fileNo", toString(fileNo));
        error.addData("fileEntry.offset", toString(fileEntry.offset));
        s_mutex->unlock();
        throw error;
    }

    _isReaderOpen = true;

    #if DEBUG
    /**std::string stdString("");
    toString(fileType).toUTF8String(stdString);
    std::cout << "[DataManager] " << stdString << " #" << fileNo << " {offset: " << fileEntry.offset <<
        ", length: " << fileEntry.size << "}\n";**/
    #endif

    return BinaryReaderPtr(new BinaryReader(_file, fileType, fileNo, fileEntry.size, DataManager::onBinaryReaderClosed));
}

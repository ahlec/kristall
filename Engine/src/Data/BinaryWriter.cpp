#include "Data/BinaryWriter.h"
#include "Errors/ExecutionException.h"
#include <cstdio>

BinaryWriter::BinaryWriter(FILE* file) : _file(file), _bytesWritten(0)
{
}

BinaryWriter::~BinaryWriter()
{
}

void BinaryWriter::close()
{
    fclose(_file);
    _file = nullptr;
}

uint64_t BinaryWriter::getBytesWrittenCount() const
{
    return _bytesWritten;
}

void BinaryWriter::resetBytesWrittenCount()
{
    _bytesWritten = 0;
}

inline void BinaryWriter::writeBytes(const void* data, size_t size, size_t count = 1)
{
    if (data == nullptr)
    {
        ExecutionException error("Attempted to write data that was of a nullptr. This shouldn't ever happen.");
        throw error;
    }

    if (size == 0)
    {
        throw ExecutionException("Attempted to write data without a length. This shouldn't ever happen.");
    }

    if (count == 0)
    {
        throw ExecutionException("Attempted to write data without a count. This shouldn't ever happen.");
    }

    size_t nWrittenElements = fwrite(data, size, count, _file);
    if (nWrittenElements != count)
    {
        throw ExecutionException("Failed in writing!");
    }

    _bytesWritten += size * count;
}

void BinaryWriter::writeString(String str)
{
    uint32_t stringLength(str.length());
    writeBytes(&stringLength, sizeof(uint32_t));
    writeBytes(str.getBuffer(), sizeof(uint16_t), stringLength);
}

void BinaryWriter::writeStandardString(std::string str)
{
    if (str.length() > 0xFFFFFFFF)
    {
        throw ExecutionException("Provided string is too long!");
    }
    uint32_t strLength = static_cast<uint32_t>(str.length());
    writeBytes(&strLength, sizeof(uint32_t));
    writeBytes(str.c_str(), sizeof(char), static_cast<size_t>(strLength));
}

void BinaryWriter::writeUInt16(uint16_t value)
{
    writeBytes(&value, sizeof(uint16_t));
}

void BinaryWriter::writeInt16(int16_t value)
{
    writeBytes(&value, sizeof(int16_t));
}

void BinaryWriter::writeUInt32(uint32_t value)
{
    writeBytes(&value, sizeof(uint32_t));
}

void BinaryWriter::writeInt32(int32_t value)
{
    writeBytes(&value, sizeof(int32_t));
}

void BinaryWriter::writeRect(Rect value)
{
    writeBytes(&value.x, sizeof(int32_t));
    writeBytes(&value.y, sizeof(int32_t));
    writeBytes(&value.width, sizeof(int32_t));
    writeBytes(&value.height, sizeof(int32_t));
}

void BinaryWriter::writeUInt8(uint8_t value)
{
    writeBytes(&value, sizeof(uint8_t));
}

void BinaryWriter::writeBool(bool value)
{
    uint8_t uint8_tValue = 0;
    if (value)
    {
        uint8_tValue = 1;
    }
    writeBytes(&uint8_tValue, sizeof(uint8_t));
}

void BinaryWriter::writeElementalType(ElementalType value)
{
    uint8_t uint8_tValue = static_cast<uint8_t>(value);
    writeBytes(&uint8_tValue, sizeof(uint8_t));
}

void BinaryWriter::writeDirection(Direction value)
{
    uint8_t uint8_tValue = static_cast<uint8_t>(value);
    writeBytes(&uint8_tValue, sizeof(uint8_t));
}

void BinaryWriter::writeInt8(int8_t value)
{
    writeBytes(&value, sizeof(int8_t));
}

void BinaryWriter::writeCharArray(const char* value, uint32_t length)
{
    writeUInt32(length);
    if (length > 0)
    {
        writeBytes(value, sizeof(char), length);
    }
}

void BinaryWriter::writeUInt64(uint64_t value)
{
    writeBytes(&value, sizeof(uint64_t));
}

void BinaryWriter::writeInt64(int64_t value)
{
    writeBytes(&value, sizeof(int64_t));
}

void BinaryWriter::writeFloat(float value)
{
    int32_t intValue = static_cast<int32_t>(value * 100);
    writeBytes(&intValue, sizeof(int32_t));
}

void BinaryWriter::writeTimeSpan(TimeSpan value)
{
}

void BinaryWriter::writeDateTime(DateTime value)
{
}

void BinaryWriter::writeRawBytes(const void* data, size_t elementSize, size_t nElements)
{
    writeBytes(data, elementSize, nElements);
}

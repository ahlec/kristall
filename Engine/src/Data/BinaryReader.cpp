#include "Data/BinaryReader.h"
#include <cstdio>
#include <cstring>
#include "Lua/LuaScript.h"
#include "Graphics/Texture.h"
#define STBI_HEADER_FILE_ONLY
#include "Data/stb_image.cpp"
#include <SDL.h>
#include <iostream>

BinaryReader::BinaryReader(FILE* file, FileType fileType, uint16_t fileNo,
                           uint32_t size, FunctionPtr<void> closeCallback) :
    _file(file), _fileType(fileType), _fileNo(fileNo), _size(size),
    _closeCallback(closeCallback), _sizeDataRead(0)
{
}

BinaryReader::~BinaryReader()
{
    _file = nullptr;
    if (_closeCallback != nullptr)
    {
        _closeCallback();
        _closeCallback = nullptr;
    }
}

void BinaryReader::close()
{
    if (_file == nullptr)
    {
        ExecutionException error("Attempted to close a BinaryReader that is not currently open.");
        error.addData("_fileType", toString(_fileType));
        error.addData("_fileNo", toString(_fileNo));
        throw error;
    }

    _file = nullptr;
    if (_closeCallback != nullptr)
    {
        _closeCallback();
        _closeCallback = nullptr;
    }
}

void BinaryReader::readRawData(void* data, size_t elementSize, size_t nElements)
{
    if (data == nullptr)
    {
        ExecutionException error("data must be a valid pointer. nullptr provided.");
        error.addData("_fileType", toString(_fileType));
        error.addData("_fileNo", toString(_fileNo));
        throw error;
    }

    if (_file == nullptr)
    {
        ExecutionException error("Cannot read from a BinaryReader that doesn't have an open file.");
        error.addData("_fileType", toString(_fileType));
        error.addData("_fileNo", toString(_fileNo));
        throw error;
    }

    size_t rawBytesRead = elementSize * nElements;

    if (feof(_file) != 0 || _sizeDataRead + rawBytesRead > _size)
    {
        ExecutionException error("Attempted to read beyond the bounds of the open file.");
        error.addData("_sizeDataRead", toString(_sizeDataRead));
        error.addData("_sizeDataRead + rawBytesRead", toString(_sizeDataRead + rawBytesRead));
        error.addData("_size", toString(_size));
        error.addData("_fileType", toString(_fileType));
        error.addData("_fileNo", toString(_fileNo));
        throw error;
    }

    _sizeDataRead += rawBytesRead;

    if (fread(data, elementSize, nElements, _file) != nElements)
    {
        if (feof(_file) != 0)
        {
            ExecutionException error("Read beyond the bounds of the open file.");
            error.addData("_sizeDataRead", toString(_sizeDataRead));
            error.addData("_size", toString(_size));
            error.addData("_fileType", toString(_fileType));
            error.addData("_fileNo", toString(_fileNo));
            throw error;
        }

        ExecutionException error("Encountered an error when reading from an open file.");
        std::string errorText(strerror(errno));
        error.addData("error text", toString(errorText));
        error.addData("_fileType", toString(_fileType));
        error.addData("_fileNo", toString(_fileNo));
        throw error;
    }
}

String BinaryReader::readString()
{
    uint32_t stringLength(readUInt32());

    // Check the length of the string first. If it's 0, then we can return now, since there's nothing left for
    // us to read, and it will simplify things in a bit.
    if (stringLength == 0)
    {
        return L"";
    }


    // Read in the number of characters that we have for the string. Since the underlying type of UChar is
    // uint16_t, we can use readUInt16 here. Note that there is a constructor that allows us to
    // specify the UChar array and the number of characters to copy from, so we don't need to worry about
    // a NULL terminator here.
    UChar* characterBuffer = new UChar[stringLength];
    readRawData(characterBuffer, sizeof(uint16_t), stringLength);
    String returnValue(characterBuffer, stringLength);

    // Clean up and return
    delete[] characterBuffer;
    return returnValue;
}

std::string BinaryReader::readStandardString()
{
    uint32_t strLength;
    readRawData(&strLength, sizeof(uint32_t), 1);
    char* rawCharData = new char[strLength + 1];
    readRawData(rawCharData, sizeof(char), strLength);
    rawCharData[strLength] = 0;
    std::string standardString(rawCharData);
    delete[] rawCharData;
    return standardString;
}

uint32_t BinaryReader::readUInt32()
{
    uint32_t value;
    readRawData(&value, sizeof(uint32_t), 1);
    return value;
}

int32_t BinaryReader::readInt32()
{
    int32_t value;
    readRawData(&value, sizeof(int32_t), 1);
    return value;
}

uint16_t BinaryReader::readUInt16()
{
    uint16_t value;
    readRawData(&value, sizeof(uint16_t), 1);
    return value;
}

int16_t BinaryReader::readInt16()
{
    int16_t value;
    readRawData(&value, sizeof(int16_t), 1);
    return value;
}

uint8_t BinaryReader::readUInt8()
{
    uint8_t value;
    readRawData(&value, sizeof(uint8_t), 1);
    return value;
}

int8_t BinaryReader::readInt8()
{
    int8_t value;
    readRawData(&value, sizeof(int8_t), 1);
    return value;
}

bool BinaryReader::readBool()
{
    bool value;
    readRawData(&value, sizeof(bool), 1);
    return value;
}

ElementalType BinaryReader::readElementalType()
{
    ElementalType value;
    readRawData(&value, sizeof(uint8_t), 1);
    return value;
}

Direction BinaryReader::readDirection()
{
    uint8_t value;
    readRawData(&value, sizeof(uint8_t), 1);

    switch (value)
    {
        case 0:
            {
                return Direction::North;
            }
        case 1:
            {
                return Direction::East;
            }
        case 2:
            {
                return Direction::South;
            }
        case 3:
            {
                return Direction::West;
            }
        default:
            {
                ExecutionException error("Read an invalid value for a direction.");
                error.addData("value", toString(value));
                error.addData("_fileType", toString(_fileType));
                error.addData("_fileNo", toString(_fileNo));
                throw error;
            }
    }
}

LuaScriptPtr BinaryReader::readLuaScript()
{
    LuaScriptPtr scriptPtr(new LuaScript());
    scriptPtr->_nBytes = readUInt32();
    scriptPtr->_codeData = new char[scriptPtr->_nBytes];
    readRawData(scriptPtr->_codeData, sizeof(char), scriptPtr->_nBytes);
    return scriptPtr;
}

uint64_t BinaryReader::readUInt64()
{
    uint64_t value;
    readRawData(&value, sizeof(uint64_t), 1);
    return value;
}

int64_t BinaryReader::readInt64()
{
    int64_t value;
    readRawData(&value, sizeof(int64_t), 1);
    return value;
}

float BinaryReader::readFloat()
{
    int32_t intValue;
    readRawData(&intValue, sizeof(int32_t), 1);

    float floatValue = (intValue / 100.0f);
    return floatValue;
}

Texture* BinaryReader::readTexture(SDL_Renderer* renderer)
{
    // Create the texture now
    SDL_Surface* surface(readSdlSurface());
    SDL_Texture* texture(nullptr);
    texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);

    if (texture == nullptr)
    {
        throw ExecutionException("Unable to create a texture from the surface entity.");
    }

    // Return
    return new Texture(texture);
}

Rect BinaryReader::readRect()
{
    Rect rectangle;

    rectangle.x = readInt32();
    rectangle.y = readInt32();
    rectangle.width = readInt32();
    rectangle.height = readInt32();

    return rectangle;
}

SDL_Surface* BinaryReader::readSdlSurface()
{
    uint64_t imageSize = readUInt64();
    unsigned char* imageData = new unsigned char[imageSize];
    readRawData(imageData, sizeof(unsigned char), imageSize);

    // Use STBI to parse the raw PNG data in memory
    int32_t width;
    int32_t height;
    int32_t comp; // <-- seems to be 'colour' (4 = 32bit colour image, 3 = 24bit colour image)
    unsigned char* data(stbi_load_from_memory(imageData, imageSize, &width, &height, &comp, 0));

    // Clean up the raw entity retrieved from the ResourceManager
    delete[] imageData;

    // Create the RGBA masks
    #if SDL_BYTEORDER == SDL_BIG_ENDIAN
    uint32_t rMask(0xff000000);
    uint32_t gMask(0x00ff0000);
    uint32_t bMask(0x0000ff00);
    uint32_t aMask(0x000000ff);
    #else
    uint32_t rMask(0x000000ff);
    uint32_t gMask(0x0000ff00);
    uint32_t bMask(0x00ff0000);
    uint32_t aMask(0xff000000);
    #endif

    // Create the appropriate surface
    SDL_Surface* surface;
    switch (comp)
    {
        case 4:
            {
                surface = SDL_CreateRGBSurface(0, width, height, 32, rMask, gMask, bMask, aMask);
                break;
            }
        case 3:
            {
                surface = SDL_CreateRGBSurface(0, width, height, 24, rMask, gMask, bMask, 0);
                break;
            }
        default:
            {
                // Unsupported composition of image. This shouldn't ever happen in practise, though.
                stbi_image_free(data);
                return nullptr;
            }
    }

    // Copy over the pixel data from the STBI parsed data into the surface now
    memcpy(surface->pixels, data, comp * width * height);

    // Clean up
    stbi_image_free(data);

    return surface;
}

#include "Data/FileType.h"

template<>
String toString<FileType>(FileType type)
{
    switch (type)
    {
        case FileType::SpriteSheet:
        {
            return L"SpriteSheet";
        }
        case FileType::PokemonSpecies:
        {
            return L"PokemonSpecies";
        }
        case FileType::Item:
        {
            return L"Item";
        }
        case FileType::GymBadge:
        {
            return L"GymBadge";
        }
        case FileType::PokemonMove:
        {
            return L"PokemonMove";
        }
        case FileType::Region:
        {
            return L"Region";
        }
        case FileType::Map:
        {
            return L"Map";
        }
        case FileType::PlayerGame:
        {
            return L"PlayerGame";
        }
        case FileType::GameSettings:
        {
            return L"GameSettings";
        }
        case FileType::Texture:
        {
            return L"Texture";
        }
        case FileType::Frame:
        {
            return L"Frame";
        }
        case FileType::COUNT:
        {
            break;
        }
    }

    return L"<Unrecognized FileType>";
};

#include "Game.h"
#include "Data/DataManager.h"
#include "Interface/GameWindow.h"

GameWindowPtr Game::s_gameWindow = nullptr;
GameSettings* Game::s_gameSettings = nullptr;

const GameWindowPtr& Game::getGameWindow()
{
    return s_gameWindow;
}

const GameSettings& Game::getSettings()
{
    return *s_gameSettings;
}

void Game::initialize()
{
    s_gameSettings = new GameSettings(DataManager::openFileReader(FileType::GameSettings, 0));
    s_gameWindow = GameWindowPtr(new GameWindow());
}

void Game::deinitialize()
{
    s_gameWindow = nullptr;
}

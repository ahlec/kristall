#include "Player/PlayerGame.h"
#include "Data/BinaryReader.h"
#include "World/MapLocation.h"
#include "Characters/PlayerCharacter.h"

// Class functions
PlayerGame::PlayerGame() : _character(nullptr)
{

}

PlayerGame::PlayerGame(BinaryReaderPtr& reader, MapLocation& characterLocation)
{
    _trainerId = reader->readUInt16();
    _secretId = reader->readUInt16();
    characterLocation = MapLocation(reader);
    _character = new PlayerCharacter(reader, this);
}

// Member access functions (nothing too special beyond this point)
String PlayerGame::getName() const
{
    return _name;
}

uint16_t PlayerGame::getTrainerId() const
{
    return _trainerId;
}

uint16_t PlayerGame::getSecretId() const
{
    return _secretId;
}

PlayerCharacter* PlayerGame::getCharacter() const
{
    return _character;
}

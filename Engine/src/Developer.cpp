#include "Cornerstones.h"
#include <cstdio>
#include <stdarg.h>

#if DEBUG

namespace DebugLogger
{
    void writeLine(String line)
    {
        std::string outputStr("");
        line.toUTF8String(outputStr);
        writeLine(outputStr.c_str());
    }

    void writeLinef(const char* format, ...)
    {
        char buffer[1024];
        va_list args;
        va_start(args, format);
        vsnprintf(buffer, sizeof(buffer), format, args);
        va_end(args);

        writeLine(buffer);
    }

    void writeLine(const char* line)
    {
        /// ---------------------- Write to the file
        static FILE* s_logFile(nullptr);
        if (s_logFile == nullptr)
        {
            s_logFile = fopen(DEVEL_LOGGER_OUTPUT_FILE, "wt");
            if (s_logFile == nullptr)
            {
                ExecutionException error("Can not open the debug logger file for writing.");
                throw error;
            }
        }
        fputs(line, s_logFile);
        fflush(s_logFile);

        /// ---------------------- Write to the console
        fprintf(stdout, line);
    }
};

#endif

#include "Errors/Signals.h"
#include <csignal>
#include "Errors/ExecutionException.h"
#include "Errors/ErrorHandling.h"

void signalHandler(int signalNo)
{
    String exceptionMessage;
    if (signalNo == SIGSEGV)
    {
        exceptionMessage = "Encountered a segmentation fault.";
    }
    else if (signalNo == SIGFPE)
    {
        exceptionMessage = "Encountered a floating-point arithmetic signal.";
    }
    else
    {
        exceptionMessage = "Encountered an unknown signal.";
    }
    ExecutionException error(exceptionMessage);
    error.addData("signalNo", toString(signalNo));

    ErrorHandler::reportException(error);
}

namespace Signals
{
    void initializeHandlers()
    {
        signal(SIGFPE, signalHandler);
        signal(SIGSEGV, signalHandler);
    }
};

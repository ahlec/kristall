#include "Errors/Issue.h"
#include <curl/curl.h>
#include <sstream>
#include "Errors/ComputerInformation.h"

Issue::Issue() : _title(""), _description(""), _reproductionSteps(""),
                 _type(IssueType::BugReport), _reproducability(Reproducability::NotSpecified),
                 _category(ErrorCategory::NotSpecified), _exception(nullptr),
                 _hasException(false), _isFatalException(false)
{
}

void Issue::submit()
{
    const int TBG3_KRISTALL_ID(1);
    const std::string TBG3_USERNAME("Kristall");
    const std::string TBG3_PASSWORD("OGqvjDnpoBojxSdE");
    //const std::string TBG3_HOST("http://deitloff.com/issues/thebuggenie/pokmonkristall/issues/new");
    const std::string TBG3_HOST("http://deitloff.com/kristal.php");

    std::stringstream postData;
    postData << "tbg3_username=" << TBG3_USERNAME << "&tbg3_password=" <<
                TBG3_PASSWORD << "&format=json&project_id=" << TBG3_KRISTALL_ID;

    std::string titleStr("");
    _title.toUTF8String(titleStr);
    char* escapeString = curl_escape(titleStr.c_str(), titleStr.size());
    postData << "&title=";
    if (escapeString != nullptr)
    {
        postData << escapeString;
        curl_free(escapeString);
    }
    else
    {
        postData << "ERROR";
    }

    std::string descriptionStr;
    if (_hasException)
    {
        _exception->_message.toUTF8String(descriptionStr);
    }
    else if (!_description.isEmpty())
    {
        _description.toUTF8String(descriptionStr);
    }
    else
    {
        descriptionStr = "none";
    }
    postData << "&description=";
    escapeString = curl_escape(descriptionStr.c_str(), descriptionStr.size());
    if (escapeString != nullptr)
    {
        postData << escapeString;
        curl_free(escapeString);
    }
    else
    {
        postData << "ERROR";
    }

    if (_type == IssueType::BugReport)
    {
        if (!_reproductionSteps.isEmpty())
        {
            std::string reproductionStr;
            _reproductionSteps.toUTF8String(reproductionStr);
            escapeString = curl_escape(reproductionStr.c_str(), reproductionStr.size());
        }
        else
        {
            escapeString = curl_escape("none", 4);
        }
        postData << "&reproduction_steps=";
        if (escapeString != nullptr)
        {
            postData << escapeString;
            curl_free(escapeString);
        }
        else
        {
            postData << "ERROR";
        }
        postData << "&reproducability_id=" << static_cast<int32_t>(_reproducability);
    }

    postData << "&issuetype_id=" << (static_cast<int32_t>(_type) + 1);
    postData << "&category_id=" << static_cast<int32_t>(_category);

    std::string systemInfoString = ComputerInformation::getOperatingSystemName();
    escapeString = curl_escape(systemInfoString.c_str(), systemInfoString.length());
    postData << "&operatingsystem_value=";
    if (escapeString != nullptr)
    {
        postData << escapeString;
        curl_free(escapeString);
    }
    else
    {
        postData << "ERROR";
    }

    systemInfoString = ComputerInformation::getComputerType();
    escapeString = curl_escape(systemInfoString.c_str(), systemInfoString.length());
    postData << "&computertype_value=";
    if (escapeString != nullptr)
    {
        postData << escapeString;
        curl_free(escapeString);
    }
    else
    {
        postData << "ERROR";
    }

    systemInfoString = ComputerInformation::getComputerManufacturer();
    escapeString = curl_escape(systemInfoString.c_str(), systemInfoString.length());
    postData << "&computermanufacturer_value=";
    if (escapeString != nullptr)
    {
        postData << escapeString;
        curl_free(escapeString);
    }
    else
    {
        postData << "ERROR";
    }

    systemInfoString = ComputerInformation::getComputerModel();
    escapeString = curl_escape(systemInfoString.c_str(), systemInfoString.length());
    postData << "&computermodel_value=";
    if (escapeString != nullptr)
    {
        postData << escapeString;
        curl_free(escapeString);
    }
    else
    {
        postData << "ERROR";
    }

    systemInfoString = ComputerInformation::getSystemMemory();
    escapeString = curl_escape(systemInfoString.c_str(), systemInfoString.length());
    postData << "&computermemory_value=";
    if (escapeString != nullptr)
    {
        postData << escapeString;
        curl_free(escapeString);
    }
    else
    {
        postData << "ERROR";
    }

    postData << "&computeris64bit_value=" << (ComputerInformation::getIsComputer64Bit() ? "Yes" : "No");

    if (_hasException)
    {
        postData << "&exceptionfatal_value=" << (_isFatalException ? "Yes" : "No");

        std::stringstream exceptionData;
        std::string tempStr;
        for (ExecutionException::DataNode node : _exception->_data)
        {
            node.key.toUTF8String(tempStr);
            exceptionData << tempStr;
            exceptionData << ": `";
            node.value.toUTF8String(tempStr);
            exceptionData << tempStr;
            exceptionData << "`\n";
        }

        std::string exceptionStr = exceptionData.str();
        escapeString = curl_escape(exceptionStr.c_str(), exceptionStr.length());
        postData << "&exceptiondata_value=%3Cnowiki%3E%3Cpre%3E";
        if (escapeString != nullptr)
        {
            postData << escapeString;
            curl_free(escapeString);
        }
        else
        {
            postData << "ERROR";
        }
        postData << "%3C%2Fpre%3E%3C%2Fnowiki%3E";

        escapeString = curl_escape(_exception->_stackTrace.c_str(), _exception->_stackTrace.length());
        postData << "&execptionstacktrace_value=";
        if (escapeString != nullptr)
        {
            postData << escapeString;
            curl_free(escapeString);
        }
        else
        {
            postData << "ERROR";
        }
    }

    postData << "&playergame_value=none&playercharacter_value=none";

    CURL* curlHandle = curl_easy_init();

    if (curlHandle == nullptr)
    {
        throw ExecutionException("curl_easy_init problem.");
    }

    curl_slist* headersList = nullptr;
    headersList = curl_slist_append(headersList, "x-requested-with: xmlhttprequest");

    if (headersList == nullptr)
    {
        printf("error!!!\n");
    }

    curl_easy_setopt(curlHandle, CURLOPT_COPYPOSTFIELDS, postData.str().c_str());
    curl_easy_setopt(curlHandle, CURLOPT_URL, TBG3_HOST.c_str());
    //curl_easy_setopt(curlHandle, CURLOPT_HTTPHEADER, headersList);

    CURLcode result = curl_easy_perform(curlHandle);

    if (result == CURLE_OK)
    {
        printf("cURL success?\n");
    }
    else
    {
        printf("cURL error: %s\n", curl_easy_strerror(result));
        FILE* file = fopen("error.txt", "w");
        if (file == nullptr)
        {
            throw ExecutionException("oops!");
        }

        fprintf(file, "%s", postData.str().c_str());
        fclose(file);
    }

    curl_slist_free_all(headersList);
    curl_easy_cleanup(curlHandle);
}

void Issue::setTitle(String title)
{
    _title = title;
}

void Issue::setDescription(String description)
{
    _description = description;
}

void Issue::setReproduction(Reproducability reproducability, String steps)
{
    _reproducability = reproducability;
    _reproductionSteps = steps;
}

void Issue::setType(IssueType type)
{
    _type = type;
}

void Issue::setCategory(ErrorCategory category)
{
    _category = category;
}

void Issue::setException(ExecutionException error, bool isFatal)
{
    _hasException = true;
    _exception = new ExecutionException(error);
    _isFatalException = isFatal;
}

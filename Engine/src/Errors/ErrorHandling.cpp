#include "Errors/ErrorHandling.h"
#include <exception>
#include <cstdlib>
#include "Errors/Issue.h"

void terminationHandler();

void ErrorHandler::initializeErrorHandler()
{
    std::set_terminate(terminationHandler);
}

void terminationHandler()
{
    //auto hi = std::current_exception();
    abort();
}

namespace ErrorHandler
{
    void reportException(ExecutionException error)
    {
        try
        {
            Issue issue;
            issue.setTitle(error.getErrorMessage());
            issue.setDescription("There was a problem!");
            issue.setCategory(ErrorCategory::General);
            issue.setException(error, true);
            issue.submit();
        }
        catch (ExecutionException const& secondError)
        {
        }
    }
};

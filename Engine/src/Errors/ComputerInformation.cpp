#include "Errors/ComputerInformation.h"
#include <sstream>

#if defined(_WIN32_WINNT) && _WIN32_WINNT < 0x0501
#undef _WIN32_WINNT
#endif

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0501
#endif

#include <windows.h>
#include <powrprof.h>

namespace ComputerInformation
{
    std::string getComputerName()
    {
        TCHAR nameBuffer[MAX_COMPUTERNAME_LENGTH + 1];
        DWORD nameLength = MAX_COMPUTERNAME_LENGTH + 1;

        if (GetComputerName(nameBuffer, &nameLength) == TRUE)
        {
            return std::string(nameBuffer);
        }

        return std::string("UNDETERMINED");
    }

    std::string getOperatingSystemName()
    {
        OSVERSIONINFOEX osInfo;
        ZeroMemory(&osInfo, sizeof(OSVERSIONINFOEX));
        osInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

        if (GetVersionEx((OSVERSIONINFO*)&osInfo) == FALSE)
        {
            return std::string("UNDETERMINED(1)");
        }

        if (osInfo.dwPlatformId != VER_PLATFORM_WIN32_NT)
        {
            return std::string("UNDETERMINED(2)");
        }

        std::stringstream stream;
        bool isRecognized = false;
        stream << "Microsoft ";

        if (osInfo.dwMajorVersion == 6)
        {
            if (osInfo.wProductType == VER_NT_WORKSTATION)
            {
                if (osInfo.dwMinorVersion == 2)
                {
                    stream << "Windows 8";
                }
                else if (osInfo.dwMinorVersion == 1)
                {
                    stream << "Windows 7";
                }
                else if (osInfo.dwMinorVersion == 0)
                {
                    stream << "Windows Vista";
                }
            }
            else
            {
                if (osInfo.dwMinorVersion == 2)
                {
                    stream << "Windows Server 2012";
                }
                else if (osInfo.dwMinorVersion == 1)
                {
                    stream << "Windows Server 2008 R2";
                }
                else if (osInfo.dwMinorVersion == 0)
                {
                    stream << "Windows Server 2008";
                }
            }

            DWORD productType;
            typedef BOOL (WINAPI* getProductInfoFunc)(DWORD, DWORD, DWORD, DWORD, PDWORD);
            getProductInfoFunc getProductInfo =
                (getProductInfoFunc)GetProcAddress(GetModuleHandle(__TEXT("kernel32.dll")), "GetProductInfo");
            if (getProductInfo(osInfo.dwMajorVersion, osInfo.dwMinorVersion, 0, 0, &productType) != 0)
            {
                switch (productType)
                {
                    case 0x00000001: //PRODUCT_ULTIMATE
                    {
                        stream << " Ultimate Edition";
                        break;
                    }
                    case 0x00000030: //PRODUCT_PROFESSIONAL
                    {
                        stream << " Professional";
                        break;
                    }
                    case 0x00000003: //PRODUCT_HOME_PREMIUM
                    {
                        stream << " Home Premium Edition";
                        break;
                    }
                    case 0x00000002: //PRODUCT_HOME_BASIC
                    {
                        stream << " Home Basic Edition";
                        break;
                    }
                    case 0x00000004: //PRODUCT_ENTERPRISE
                    {
                        stream << " Enterprise Edition";
                        break;
                    }
                    case 0x00000006: //PRODUCT_BUSINESS
                    {
                        stream << " Business Edition";
                        break;
                    }
                    case 0x0000000B: //PRODUCT_STARTER
                    {
                        stream << " Starter Edition";
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
            }

            isRecognized = true;
        }
        else if (osInfo.dwMajorVersion == 5 && osInfo.dwMinorVersion == 2)
        {
            if (GetSystemMetrics(SM_SERVERR2) != 0)
            {
                stream << "Windows Server 2003 R2";
            }
            else
            {
                stream << "Windows Server 2003";
            }

            isRecognized = true;
        }
        else if (osInfo.dwMajorVersion == 5 && osInfo.dwMinorVersion == 1)
        {
            if ((osInfo.wSuiteMask & VER_SUITE_PERSONAL) == VER_SUITE_PERSONAL)
            {
                stream << "Windows XP Home Edition";
            }
            else
            {
                stream << "Windowxs XP";
            }

            isRecognized = true;
        }
        else if (osInfo.dwMajorVersion == 5 && osInfo.dwMinorVersion == 0)
        {
            stream << "Windows 2000";
            isRecognized = true;
        }

        if (isRecognized)
        {
            return stream.str();
        }
        else
        {
            return std::string("UNDETERMINED(3)");
        }
    }

    bool getIsComputer64Bit()
    {
        SYSTEM_INFO systemInfo;
        GetNativeSystemInfo(&systemInfo);

        if (systemInfo.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64 ||
            systemInfo.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_IA64)
        {
            return true;
        }

        return false;
    }

    std::string getComputerManufacturer()
    {
        HKEY registryKey;

        if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, __TEXT("SYSTEM\\CurrentControlSet\\Control\\SystemInformation"),
                         0, KEY_QUERY_VALUE, &registryKey) != ERROR_SUCCESS)
        {
            return std::string("UNDETERMINED(1)");
        }

        TCHAR manufacturerBuffer[256];
        manufacturerBuffer[255] = static_cast<TCHAR>(0);
        DWORD manufacturerBufferSize = sizeof(manufacturerBuffer) - 1;

        LONG returnValue = RegQueryValueEx(registryKey, __TEXT("SystemManufacturer"), NULL,
                                           NULL, (LPBYTE)manufacturerBuffer, &manufacturerBufferSize);

        RegCloseKey(registryKey);

        if (returnValue == ERROR_SUCCESS || returnValue == ERROR_MORE_DATA)
        {
            std::stringstream stream;
            stream << manufacturerBuffer;

            if (returnValue == ERROR_MORE_DATA)
            {
                stream << "...";
            }

            return stream.str();
        }

        return std::string("UNDETERMINED");
    }

    std::string getComputerModel()
    {
        HKEY registryKey;

        if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, __TEXT("SYSTEM\\CurrentControlSet\\Control\\SystemInformation"),
                         0, KEY_QUERY_VALUE, &registryKey) != ERROR_SUCCESS)
        {
            return std::string("UNDETERMINED(1)");
        }

        TCHAR modelBuffer[256];
        modelBuffer[255] = static_cast<TCHAR>(0);
        DWORD modelBufferSize = sizeof(modelBuffer) - 1;

        LONG returnValue = RegQueryValueEx(registryKey, __TEXT("SystemProductName"), NULL,
                                           NULL, (LPBYTE)modelBuffer, &modelBufferSize);

        RegCloseKey(registryKey);

        if (returnValue == ERROR_SUCCESS || returnValue == ERROR_MORE_DATA)
        {
            std::stringstream stream;
            stream << modelBuffer;

            if (returnValue == ERROR_MORE_DATA)
            {
                stream << "...";
            }

            return stream.str();
        }

        return std::string("UNDETERMINED(2)");
    }

    std::string getComputerType()
    {
        SYSTEM_POWER_STATUS powerStatus;
        if (GetSystemPowerStatus(&powerStatus) == 0)
        {
            return std::string("UNDETERMINED(1)");
        }

        if (powerStatus.ACLineStatus == 255)
        {
            return std::string("Desktop");
        }

        if (powerStatus.BatteryFlag < 128)
        {
            return std::string("Notebook");
        }

        SYSTEM_POWER_CAPABILITIES powerCapabilities;
        if (GetPwrCapabilities(&powerCapabilities) == 0)
        {
            return std::string("UNDETERMINED(2)");
        }

        if (powerCapabilities.LidPresent == 1)
        {
            return std::string("Notebook");
        }

        OSVERSIONINFOEX osInfo;
        ZeroMemory(&osInfo, sizeof(OSVERSIONINFOEX));
        osInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

        if (GetVersionEx((OSVERSIONINFO*)&osInfo) != FALSE &&
            osInfo.dwMajorVersion >= 6)
        {
            typedef int (WINAPI* determinePwrRoleFunc)();
            determinePwrRoleFunc powerDeterminePlatformRole =
                (determinePwrRoleFunc)GetProcAddress(GetModuleHandle(__TEXT("PowrProf.dll")),
                                                     "PowerDeterminePlatformRole");

            int powerRole = powerDeterminePlatformRole();
            if (powerRole == 2) //PlatformRoleMobile
            {
                return std::string("Notebook");
            }
            else if (powerRole == 1 && // PlatformRoleDesktop
                     powerCapabilities.SystemBatteriesPresent == TRUE &&
                     powerCapabilities.BatteriesAreShortTerm == FALSE)
            {
                return std::string("Notebook");
            }

            return std::string("Desktop");
        }

        return std::string("UNDETERMINED(3)");
    }

    std::string getSystemMemory()
    {
        MEMORYSTATUSEX memoryStatus;
        memoryStatus.dwLength = sizeof(MEMORYSTATUSEX);

        if (GlobalMemoryStatusEx(&memoryStatus) == 0)
        {
            return std::string("UNDETERMINED(1)");
        }

        std::stringstream stream;
        stream.precision(2);
        stream.setf(std::ios::fixed, std::ios::floatfield);
        if (memoryStatus.ullTotalPhys >= 1099511627776)
        {
            stream << (memoryStatus.ullTotalPhys / 1099511627776.0f) << "TB";
        }
        else if (memoryStatus.ullTotalPhys >= 1073741824)
        {
            stream << (memoryStatus.ullTotalPhys / 1073741824.0f) << "GB";
        }
        else if (memoryStatus.ullTotalPhys >= 1048576)
        {
            stream << (memoryStatus.ullTotalPhys / 1048576.0f) << "MB";
        }
        else if (memoryStatus.ullTotalPhys >= 1024)
        {
            stream << (memoryStatus.ullTotalPhys / 1024.0f) << "KB";
        }
        else
        {
            stream << memoryStatus.ullTotalPhys << "B";
        }

        return stream.str();
    }
};

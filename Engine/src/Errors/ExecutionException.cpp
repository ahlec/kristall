#include "Errors/ExecutionException.h"
#include <dbg/frames.hpp>
#include <dbg/symbols.hpp>
#include "DataTypes.h"
#include <sstream>

struct dbgSymbolSink : dbg::symsink
{
    dbgSymbolSink() : address(nullptr)
    {
    }

    virtual void process_function(const void* programCounter, const char* name, const char* module)
    {
        functionName = name;
        moduleName = module;
        address = programCounter;
    }

    std::string functionName;
    std::string moduleName;
    const void* address;
};

ExecutionException::ExecutionException(String errorMessage) :
    _message(errorMessage), _data()
{
    dbg::call_stack<64> callStack;
    callStack.collect(1); // we will ignore the constructor for this exception

    unsigned int numberFrames = callStack.size();
    dbg::symdb symbolsDatabase;
    dbgSymbolSink symbolSink;

    std::stringstream stackTrace;

    for (int index = 0; index != numberFrames; ++index)
    {
        const void* programCounter = callStack.pc(index);
        stackTrace << "[" << index << "] `";
        if (symbolsDatabase.lookup_function(programCounter, symbolSink))
        {
            stackTrace << symbolSink.functionName;
        }
        else
        {
            stackTrace << "???";
        }
        stackTrace << "` (" << programCounter << ")\n";
    }

    _stackTrace = stackTrace.str();
}

ExecutionException::ExecutionException(const ExecutionException& other) :
    _message(other._message), _stackTrace(other._stackTrace), _data(other._data)
{
}

ExecutionException& ExecutionException::operator=(const ExecutionException& other)
{
    if (this == &other)
    {
        return *this;
    }

    _message = other._message;
    _data = other._data;
    _stackTrace = other._stackTrace;

    return *this;
}

String ExecutionException::getErrorMessage() const
{
    return _message;
}

String ExecutionException::getStackTrace() const
{
    return String(_stackTrace.c_str());
}

std::map<String, String> ExecutionException::getData() const
{
    std::map<String, String> retData;
    for (const DataNode& node : _data)
    {
        if (retData.count(node.key) == 0)
        {
            retData[node.key] = node.value;
        }
        else
        {
            uint64_t overflowNo(1);
            while (retData.count(node.key + "(" + toString(overflowNo) + ")") > 0)
            {
                ++overflowNo;
            }
            retData[node.key + "(" + toString(overflowNo) + ")"] = node.value;
        }
    }
    return retData;
}

void ExecutionException::addData(String key, String value)
{
    DataNode node;
    node.key = key;
    node.value = value;
    _data.push_back(node);
}

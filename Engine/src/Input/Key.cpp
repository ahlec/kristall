#include "Input/Key.h"

namespace Keys
{
    Key toKey(int16_t keyCode)
    {
        // Handle the keys that are most likely to be requested first
        if ((keyCode >= 65 && keyCode <= 90) || // Alphabet
            keyCode == 32 || // Space
            (keyCode >= 48 && keyCode <= 57)) // Number bar (not Numpad)
        {
            return static_cast<Key>(keyCode);
        }

        // Determine that we are within the range of the enum values
        if (keyCode <= 7 || // There are no keys [1-7], and 0 returns None anyway
            keyCode > 254)
        {
            return Key::None;
        }

        // Handle the 202-254 range, checking for invalid values
        if (keyCode == 252 || keyCode == 245 ||
            (keyCode > 223 && keyCode < 242 && keyCode != 226 && keyCode != 229) ||
            (keyCode > 203 && keyCode < 219))
        {
            return Key::None;
        }

        // Handle the 96-201 range, checking for invalid values
        if ((keyCode > 192 && keyCode < 202) ||
            (keyCode > 183 && keyCode < 186) ||
            (keyCode > 145 && keyCode < 160) ||
            (keyCode > 135 && keyCode < 144))
        {
            return Key::None;
        }

        // Handle the 8-95 range, checking for invalid values
        if (keyCode == 94 ||
            (keyCode > 57 && keyCode < 65) ||
            (keyCode > 29 && keyCode < 32) ||
            keyCode == 26 ||
            (keyCode > 21 && keyCode < 25) ||
            (keyCode > 13 && keyCode < 19) ||
            (keyCode > 9 && keyCode < 13))
        {
            return Key::None;
        }

        // The provided keyCode is valid, so convert and return
        return static_cast<Key>(keyCode);
    }

    bool isReservedKey(Key key)
    {
        switch (key)
        {
            case Key::F1:
            case Key::F10:
            case Key::RightControl:
            case Key::LeftControl:
            case Key::PrintScreen:
                {
                    return true;
                }
            default:
                {
                    return false;
                }
        }
    }
};

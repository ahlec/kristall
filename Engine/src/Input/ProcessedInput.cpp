#include "Input/ProcessedInput.h"

ProcessedInput::ProcessedInput() :
    inputController(nullptr),
    cursor(nullptr),
    isUpPressed(false),
    isDownPressed(false),
    isLeftPressed(false),
    isRightPressed(false),
    isAPressed(false),
    isBPressed(false),
    isStartPressed(false),
    isSelectPressed(false),
    isLeftShoulderPressed(false),
    isRightShoulderPressed(false),
    isAnyMovementButtonPressed(false),
    isScreenshotButtonPressed(false),
    isHelpButtonPressed(false),
    isReportButtonPressed(false),
    isCtrlPressed(false),
    isUpDown(false),
    isDownDown(false),
    isLeftDown(false),
    isRightDown(false),
    isADown(false),
    isBDown(false),
    isStartDown(false),
    isSelectDown(false),
    isLeftShoulderDown(false),
    isRightShoulderDown(false),
    isAnyMovementButtonDown(false)
{
}

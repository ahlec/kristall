#include "Input/CursorData.h"

const CursorData CursorData::NONE(0, 0, false, false, false, false, false, false);

CursorData::CursorData(int positionX, int positionY, bool isLeftPressed, bool isLeftDown,
           bool isRightPressed, bool isRightDown, bool isOverWindow,
           bool hasMoved) : x(positionX), y(positionY), isLeftButtonPressed(isLeftPressed),
           isRightButtonPressed(isRightPressed), isLeftButtonDown(isLeftDown),
           isRightButtonDown(isRightDown), isCursorOverWindow(isOverWindow),
           hasCursorMoved(hasMoved)
{
}

CursorData::CursorData(const CursorData& other) : x(other.x), y(other.y),
                    isLeftButtonPressed(other.isLeftButtonPressed),
                    isRightButtonPressed(other.isRightButtonPressed),
                    isLeftButtonDown(other.isLeftButtonDown),
                    isRightButtonDown(other.isRightButtonDown),
                    isCursorOverWindow(other.isCursorOverWindow),
                    hasCursorMoved(other.hasCursorMoved)
{
}

CursorData& CursorData::operator=(const CursorData& other)
{
}

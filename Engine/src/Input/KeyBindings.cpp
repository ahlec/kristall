#include "Input/KeyBindings.h"
#include "Data/BinaryReader.h"

KeyBindings::KeyBindings(BinaryReaderPtr reader)
{
    int16_t keyRead = -1;
    for (size_t index = 0; index < LENGTH_InputButtons - 1; ++index)
    {
        keyRead = reader->readInt16();
        _bindings.push_back(Keys::toKey(keyRead));
        if (_bindings.back() == Key::None)
        {
            ExecutionException error("An invalid key was read from the loaded KeyBindings data.");
            error.addData("KeyCode", keyRead);
            throw error;
        }
    }
}

Key KeyBindings::operator() (InputButton button) const
{
    return _bindings[static_cast<int>(button) - 1];
}

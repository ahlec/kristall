#include "Input/InputController.h"
#include <windows.h>
#include "Input/CursorData.h"
#include "Input/KeyBindings.h"
#include "Game.h"
#include "Input/ProcessedInput.h"
#include "Errors/ExecutionException.h"
#include "Input/InputButtonState.h"

InputController::ButtonInfo::ButtonInfo() : button(InputButton::None), defaultLength(0),
                                            currentLength(0), isDepressed(false),
                                            depressionLength(0), isBlocked(false),
                                            delayTime(0)
{
}

InputController::ButtonInfo::ButtonInfo(InputButton inputButton, uint16_t defaultDelayLength) :
                                        button(inputButton), defaultLength(defaultDelayLength),
                                        currentLength(defaultDelayLength), isDepressed(false),
                                        depressionLength(0), isBlocked(false),
                                        delayTime(0)
{
}

InputController::InputController()
{
    const uint16_t defaultDelayLength = 80;

    _buttons = new ButtonInfo[LENGTH_InputButtons - 1];
    for (int index = 0; index < LENGTH_InputButtons - 1; ++index)
    {
        _buttons[index] = ButtonInfo(static_cast<InputButton>(index + 1),
                                     defaultDelayLength);
    }
    _buttons[static_cast<int>(InputButton::A) - 1] = ButtonInfo(InputButton::A, 0);
    _buttons[static_cast<int>(InputButton::B) - 1] = ButtonInfo(InputButton::B, 0);
}

InputController::~InputController()
{
    if (_buttons != nullptr)
    {
        delete[] _buttons;
        _buttons = nullptr;
    }
}

ProcessedInput InputController::getProcessedInput(uint32_t elapsedTime, bool hasFocus, bool useMouse,
                                                  AnimationInstance* cursorAnimation)
{
    ProcessedInput processedInput;
    processedInput.inputController = this;

    if (!hasFocus)
    {
        return processedInput;
    }

    // Handle mouse input
    /*CursorData cursor(CursorData::NONE);
    if (useMouse)
    {
        POINT mousePosition;
        bool gotMouseCoordinates = GetCursorPos(&mousePosition);
        if (!gotMouseCoordinates)
        {
            ExecutionException error("Encountered an error while attempting to get the cursor position.");
            TCHAR errorBuffer[256] = TEXT("?");
            FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                          MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), errorBuffer,
                          255, NULL);
            error.addData("Description", std::string(errorBuffer));
            throw error;
        }

        if (!ScreenToClient(mainWindow->getSystemHandle(), &mousePosition))
        {
            ExecutionException error("Encountered an error while attempting to translate cursor "
                                     "position from screen to client.");
            TCHAR errorBuffer[256] = TEXT("?");
            FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                          MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), errorBuffer,
                          255, NULL);
            error.addData("Description", std::string(errorBuffer));
            throw error;
        }

        int mouseX = int(mousePosition.x);
        int mouseY = int(mousePosition.y);

        if (cursorAnimation != 0)
        {
            //Rectangle cursorRectangle = SpriteCatalogs.Cursors[cursorAnimation.CurrentFrame, SpriteSheetEntryType.Sprite];
            //X = mouse.X - 4 + (24 - cursorRectangle.Width);
            //Y = mouse.Y - 4 + (28 - cursorRectangle.Height);
        }

        bool hasMouseMoved = (mouseX != _lastMouseX || mouseY != _lastMouseY);
        if (hasMouseMoved)
        {
            _lastMouseX = mouseX;
            _lastMouseY = mouseY;
        }

        bool cursorOverWindow = !(mouseX < 0 || mouseY < 0 ||
                                  mouseX > mainWindow->getSize().x ||
                                  mouseY > mainWindow->getSize().y);

        bool areMouseButtonsSwapped = GetSystemMetrics(SM_SWAPBUTTON);
        bool isMouseLeftDown = false;
        bool isLeftMouseButtonPressed = ((GetAsyncKeyState(areMouseButtonsSwapped ?
                                                          VK_RBUTTON : VK_LBUTTON) & 0x8000) != 0);
        if (isLeftMouseButtonPressed && !_isMouseLeftDown)
        {
            isMouseLeftDown = true;
            _isMouseLeftDown = true;
        }
        else if (!isLeftMouseButtonPressed && _isMouseLeftDown)
        {
            _isMouseLeftDown = false;
        }

        bool isMouseRightDown = false;
        bool isRightMouseButtonPressed = ((GetAsyncKeyState(areMouseButtonsSwapped ?
                                                            VK_LBUTTON : VK_RBUTTON) & 0x8000) != 0);
        if (isRightMouseButtonPressed && !_isMouseRightDown)
        {
            isMouseRightDown = true;
            _isMouseRightDown = true;
        }
        else if (!isRightMouseButtonPressed && _isMouseRightDown)
        {
            _isMouseRightDown = false;
        }

        cursor = CursorData(int(mousePosition.x), int(mousePosition.y),
                            isLeftMouseButtonPressed, isMouseLeftDown,
                            isRightMouseButtonPressed, isMouseRightDown,
                            cursorOverWindow, hasMouseMoved);
    }
    else*/
    {
        processedInput.cursor = nullptr;
    }

    // Handle keyboard input
    const KeyBindings& keyBindings(Game::getSettings().getKeyBindings());
    InputButtonState buttonStates[LENGTH_InputButtons - 1];
    for (int index = 0; index < LENGTH_InputButtons - 1; ++index)
    {
        if (_buttons[index].delayTime > 0)
        {
            if (_buttons[index].delayTime <= elapsedTime)
            {
                _buttons[index].delayTime = 0;
            }
            else
            {
                _buttons[index].delayTime -= elapsedTime;
            }
            buttonStates[index] = InputButtonState::Up;
            continue;
        }

        if ((GetAsyncKeyState(static_cast<int>(keyBindings(_buttons[index].button))) & 0x8000) != 0) // key is down
        {
            // Is this button flagged as being delayed?
            if (_buttons[index].isBlocked)
            {
                buttonStates[index] = InputButtonState::Up;
                continue;
            }

            buttonStates[index] = InputButtonState::Down;
            _buttons[index].isDepressed = true;
            _buttons[index].depressionLength += elapsedTime;

            if (_buttons[index].depressionLength >= _buttons[index].currentLength)
            {
                _buttons[index].depressionLength = 0;
            }

            // If the depress length on this button is 0, it has either just been
            // depressed or just overflowed the wait length, meaning we should
            // mark this button as being depressed
            if (_buttons[index].depressionLength == 0)
            {
                buttonStates[index] = InputButtonState::Pressed;
            }
        }
        else
        {
            buttonStates[index] = InputButtonState::Up;
            if (_buttons[index].depressionLength > 0)
            {
                _buttons[index].isDepressed = false;
                _buttons[index].depressionLength = 0;
            }

            // is this button flagged as being delayed?
            if (_buttons[index].isBlocked)
            {
                _buttons[index].isBlocked = false; // unset this button in the delayed list
                continue;
            }
        }
    }

    processedInput.isUpDown = (buttonStates[0] != InputButtonState::Up);
    processedInput.isUpPressed = (buttonStates[0] == InputButtonState::Pressed);
    processedInput.isDownDown = (buttonStates[1] != InputButtonState::Up);
    processedInput.isDownPressed = (buttonStates[1] == InputButtonState::Pressed);
    processedInput.isLeftDown = (buttonStates[2] != InputButtonState::Up);
    processedInput.isLeftPressed = (buttonStates[2] == InputButtonState::Pressed);
    processedInput.isRightDown = (buttonStates[3] != InputButtonState::Up);
    processedInput.isRightPressed = (buttonStates[3] == InputButtonState::Pressed);
    processedInput.isADown = (buttonStates[4] != InputButtonState::Up);
    processedInput.isAPressed = (buttonStates[4] == InputButtonState::Pressed);
    processedInput.isBDown = (buttonStates[5] != InputButtonState::Up);
    processedInput.isBPressed = (buttonStates[5] == InputButtonState::Pressed);
    processedInput.isStartDown = (buttonStates[6] != InputButtonState::Up);
    processedInput.isStartPressed = (buttonStates[6] == InputButtonState::Pressed);
    processedInput.isSelectDown = (buttonStates[7] != InputButtonState::Up);
    processedInput.isSelectPressed = (buttonStates[7] == InputButtonState::Pressed);
    processedInput.isLeftShoulderDown = (buttonStates[8] != InputButtonState::Up);
    processedInput.isLeftShoulderPressed = (buttonStates[8] == InputButtonState::Pressed);
    processedInput.isRightShoulderDown = (buttonStates[9] != InputButtonState::Up);
    processedInput.isRightShoulderPressed = (buttonStates[9] == InputButtonState::Pressed);

    // Special buttons
    if ((GetAsyncKeyState(static_cast<int>(Key::PrintScreen)) & 0x8000) != 0)
    {
        if (!_isPrintScreenDown)
        {
            processedInput.isScreenshotButtonPressed = true;
            _isPrintScreenDown = true;
        }
        else
        {
            processedInput.isScreenshotButtonPressed = false;
        }
    }
    else
    {
        _isPrintScreenDown = false;
        processedInput.isScreenshotButtonPressed = false;
    }

    if ((GetAsyncKeyState(static_cast<int>(Key::F1)) & 0x8000) != 0)
    {
        if (!_isHelpButtonDown)
        {
            processedInput.isHelpButtonPressed = true;
            _isHelpButtonDown = true;
        }
        else
        {
            processedInput.isHelpButtonPressed = false;
        }
    }
    else
    {
        _isHelpButtonDown = false;
        processedInput.isHelpButtonPressed = false;
    }

    if ((GetAsyncKeyState(static_cast<int>(Key::F10)) & 0x8000) != 0)
    {
        if (!_isReportButtonDown)
        {
            processedInput.isReportButtonPressed = true;
            _isReportButtonDown = true;
        }
        else
        {
            processedInput.isReportButtonPressed = false;
        }
    }
    else
    {
        _isReportButtonDown = false;
        processedInput.isReportButtonPressed = false;
    }

    processedInput.isCtrlPressed = ((GetAsyncKeyState(static_cast<int>(Key::LeftControl)) & 0x8000) != 0) ||
                      ((GetAsyncKeyState(static_cast<int>(Key::RightControl)) & 0x8000) != 0);

    return processedInput;
}

bool InputController::isInInitialState() const
{
    return _isInInitialState;
}

void InputController::reset()
{
    for (int index = 1; index < LENGTH_InputButtons; ++index)
    {
        if (_buttons[index].currentLength != _buttons[index].defaultLength)
        {
            _buttons[index].currentLength = _buttons[index].defaultLength;
            _buttons[index].isBlocked = true;
            _buttons[index].depressionLength = 0;
        }
    }
    _isInInitialState = true;
}

void InputController::delayButton(InputButton button, unsigned short delayDuration,
                                  bool isReleaseRequired)
{
    int buttonIndex = static_cast<int>(button) - 1;
    if (isReleaseRequired)
    {
        _buttons[buttonIndex].isBlocked = true;
    }
    _buttons[buttonIndex].delayTime = delayDuration;
    _buttons[buttonIndex].isDepressed = false;
    _buttons[buttonIndex].depressionLength = 0;
}

void InputController::requireButtonRelease(InputButton button)
{
    _buttons[static_cast<int>(button) - 1].isBlocked = true;
}

void InputController::setButtonDelayLength(InputButton button, unsigned short newDelayLength)
{
    int buttonIndex = static_cast<int>(button) - 1;
    _buttons[buttonIndex].isBlocked = true; // We have to set the depress duration
                                            // back to 0, so delay this button so
                                            // we don't report it
    _buttons[buttonIndex].depressionLength = 0;
    _buttons[buttonIndex].currentLength = newDelayLength;
    _isInInitialState = false;
}

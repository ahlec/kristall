#include "Battles/BattleCalculations.h"

namespace BattleCalculations
{
    TypeEffectiveness getTypeEffectiveness(ElementalType attackingType, ElementalType defendingType,
                                           PokemonAbility attackersAbility)
    {
        // Scrappy
        if (attackersAbility == PokemonAbility::Scrappy && defendingType == ElementalType::Ghost &&
            (attackingType == ElementalType::Normal || attackingType == ElementalType::Fighting))
        {
            return TypeEffectiveness::One;
        }

        // Default
        return ElementalTypes::getEffectiveness(attackingType, defendingType);
    }
}

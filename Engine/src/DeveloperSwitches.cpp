#include "DeveloperSwitches.h"

DeveloperSwitches* DeveloperSwitches::getInstance()
{
    static DeveloperSwitches* s_instance(nullptr);
    if (s_instance == nullptr)
    {
        s_instance = new DeveloperSwitches();
    }
    return s_instance;
}

bool DeveloperSwitches::get(DeveloperSwitch devSwitch)
{
    if (devSwitch >= COUNT_DEVELOPER_SWITCHES)
    {
        return false;
    }

    return m_switches[devSwitch];
}

void DeveloperSwitches::set(DeveloperSwitch devSwitch, bool value)
{
    if (devSwitch < COUNT_DEVELOPER_SWITCHES)
    {
        m_switches[devSwitch] = value;
    }
}

DeveloperSwitches::DeveloperSwitches()
{
    m_switches.resize(COUNT_DEVELOPER_SWITCHES, false);
}

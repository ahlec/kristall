#include "Lua/LuaScript.h"

LuaScript::LuaScript() : _nBytes(0), _codeData(nullptr)
{
}

LuaScript::~LuaScript()
{
    if (_codeData != nullptr)
    {
        delete[] _codeData;
        _codeData = nullptr;
    }
}

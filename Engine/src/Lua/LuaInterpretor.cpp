#include "Lua/LuaInterpretor.h"
#include <lua.hpp>
#include "Errors/ExecutionException.h"

LuaInterpretor::LuaInterpretor(std::string name) : _name(name),
    _lua(nullptr), _isCurrentlyExecuting(false), _executionComplete(nullptr),
    _executionThread(nullptr)
{
    _lua = luaL_newstate();

    luaL_requiref(_lua, "base", luaopen_base, 1);
    lua_settop(_lua, 0);
    luaL_requiref(_lua, "io", luaopen_io, 1);
    lua_settop(_lua, 0);
}

LuaInterpretor::~LuaInterpretor()
{
    if (_executionThread != nullptr)
    {
        delete _executionThread;
        _executionThread = nullptr;
    }

    if (_lua != nullptr)
    {
        lua_close(_lua);
        _lua = nullptr;
    }
}

bool LuaInterpretor::getIsCurrentlyExecuting()
{
    bool isExecuting;
    _isExecutingMutex.lock();
    isExecuting = _isCurrentlyExecuting;
    _isExecutingMutex.unlock();
    return isExecuting;
}

bool LuaInterpretor::evaluate(const std::string& luaCode)
{
    _mutex.lock();

    lua_Hook initialHook = lua_gethook(_lua); // we only want hooks to affect execute()!
    int initialHookMask = lua_gethookmask(_lua);
    int initialHookCount = lua_gethookcount(_lua);
    lua_sethook(_lua, nullptr, 0, 0);

    int stackTopInitial = lua_gettop(_lua);
    int loadStatus = luaL_loadstring(_lua, luaCode.c_str());
    if (loadStatus == LUA_OK)
    {
        int callReturnValue = lua_pcall(_lua, 0, 1, 0);

        if (callReturnValue == LUA_OK)
        {
            int stackTopFinal = lua_gettop(_lua);
            if (stackTopInitial == stackTopFinal)
            {
                ExecutionException error("The Lua code evaluated did not return any values.");
                error.addData("LuaInterpretor name", toString(_name));
                error.addData("luaCode", toString(luaCode));

                lua_sethook(_lua, initialHook, initialHookMask, initialHookCount);
                _mutex.unlock();
                throw error;
            }
            else
            {
                int returnType = lua_type(_lua, -1);
                if (returnType == LUA_TBOOLEAN)
                {
                    if (lua_toboolean(_lua, -1) == 1)
                    {
                        lua_pop(_lua, 1);
                        lua_sethook(_lua, initialHook, initialHookMask, initialHookCount);
                        _mutex.unlock();
                        return true;
                    }
                    else
                    {
                        lua_pop(_lua, 1);
                        lua_sethook(_lua, initialHook, initialHookMask, initialHookCount);
                        _mutex.unlock();
                        return false;
                    }
                }
                else
                {
                    ExecutionException error("The Lua code evaluated did not return a boolean.");
                    error.addData("LuaInterpretor name", toString(_name));
                    error.addData("luaCode", toString(luaCode));

                    std::string returnValueType(lua_typename(_lua, returnType));
                    error.addData("return value type", toString(returnValueType));

                    if (returnType == LUA_TNUMBER || returnType == LUA_TSTRING)
                    {
                        std::string returnValue(lua_tostring(_lua, -1));
                        error.addData("return value", returnValue);
                    }

                    lua_sethook(_lua, initialHook, initialHookMask, initialHookCount);
                    _mutex.unlock();
                    throw error;
                }
            }
        }
        else if (callReturnValue == LUA_ERRRUN)
        {
            ExecutionException error("A Lua runtime error was encountered when evaluating code.");
            error.addData("LuaInterpretor name", _name);
            error.addData("luaCode", luaCode);

            std::string luaError(lua_tostring(_lua, lua_gettop(_lua)));
            error.addData("luaError", luaError);

            lua_sethook(_lua, initialHook, initialHookMask, initialHookCount);
            _mutex.unlock();
            throw error;
        }
        else if (callReturnValue == LUA_ERRMEM)
        {
            ExecutionException error("An allocation error occurred when evaluating Lua code.");
            error.addData("LuaInterpretor name", _name);
            error.addData("luaCode", luaCode);

            lua_sethook(_lua, initialHook, initialHookMask, initialHookCount);
            _mutex.unlock();
            throw error;
        }
        else
        {
            ExecutionException error("A Lua error was encountered when running the error handler.");
            error.addData("LuaInterpretor name", _name);
            error.addData("luaCode", luaCode);

            std::string luaError(lua_tostring(_lua, lua_gettop(_lua)));
            error.addData("luaError", luaError);

            lua_sethook(_lua, initialHook, initialHookMask, initialHookCount);
            _mutex.unlock();
            throw error;
        }
    }
    else if (loadStatus == LUA_ERRSYNTAX)
    {
        ExecutionException error("A syntax error was found in the Lua code provided for evaluation.");
        error.addData("LuaInterpretor name", _name);
        error.addData("luaCode", luaCode);

        std::string luaError(lua_tostring(_lua, lua_gettop(_lua)));
        error.addData("luaError", luaError);

        lua_sethook(_lua, initialHook, initialHookMask, initialHookCount);
        _mutex.unlock();
        throw error;
    }
    else
    {
        ExecutionException error("An allocation error occurred when loading Lua code for evaluation.");
        error.addData("LuaInterpretor name", _name);
        error.addData("luaCode", luaCode);

        std::string luaError(lua_tostring(_lua, lua_gettop(_lua)));
        error.addData("luaError", luaError);

        lua_sethook(_lua, initialHook, initialHookMask, initialHookCount);
        _mutex.unlock();
        throw error;
    }
}

void LuaInterpretor::execute(const std::string& luaCode, const std::string& fileName,
                 const std::string& fieldName, LuaExecutionCompleteDelegate completedDelegate)
{
    _isExecutingMutex.lock();
    if (_isCurrentlyExecuting)
    {
        ExecutionException error("Attempted to execute Lua code on a LuaInterpretor which is already executing code.");
        error.addData("LuaInterpretor name", _name);
        error.addData("luaCode", luaCode);
        error.addData("_currentLuaCode", _currentLuaCode);

        _isExecutingMutex.unlock();
        throw error;
    }
    _isCurrentlyExecuting = true;
    _currentLuaCode = luaCode;
    _executionComplete = completedDelegate;
    _isExecutingMutex.unlock();

    if (_executionThread == nullptr)
    {
        #warning "RETURN HERE";
        //_executionThread = SDL_CreateThread(&LuaInterpretor::executeBody,    SDL_ThreadFunction fn, const char *name, void *data);
        //_executionThread = new sf::Thread(&LuaInterpretor::executeBody, this);
    }
    //_executionThread->launch();
}

void LuaInterpretor::executeBody()
{
    int stackTopInitial = lua_gettop(_lua);

    // Load the Lua code
    int loadStatus = luaL_loadstring(_lua, _currentLuaCode.c_str());
    if (loadStatus == LUA_ERRSYNTAX)
    {
        ExecutionException error("A syntax error was found in the Lua code provided for execution.");
        error.addData("LuaInterpretor name", _name);
        error.addData("current lua code", _currentLuaCode);

        std::string luaError(lua_tostring(_lua, lua_gettop(_lua)));
        error.addData("luaError", luaError);

        throw error;
    }
    else if (loadStatus == LUA_ERRMEM)
    {
        ExecutionException error("An allocation error occurred when loading Lua code for execution.");
        error.addData("LuaInterpretor name", _name);
        error.addData("current lua code", _currentLuaCode);

        std::string luaError(lua_tostring(_lua, lua_gettop(_lua)));
        error.addData("luaError", luaError);

        throw error;
    }

    // Execute the Lua code
    int callReturnValue = lua_pcall(_lua, 0, LUA_MULTRET, 0);
    if (callReturnValue == LUA_ERRRUN)
    {
        ExecutionException error("A Lua runtime error was encountered when evaluating code.");
        error.addData("LuaInterpretor name", _name);
        error.addData("current Lua code", _currentLuaCode);

        std::string luaError(lua_tostring(_lua, lua_gettop(_lua)));
        error.addData("luaError", luaError);

        throw error;
    }
    else if (callReturnValue == LUA_ERRMEM)
    {
        ExecutionException error("An allocation error occurred when evaluating Lua code.");
        error.addData("LuaInterpretor name", _name);
        error.addData("current Lua code", _currentLuaCode);

        throw error;
    }
    else if (callReturnValue == LUA_ERRERR)
    {
        ExecutionException error("A Lua error was encountered when running the error handler.");
        error.addData("LuaInterpretor name", _name);
        error.addData("current Lua code", _currentLuaCode);

        std::string luaError(lua_tostring(_lua, lua_gettop(_lua)));
        error.addData("luaError", luaError);

        throw error;
    }

    onExecutionCompleted();
    if (_executionComplete != nullptr)
    {
        _executionComplete();
        _executionComplete = nullptr;
    }

    _isExecutingMutex.lock();

    _mutex.lock();  // we lock here because we only want to start modifying the stack if nothing
                    // else is currently modifying the stack
    lua_settop(_lua, stackTopInitial);
    lua_sethook(_lua, nullptr, 0, 0);
    _isCurrentlyExecuting = false;
    _mutex.unlock(); // we're done modifying the stack

    _isExecutingMutex.unlock();
}

void stopExecutionHook(lua_State* lua, lua_Debug* debug)
{
    luaL_error(lua, "StopExecution();");
}

void LuaInterpretor::stopExecution()
{
    _mutex.lock();  // if _mutex is locked, something is happening to _lua that we don't
                    // want to interrupt (evaluate(), the finalizing of execute(), etc)

    _isExecutingMutex.lock();
    if (!_isCurrentlyExecuting)
    {
        _isExecutingMutex.unlock();
        return;
    }

    lua_sethook(_lua, &stopExecutionHook, LUA_MASKCALL | LUA_MASKLINE, 0);

    _isExecutingMutex.unlock();
    _mutex.unlock();
}

void LuaInterpretor::onExecutionCompleted()
{
    // Do nothing (this function exists only for child classes to override)
}

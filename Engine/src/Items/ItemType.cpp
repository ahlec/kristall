#include "Items/ItemType.h"

std::string GetItemTypePocketName(ItemType itemType)
{
    switch (itemType)
    {
        case ItemType::Item:
        {
            return "Items pocket";
        }
        case ItemType::Medicine:
            {
                return "Medicine pocket";
            }
        case ItemType::PokeBall:
            {
                return "Poké Balls pocket";
            }
        case ItemType::MoveMachine:
            {
                return "TM & HM pocket";
            }
        case ItemType::Mail:
            {
                return "Mail pocket";
            }
        case ItemType::KeyItem:
            {
                return "Key Items pocket";
            }
        case ItemType::Berry:
            {
                return "Berries pouch";
            }
        case ItemType::BattleItem:
            {
                return "Battle Items pocket";
            }
    }
}

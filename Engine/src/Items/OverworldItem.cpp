#include "Items/OverworldItem.h"
#include "Data/BinaryReader.h"

OverworldItem::OverworldItem(BinaryReaderPtr reader, PlayerGame& playerGame)
{
    _itemNo = reader->readUInt32();
    _registrationCode = reader->readUInt32();
    _form = static_cast<ItemOverworldObjectForm>(reader->readUInt8());
}

#include "Items/Bag.h"

/*
Bag::Bag() : _totalCount(0), _uniqueItemsCount(0), _head(nullptr)
{
}

Bag::~Bag()
{
    Node* currentNode = _head;
    Node* nextNode = nullptr;
    while (currentNode != nullptr)
    {
        nextNode = currentNode->next;
        delete currentNode;
        currentNode = nextNode;
    }

    _head = nullptr;
}

bool Bag::has(const Item& item)
{
    _mutex.lock();

    Node* currentNode = _head;
    while (currentNode != nullptr)
    {
        if (currentNode->item == item)
        {
            _mutex.unlock();
            return true;
        }

        currentNode = currentNode->next;
    }

    _mutex.unlock();
    return false;
}

bool Bag::has(const std::string& itemHandle)
{
    _mutex.lock();

    Node* currentNode = _head;
    while (currentNode != nullptr)
    {
        if (currentNode->item.getHandle().compare(itemHandle) == 0)
        {
            _mutex.unlock();
            return true;
        }

        currentNode = currentNode->next;
    }

    _mutex.unlock();
    return false;
}

int32_t Bag::count(const Item& item)
{
    _mutex.lock();

    Node* currentNode = _head;
    int32_t amount = 0;
    while (currentNode != nullptr)
    {
        if (currentNode->item == item)
        {
            amount = currentNode->amount;
            break;
        }
        currentNode = currentNode->next;
    }

    _mutex.unlock();
    return amount;
}

int32_t Bag::count(const std::string& itemHandle)
{
    _mutex.lock();

    Node* currentNode = _head;
    int32_t amount = 0;
    while (currentNode != nullptr)
    {
        if (currentNode->item.getHandle().compare(itemHandle) == 0)
        {
            amount = currentNode->amount;
            break;
        }
        currentNode = currentNode->next;
    }

    _mutex.unlock();
    return amount;
}

void Bag::give(const Item& item, uint16_t amount)
{
    _mutex.lock();

    if (_head == nullptr)
    {
        _totalCount = (amount > 999 ? 999 : amount);
        _head = new Node(item, amount);
        _uniqueItemsCount = 1;
    }
    else
    {
        Node* currentNode = _head;
        Node* previousNode = nullptr;
        bool itemAdded = false;
        while (currentNode != nullptr)
        {
            if (currentNode->item == item)
            {
                _totalCount += (currentNode->amount + amount > 999 ? 999 - currentNode->amount : amount);
                currentNode->amount = (currentNode->amount + amount > 999 ? 999 : currentNode->amount + amount);
                itemAdded = true;
                break;
            }

            previousNode = currentNode;
            currentNode = currentNode->next;
        }

        if (!itemAdded)
        {
            _totalCount += (amount > 999 ? 999 : amount);
            ++_uniqueItemsCount;

            previousNode->next = new Node(item, (amount > 999 ? 999 : amount));
        }
    }

    _mutex.unlock();
}

void Bag::give(const std::string& itemHandle, uint16_t amount)
{
    give(Item::get(itemHandle), amount);
}

void Bag::take(const Item& item, uint16_t amount)
{
    _mutex.lock();

    Node* currentNode = _head;
    Node* previousNode = nullptr;
    while (currentNode != nullptr)
    {
        if (currentNode->item == item)
        {
            _totalCount -= (currentNode->amount - amount < 0 ? currentNode->amount : amount);
            currentNode->amount -= amount;
            if (currentNode->amount <= 0)
            {
                --_uniqueItemsCount;
                if (previousNode != nullptr || currentNode->next != nullptr)
                {
                    if (previousNode == nullptr)
                    {
                        _head = currentNode->next;
                    }
                    else
                    {
                        previousNode->next = currentNode->next;
                    }
                }
                else
                {
                    _head = nullptr;
                }

                delete currentNode;
            }

            break;
        }

        previousNode = currentNode;
        currentNode = currentNode->next;
    }

    _mutex.unlock();
}

void Bag::take(const std::string& itemHandle, uint16_t amount)
{
    take(Item::get(itemHandle), amount);
}

void Bag::swapPositions(const Item& itemA, const Item& itemB)
{
    if (itemA == itemB)
    {
        return;
    }

    _mutex.lock();

    Node* previousNodeA = nullptr;
    Node* previousNodeB = nullptr;
    bool isAFound = false;
    bool isBFound = false;
    Node* currentNode = _head;
    while (currentNode != nullptr)
    {
        if (!isAFound && currentNode->item == itemA)
        {
            isAFound = true;
            if (isBFound)
            {
                break;
            }
        }
        else if (!isAFound)
        {
            previousNodeA = currentNode;
        }

        if (!isBFound && currentNode->item == itemB)
        {
            isBFound = true;
            if (isAFound)
            {
                break;
            }
        }
        else if (!isBFound)
        {
            previousNodeB = currentNode;
        }

        currentNode = currentNode->next;
    }

    if (!isAFound || !isBFound)
    {
        return;
    }

    if (previousNodeA == nullptr && previousNodeB == _head)
    {
        Node* tempA = _head;
        Node* tempANext = _head->next;
        _head = previousNodeB->next;
        tempA->next = previousNodeB->next->next;
        _head->next = tempA;
    }
    else if (previousNodeB == nullptr && previousNodeA == _head)
    {
        Node* tempB = _head;
        Node* tempBNext = _head->next;
        _head = previousNodeA->next;
        tempB->next = previousNodeA->next->next;
        _head->next = tempB;
    }
    else if (previousNodeA == nullptr || previousNodeA->next == previousNodeB)
    {
        Node* tempA = _head;
        Node* tempANext = _head->next;
        _head = previousNodeB->next;
        tempA->next = previousNodeB->next->next;
        _head->next = tempANext;
        previousNodeB->next = tempA;
    }
    else if (previousNodeB == nullptr || previousNodeB->next == previousNodeA)
    {
        Node* tempB = _head;
        Node* tempBNext = _head->next;
        _head = previousNodeA->next;
        tempB->next = previousNodeA->next->next;
        _head->next = tempBNext;
        previousNodeA->next = tempB;
    }
    else
    {
        Node* tempB = previousNodeB->next;
        Node* tempBNext = previousNodeB->next->next;
        tempB->next = previousNodeA->next->next;
        previousNodeB->next = previousNodeA->next;
        previousNodeB->next->next = tempBNext;
        previousNodeA->next = tempB;
    }

    _mutex.unlock();
}

int Bag::getTotalItemCount() const
{
    return _totalCount;
}

int Bag::getUniqueItemsCount() const
{
    return _uniqueItemsCount;
}

Bag::Node::Node(Item itm, int initialAmount) : item(itm), amount(initialAmount),
                                               next(nullptr)
{
}
*/

#include "Items/Item.h"

/*
CyclicalLog<std::string, Item> Item::__parsedItems(50);

Item::Item()
{
}

Item Item::get(std::string handle)
{
    if (!__parsedItems.containsKey(handle))
    {
        BinaryReader reader("NEEDTOIMPLEMENT");//Data::getFile("items", handle, ".item"));

        Item* item = nullptr;
        unsigned char itemTypeData;
        reader.readUnsignedChar(&itemTypeData);
        ItemType type = static_cast<ItemType>(itemTypeData);
        switch (type)
        {
            case ItemType::PokeBall:
            {
                item = new PokeballItem();
                break;
            }
            case ItemType::Berry:
            {
                item = new BerryItem();
                break;
            }
            case ItemType::KeyItem:
            {
                item = new KeyItem();
                break;
            }
            default:
            {
                item = new Item();
                break;
            }
        }

        item->readHeader(reader);
        item->_type = type;
        item->_handle = reader.readString();
        item->_name = reader.readString();
        item->_pluralName = reader.readString();
        reader.readUnsignedShort(&item->_spriteNo);
        reader.readBoolean(&item->_isBuyable);
        if (item->_isBuyable)
        {
            uint32_t buyPrice;
            reader.readUnsignedInt(&buyPrice);
            item->_buyPrice = Money(buyPrice);
        }
        reader.readBoolean(&item->_isSellable);
        if (item->_isSellable)
        {
            uint32_t sellPrice;
            reader.readUnsignedInt(&sellPrice);
            item->_sellPrice = Money(sellPrice);
        }
        item->_description = reader.readString();
        reader.readBoolean(&item->_isUsableInBattle);
        if (item->_isUsableInBattle)
        {
            reader.readBoolean(&item->_isTargetingInBattle);
            item->_battleUsabilityCode = reader.readString();
            item->_battleCode = reader.readString();
        }
        reader.readBoolean(&item->_isUsableOutsideBattle);
        if (item->_isUsableOutsideBattle)
        {
            reader.readBoolean(&item->_isTargetingOutsideBattle);
            item->_outsideBattleUsabilityCode = reader.readString();
            item->_outsideBattleCode = reader.readString();
        }

        reader.close();

        __parsedItems.add(handle, *item);
        delete item;
    }

    return __parsedItems.get(handle);
}

void Item::readHeader(BinaryReader& reader)
{
    // do nothing!
}


bool Item::isEvEnhancing(PokemonStat& enhancedStat) const
{
    if (_handle.compare("PowerWeight") == 0)
    {
        enhancedStat = PokemonStat::HP;
        return true;
    }

    if (_handle.compare("PowerBracer") == 0)
    {
        enhancedStat = PokemonStat::Attack;
        return true;
    }

    if (_handle.compare("PowerBelt") == 0)
    {
        enhancedStat = PokemonStat::Defense;
        return true;
    }

    if (_handle.compare("PowerLens") == 0)
    {
        enhancedStat = PokemonStat::SpecialAttack;
        return true;
    }

    if (_handle.compare("PowerBand") == 0)
    {
        enhancedStat = PokemonStat::SpecialDefense;
        return true;
    }

    if (_handle.compare("PowerAnklet") == 0)
    {
        enhancedStat = PokemonStat::Speed;
        return true;
    }

    return false;
}


const std::string& Item::getSingularName() const
{
    return _name;
}

const std::string& Item::getPluralName() const
{
    return _pluralName;
}

ItemType Item::getItemType() const
{
    return _type;
}

const std::string& Item::getHandle() const
{
    return _handle;
}

bool Item::getIsBuyable() const
{
    return _isBuyable;
}

Money Item::getBuyPrice() const
{
    return _buyPrice;
}

bool Item::getIsSellable() const
{
    return _isSellable;
}

Money Item::getSellPrice() const
{
    return _sellPrice;
}

const std::string& Item::getDescription() const
{
    return _description;
}

uint16_t Item::getSpriteNo() const
{
    return _spriteNo;
}

bool Item::getIsUsableInBattle() const
{
    return _isUsableInBattle;
}

bool Item::getIsTargetingInBattle() const
{
    return _isTargetingInBattle;
}

const std::string& Item::getBattleUsabilityCode() const
{
    return _battleUsabilityCode;
}

const std::string& Item::getBattleCode() const
{
    return _battleCode;
}

bool Item::getIsUsableOutsideBattle() const
{
    return _isUsableOutsideBattle;
}

bool Item::getIsTargetingOutsideBattle() const
{
    return _isTargetingOutsideBattle;
}

const std::string& Item::getOutsideBattleUsabilityCode() const
{
    return _outsideBattleUsabilityCode;
}

const std::string& Item::getOutsideBattleCode() const
{
    return _outsideBattleCode;
}

bool Item::operator==(const Item& other) const
{
    return (_handle.compare(other._handle) == 0);
}

bool Item::operator!=(const Item& other) const
{
    return !(*this == other);
}
*/

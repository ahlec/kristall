#include "Items/GymBadgeCase.h"

/*
GymBadgeCase::GymBadgeCase() : _head(nullptr), _count(0), _obedienceLevel(0),
                               _onBadgeReceived(nullptr)
{
}

GymBadgeCase::~GymBadgeCase()
{
    Node* currentNode = _head;
    Node* nextNode = nullptr;
    while (currentNode != nullptr)
    {
        nextNode = currentNode->next;
        delete currentNode;
        currentNode = nextNode;
    }
    _head = nullptr;
}

bool GymBadgeCase::has(const GymBadge& badge) const
{
    Node* currentNode = _head;
    while (currentNode != nullptr)
    {
        if (currentNode->badge == badge)
        {
            return true;
        }
        currentNode = currentNode->next;
    }
    return false;
}

bool GymBadgeCase::has(const std::string& badgeHandle) const
{
    Node* currentNode = _head;
    while (currentNode != nullptr)
    {
        if (currentNode->badge.getHandle().compare(badgeHandle) == 0)
        {
            return true;
        }
        currentNode = currentNode->next;
    }
    return false;
}

void GymBadgeCase::give(const GymBadge& badge)
{
    _mutex.lock();

    ++_count;

    Node* currentNode = _head;
    while (currentNode != nullptr && currentNode->next != nullptr)
    {
        if (currentNode->badge == badge)
        {
            ExecutionException error("This GymBadgeCase already has an instance of the given GymBadge.");
            error.addData("const GymBadge& badge", badge.getHandle());
            throw error;
        }

        currentNode = currentNode->next;
    }

    if (currentNode == nullptr)
    {
        // because of the while loop above, currentNode will only be equal to nullptr if
        // it began as nullptr (because if _head is not nullptr, the loop above will run
        // until currentNode is equal to the last node). So here, we assign to _head.
        _head = new Node(badge);
    }
    else
    {
        currentNode->next = new Node(badge);
    }

    if (badge.getObedienceLevel() > _obedienceLevel)
    {
        _obedienceLevel = badge.getObedienceLevel();
    }

    if (_onBadgeReceived != nullptr)
    {
        _onBadgeReceived(badge);
    }

    _mutex.unlock();
}

void GymBadgeCase::give(const std::string& badgeHandle)
{

    give(GymBadge::get(badgeHandle));
}

bool GymBadgeCase::canUseHm(uint16_t hmNo) const
{
    Node* currentNode = _head;
    while (currentNode != nullptr)
    {
        if (currentNode->badge.getDoesAllowHmUse() && currentNode->badge.getAllowedHm() == hmNo)
        {
            return true;
        }
        currentNode = currentNode->next;
    }
    return false;
}

uint8_t GymBadgeCase::getObedienceLevel() const
{
    return _obedienceLevel;
}

uint16_t GymBadgeCase::getCount() const
{
    return _count;
}

void GymBadgeCase::setOnBadgeReceived(gymBadgeReceivedDelegate delegate)
{
    _mutex.lock();
    _onBadgeReceived = delegate;
    _mutex.unlock();
}

GymBadgeCase::Node::Node(GymBadge gymBadge) : badge(gymBadge), next(nullptr)
{
}
*/

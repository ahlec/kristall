#include "Characters/HumanCharacter.h"
#include "Game.h"
#include "Interface/GameWindow.h"
#include "Graphics/AnimationInstance.h"
#include "Graphics/SpriteSheetLibrary.h"
#include "Graphics/Animations.h"

// Class functions
void HumanCharacter::playAnimation(const std::string& animationHandle,
                                    AnimationFinishedDelegate animationFinished)
{
    DEVEL_NOT_TRANSLATED;
}

void HumanCharacter::setAnimationBaseNo(uint32_t animationBaseNo)
{
    _animation = Game::getGameWindow()->getSpriteSheetLibrary().createAnimationInstance(SpriteSheet::Characters,
                                                                                        animationBaseNo);
}

void HumanCharacter::updateAnimation(uint32_t elapsedTime)
{
    // Don't update the animation if we're sliding on ice
    if (_isSlidingOnIce)
    {
        return;
    }

    // If we're spinning because of a spinner, we need to animate the rotation/spin,
    // based on the amount of time we've been spinning
    if (_isSpinningBecauseOfSpinner)
    {
        _spinTimeElapsed += elapsedTime;
        if (_spinTimeElapsed >= 66)
        {
            _spinTimeElapsed = 0;
            int32_t spinAnimationNo;
            switch (_currentSpinningDirection)
            {
                case Direction::North:
                    {
                        spinAnimationNo = Animations::Characters::NORTH;
                        break;
                    }
                case Direction::East:
                    {
                        spinAnimationNo = Animations::Characters::EAST;
                        break;
                    }
                case Direction::South:
                    {
                        spinAnimationNo = Animations::Characters::SOUTH;
                        break;
                    }
                case Direction::West:
                    {
                        spinAnimationNo = Animations::Characters::WEST;
                        break;
                    }
            }

            _animation->playAnimation(spinAnimationNo);
            _animation->update(elapsedTime);
        }

        return;
    }

    // If we aren't moving anymore, we need to make sure our animation isn't moving anymore, either
    if (!_isMoving && !_isColliding && !_isFakeMoving)
    {
        uint32_t idleAnimationNo = Animations::Characters::getIdleAnimation(getCurrentTransportMode(),
                                                                            m_directionFacing);
        if (_animation->getCurrentAnimationNo() != idleAnimationNo)
        {
            _animation->playAnimation(idleAnimationNo);
        }
    }

    // Update the animation instance
    _animation->update(elapsedTime);
}

void HumanCharacter::animateMoveForward(bool isLeadingLeftLeg)
{
    _animation->playAnimation(Animations::Characters::getMovementAnimation(getCurrentTransportMode(),
                                                                           m_directionFacing,
                                                                           isLeadingLeftLeg));
}

void HumanCharacter::animateStandingIdle()
{
    // Determine the appropriate animation number
    uint32_t idleAnimationNo;
    switch (m_directionFacing)
    {
        case Direction::North:
            {
                idleAnimationNo = Animations::Characters::NORTH;
                break;
            }
        case Direction::East:
            {
                idleAnimationNo = Animations::Characters::EAST;
                break;
            }
        case Direction::South:
            {
                idleAnimationNo = Animations::Characters::SOUTH;
                break;
            }
        case Direction::West:
            {
                idleAnimationNo = Animations::Characters::WEST;
                break;
            }
    }

    // Play the new animation, if it isn't already the one playing
    if (_animation->getCurrentAnimationNo() != idleAnimationNo)
    {
        _animation->playAnimation(idleAnimationNo);
    }
}


// Member access functions (nothing too special beyond this point)
uint32_t HumanCharacter::getSpriteNo() const
{
    return _animation->getSpriteNo();
}

SpriteSheet HumanCharacter::getSpriteSheet() const
{
    return SpriteSheet::Characters;
}

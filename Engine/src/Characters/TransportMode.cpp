#include "Characters/TransportMode.h"

template<>
String toString<TransportMode>(TransportMode value)
{
    switch (value)
    {
        case TransportMode::Walk:
            {
                return String(L"Walk");
            }
        case TransportMode::Run:
            {
                return String(L"Run");
            }
        case TransportMode::Bike:
            {
                return String(L"Bike");
            }
        case TransportMode::Surf:
            {
                return String(L"Surf");
            }
    }

    return "<Unrecognized TransportMode: " + toString(static_cast<uint32_t>(value)) + ">";
};

namespace TransportModes
{
    uint32_t getJumpDuration()
    {
        return 272;
    }

    uint32_t getMoveDuration(TransportMode mode)
    {
        switch (mode)
        {
            case TransportMode::Walk:
            case TransportMode::Surf:
                {
                    return 272;
                }
            case TransportMode::Run:
                {
                    return 133;
                }
            case TransportMode::Bike:
                {
                    return 100;
                }
        }
    }
};

#include "Characters/DebugCharacter.h"
#include "World/Tile.h"

DebugCharacter::DebugCharacter()
{
    setAnimationBaseNo(1);
}

void DebugCharacter::beginDebug()
{
}

OverworldOccupantType DebugCharacter::getType() const
{
    return OverworldOccupantType::NonPlayerCharacter;
}

String DebugCharacter::getName() const
{
    return L"DEBUG CHARACTER";
}

void DebugCharacter::activate(Character& activator)
{
}

void DebugCharacter::onUpdate(uint32_t elapsedTime)
{
    if (!isMoving())
    {
        Tile* parent(getParent());
        if (parent->getX() == 9 && getDirectionFacing() != Direction::East)
        {
            turn(Direction::East);
        }
        else if (parent->getX() == 18 && getDirectionFacing() != Direction::West)
        {
            turn(Direction::West);
        }
        else
        {
            this->moveForward();
        }
    }
}

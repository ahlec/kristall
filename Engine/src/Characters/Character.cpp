#include "Characters/Character.h"
#include "Cornerstones.h"
#include "World/Tile.h"
#include "World/Map.h"
#include "World/OverworldObjects/OverworldObject.h"
#include "World/MapBridgeTile.h"
#include "World/DoorTile.h"
#include "General/Math.h"

Character::Character() :
    _gender(Gender::Male),
    _lastDirectionWalked(Direction::South),
    _spinTimeElapsed(0),
    _currentSpinningDirection(Direction::South),
    _isMoving(false),
    _isFakeMoving(false),
    _isColliding(false),
    _isSlidingOnIce(false),
    _isSpinningBecauseOfSpinner(false),
    _leader(nullptr),
    _follower(nullptr),
    _isJumping(false),
    _isSpeaking(false),
    _additionalYOffset(0),
    _currentEmotion(nullptr),
    _currentTransportMode(TransportMode::Walk),
    _isPreparingToMove(false),
    _isFinishingMove(false),
    _isLeadingWithLeftLeg(false),
    _moveTimeRemaining(0),
    _moveTimeDuration(0),
    _heldMoveMutex(nullptr),
    _lastParent(nullptr),
    _lastMap(nullptr),
    _moveCurrentMapChangeMode(MapChangeMode::MapBridge),
    _currentlyUsedStaircase(StaircaseType::NotAStaircase),
    _isUsingStaircaseAndAlreadyChangedMaps(false),
    _moveFinishedCallback(nullptr),
    _pathfindingInfo(nullptr)
{
}

Character::~Character()
{
}

Tile* Character::getCurrentlyFacedTile() const
{
    // If no parent, there is no currently faced tile
    if (m_parent == nullptr)
    {
        return nullptr;
    }

    // If we're standing on a MapBridge, facing the proper direction, the parent tile
    // can tell us which tile we're facing on the other map
    if (m_parent->getType() == TileType::MapBridgeTile &&
        m_parent->getInwardArrow() == m_directionFacing)
    {
        return m_parent->getDestination();
    }

    // Locate the appropriate position for the tile facing forward, and then query the map
    switch (m_directionFacing)
    {
        case Direction::North:
            {
                return m_parent->getMap()->getTile(m_parent->getX(), m_parent->getY() - 1);
            }
        case Direction::East:
            {
                return m_parent->getMap()->getTile(m_parent->getX() + 1, m_parent->getY());
            }
        case Direction::South:
            {
                return m_parent->getMap()->getTile(m_parent->getX(), m_parent->getY() + 1);
            }
        case Direction::West:
            {
                return m_parent->getMap()->getTile(m_parent->getX() - 1, m_parent->getY());
            }
    }

    return nullptr;
}

void Character::setAt(uint32_t mapNo, uint16_t tileX, uint16_t tileY)
{
    MapPtr targetMap(Map::get(mapNo));
    if (targetMap != nullptr)
    {
        this->setAt(targetMap->getTile(tileX, tileY));
    }
    else
    {
        this->setAt(nullptr);
    }
}

void Character::setAt(Tile* tile)
{
    // If we were given a tile, then we let the tile handle changing all of the variables; if we were
    // passed nullptr, then we let our current parent be occupied by nullptr, so that it will flush
    // us out of occupation.
    if (tile != nullptr)
    {
        tile->setCurrentOccupant(this);
    }
    else if (m_parent != nullptr)
    {
        m_parent->setCurrentOccupant(nullptr);
    }

    // Reset moving tokens, because those no longer apply
    _isSlidingOnIce = false;
    #warning "should this also consider spinning? jumping? etc? what is the feasibility of when this function is called?"
}

int16_t Character::getAdditionalYOffset() const
{
    return _additionalYOffset;
}

bool Character::isMoving() const
{
    return (isActuallyMoving() || _isSpinningBecauseOfSpinner || _isSlidingOnIce ||
            _pathfindingInfo != nullptr);
}

bool Character::canMoveForward() const
{
    if (m_parent == nullptr)
    {
        return false;
    }

    switch (m_directionFacing)
    {
        case Direction::North:
            {
                return (calculateTileEntrance(m_parent->getX(), m_parent->getY() - 1,
                                              _currentTransportMode, true) !=
                        TileEntrancePermissions::CannotEnter);
            }
        case Direction::East:
            {
                return (calculateTileEntrance(m_parent->getX() + 1, m_parent->getY(),
                                              _currentTransportMode, true) !=
                        TileEntrancePermissions::CannotEnter);
            }
        case Direction::South:
            {
                return (calculateTileEntrance(m_parent->getX(), m_parent->getY() + 1,
                                              _currentTransportMode, true) !=
                        TileEntrancePermissions::CannotEnter);
            }
        case Direction::West:
            {
                return (calculateTileEntrance(m_parent->getX() - 1, m_parent->getY(),
                                              _currentTransportMode, true) !=
                        TileEntrancePermissions::CannotEnter);
            }
    }

    return false;
}

TransportMode Character::getCurrentTransportMode() const
{
    return _currentTransportMode;
}

void Character::setTransportMode(TransportMode newMode)
{
    if (_pathfindingInfo != nullptr)
    {
        DEVEL_NOT_TRANSLATED;
    }

    if (_isSlidingOnIce || _isSpinningBecauseOfSpinner)
    {
        return;
    }

    if (m_parent != nullptr && m_parent->getNature().type == TileNatureType::BikePath &&
        newMode != TransportMode::Bike)
    {
        return;
    }

    TransportMode oldTransportMode = _currentTransportMode;
    _currentTransportMode = newMode;
    onTransportModeChanged(oldTransportMode, newMode);
}

void Character::moveForward(bool isFollowerIgnored, Mutex* threadMutex)
{

    // If this character is following another character, that character must initiate
    // the moving
    if (_leader != nullptr)
    {
        _leader->moveForward(isFollowerIgnored, threadMutex);
        return;
    }

    // Even if we ignore followers, logistically, we can't move while we have any
    // followers moving.
    Character* followerNode(_follower);
    while (followerNode != nullptr)
    {
        if (followerNode->isActuallyMoving())
        {
            return;
        }

        followerNode = followerNode->_follower;
    }

    // Setup and begin
    move(false, isFollowerIgnored, true, threadMutex);
}

void Character::turn(Direction newDirection)
{
    if (newDirection != m_directionFacing)
    {
        _isColliding = false;
    }

    OverworldOccupant::turn(newDirection);
}

bool Character::getIsSign() const
{
    return false;
}

void Character::update(uint32_t elapsedTime)
{
    #warning "return here!!!"
    // Update the animations
    updateAnimation(elapsedTime);

    // Moving
    if (_isMoving || _isColliding)
    {
        if (_moveTimeRemaining < elapsedTime)
        {
            _moveTimeRemaining = 0;
        }
        else
        {
            _moveTimeRemaining -= elapsedTime;
        }

        // Finish up moving
        if (_moveTimeRemaining <= 0)
        {
            // Restore/clear all movement-related states
            m_offsetX = 0;
            m_offsetY = 0;
            #warning "return here when dealing with strength.";

            bool shouldReportMoveFinish(_isMoving);
            bool wasColliding(_isColliding);
            _isMoving = false;
            _isColliding = false;
            _moveTimeDuration = 0;
            _moveTimeRemaining = 0;
            _currentlyUsedStaircase = StaircaseType::NotAStaircase;
            _additionalYOffset = 0;
            _isJumping = false;

            // Call the delegates
            if (_lastParent != nullptr && _lastParent->getType() == TileType::DoorTile && wasColliding)
            {
                _lastParent->occupantHasLeft(this);
            }

            // Begin working with parent natures
            TileNature parentNature(m_parent->getNature());

            // Deal with ice
            if (parentNature.type == TileNatureType::Ice &&
                (_leader == nullptr || _leader->_isSlidingOnIce) &&
                canMoveForward())
            {
                if (_currentTransportMode == TransportMode::Run)
                {
                    setTransportMode(TransportMode::Walk);
                }

                _isSlidingOnIce = true;
            }
            else
            {
                _isSlidingOnIce = false;
            }

            // Deal with spinners
            if (parentNature.type == TileNatureType::Spinner &&
                (_leader == nullptr || _leader->_isSpinningBecauseOfSpinner) &&
                !_isSpinningBecauseOfSpinner)
            {
                turn(parentNature.direction);
                if (canMoveForward())
                {
                    if (_currentTransportMode != TransportMode::Walk)
                    {
                        setTransportMode(TransportMode::Walk);
                    }
                    _isSpinningBecauseOfSpinner = true;
                    _currentSpinningDirection = m_directionFacing;
                    _spinTimeElapsed = 0;
                }
            }
            else if (parentNature.type == TileNatureType::Spinner &&
                     _isSpinningBecauseOfSpinner &&
                     m_directionFacing != parentNature.direction)
             {
                 // We need to use the base instance of turn(), because the Character
                 // override of turn() will abort if the Character is currently
                 // spinning, among other things
                 OverworldOccupant::turn(parentNature.direction);
             }

            // Deal with stoppers
            if (_isSpinningBecauseOfSpinner && (parentNature.type == TileNatureType::Stopper ||
                                                (_leader != nullptr && !_leader->_isSpinningBecauseOfSpinner) ||
                                                !canMoveForward()))
            {
                _isSpinningBecauseOfSpinner = false;
                animateStandingIdle();
            }

            // If we're at the end of a follower chain, but our chain leader is doing
            // something because of a tile, then we need to move that leader forward, in order
            // to bring our chain along (remember, if we're in a follower chain, we can only
            // be subjected to tile influences if our leader is also).
            if (_follower == nullptr)
            {
                Character* chainLeader(_leader);
                while (chainLeader != nullptr)
                {
                    if (chainLeader->_leader == nullptr)
                    {
                        break;
                    }

                    chainLeader = chainLeader->_leader;
                }

                if (chainLeader != nullptr && (chainLeader->_isSlidingOnIce ||
                                               chainLeader->_isSpinningBecauseOfSpinner) &&
                    !chainLeader->isActuallyMoving())
                {
                    chainLeader->moveForward(false, chainLeader->_heldMoveMutex);
                }
            }

            // Call UpdateAnimation specially; this should start any animation changes that have
            // happened recently; but, don't provide any time, so that we don't shortchange ourselves
            // on playback time.
            updateAnimation(0);

            // Report that we've finished moving
            if (shouldReportMoveFinish)
            {
                onMovingFinished();
                if (_moveFinishedCallback != nullptr)
                {
                    _moveFinishedCallback(this);
                    _moveFinishedCallback = nullptr;
                }
            }

            // Handle if we've finished moving forward, but are still pathfinding
            if (_pathfindingInfo != nullptr)
            {
                DEVEL_NOT_TRANSLATED;
            }

            // Release our mutex, if we acquired one
            if (!_isMoving && _heldMoveMutex != nullptr)
            {
                // We can simply release it here, without having to go through any thread manipulations,
                // because update() will ALWAYS happen on the main thread, and only on the main thread.
                _heldMoveMutex->unlock();
                _heldMoveMutex = nullptr;
                #warning "how do we want to handle Thread.Yield??"
            }

            // If we've finished moving, we quit updating here
            return;
        }
        else if (_isMoving && _moveTimeRemaining > 0)
        {
            static double prevPercent = 0.0;
            double moveProgressPercent = ((double)_moveTimeRemaining / _moveTimeDuration);
            DEVEL_LOGF("moveProgressPercent = %f (%f change)\n", moveProgressPercent, (moveProgressPercent - prevPercent));
            prevPercent = moveProgressPercent;

            // Update the additional Y offset
            if (_isJumping)
            {
                _additionalYOffset = std::floor(-8.0 * std::abs(std::sin(Math::PI * moveProgressPercent)));
            }

            // Update our offsets
            switch (m_directionFacing)
            {
                case Direction::North:
                    {
                        if (_isJumping)
                        {
                            m_offsetY = (int)ceil(_moveTimeRemaining > _moveTimeDuration / 2 ? 32.0 : 24.0) *
                                        moveProgressPercent;
                        }
                        else
                        {
                            m_offsetY = TILE_SIZE * moveProgressPercent;
                        }
                        break;
                    }
                case Direction::East:
                    {
                        m_offsetX = (int)ceil((_isJumping ? -TILE_SIZE_DOUBLE : -TILE_SIZE) * moveProgressPercent);
                        break;
                    }
                case Direction::South:
                    {
                        if (_isJumping)
                        {
                            m_offsetY = (int)ceil((_moveTimeRemaining > _moveTimeDuration / 2 ? -32.0 : -24.0) *
                                        moveProgressPercent);
                        }
                        else
                        {
                            m_offsetY = (int)ceil(-TILE_SIZE * moveProgressPercent);
                        }
                        break;
                    }
                case Direction::West:
                    {
                        m_offsetX = (int)ceil((_isJumping ? TILE_SIZE_DOUBLE : TILE_SIZE) * moveProgressPercent);

                        break;
                    }
            }
        }
    }
    else if (_isFakeMoving)
    {
    }

    onUpdate(elapsedTime);

    #warning "return here"
}

void Character::onParentChanged(Tile* oldParent, Tile* newParent)
{
    _lastParent = oldParent;
}

void Character::onTransportModeChanged(TransportMode oldMode, TransportMode newMode)
{
}

void Character::onMovingBegun(Tile* destination)
{
}

void Character::onMovingFinished()
{
}

void Character::onMapChanged(MapPtr oldMap, MapPtr newMap,
                            MapChangeMode changeMode)
{
}

void Character::onJump()
{
}

bool Character::isStrengthActive() const
{
    return false;
}

Character::TileEntrancePermissions Character::calculateTileEntrance(int32_t tileX, int32_t tileY,
                                                                    TransportMode transportation,
                                                                    bool isStrengthAllowed) const
{
    Direction resultantDirection = Directions::calculate(m_parent->getX(), m_parent->getY(),
                                                         tileX, tileY);

    // Would traveling this direction put us through a MapBridge/Door?
    if ((m_parent->getType() == TileType::DoorTile ||
         m_parent->getType() == TileType::MapBridgeTile) &&
         m_parent->getInwardArrow() == resultantDirection)
    {
        return TileEntrancePermissions::CanEnter;
    }

    // Would entering this tile put us outside map bounds? If so, we return, because we've already
    // handled the only cases for leaving the current map.
    if (tileX < 0 || tileY < 0 || tileX >= m_parent->getMap()->getWidth() ||
        tileY >= m_parent->getMap()->getHeight())
    {
        return TileEntrancePermissions::CannotEnter;
    }

    // Let's start working with the destination tile itself
    Tile* destination(m_parent->getMap()->getTile(tileX, tileY));

    // Is the Tile already occupied?
    OverworldOccupantPtr destinationOccupant(destination->getCurrentOccupant());
    if (destinationOccupant != nullptr)
    {
        // If the occupant at the destination is movable through using strength, and we can
        // use strength, determine if we are capable of moving the occupant in the forward
        // direction.
        if (isStrengthAllowed && isStrengthActive() &&
            destinationOccupant->getType() == OverworldOccupantType::OverworldObject &&
            static_cast<OverworldObject&>(*destinationOccupant).getIsMovable())
        {
            int32_t movableNewDestinationX(tileX);
            int32_t movableNewDestinationY(tileY);
            Directions::applyDirectionToTileLocation(resultantDirection,
                                                     movableNewDestinationX,
                                                     movableNewDestinationY);

            return (calculateTileEntrance(movableNewDestinationX, movableNewDestinationY,
                                          transportation, false) == TileEntrancePermissions::CanEnter ?
                    TileEntrancePermissions::CanEnterUsingStrength : TileEntrancePermissions::CannotEnter);
        }

        // If the destination occupant is a link on our follower chain, then we can
        // continue to process, because we are able to move through follower chain nodes;
        // otherwise, we've reached an impasse.
        if (!OverworldOccupantTypes::isCharacter(destinationOccupant->getType()))
        {
            return TileEntrancePermissions::CannotEnter;
        }

        Character& destinationCharacter(static_cast<Character&>(*destinationOccupant));
        if (destinationCharacter._leader != this && destinationCharacter._follower != this)
        {
            return TileEntrancePermissions::CannotEnter;
        }
    }
    else if (destination->hasOccupant())
    {
        // When dealing with DoorTiles, there are states where the Door doesn't have an occupant,
        // but is preparing for one, and so considers itself occupied; we can't enter tiles that
        // are waiting for an occupant, or have an occupant leaving them (but hasn't left fully yet).
        return TileEntrancePermissions::CannotEnter;
    }

    // Now, determine if we can enter based on the nature of the destination tile.
    TileNature destinationNature(destination->getNature());
    if ((TileNatureTypes::isSurfable(destinationNature.type) && transportation != TransportMode::Surf) ||
        (destinationNature.type == TileNatureType::Ledge && destinationNature.direction != resultantDirection))
    {
        return TileEntrancePermissions::CannotEnter;
    }
    else if (destinationNature.type == TileNatureType::Solid &&
             (!destinationNature.hasDirection ||
              Directions::complement(resultantDirection) == destinationNature.direction))
    {
        return TileEntrancePermissions::CannotEnter;
    }

    // Handle directional solids for the current tile
    TileNature parentNature(m_parent->getNature());
    if (parentNature.type == TileNatureType::Solid &&
        (!parentNature.hasDirection || resultantDirection == parentNature.direction))
    {
        return TileEntrancePermissions::CannotEnter;
    }

    // Passed all checks
    return TileEntrancePermissions::CanEnter;
}

Character::TilePreparationFunctor::TilePreparationFunctor(Character* character,
                            bool areFollowersIgnored,
                            Character::TileEntrancePermissions entrancePermissions) :
                                _character(character), _areFollowersIgnored(areFollowersIgnored),
                                _entrancePermissions(entrancePermissions)
{
}

void Character::TilePreparationFunctor::operator()(Tile* tile)
{
    _character->moveCharacterIntoTile(tile, _areFollowersIgnored,
                                      _entrancePermissions);
}

bool Character::isActuallyMoving() const
{
    return (_isPreparingToMove || _isMoving || _isColliding || _isFinishingMove || _isFakeMoving);
}

void Character::move(bool areChecksIgnored, bool isFollowerIgnored, bool isStrengthAllowed,
                     Mutex* threadMutex)
{
    bool areFollowersIgnored(isFollowerIgnored);

    // If we're already moving, we should stop now
    if (isActuallyMoving() && !areChecksIgnored)
    {
        return;
    }

    // If we aren't currently on a tile, we should stop now
    if (m_parent == nullptr)
    {
        return;
    }

    // If we were given a mutex, lock it and save it
    if (threadMutex != nullptr)
    {
        #warning "WE REALLY NEED TO RETURN HERE, AND TO REMEMBER WHY IT WAS IMPORTANT TO CROSS-THREAD LOCK A MUTEX! It was something important, I think; but see if it is still the best route to go."
       // Game::lockMutex(threadMutex);
        threadMutex->lock();
        _heldMoveMutex = threadMutex;
    }

    // 'Officially' begin the moving process
    _isPreparingToMove = true;

    // Determine the destination location
    int32_t destinationX(m_parent->getX());
    int32_t destinationY(m_parent->getY());
    switch (m_directionFacing)
    {
        case Direction::North:
            {
                --destinationY;
                break;
            }
        case Direction::East:
            {
                ++destinationX;
                break;
            }
        case Direction::South:
            {
                ++destinationY;
                break;
            }
        case Direction::West:
            {
                --destinationX;
                break;
            }
    }

    // Determine entrance permissions
    TileEntrancePermissions entrancePermissions = calculateTileEntrance(destinationX, destinationY,
                                                                        _currentTransportMode,
                                                                        isStrengthAllowed);
    if (entrancePermissions == TileEntrancePermissions::CannotEnter && !areChecksIgnored)
    {
        collide();
        return;
    }

    // Determine the destination tile
    Tile* destination = nullptr;
    if (m_parent->getType() == TileType::MapBridgeTile &&
        static_cast<MapBridgeTile&>(*m_parent).getInwardArrow() == m_directionFacing)
    {
        destination = static_cast<MapBridgeTile&>(*m_parent).getDestination();
        _moveCurrentMapChangeMode = MapChangeMode::MapBridge;
    }
    else if (m_parent->getType() == TileType::DoorTile &&
             static_cast<DoorTile&>(*m_parent).getInwardArrow() == m_directionFacing &&
             static_cast<DoorTile&>(*m_parent).getIsUsedOnExit())
    {
        destination = static_cast<DoorTile&>(*m_parent).getDestination();
        _moveCurrentMapChangeMode = MapChangeMode::Door;

        if (destination->getType() == TileType::DoorTile &&
            !static_cast<DoorTile&>(*destination).getIsUsedOnExit())
        {
            destination = getCurrentlyFacedTile();
            areFollowersIgnored = true;
        }
    }
    else
    {
        destination = m_parent->getMap()->getTile(destinationX, destinationY);
    }

    // If the movement is incompatible with the current transport mode, change the current
    // transport mode.
    if (_currentTransportMode == TransportMode::Surf &&
        !TileNatureTypes::isSurfable(destination->getNature().type))
    {
        setTransportMode(TransportMode::Walk);
    }
    else if (destination->getNature().type == TileNatureType::BikePath &&
             _currentTransportMode != TransportMode::Bike)
    {
        setTransportMode(TransportMode::Bike);
    }

    // Process now if the destination requires us to jump
    bool isJumpRequired = (destination->getNature().type == TileNatureType::Ledge);
    if (isJumpRequired)
    {
        switch (m_directionFacing)
        {
            case Direction::North:
                {
                    --destinationY;
                    break;
                }
            case Direction::East:
                {
                    ++destinationX;
                    break;
                }
            case Direction::South:
                {
                    ++destinationY;
                    break;
                }
            case Direction::West:
                {
                    --destinationX;
                    break;
                }
        }

        entrancePermissions = calculateTileEntrance(destinationX, destinationY, _currentTransportMode,
                                                    isStrengthAllowed);
        if (entrancePermissions == TileEntrancePermissions::CannotEnter)
        {
            collide();
            return;
        }

        // Get the real destination (which is the destination after jumping over the ledge)
        // However, ignore this if we're travelling through a MapBridge
        if (m_parent->getType() != TileType::MapBridgeTile ||
            static_cast<MapBridgeTile&>(*m_parent).getInwardArrow() != m_directionFacing)
        {
            destination = m_parent->getMap()->getTile(destinationX, destinationY);
        }
    }

    // Begin the moving process
    onMovingBegun(destination);
    _isJumping = isJumpRequired;

    // Process special Door cases
    if (m_parent->getType() == TileType::DoorTile)
    {
        DoorTile& parentDoor(static_cast<DoorTile&>(*m_parent));
        // Use on exit
        if (parentDoor.getIsUsedOnExit())
        {
            DEVEL_NOT_TRANSLATED;
        }

        // Staircasing
        _currentlyUsedStaircase = parentDoor.getStaircaseType();
        if (_currentlyUsedStaircase != StaircaseType::NotAStaircase &&
            parentDoor.getInwardArrow() == m_directionFacing)
        {
                _isUsingStaircaseAndAlreadyChangedMaps = false;
                areFollowersIgnored = true;
                if (_currentTransportMode != TransportMode::Walk)
                {
                    setTransportMode(TransportMode::Walk);
                }
        }
    }

    // Move into the destination
    OccupantMayEnterTileDelegate onTilePrepared(new TilePreparationFunctor(this,
                                                                           areFollowersIgnored,
                                                                           entrancePermissions));
    destination->prepareForOccupant(this, onTilePrepared);
}

void Character::collide()
{
    _isPreparingToMove = false;
    if (_isColliding)
    {
        return;
    }

    _isMoving = false;
    _isColliding = true;

    // If we aren't sliding on ice or spinning, we need to animate our "move forward" all the same
    if (!_isSlidingOnIce && !_isSpinningBecauseOfSpinner)
    {
        animateMoveForward(_isLeadingWithLeftLeg);
        _isLeadingWithLeftLeg = !_isLeadingWithLeftLeg;
    }

    // Set the times for the collision
    _moveTimeDuration = TransportModes::getMoveDuration(_currentTransportMode);
    _moveTimeRemaining = _moveTimeDuration;
}

void Character::moveCharacterIntoTile(Tile* destination, bool isFollowerIgnored,
                                      TileEntrancePermissions enterPermissions)
{
    //Logger::write("BEGIN MOVE");
    _moveTimeRemaining = (_isJumping ? TransportModes::getJumpDuration() :
                          TransportModes::getMoveDuration(_currentTransportMode));
    if (_currentlyUsedStaircase != StaircaseType::NotAStaircase)
    {
        _moveTimeRemaining *= 2;
    }
    _moveTimeDuration = _moveTimeRemaining;

    // If we aren't sliding on ice or spinning, we need to animate our "move forward" all the same
    if (!_isSlidingOnIce && !_isSpinningBecauseOfSpinner)
    {
        animateMoveForward(_isLeadingWithLeftLeg);
        _isLeadingWithLeftLeg = !_isLeadingWithLeftLeg;
    }

    // Start it
    _isMoving = true;
    _isPreparingToMove = false;

    // Process follower
    bool isFollowPossible = true;
    if (!isFollowerIgnored && _follower != nullptr)
    {
        if (_follower->m_parent == nullptr)
        {
            isFollowPossible = false;
        }
        else if (_follower->m_parent->getMap() != m_parent->getMap())
        {
            if (_follower->m_parent->getType() == TileType::MapBridgeTile)
            {
                _follower->turn(static_cast<MapBridgeTile&>(*_follower->m_parent).getInwardArrow());
            }
            else
            {
                isFollowPossible = false;
            }
        }
        else
        {
            _follower->turn(Directions::calculate(_follower->m_parent->getX(), _follower->m_parent->getY(),
                                                  m_parent->getX(), m_parent->getY()));
        }

        if (isFollowPossible)
        {
            _follower->move(false, false, false, nullptr);
        }
    }

    // Handle if we used strength to move into this tile, what we do with the occupant now
    if (enterPermissions == TileEntrancePermissions::CanEnterUsingStrength)
    {
        DEVEL_NOT_TRANSLATED;
    }

    // Handle destinations and staircasings
    if (_currentlyUsedStaircase != StaircaseType::NotAStaircase)
    {
        DEVEL_NOT_TRANSLATED;
    }
    else
    {
        destination->setCurrentOccupant(this);
    }

    // Finish up with the follower
    if (_follower != nullptr && !isFollowPossible && !isFollowerIgnored)
    {
        _follower->setAt(_lastParent);
        _follower->turn(_lastDirectionWalked);
    }
    _lastDirectionWalked = m_directionFacing;

    // Set the offsets
    switch (m_directionFacing)
    {
        case Direction::North:
            {
                m_offsetY = (_isJumping ? TILE_SIZE_DOUBLE : TILE_SIZE);
                m_offsetX = 0;
                break;
            }
        case Direction::East:
            {
                m_offsetX = (_isJumping ? -TILE_SIZE_DOUBLE : -TILE_SIZE);
                m_offsetY = 0;
                break;
            }
        case Direction::South:
            {
                m_offsetX = 0;
                m_offsetY = (_isJumping ? -TILE_SIZE_DOUBLE : -TILE_SIZE);
                break;
            }
        case Direction::West:
            {
                m_offsetX = (_isJumping ? TILE_SIZE_DOUBLE : TILE_SIZE);
                m_offsetY = 0;
                break;
            }
    }

    // Set the offsets for the disposed object
    if (enterPermissions == TileEntrancePermissions::CanEnterUsingStrength)
    {
        DEVEL_NOT_TRANSLATED;
    }
}

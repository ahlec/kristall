#include "Characters/PlayerCharacter.h"
#include "Data/BinaryReader.h"
#include "World/Tile.h"
#include "Player/PlayerGame.h"
#include "Items/PokeballItem.h"
#include "Characters/DayCare.h"
#include "Input/ProcessedInput.h"

// Virtual function definitions

// Class functions
PlayerCharacter::PlayerCharacter(BinaryReaderPtr& reader, PlayerGame* playerGame) :
    HumanCharacter(), _playerGame(playerGame), _isInputDisabled(false), _isInputDirectionQueued(false),
    _hasTurnedSinceLastMove(false)
{
    _gender = static_cast<Gender>(reader->readUInt8());
    setAnimationBaseNo(1);
}

PlayerCharacter::~PlayerCharacter()
{
    DEVEL_NOT_IMPLEMENTED;
}

void PlayerCharacter::interact()
{
    DEVEL_NOT_TRANSLATED;
}

void PlayerCharacter::activate(Character& activator)
{
    DEVEL_NOT_TRANSLATED;
}

void PlayerCharacter::onUpdate(uint32_t elapsedTime)
{
    WARN_DEVEL_NOT_TRANSLATED;
}

void PlayerCharacter::onMapChanged(MapPtr oldMap, MapPtr newMap,
                                   MapChangeMode changeMode)
{
    WARN_DEVEL_NOT_TRANSLATED;
}

void PlayerCharacter::onTurned(Direction oldDirection, Direction newDirection)
{
    WARN_DEVEL_NOT_TRANSLATED;
}

void PlayerCharacter::onMovingBegun(Tile* destination)
{
    WARN_DEVEL_NOT_TRANSLATED;
}

void PlayerCharacter::onMovingFinished()
{
    WARN_DEVEL_NOT_TRANSLATED;
}

void PlayerCharacter::onTransportModeChanged(TransportMode oldMode,
                                             TransportMode newMode)
{
    WARN_DEVEL_NOT_TRANSLATED;
}

bool PlayerCharacter::isStrengthActive() const
{
    WARN_DEVEL_NOT_TRANSLATED;
    return false;
}

void PlayerCharacter::interpretInput(const ProcessedInput& input)
{
    if (_isInputDisabled)
    {
        return;
    }

    if (isMoving())
    {
        if (input.isRightPressed)
        {
            _isInputDirectionQueued = true;
            _queuedInputDirection = Direction::East;
        }
        else if (input.isLeftPressed)
        {
            _isInputDirectionQueued = true;
            _queuedInputDirection = Direction::West;
        }
        else if (input.isDownPressed)
        {
            _isInputDirectionQueued = true;
            _queuedInputDirection = Direction::South;
        }
        else if (input.isUpPressed)
        {
            _isInputDirectionQueued = true;
            _queuedInputDirection = Direction::North;
        }
        return;
    }

    // Turning
    if (_isInputDirectionQueued && _queuedInputDirection != m_directionFacing)
    {
        turn(_queuedInputDirection);
        _isInputDirectionQueued = false;
        _hasTurnedSinceLastMove = true;
    }

    // Moving
    bool hasBegunMoving = false;
    if ((input.isUpPressed && m_directionFacing == Direction::North) ||
        (input.isUpDown && _lastDirectionWalked == Direction::North &&
            m_directionFacing == Direction::North && !_hasTurnedSinceLastMove) ||
        (input.isDownPressed && m_directionFacing == Direction::South) ||
        (input.isDownDown && _lastDirectionWalked == Direction::South &&
            m_directionFacing == Direction::South && !_hasTurnedSinceLastMove) ||
        (input.isRightPressed && m_directionFacing == Direction::East) ||
        (input.isRightDown && _lastDirectionWalked == Direction::East &&
            m_directionFacing == Direction::East && !_hasTurnedSinceLastMove) ||
        (input.isLeftPressed && m_directionFacing == Direction::West) ||
        (input.isLeftDown && _lastDirectionWalked == Direction::West &&
            m_directionFacing == Direction::West && !_hasTurnedSinceLastMove))
    {
        DEVEL_LOG("[Player began moving]\n");
        moveForward(false, nullptr);
        _hasTurnedSinceLastMove = false;
        hasBegunMoving = true;
    }
    else if (input.isRightDown)
    {
        turn(Direction::East);
        _hasTurnedSinceLastMove = true;
        hasBegunMoving = true;
    }
    else if (input.isLeftDown)
    {
        turn(Direction::West);
        _hasTurnedSinceLastMove = true;
        hasBegunMoving = true;
    }
    else if (input.isDownDown)
    {
        turn(Direction::South);
        _hasTurnedSinceLastMove = true;
        hasBegunMoving = true;
    }
    else if (input.isUpDown)
    {
        turn(Direction::North);
        _hasTurnedSinceLastMove = true;
        hasBegunMoving = true;
    }

    if (!hasBegunMoving)
    {
        if (m_parent != nullptr)
        {
            TileNature parentTileNature = m_parent->getNature();
            if (parentTileNature.type == TileNatureType::BikePath &&
                parentTileNature.hasDirection)
            {
                if (m_directionFacing != parentTileNature.direction)
                {
                    turn(parentTileNature.direction);
                }

                if (canMoveForward())
                {
                    moveForward(false, nullptr);
                }
            }
        }
    }
}

// Member access functions (nothing too special beyond this point)
OverworldOccupantType PlayerCharacter::getType() const
{
    return OverworldOccupantType::PlayerCharacter;
}

String PlayerCharacter::getName() const
{
    return _playerGame->getName();
}

uint16_t PlayerCharacter::getTrainerNo() const
{
    return _playerGame->getTrainerId();
}

uint16_t PlayerCharacter::getSecretNo() const
{
    return _playerGame->getSecretId();
}

TrainerClass PlayerCharacter::getTrainerClass() const
{
    return TrainerClass::PKMNTrainer;
}

const PokemonTrainerMusicSelection& PlayerCharacter::getMusicSelection() const
{
    DEVEL_NOT_TRANSLATED;
}

const std::string& PlayerCharacter::getTextOnDefeat() const
{
    DEVEL_NOT_TRANSLATED;
}

uint16_t PlayerCharacter::getBasePayout() const
{
    DEVEL_NOT_TRANSLATED;
}

int32_t PlayerCharacter::getTotalNumberPokemon() const
{
    DEVEL_NOT_TRANSLATED;
}

PokemonLocationPtr PlayerCharacter::givePokemon(Pokemon& pokemonGiven,
                                                PokemonAcquisitionMethod acquisitionMethod,
                                                PokeballItem containingPokeball)
{
    DEVEL_NOT_TRANSLATED;
}

std::vector<PokemonPtr> PlayerCharacter::getParty()
{
    DEVEL_NOT_TRANSLATED;
}

PokemonPtr PlayerCharacter::getFirstPokemonInParty(Predicate<Pokemon> condition)
{
    DEVEL_NOT_TRANSLATED;
}

PokemonPtr PlayerCharacter::getPokemon(PokemonLocationPtr location)
{
    DEVEL_NOT_TRANSLATED;
}

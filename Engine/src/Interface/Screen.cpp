#include "Interface/Screen.h"
#include "Errors/ExecutionException.h"

// Virtual function definitions
Screen::~Screen()
{
}

void Screen::onInitialize(PlayerGame* playerGame, SpriteBatch* spriteBatch)
{
}

void Screen::onClose()
{
}

// Class functions
Screen::Screen(bool doesTakeInput, bool isGameplayPaused, bool isOverworldDrawn) :
                            m_doesTakeInput(doesTakeInput), m_isGameplayPaused(isGameplayPaused),
                            m_isOverworldDrawn(isOverworldDrawn), m_isClosed(false), m_isInitialized(false)
{
}

void Screen::registerCloseCallback(FunctionPtr<void, Screen*> callback)
{
    if (callback != nullptr)
    {
        m_closeCallbacks.push_back(callback);
    }
}

void Screen::close()
{
    if (m_isClosed)
    {
        ExecutionException error("Cannot close a Screen that has already been closed.");
        throw error;
    }

    m_isClosed = true;
    onClose();

    for (FunctionPtr<void, Screen*> callback : m_closeCallbacks)
    {
        callback(this);
    }
}

void Screen::initialize(PlayerGame* playerGame, SpriteBatch* spriteBatch)
{
    if (m_isInitialized)
    {
        ExecutionException error("Cannot initialize a Screen that has already been initialized.");
        throw error;
    }

    m_isInitialized = true;
    onInitialize(playerGame, spriteBatch);
}

// Member access functions (nothing too special beyond this point)
bool Screen::doesTakeInput() const
{
    return m_doesTakeInput;
}

bool Screen::isGameplayPaused() const
{
    return m_isGameplayPaused;
}

bool Screen::isOverworldDrawn() const
{
    return m_isOverworldDrawn;
}

bool Screen::isClosed() const
{
    return m_isClosed;
}

bool Screen::isInitialized() const
{
    return m_isInitialized;
}

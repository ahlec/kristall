#include "Interface/Screens/InGameMenuScreen.h"
#include "Graphics/SpriteBatch.h"
#include "Input/ProcessedInput.h"
#include "Game.h"

const int MENU_WIDTH = 104;
const int MENU_ITEM_HEIGHT = 24;
const int MENU_ITEM_RECTANGLE_MARGIN = 8;

class InGameMenuScreen::MenuItem
{
public:
    MenuItem() : activationFunction(nullptr)
    {
    }

    UnicodeString       text;
    FunctionPtr<void>   activationFunction;
};


InGameMenuScreen::InGameMenuScreen(GameWindow* window, PlayerGame* playerGame) : Screen(true, true, true),
    m_currentSelectionIndex(0)
{
    m_menuFrame = Game::getSettings().getFrame();

    /** START OF MENU ITEM DEFINITIONS **/

    #if DEBUG
    {
        MenuItem* debugItem = new MenuItem();
        debugItem->text = "DEBUG";
        m_menuItems.push_back(debugItem);
    }
    #endif

    MenuItem* bagItem = new MenuItem();
    bagItem->text = "BAG";
    m_menuItems.push_back(bagItem);

    MenuItem* saveItem = new MenuItem();
    saveItem->text = "SAVE";
    m_menuItems.push_back(saveItem);

    MenuItem* settingsItem = new MenuItem();
    settingsItem->text = "SETTINGS";
    m_menuItems.push_back(settingsItem);

    MenuItem* closeItem = new MenuItem();
    closeItem->text = "CLOSE";
    m_menuItems.push_back(closeItem);

    MenuItem* quitItem = new MenuItem();
    quitItem->text = "QUIT";
    m_menuItems.push_back(quitItem);

    /** END OF MENU ITEM DEFINITIONS **/

    int windowWidth = window->getWidth();
    int windowHeight = window->getHeight();
    m_menuHeight = m_menuItems.size() * MENU_ITEM_HEIGHT + (MENU_ITEM_RECTANGLE_MARGIN * 2);
    m_menuOffset = Vector2(windowWidth - MENU_WIDTH - 16, (int)((windowHeight - m_menuHeight) / 2));
    m_menuRectangle = Rect(m_menuOffset.x, m_menuOffset.y, MENU_WIDTH, m_menuHeight);
}

InGameMenuScreen::~InGameMenuScreen()
{
    for (MenuItem* item : m_menuItems)
    {
        delete item;
    }
}

void InGameMenuScreen::update(uint32_t elapsedTime, PlayerGame* playerGame, const ProcessedInput& input)
{
    if (input.isDownPressed)
    {
        ++m_currentSelectionIndex;
    }
    else if (input.isUpPressed)
    {
        --m_currentSelectionIndex;
    }

    if (m_currentSelectionIndex < 0)
    {
        m_currentSelectionIndex = m_menuItems.size() - 1;
    }
    else if (m_currentSelectionIndex >= m_menuItems.size())
    {
        m_currentSelectionIndex = 0;
    }

    if (input.isBPressed || input.isStartPressed)
    {
        this->close();
    }
}

void InGameMenuScreen::draw(SpriteBatch* spriteBatch, PlayerGame* playerGame)
{
    spriteBatch->drawFrame(m_menuFrame, m_menuRectangle);

    Rect menuItemRect(m_menuOffset.x + MENU_ITEM_RECTANGLE_MARGIN,
                      m_menuOffset.y + MENU_ITEM_RECTANGLE_MARGIN,
                      MENU_WIDTH - MENU_ITEM_RECTANGLE_MARGIN - MENU_ITEM_RECTANGLE_MARGIN,
                      MENU_ITEM_HEIGHT);
    for (int index = 0; index < m_menuItems.size(); ++index)
    {
        if (index == m_currentSelectionIndex)
        {
            spriteBatch->drawRectangle(menuItemRect, Color(64, 224, 208));
        }
        spriteBatch->drawString(m_menuItems[index]->text, Font::Normal, FontColor::Black, menuItemRect.x + 20,
            menuItemRect.y + 5);
        menuItemRect.y += MENU_ITEM_HEIGHT;
    }
}

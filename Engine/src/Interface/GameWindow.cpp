#include "Interface/GameWindow.h"
#include "Interface/Screen.h"
#include "Graphics/SpriteBatch.h"
#include "Data/DataManager.h"
#include "DeveloperSwitches.h"
#include "Game.h"
#include "Characters/PlayerCharacter.h"
#include "Player/PlayerGame.h"
#include "Data/BinaryReader.h"
#include "Graphics/SpriteSheetLibrary.h"
#include "General/Math.h"
#include "Input/ProcessedInput.h"
#include "Input/InputController.h"
#include "World/Overworld.h"
#include "Lua/OverworldLuaInterpretor.h"
#include <SDL.h>
#include "Graphics/Color.h"
#include <cstdio>
#include "Graphics/Texture.h"
#include "Cornerstones.h"
#include "General/MutexScopeLock.h"
#include "Interface/Screens/InGameMenuScreen.h"
#include "World/MapLocation.h"
#include "Player/PlayerGame.h"
#include "World/Map.h"
#include "World/Tile.h"
#include <iostream>
#include "Characters/DebugCharacter.h"

#if DEBUG
class GameWindow::GameWindowDebugData
{
public:
    ProcessedInput cachedInput;
};
#endif

GameWindow::GameWindow() : m_isActive(false), m_overworld(new Overworld()),
    m_spriteSheetLibrary(new SpriteSheetLibrary()), m_spriteBatch(new SpriteBatch(m_spriteSheetLibrary)),
    m_inputController(new InputController()), m_currentPlayerGame(nullptr),
    m_systemScreen(nullptr), m_doScreensAllowOverworldDrawing(true), m_screensMutex(new Mutex())
{
    m_window = SDL_CreateWindow("Pokémon Kristall", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (m_window == nullptr)
    {
        throw ExecutionException("Could not create the window.");
    }
    m_windowSurface = SDL_GetWindowSurface(m_window);
    if (m_windowSurface == nullptr)
    {
        throw ExecutionException("Could not get the window surface.");
    }
    m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (m_renderer == nullptr)
    {
        throw ExecutionException("Could not create the renderer.");
    }

    #if DEBUG
    {
        m_debugData = new GameWindowDebugData();
    }
    #endif
}

void GameWindow::initializePlayerGame()
{
    BinaryReaderPtr playerGameReader = DataManager::openFileReader(FileType::PlayerGame, 0);
    MapLocation playerCharacterLocation;
    m_currentPlayerGame = new PlayerGame(playerGameReader, playerCharacterLocation);
    playerGameReader->close();

    Map::resetCache(m_currentPlayerGame);

    // Put the player character in the appropriate position
    Tile* playerStartTile(Map::get(playerCharacterLocation.getMapNo())->getTile(playerCharacterLocation.getTileX(),
                                                          playerCharacterLocation.getTileY()));
    playerStartTile->setCurrentOccupant(m_currentPlayerGame->getCharacter());

    DebugCharacter* testCharacter = new DebugCharacter();
    testCharacter->setAt(4, 9, 13);
    testCharacter->beginDebug();
}

void GameWindow::run()
{
    // If the game is already running, that means this function has already been called (run() should only ever be called exactly
    // once during execution; even after the function ends, it should not be allowed to be called again, as this function terminating
    // means the game has been closed.
    if (m_isActive)
    {
        ExecutionException error("The game is already running; this function cannot be called again.");
        throw error;
    }
    m_isActive = true;

    // Cache the current size of the window for drawing purposes (this is a very frequently used variable, and should be
    // passed by const reference rather than calculated, as it is used in every render call). It should be updated by
    // resize events when polling events in the main game loop.
    SDL_Rect windowDisplayRect;
    windowDisplayRect.x = 0;
    windowDisplayRect.y = 0;
    SDL_GetWindowSize(m_window, &windowDisplayRect.w, &windowDisplayRect.h);

    /**
     * ALRIGHT. SO. LET'S EXPLAIN WHY THIS ISN'T AS FUCKING AWFUL AS IT LOOKS.
     * I mean, it kind of is. I'd prefer to have a better solution for framerate, but
     * for the moment, it was too choppy and was incredibly jerky during character
     * movement (noticeably so). Choosing a fixed framerate over a variable one is,
     * so far, making this a super easier go of things, and given that we're working
     * in 2D and therefore our rendering is SIGNIFICANTLY easier than it is for 3D,
     * I suppose having a fixed framerate is a much smaller pill to swallow. In the
     * end, should this arise as a significant problem, there is a hybrid approach
     * we could take: collect the most recent X number of variable framerates, average
     * then, and output a framerate to pass down to the other functions as the current
     * framerate. This would accomodate for varying speeds while also providing a
     * (mostly) consistent framerate. However, I'm not going to implement that right
     * now because the current setup is working, and I'm not going to try to fix it
     * until it's actually a problem.
     *
     * Note that when changing this, pay attention to TransportMode framerate speeds
     * in particular.
     *
     * Also note that this should never be a public constant variable; to allow the
     * possibility of future updates changing how framerates work, only this function
     * should be privvy to how we're handling framerates.
     **/
    const uint32_t ELAPSED_MILLISECONDS = 17;

    /// ---------------------------------------------------- Our main event loop
    SDL_Event event;
    bool isClosing(false);
    bool hasFocus(true);
    while (true)
    {
        // Poll all of the events in the queue
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_QUIT:
                    {
                        isClosing = true;
                        break;
                    }
                case SDL_WINDOWEVENT:
                    {
                        switch (event.window.event)
                        {
                            case SDL_WINDOWEVENT_RESIZED:
                                {
                                    // If the window was resized, then we should update windowDisplayRect
                                    windowDisplayRect.w = event.window.data1;
                                    windowDisplayRect.h = event.window.data2;
                                    break;
                                }
                            case SDL_WINDOWEVENT_FOCUS_LOST:
                                {
                                    hasFocus = false;
                                    break;
                                }
                            case SDL_WINDOWEVENT_FOCUS_GAINED:
                                {
                                    hasFocus = true;
                                    break;
                                }
                        }
                        break;
                    }
            }
        }

        // Don't bother with drawing another round; we've already set upon ourselves to close.
        // Exit the loop now.
        if (isClosing)
        {
            break;
        }

        update(ELAPSED_MILLISECONDS);
        draw();
    }
}

void GameWindow::update(uint32_t elapsedTime)
{
    m_spriteSheetLibrary->update(elapsedTime);

    ProcessedInput processedInput(m_inputController->getProcessedInput(elapsedTime, hasFocus(), false, nullptr));
    #if DEBUG
    {
        m_debugData->cachedInput = processedInput;
    }
    #endif

    bool hasInputBeenConsumed = false; // Input can only be consumed by one "destination" per update (ie, screen, user)

    /** --------- System screens --------- **/
    // Coming soon!

    /** --------- Screens --------- **/
    if (m_screens.size() > 0)
    {
        MutexScopeLock lock(m_screensMutex);

        bool isGamePausingScreenOpen = false;
        for (Screen* screen : m_screens)
        {
            if (!screen->isInitialized())
            {
                screen->initialize(m_currentPlayerGame, m_spriteBatch);
            }

            if (screen->doesTakeInput())
            {
                hasInputBeenConsumed = true;
            }

            screen->update(elapsedTime, m_currentPlayerGame, processedInput);

            if (screen->isGameplayPaused())
            {
                isGamePausingScreenOpen = true;
            }
        }

        if (isGamePausingScreenOpen)
        {
            return;
        }
    }

    /** --------- Overworld --------- **/
    m_overworld->update(elapsedTime, m_currentPlayerGame);

    // Input processing (PlayerCharacter)
    if (!m_overworld->getLuaInterpretor()->getIsCurrentlyExecuting())
    {
        m_currentPlayerGame->getCharacter()->interpretInput(processedInput);
    }

    /** --------- Interaction --------- **/
    if (processedInput.isAPressed && !hasInputBeenConsumed)
    {
        m_currentPlayerGame->getCharacter()->interact();
        m_inputController->requireButtonRelease(InputButton::A);
        hasInputBeenConsumed = true;
    }

    /** --------- In-game menu --------- **/
    if (processedInput.isStartPressed && !hasInputBeenConsumed)
    {
        InGameMenuScreen* menuScreen = new InGameMenuScreen(this, m_currentPlayerGame);
        menuScreen->registerCloseCallback(FunctionPtr<void, Screen*>::fromMember(this, &GameWindow::onInGameMenuScreenClosed));
        openScreen(menuScreen);
        hasInputBeenConsumed = true;
    }
}

inline void convertToSdlRect(const Rect& rect, SDL_Rect& sdlRect)
{
    sdlRect.x = rect.x;
    sdlRect.y = rect.y;
    sdlRect.w = rect.width;
    sdlRect.h = rect.height;
};

struct RenderTextSurface
{
    SDL_Texture* texture;
    SDL_Rect shadowRect;
    SDL_Rect mainRect;
    FontColor fontColor;
};

void GameWindow::draw()
{
    SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 255);
    SDL_RenderClear(m_renderer);

    m_spriteBatch->reset();

    /** --------- Overworld --------- **/
    if (m_systemScreen == nullptr && m_doScreensAllowOverworldDrawing)
    {
        m_overworld->draw(m_spriteBatch, m_currentPlayerGame);
        // More stuff to translate here
    }
    else if (m_systemScreen != nullptr)
    {
        // Coming soon
    }

    /** --------- Interface --------- **/
    if (m_screens.size() > 0)
    {
        MutexScopeLock lock(m_screensMutex);

        for (Screen* screen : m_screens)
        {
            screen->draw(m_spriteBatch, m_currentPlayerGame);
        }
    }

    /** --------- Debug --------- **/
    #if DEBUG
    {
        m_spriteBatch->drawString(UnicodeString("[UP   ] Pressed: ") + (m_debugData->cachedInput.isUpPressed ? "YES" : "NO ") + " | Down: " +
            (m_debugData->cachedInput.isUpDown ? "YES" : "NO"), Font::Normal, FontColor::Black, 10, 10);
        m_spriteBatch->drawString(UnicodeString("[DOWN ] Pressed: ") + (m_debugData->cachedInput.isDownPressed ? "YES" : "NO ") + " | Down: " +
            (m_debugData->cachedInput.isDownDown ? "YES" : "NO"), Font::Normal, FontColor::Black, 10, 26);
        m_spriteBatch->drawString(UnicodeString("[LEFT ] Pressed: ") + (m_debugData->cachedInput.isLeftPressed ? "YES" : "NO ") + " | Down: " +
            (m_debugData->cachedInput.isLeftDown ? "YES" : "NO"), Font::Normal, FontColor::Black, 10, 42);
        m_spriteBatch->drawString(UnicodeString("[RIGHT] Pressed: ") + (m_debugData->cachedInput.isRightPressed ? "YES" : "NO ") + " | Down: " +
            (m_debugData->cachedInput.isRightDown ? "YES" : "NO"), Font::Normal, FontColor::Black, 10, 58);
    }
    #endif

    /** --------- SpriteBatch --------- **/
    SDL_Rect destRect;
    SDL_Rect srcRect;
    SpriteBatch::BatchNode* currentNode(m_spriteBatch->m_head);
    SDL_Texture* currentTexture;
    while (currentNode != nullptr)
    {
        if (m_textures.count(currentNode->textureId) == 0)
        {
            BinaryReaderPtr textureReader(DataManager::openFileReader(FileType::Texture, currentNode->textureId));
            m_textures.insert(std::pair<uint32_t, Texture*>(currentNode->textureId,
                textureReader->readTexture(m_renderer)));
            DEVEL_LOGF("[Reading texture %d]\n", currentNode->textureId);
        }

        convertToSdlRect(currentNode->sourceRect, srcRect);
        convertToSdlRect(currentNode->destRect, destRect);
        currentTexture = m_textures[currentNode->textureId]->getSdlTexture();
        SDL_SetTextureColorMod(currentTexture, currentNode->tinting.r, currentNode->tinting.g, currentNode->tinting.b);
        SDL_RenderCopy(m_renderer, m_textures[currentNode->textureId]->getSdlTexture(), &srcRect,
            &destRect);
        currentNode = currentNode->next;
    }

    SDL_Texture* tempRenderTextTexture; // What an awful variable name, but you propose a better one!
    for (SpriteBatch::SurfaceRender render : m_spriteBatch->m_surfaces)
    {
        convertToSdlRect(render.destRect, destRect);
        tempRenderTextTexture = SDL_CreateTextureFromSurface(m_renderer, render.surface);
        SDL_RenderCopy(m_renderer, tempRenderTextTexture, NULL, &destRect);
        SDL_DestroyTexture(tempRenderTextTexture);
        SDL_FreeSurface(render.surface);
    }

    SDL_RenderPresent(m_renderer);
}

void GameWindow::removeScreenFromList(Screen* screen, bool doDeleteScreen)
{
    /** NOTE: THIS FUNCTION MUST ONLY BE CALLED WHEN THE MUTEX IS ALREADY CLAIMED **/

    int existingIndex(-1); // The index of the screen in the vector that's already open
    m_doScreensAllowOverworldDrawing = true;
    for (int index = 0; index < m_screens.size(); ++index)
    {
        if (m_screens[index]->getScreenType() == screen->getScreenType())
        {
            // Proper use of this function will prevent multiple instances of this
            // screen in the vector so that this index is the only time the screen
            // is found.
            existingIndex = index;
        }
        else if (!m_screens[index]->isOverworldDrawn())
        {
            m_doScreensAllowOverworldDrawing = false;
        }
    }

    if (existingIndex >= 0)
    {
        if (doDeleteScreen)
        {
            delete m_screens[existingIndex];
        }

        m_screens.erase(m_screens.begin() + existingIndex);
    }
}

void GameWindow::onInGameMenuScreenClosed(Screen* screen)
{
    m_inputController->requireButtonRelease(InputButton::Start);
}

void GameWindow::onScreenClosed(Screen* screen)
{
    MutexScopeLock lock(m_screensMutex);

    removeScreenFromList(screen, false); // We delete the screen manually

    if (screen->doesTakeInput())
    {
        for (Screen* screen : m_screens)
        {
            if (screen->doesTakeInput())
            {
                // Don't progress further; we have a screen that still requires special input configuration
                delete screen;
                return;
            }
        }

        if (!m_inputController->isInInitialState())
        {
            m_inputController->reset();
        }
    }

    delete screen;
}

const SpriteSheetLibrary& GameWindow::getSpriteSheetLibrary() const
{
    return *m_spriteSheetLibrary;
}

int32_t GameWindow::getWidth()
{
    int32_t width;
    SDL_GetWindowSize(m_window, &width, nullptr);
    return width;
}

int32_t GameWindow::getHeight()
{
    int32_t height;
    SDL_GetWindowSize(m_window, nullptr, &height);
    return height;
}

bool GameWindow::hasFocus()
{
    uint32_t flags(SDL_GetWindowFlags(m_window));
    return (((flags & SDL_WINDOW_INPUT_FOCUS) | (flags & SDL_WINDOW_MOUSE_FOCUS)) > 0);
}

void GameWindow::openScreen(Screen* screen)
{
    if (screen == nullptr)
    {
        return;
    }

    MutexScopeLock lock(m_screensMutex);

    if (m_inputController->isInInitialState() && screen->doesTakeInput())
    {
        m_inputController->setButtonDelayLength(InputButton::Up, 50);
        m_inputController->setButtonDelayLength(InputButton::Down, 50);
        m_inputController->setButtonDelayLength(InputButton::Left, 50);
        m_inputController->setButtonDelayLength(InputButton::Right, 50);
    }

    removeScreenFromList(screen, true); // Delete the existing screen when we remove it

    if (!screen->isOverworldDrawn())
    {
        m_doScreensAllowOverworldDrawing = false;
    }

    screen->registerCloseCallback(FunctionPtr<void, Screen*>::fromMember(this, &GameWindow::onScreenClosed));
    m_screens.push_back(screen);
}

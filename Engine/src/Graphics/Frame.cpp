#include "Graphics/Frame.h"
#include "Data/Cache.h"
#include "Data/BinaryReader.h"

static Cache<Frame> s_cache(FileType::Frame);

FramePtr Frame::get(uint16_t frameNo)
{
    FramePtr pointer(s_cache.get(frameNo));
    if (pointer != nullptr)
    {
        return pointer;
    }

    BinaryReaderPtr reader(DataManager::openFileReader(FileType::Frame, frameNo));
    return s_cache.add(frameNo, new Frame(reader));
}

Frame::Frame(BinaryReaderPtr reader)
{
    reader->readString();
    m_topLeftSpriteNo = reader->readUInt32();
    m_topSpriteNo = reader->readUInt32();
    m_topRightSpriteNo = reader->readUInt32();
    m_leftSpriteNo = reader->readUInt32();
    m_centerSpriteNo = reader->readUInt32();
    m_rightSpriteNo = reader->readUInt32();
    m_bottomLeftSpriteNo = reader->readUInt32();
    m_bottomSpriteNo = reader->readUInt32();
    m_bottomRightSpriteNo = reader->readUInt32();
}

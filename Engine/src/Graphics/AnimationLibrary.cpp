#include "Kristall.hpp"

// Major class functions
AnimationInstancePtr AnimationLibrary::createAnimationInstance(SpriteSheets spriteSheet,
                                                               uint32 animationBaseNo)
{
    if (!SpriteBatch::s_areSpriteSheetsInitialized)
    {
        ExecutionException error("Attempted to use an AnimationLibrary function before SpriteSheets had been loaded.");
        error.addData("function", "AnimationLibrary::createAnimationInstance");
        error.addData("spriteSheet", toString(spriteSheet));
        error.addData("animationBaseNo", toString(animationBaseNo));
        throw error;
    }

    SpriteSheet* spriteSheetPtr = SpriteBatch::getSpriteSheet(spriteSheet);
    if (spriteSheetPtr == nullptr)
    {
        ExecutionException error("Attempted to use an AnimationLibrary function involving a SpriteSheet that isn't loaded.");
        error.addData("function", "AnimationLibrary::createAnimationInstance");
        error.addData("spriteSheet", toString(spriteSheet));
        error.addData("animationBaseNo", toString(animationBaseNo));
        throw error;
    }

    return spriteSheetPtr->getAnimation(animationBaseNo);
}

AnimationResolutionResults AnimationLibrary::lookupAnimationNo(SpriteSheets spriteSheet,
                                           const std::string& animationHandle)
{
    if (!SpriteBatch::s_areSpriteSheetsInitialized)
    {
        ExecutionException error("Attempted to use an AnimationLibrary function before SpriteSheets had been loaded.");
        error.addData("function", "AnimationLibrary::lookupAnimationNo");
        error.addData("spriteSheet", toString(spriteSheet));
        error.addData("animationHandle", animationHandle);
        throw error;
    }

    SpriteSheet* spriteSheetPtr = SpriteBatch::getSpriteSheet(spriteSheet);
    if (spriteSheetPtr == nullptr)
    {
        ExecutionException error("Attempted to use an AnimationLibrary function involving a SpriteSheet that isn't loaded.");
        error.addData("function", "AnimationLibrary::lookupAnimationNo");
        error.addData("spriteSheet", toString(spriteSheet));
        error.addData("animationHandle", animationHandle);
        throw error;
    }

    return spriteSheetPtr->lookupAnimationNo(animationHandle);
}

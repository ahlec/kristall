#include "Graphics/DeveloperColorSwatch.h"
#include "Graphics/Color.h"

DeveloperColorSwatch::DeveloperColorSwatch(int offset) : _offset(offset)
{
}

Color DeveloperColorSwatch::getColor(int number) const
{
    switch ((number + _offset) % 9)
    {
        case 0:
            {
                return Color(0, 255, 0);
            }
        case 1:
            {
                return Color(255, 0, 0);
            }
        case 2:
            {
                return Color(0, 0, 255);
            }
        case 3:
            {
                return Color(255, 255, 0); // Yellow
            }
        case 4:
            {
                return Color(255, 0, 255); // Magenta
            }
        case 5:
            {
                return Color(0, 255, 255); // Cyan
            }
        case 6:
            {
                return Color(192, 192, 192); // Grey
            }
        case 7:
            {
                return Color(255, 140, 0); // Dark Orange
            }
        case 8:
            {
                return Color(255, 20, 147); // Deep Pink
            }
    }

    return Color(0, 0, 0);
}

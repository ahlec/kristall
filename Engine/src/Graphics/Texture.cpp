#include "Graphics/Texture.h"

Texture::Texture(SDL_Texture* texture) : _texture(texture)
{
}

Texture::~Texture()
{
    if (_texture != nullptr)
    {
        SDL_DestroyTexture(_texture);
        _texture = nullptr;
    }
}

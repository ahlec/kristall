#include "Graphics/Color.h"

const Color Color::White(255, 255, 255, 255);
const Color Color::Black(0, 0, 0, 255);
const Color Color::Red(255, 0, 0, 255);
const Color Color::Green(0, 255, 0, 255);
const Color Color::Blue(0, 0, 255, 255);
const Color Color::Yellow(255, 255, 0, 255);
const Color Color::Magenta(255, 0, 255, 255);
const Color Color::Cyan(0, 255, 255, 255);
const Color Color::Gray(192, 192, 192, 255);
const Color Color::Orange(255, 140, 0, 255);
const Color Color::Pink(255, 20, 147, 255);

Color::Color() : r(0), g(0), b(0), a(255)
{
}

Color::Color(uint8_t pR, uint8_t pG, uint8_t pB, uint8_t pA) : r(pR), g(pG), b(pB), a(pA)
{
}

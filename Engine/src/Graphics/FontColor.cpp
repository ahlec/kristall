#include "Graphics/FontColor.h"
#include "Graphics/Color.h"
#include "Errors/ExecutionException.h"

namespace FontColors
{
    Color getTextColor(FontColor color)
    {
        if (color == FontColor::Black)
        {
            return Color(133, 133, 131);
        }
        else if (color == FontColor::White)
        {
            return Color::White;
        }
        else if (color == FontColor::Yellow)
        {
            return Color::Yellow;
        }

        DEVEL_NOT_IMPLEMENTED;
    }

    Color getShadowColor(FontColor color)
    {
        if (color == FontColor::Black)
        {
            return Color(218, 218, 215);
        }
        else if (color == FontColor::White ||
            color == FontColor::Yellow)
        {
            return Color::Black;
        }

        DEVEL_NOT_IMPLEMENTED;
    }
};

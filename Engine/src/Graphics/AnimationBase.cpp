#include "Graphics/AnimationBase.h"
#include "Data/BinaryReader.h"
#include "Graphics/AnimationInformation.h"
#include "Graphics/AnimationInstance.h"

// Major class functions
AnimationBase::AnimationBase(SpriteSheet spriteSheet, uint32_t animationBaseNo,
                             uint32_t nAnimations, BinaryReaderPtr reader) :
                                 _spriteSheet(spriteSheet),
                                 _animationBaseNo(animationBaseNo)
{
    _animations.reserve(nAnimations);
    for (uint32_t index = 0; index < nAnimations; ++index)
    {
        if (!reader->readBool())
        {
            // This animation is not defined for this AnimationBase
            _animations.push_back(nullptr);
            continue;
        }

        _animations.push_back(AnimationInformationPtr(new AnimationInformation(index, reader)));
    }

    _initialAnimationNo = reader->readUInt32();
}

AnimationInstancePtr AnimationBase::createInstance() const
{
    return AnimationInstancePtr(new AnimationInstance(*this, _initialAnimationNo));
}

bool AnimationBase::hasAnimation(const std::string& animationHandle) const
{
    DEVEL_NOT_TRANSLATED;
}

bool AnimationBase::hasAnimation(uint32_t animationNo) const
{
    if (animationNo >= _animations.size())
    {
        return false;
    }

    return (_animations[animationNo] != nullptr);
}

AnimationInformationPtr AnimationBase::getAnimation(uint32_t animationNo) const
{
    if (!hasAnimation(animationNo))
    {
        ExecutionException error("The requested animation does not exist!");
        error.addData("animationNo", animationNo);
        error.addData("animationBaseNo", _animationBaseNo);
        error.addData("spriteSheet", _spriteSheet);
        throw error;
    }

    return _animations[animationNo];
}

// Member access functions (nothing too special beyond this point)
SpriteSheet AnimationBase::getSpriteSheet() const
{
    return _spriteSheet;
}

uint32_t AnimationBase::getAnimationBaseNo() const
{
    return _animationBaseNo;
}

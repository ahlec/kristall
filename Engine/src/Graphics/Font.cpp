#include "Graphics/Font.h"
#include "Errors/ExecutionException.h"

static TTF_Font* s_font = nullptr;

namespace Fonts
{
    TTF_Font* getTtfFont(Font font)
    {
        if (font == Font::Normal)
        {
            if (s_font == nullptr)
            {
                s_font = TTF_OpenFont("dbg.ttf", 16);
            }

            return s_font;
        }

        DEVEL_NOT_IMPLEMENTED;
    }
};

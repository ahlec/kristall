#include "Graphics/Animations.h"
#include "Errors/ExecutionException.h"

namespace Animations
{
    namespace Characters
    {
        uint32_t getIdleAnimation(TransportMode transportMode, Direction direction)
        {
            switch (transportMode)
            {
                case TransportMode::Walk:
                case TransportMode::Run:
                    {
                        switch (direction)
                        {
                            case Direction::North:
                                {
                                    return NORTH;
                                }
                            case Direction::East:
                                {
                                    return EAST;
                                }
                            case Direction::South:
                                {
                                    return SOUTH;
                                }
                            case Direction::West:
                                {
                                    return WEST;
                                }
                        }

                        break;
                    }
                case TransportMode::Bike:
                    {
                        switch (direction)
                        {
                            case Direction::North:
                                {
                                    return BIKE_NORTH_IDLE;
                                }
                            case Direction::East:
                                {
                                    return BIKE_EAST_IDLE;
                                }
                            case Direction::South:
                                {
                                    return BIKE_SOUTH_IDLE;
                                }
                            case Direction::West:
                                {
                                    return BIKE_WEST_IDLE;
                                }
                        }

                        break;
                    }
                case TransportMode::Surf:
                    {
                        switch (direction)
                        {
                            case Direction::North:
                                {
                                    return SURF_NORTH;
                                }
                            case Direction::East:
                                {
                                    return SURF_EAST;
                                }
                            case Direction::South:
                                {
                                    return SURF_SOUTH;
                                }
                            case Direction::West:
                                {
                                    return SURF_WEST;
                                }
                        }

                        break;
                    }
            }

            ExecutionException error("Could not locate the idle animation for the direction-transport mode combination.");
            error.addData("transportMode", toString(transportMode));
            error.addData("direction", toString(direction));
            throw error;
        }

        uint32_t getMovementAnimation(TransportMode transportMode, Direction direction,
                                    bool isLeadingWithLeftLeg)
        {
            switch (transportMode)
            {
                case TransportMode::Walk:
                    {
                        switch (direction)
                        {
                            case Direction::North:
                                {
                                    return (isLeadingWithLeftLeg ? WALK_NORTH_LEFT_LEG : WALK_NORTH_RIGHT_LEG);
                                }
                            case Direction::East:
                                {
                                    return (isLeadingWithLeftLeg ? WALK_EAST_LEFT_LEG : WALK_EAST_RIGHT_LEG);
                                }
                            case Direction::South:
                                {
                                    return (isLeadingWithLeftLeg ? WALK_SOUTH_LEFT_LEG : WALK_SOUTH_RIGHT_LEG);
                                }
                            case Direction::West:
                                {
                                    return (isLeadingWithLeftLeg ? WALK_WEST_LEFT_LEG : WALK_WEST_RIGHT_LEG);
                                }
                        }

                        break;
                    }
                case TransportMode::Run:
                    {
                        switch (direction)
                        {
                            case Direction::North:
                                {
                                    return (isLeadingWithLeftLeg ? RUN_NORTH_LEFT_LEG : RUN_NORTH_RIGHT_LEG);
                                }
                            case Direction::East:
                                {
                                    return (isLeadingWithLeftLeg ? RUN_EAST_LEFT_LEG : RUN_EAST_RIGHT_LEG);
                                }
                            case Direction::South:
                                {
                                    return (isLeadingWithLeftLeg ? RUN_SOUTH_LEFT_LEG : RUN_SOUTH_RIGHT_LEG);
                                }
                            case Direction::West:
                                {
                                    return (isLeadingWithLeftLeg ? RUN_WEST_LEFT_LEG : RUN_WEST_RIGHT_LEG);
                                }
                        }

                        break;
                    }

                case TransportMode::Bike:
                    {
                        switch (direction)
                        {
                            case Direction::North:
                                {
                                    return (isLeadingWithLeftLeg ? BIKE_NORTH_LEFT_LEG : BIKE_NORTH_RIGHT_LEG);
                                }
                            case Direction::East:
                                {
                                    return (isLeadingWithLeftLeg ? BIKE_EAST_LEFT_LEG : BIKE_EAST_RIGHT_LEG);
                                }
                            case Direction::South:
                                {
                                    return (isLeadingWithLeftLeg ? BIKE_SOUTH_LEFT_LEG: BIKE_SOUTH_RIGHT_LEG);
                                }
                            case Direction::West:
                                {
                                    return (isLeadingWithLeftLeg ? BIKE_WEST_LEFT_LEG : BIKE_WEST_RIGHT_LEG);
                                }
                        }

                        break;
                    }

                case TransportMode::Surf:
                    {
                        switch (direction)
                        {
                            case Direction::North:
                                {
                                    return SURF_NORTH;
                                }
                            case Direction::East:
                                {
                                    return SURF_EAST;
                                }
                            case Direction::South:
                                {
                                    return SURF_SOUTH;
                                }
                            case Direction::West:
                                {
                                    return SURF_WEST;
                                }
                        }

                        break;
                    }
            }

            ExecutionException error("Could not determine the appropriate character movement animation number.");
            error.addData("transportMode", toString(transportMode));
            error.addData("direction", toString(direction));
            error.addData("isLeadingWithLeftLeg", toString(isLeadingWithLeftLeg));
            throw error;
        }
    };

};

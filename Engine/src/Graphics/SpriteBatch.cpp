#include "Graphics/SpriteBatch.h"
#include "Graphics/Sprite.h"
#include "Graphics/SpriteSheetLibrary.h"
#include "Graphics/Frame.h"
#include "Graphics/UserInterfaceSprites.h"
#include <iostream>

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
#define SDL_RED_MASK    (0xff000000)
#define SDL_GREEN_MASK  (0x00ff0000)
#define SDL_BLUE_MASK   (0x0000ff00)
#define SDL_ALPHA_MASK  (0x000000ff)
#else
#define SDL_RED_MASK    (0x000000ff)
#define SDL_GREEN_MASK  (0x0000ff00)
#define SDL_BLUE_MASK   (0x00ff0000)
#define SDL_ALPHA_MASK  (0xff000000)
#endif
#define SDL_RGB_WITH_ALPA_DEPTH  (32)
const size_t INITIAL_NODEPOOL_SIZE = 4000;

// Drawing functionality
void SpriteBatch::drawTexture(uint32_t textureNo, int screenX, int screenY, int width, int height, int textureX,
                              int textureY, const Color& tint, uint16_t layer)
{
    addBatch(textureNo, Rect(textureX, textureY, width, height), Rect(screenX, screenY, width, height),
        tint, layer);
}

void SpriteBatch::drawSprite(SpriteSheet spriteSheet, uint32_t spriteNo, int x, int y,
    LocationAnchor anchor, const Color& tint, uint16_t layer)
{
    const Sprite& sprite(m_spriteSheetLibrary->getSprite(spriteSheet, spriteNo));
    Rect destRect;
    if (anchor == LocationAnchor::BottomRight)
    {
        destRect = Rect(x + sprite.ingrainedOffsetX - sprite.width, y + sprite.ingrainedOffsetY - sprite.height,
            sprite.width, sprite.height);
    }
    else
    {
        destRect = Rect(x + sprite.ingrainedOffsetX,y + sprite.ingrainedOffsetY, sprite.width, sprite.height);
    }

    addBatch(m_spriteSheetLibrary->getTextureId(spriteSheet), Rect(sprite.topLeftCoords.x, sprite.topLeftCoords.y,
        sprite.width, sprite.height), destRect, tint, layer);
}

void SpriteBatch::tileSprite(SpriteSheet spriteSheet, uint32_t spriteNo, int x, int y, int tileWidth, int tileHeight,
        LocationAnchor anchor, const Color& tint, uint16_t layer)
{
    /**
     * NOTE: This function is not yet coded to take into account IngrainedOffsets of sprites. If this becomes
     * something that proves necessary then it can be added, but for now it would be needless complication since
     * frames do not likely require ingrained offsets.
     **/

    const Sprite& sprite(m_spriteSheetLibrary->getSprite(spriteSheet, spriteNo));
    Rect destRect;
    destRect.width = (tileWidth < sprite.width ? tileWidth : sprite.width);
    destRect.height = (tileHeight < sprite.height ? tileHeight : sprite.height);

    int iteratorX;
    int iteratorY = y;
    int endX(x + tileWidth);
    int endY(y + tileHeight);
    uint32_t textureId(m_spriteSheetLibrary->getTextureId(spriteSheet));
    Rect srcRect(sprite.topLeftCoords.x, sprite.topLeftCoords.y, 0, 0); // temp width, height
    while (iteratorY < endY)
    {
        iteratorX = x;
        destRect.height = (iteratorY + sprite.height >= endY ? endY - iteratorY : sprite.height);
        srcRect.height = destRect.height;

        while (iteratorX < endX)
        {
            destRect.x = iteratorX;
            destRect.y = iteratorY;
            destRect.width = (iteratorX + sprite.width >= endX ? endX - iteratorX : sprite.width);
            srcRect.width = destRect.width;

            addBatch(textureId, srcRect, destRect, tint, layer);

            if (iteratorX >= endX)
            {
                break;
            }
            else
            {
                iteratorX += sprite.width;
            }
        }

        iteratorY += sprite.height;
    }
}

void SpriteBatch::drawLine(int x1, int y1, int x2, int y2, const Color& tint)
{
    const Sprite& singlePixel(m_spriteSheetLibrary->getSprite(SpriteSheet::GeneralUserInterface, UserInterfaceSprites::SINGLE_PIXEL));
    uint32_t spriteSheet(m_spriteSheetLibrary->getTextureId(SpriteSheet::GeneralUserInterface));
    Rect sourceRectangle(singlePixel.topLeftCoords.x, singlePixel.topLeftCoords.y, 1, 1); // It's just a single pixel
    if (x1 == x2)
    {
        addBatch(spriteSheet, sourceRectangle, Rect(x1, y1, 1, y2 - y1), tint, 100);
    }
    else if (y1 == y2)
    {
        addBatch(spriteSheet, sourceRectangle, Rect(x1, y1, x2 - x1, 1), tint, 100);
    }
    else
    {
        throw ExecutionException("Cannot currently support diagonal lines, because we're not really going to be using this function much anyways.");
    }
}

void SpriteBatch::drawRectangle(Rect rectangle, const Color& tint)
{
    const Sprite& singlePixel(m_spriteSheetLibrary->getSprite(SpriteSheet::GeneralUserInterface, UserInterfaceSprites::SINGLE_PIXEL));
    uint32_t spriteSheet(m_spriteSheetLibrary->getTextureId(SpriteSheet::GeneralUserInterface));
    Rect sourceRectangle(singlePixel.topLeftCoords.x, singlePixel.topLeftCoords.y, 1, 1); // It's just a single pixel
    int right(rectangle.x + rectangle.width);
    int bottom(rectangle.y + rectangle.height);
    addBatch(spriteSheet, sourceRectangle, Rect(rectangle.x, rectangle.y, rectangle.width, 1), tint, 100); // top
    addBatch(spriteSheet, sourceRectangle, Rect(right - 1, rectangle.y, 1, rectangle.height), tint, 100); // right
    addBatch(spriteSheet, sourceRectangle, Rect(rectangle.x, bottom - 1, rectangle.width, 1), tint, 100); // bottom
    addBatch(spriteSheet, sourceRectangle, Rect(rectangle.x, rectangle.y, 1, rectangle.height), tint, 100); // left
}

void SpriteBatch::drawString(UnicodeString text, Font font, FontColor color, int x, int y, uint8_t alpha)
{
    Color tempFontColor = FontColors::getTextColor(color);
    const uint16_t* renderText = reinterpret_cast<const uint16_t*>(text.getTerminatedBuffer());
    SDL_Surface* regularText = TTF_RenderUNICODE_Solid(Fonts::getTtfFont(font), renderText,
        { tempFontColor.r, tempFontColor.g, tempFontColor.b, tempFontColor.a });
    tempFontColor = FontColors::getShadowColor(color);
    SDL_Surface* shadowText = TTF_RenderUNICODE_Solid(Fonts::getTtfFont(font), renderText,
        { tempFontColor.r, tempFontColor.g, tempFontColor.b, tempFontColor.a });
    SDL_Rect rectangle;
    SDL_GetClipRect(regularText, &rectangle);
    SDL_Surface* combined = SDL_CreateRGBSurface(0, rectangle.w + 1, rectangle.h + 1, SDL_RGB_WITH_ALPA_DEPTH, SDL_RED_MASK,
        SDL_GREEN_MASK, SDL_BLUE_MASK, SDL_ALPHA_MASK);

    // The shadow is composed of three different shadow applications to the same surface to create a proper bevel look
    rectangle.x = 1;
    rectangle.y = 1;
    SDL_BlitSurface(shadowText, NULL, combined, &rectangle);
    rectangle.x = 0;
    SDL_BlitSurface(shadowText, NULL, combined, &rectangle);
    rectangle.y = 0;
    rectangle.x = 1;
    //SDL_BlitSurface(shadowText, NULL, combined, &rectangle);
    SDL_BlitSurface(regularText, NULL, combined, NULL);
    SDL_FreeSurface(regularText);
    SDL_FreeSurface(shadowText);

    SurfaceRender newRender;
    newRender.surface = combined;
    newRender.destRect = Rect(x, y, rectangle.w + 1, rectangle.h + 1);
    m_surfaces.push_back(newRender);
}

void SpriteBatch::drawFrame(FramePtr frame, Rect rectangle, const Color& tint)
{
    /**
     * NOTE: This function is not yet coded to take into account IngrainedOffsets of sprites. If this becomes
     * something that proves necessary then it can be added, but for now it would be needless complication since
     * frames do not likely require ingrained offsets.
     **/

    if (frame == nullptr)
    {
        return;
    }

    Rect destRectangle;
    uint32_t textureId(m_spriteSheetLibrary->getTextureId(SpriteSheet::GeneralUserInterface));
    const uint16_t layer(100);
    uint16_t leftSpritesWidth;
    uint16_t rightSpritesWidth;
    uint16_t topSpritesHeight;
    uint16_t bottomSpritesHeight;

    // Top-left corner
    {
        const Sprite& sprite(m_spriteSheetLibrary->getSprite(SpriteSheet::GeneralUserInterface, frame->m_topLeftSpriteNo));
        destRectangle.x = rectangle.x;
        destRectangle.y = rectangle.y;
        destRectangle.width = sprite.width;
        destRectangle.height = sprite.height;
        addBatch(textureId, Rect(sprite.topLeftCoords.x, sprite.topLeftCoords.y, sprite.width, sprite.height),
            destRectangle, tint, layer);
        leftSpritesWidth = sprite.width;
        topSpritesHeight = sprite.height;
    }

    // Top-right corner
    {
        const Sprite& sprite(m_spriteSheetLibrary->getSprite(SpriteSheet::GeneralUserInterface, frame->m_topRightSpriteNo));
        destRectangle.x = rectangle.x + rectangle.width - sprite.width;
        destRectangle.y = rectangle.y;
        destRectangle.width = sprite.width;
        destRectangle.height = sprite.height;
        addBatch(textureId, Rect(sprite.topLeftCoords.x, sprite.topLeftCoords.y, sprite.width, sprite.height),
            destRectangle, tint, layer);
        rightSpritesWidth = sprite.width;
    }

    // Bottom-left corner
    {
        const Sprite& sprite(m_spriteSheetLibrary->getSprite(SpriteSheet::GeneralUserInterface, frame->m_bottomLeftSpriteNo));
        destRectangle.x = rectangle.x;
        destRectangle.y = rectangle.y + rectangle.height - sprite.height;
        destRectangle.width = sprite.width;
        destRectangle.height = sprite.height;
        addBatch(textureId, Rect(sprite.topLeftCoords.x, sprite.topLeftCoords.y, sprite.width, sprite.height),
            destRectangle, tint, layer);
        bottomSpritesHeight = sprite.height;
    }

    // Bottom-right corner
    {
        const Sprite& sprite(m_spriteSheetLibrary->getSprite(SpriteSheet::GeneralUserInterface, frame->m_bottomRightSpriteNo));
        destRectangle.x = rectangle.x + rectangle.width - sprite.width;
        destRectangle.y = rectangle.y + rectangle.height - sprite.height;
        destRectangle.width = sprite.width;
        destRectangle.height = sprite.height;
        addBatch(textureId, Rect(sprite.topLeftCoords.x, sprite.topLeftCoords.y, sprite.width, sprite.height),
            destRectangle, tint, layer);
    }

    // Left
    tileSprite(SpriteSheet::GeneralUserInterface, frame->m_leftSpriteNo, rectangle.x, rectangle.y + topSpritesHeight,
        leftSpritesWidth, rectangle.height - topSpritesHeight - bottomSpritesHeight, LocationAnchor::TopLeft, tint);

    // Top
    tileSprite(SpriteSheet::GeneralUserInterface, frame->m_topSpriteNo, rectangle.x + leftSpritesWidth, rectangle.y,
        rectangle.width - leftSpritesWidth - rightSpritesWidth, topSpritesHeight, LocationAnchor::TopLeft, tint);

    // Right
    tileSprite(SpriteSheet::GeneralUserInterface, frame->m_rightSpriteNo, rectangle.x + rectangle.width - rightSpritesWidth,
        rectangle.y + topSpritesHeight, rightSpritesWidth, rectangle.height - topSpritesHeight - bottomSpritesHeight,
        LocationAnchor::TopLeft, tint);

    // Bottom
    tileSprite(SpriteSheet::GeneralUserInterface, frame->m_bottomSpriteNo, rectangle.x + leftSpritesWidth,
        rectangle.y + rectangle.height - bottomSpritesHeight, rectangle.width - leftSpritesWidth - rightSpritesWidth,
        bottomSpritesHeight, LocationAnchor::TopLeft, tint);

    // Center
    {
        const Sprite& sprite(m_spriteSheetLibrary->getSprite(SpriteSheet::GeneralUserInterface, frame->m_centerSpriteNo));
        destRectangle.x = rectangle.x + leftSpritesWidth;
        destRectangle.y = rectangle.y + topSpritesHeight;
        destRectangle.width = rectangle.width - leftSpritesWidth - rightSpritesWidth;
        destRectangle.height = rectangle.height - topSpritesHeight - bottomSpritesHeight;
        addBatch(textureId, Rect(sprite.topLeftCoords.x, sprite.topLeftCoords.y, sprite.width, sprite.height),
            destRectangle, tint, layer);
    }
}

// Major class functions
SpriteBatch::SpriteBatch(SpriteSheetLibraryPtr spriteSheetLibrary) : m_spriteSheetLibrary(spriteSheetLibrary)
{
    m_nodePool.reserve(INITIAL_NODEPOOL_SIZE);
}

// Batching functionality
void SpriteBatch::reset()
{
    m_nodePoolNextIndex = 0;
    m_head = nullptr;
    m_tail = nullptr;
    m_layerLookup.clear();
    m_surfaces.clear();
}

void SpriteBatch::addBatch(uint32_t textureId, const Rect& sourceRect, const Rect& destRect,
    const Color& tinting, uint16_t layer)
{
    BatchNode* newBatchPointer(&m_nodePool[m_nodePoolNextIndex++]);
    newBatchPointer->textureId = textureId;
    newBatchPointer->sourceRect = sourceRect;
    newBatchPointer->destRect = destRect;
    newBatchPointer->tinting = tinting;

    if (m_layerLookup.count(layer) == 1)
    {
        // There is a node previously on this layer, so that means we want to add the batch
        // after this node, and then update the subsequent lookup for that layer
        newBatchPointer->previous = m_layerLookup[layer];
        newBatchPointer->next = newBatchPointer->previous->next;
        if (newBatchPointer->next == nullptr)
        {
            m_tail = newBatchPointer;
        }
        else
        {
            newBatchPointer->next->previous = newBatchPointer;
        }
        newBatchPointer->previous->next = newBatchPointer;
        m_layerLookup[layer] = newBatchPointer;
    }
    else
    {
        // There are no batches on this layer, so we need to locate the nearest layer
        // defined graphically underneath the target layer, and start our new layer
        // after that.
        uint16_t previousLayerValue;
        bool hasFoundPreviousLayerValue(false); // all numbers must be valid layers!
        for (auto& layerLookup : m_layerLookup)
        {
            if (layerLookup.first > layer)
            {
                continue;
            }

            if (!hasFoundPreviousLayerValue || previousLayerValue < layerLookup.first)
            {
                hasFoundPreviousLayerValue = true;
                previousLayerValue = layerLookup.first;
            }
        }

        // Create the new layer according to the results of the layer scan
        if (hasFoundPreviousLayerValue)
        {
            newBatchPointer->previous = m_layerLookup[previousLayerValue];
            newBatchPointer->next = m_layerLookup[previousLayerValue]->next;
            if (newBatchPointer->next == nullptr)
            {
                m_tail = newBatchPointer;
            }
            else
            {
                newBatchPointer->next->previous = newBatchPointer;
            }
            newBatchPointer->previous->next = newBatchPointer;
        }
        else if (m_tail != nullptr)
        {
            m_tail->next = newBatchPointer;
            newBatchPointer->previous = m_tail;
            newBatchPointer->next = nullptr;
            m_tail = newBatchPointer;
        }
        else
        {
            newBatchPointer->previous = nullptr;
            newBatchPointer->next = nullptr;
            m_head = newBatchPointer;
            m_tail = newBatchPointer;
        }
        m_layerLookup.insert(std::pair<uint16_t, BatchNode*>(layer, newBatchPointer));
    }
}

#include "Graphics/SpriteSheetLibrary.h"
#include "Data/DataManager.h"
#include "Data/BinaryReader.h"

// SpriteSheetLibrary
SpriteSheetLibrary::SpriteSheetLibrary() : _nSpriteSheets(0), _spriteSheets(nullptr)
{
    _nSpriteSheets = DataManager::getNumberFiles(FileType::SpriteSheet);
    _spriteSheets = new InternalSpriteSheet[_nSpriteSheets];
    for (uint8_t spriteSheetNo = 0; spriteSheetNo < _nSpriteSheets; ++spriteSheetNo)
    {
        BinaryReaderPtr reader(DataManager::openFileReader(FileType::SpriteSheet, spriteSheetNo));
        _spriteSheets[spriteSheetNo].load(static_cast<SpriteSheet>(spriteSheetNo),
                                          reader);
    }
}

SpriteSheetLibrary::~SpriteSheetLibrary()
{
    if (_spriteSheets != nullptr)
    {
        delete[] _spriteSheets;
        _spriteSheets = nullptr;
    }
}

void SpriteSheetLibrary::update(uint32_t elapsedTime)
{
    // Update the universal animations
    for (size_t index = 0; index < _nSpriteSheets; ++index)
    {
        InternalSpriteSheet& spriteSheet(_spriteSheets[index]);
        for (UniversalAnimation& universalAnimation : spriteSheet.universalAnimations)
        {
            if (universalAnimation.update(elapsedTime))
            {
                spriteSheet.sprites[universalAnimation._spriteNo] =
                    spriteSheet.sprites[universalAnimation.getCurrentSpriteNo()];
            }
        }
    }
}

AnimationInstancePtr SpriteSheetLibrary::createAnimationInstance(SpriteSheet spriteSheet,
                                                                 uint32_t animationBaseNo) const
{
    return _spriteSheets[static_cast<uint8_t>(spriteSheet)].animationBases[animationBaseNo].createInstance();
}

AnimationResolutionResults SpriteSheetLibrary::lookupAnimationNo(SpriteSheet spriteSheet,
                                                                 const std::string& animationHandle) const
{
    AnimationResolutionResults results;
    uint32_t index = 0;
    for (const std::string& animationName : _spriteSheets[static_cast<uint8_t>(spriteSheet)].animationNames)
    {
        if (animationName.compare(animationHandle) == 0)
        {
            results.doesNameExist = true;
            results.animationNo = index;
            return results;
        }
        ++index;
    }
}

uint32_t SpriteSheetLibrary::getTextureId(SpriteSheet spriteSheet) const
{
    return _spriteSheets[static_cast<uint8_t>(spriteSheet)].textureNo;
}

const Sprite& SpriteSheetLibrary::getSprite(SpriteSheet spriteSheet, uint32_t spriteNo) const
{
    return _spriteSheets[static_cast<uint8_t>(spriteSheet)].sprites[spriteNo];
}



// SpriteSheetLibrary::InternalSpriteSheet
SpriteSheetLibrary::InternalSpriteSheet::InternalSpriteSheet()
{
}

SpriteSheetLibrary::InternalSpriteSheet::~InternalSpriteSheet()
{
}

void SpriteSheetLibrary::InternalSpriteSheet::load(SpriteSheet name, BinaryReaderPtr& reader)
{
    // Load the texture
    //reader->readTexture(texture);
    textureNo = reader->readUInt32();

    // Load the Sprites
    uint32_t nSprites = reader->readUInt32();
    sprites.reserve(nSprites);
    for (size_t index = 0; index < nSprites; ++index)
    {
        sprites.push_back(Sprite(reader));
    }

    // Load the Universal Animations
    uint32_t nUniversalAnimations = reader->readUInt32();
    universalAnimations.reserve(nUniversalAnimations);
    for (size_t index = 0; index < nUniversalAnimations; ++index)
    {
        universalAnimations.push_back(UniversalAnimation(reader));
    }

    // Load the Animation Names
    uint32_t nAnimationNames = reader->readUInt32();
    animationNames.reserve(nAnimationNames);
    for (size_t index = 0; index < nAnimationNames; ++index)
    {
        animationNames.push_back(reader->readStandardString());
    }

    // Load the Animation Bases
    uint32_t nAnimationInstances = reader->readUInt32();
    animationBases.reserve(nAnimationInstances);
    for (size_t index = 0; index < nAnimationInstances; ++index)
    {
        animationBases.push_back(AnimationBase(name, index, nAnimationNames, reader));
    }
}

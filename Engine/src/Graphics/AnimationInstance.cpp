#include "Graphics/AnimationInstance.h"
#include "Graphics/AnimationBase.h"
#include "Graphics/AnimationInformation.h"
#include "Errors/ExecutionException.h"

// Major class functions
AnimationInstance::AnimationInstance(const AnimationBase& base,
                                     uint32_t initialAnimation) :
                                         _animationBase(base),
                                         _currentAnimation(nullptr),
                                         _elapsedTime(0),
                                         _currentSpriteNo(0),
                                         _currentHasOverlay(false),
                                         _currentOverlaySpriteNo(0),
                                         _animationFinished(nullptr),
                                         _immediatelyPlayAnimation(nullptr)
{
    beginAnimation(initialAnimation, nullptr, 1.0f);
}

bool AnimationInstance::update(uint32_t elapsedTime)
{
    static bool didAnimationEnd = false;
    static bool beganNewAnimation = false;
    static uint32_t lastTime = 0;
    _isNewKeyframe = false;

    if (_immediatelyPlayAnimation != nullptr)
    {
        if (_animationFinished != nullptr)
        {
            _animationFinished();
        }

        beginAnimation(_immediatelyPlayAnimation->animationNo,
                       _immediatelyPlayAnimation->animationFinished,
                       _immediatelyPlayAnimation->playSpeed);
        _immediatelyPlayAnimation = nullptr;
        beganNewAnimation = true;
        didAnimationEnd = true;
    }
    else if (_currentAnimation == nullptr && !_playQueue.empty())
    {
        playNextAnimationEnqueued();
        beganNewAnimation = true;
        didAnimationEnd = false;
    }
    else
    {
        beganNewAnimation = false;
        didAnimationEnd = false;
    }

    if (_currentAnimation == nullptr)
    {
        return false;
    }

    AnimationFramePtr frame;
    if (!beganNewAnimation)
    {
        lastTime = _elapsedTime;
        _elapsedTime += (_isPlayingNormalSpeed ? elapsedTime : static_cast<uint16_t>(elapsedTime * _playSpeed));
        frame = _currentAnimation->getFrame(_elapsedTime, lastTime);
    }
    else
    {
        frame = _currentAnimation->getFrame(_elapsedTime, 0);
    }

    if (frame == nullptr)
    {
        didAnimationEnd = true;
        if (_animationFinished != nullptr)
        {
            _animationFinished();
        }

        if (!_currentAnimation->getIsLooping())
        {
            if (_playQueue.size() > 0)
            {
                playNextAnimationEnqueued();
                frame = _currentAnimation->getFrame(_elapsedTime, 0);
            }
            else if (!_currentAnimation->getIsDeadEnd())
            {
                beginAnimation(_currentAnimation->getReturnsToAnimationNo(), nullptr, 1.0f);
                frame = _currentAnimation->getFrame(_elapsedTime, 0);
            }
            else
            {
                _currentAnimation = nullptr;
                return true;
            }
        }
        else
        {
            _elapsedTime = 0;
            frame = _currentAnimation->getFrame(_elapsedTime, 0);
        }
    }

    if (frame->isFrameChanged)
    {
        _isNewKeyframe = true;
        _currentSpriteNo = frame->spriteNo;
        //_currentHasOverlay = frame->hasOverlay;
        _currentOverlaySpriteNo = frame->overlayNo;
    }

    return didAnimationEnd;
}

void AnimationInstance::beginAnimation(uint32_t animationNo,
                                       AnimationFinishedDelegate animationFinished,
                                       float playSpeed)
{
    _currentAnimation = _animationBase.getAnimation(animationNo);
    _animationFinished = animationFinished;
    _elapsedTime = 0;
    _isPlayingNormalSpeed = (playSpeed == 1.0f);
    _playSpeed = playSpeed;

    AnimationFramePtr frame = _currentAnimation->getFrame(_elapsedTime, 0, true);
    _currentSpriteNo = frame->spriteNo;
    //_currentHasOverlay = frame->hasOverlay;
    _currentOverlaySpriteNo = frame->overlayNo;
}

void AnimationInstance::playNextAnimationEnqueued()
{
    AnimationQueueNode& node = _playQueue.front();
    beginAnimation(node.animationNo, node.animationFinished, node.playSpeed);
    _playQueue.pop();
}

bool AnimationInstance::hasAnimation(const std::string& animationHandle) const
{
    return _animationBase.hasAnimation(animationHandle);
}

bool AnimationInstance::hasAnimation(uint32_t animationNo) const
{
    return _animationBase.hasAnimation(animationNo);
}

void AnimationInstance::playAnimation(const std::string& animationHandle,
                                      AnimationFinishedDelegate animationFinished,
                                      float playSpeed)
{
    DEVEL_NOT_TRANSLATED;
    /*AnimationResolutionResults lookupResults = AnimationLibrary::lookupAnimationNo(_animationBase.getSpriteSheet(),
                                                                                   animationHandle);
    if (!lookupResults.doesNameExist)
    {
        ExecutionException error("Attempted to play an animation using an invalid animationHandle.");
        error.addData("animationHandle", animationHandle);
        error.addData("animationBaseNo", toString(_animationBase.getAnimationBaseNo()));
        error.addData("animationBaseSpriteSheet", toString(_animationBase.getSpriteSheet()));
        throw error;
    }

    clearPlayQueue();
    AnimationQueueNodePtr newNode(new AnimationQueueNode());
    newNode->animationNo = lookupResults.animationNo;
    newNode->animationFinished = animationFinished;
    newNode->playSpeed = playSpeed;
    _immediatelyPlayAnimation = newNode;*/
}

void AnimationInstance::playAnimation(uint32_t animationNo,
                                      AnimationFinishedDelegate animationFinished,
                                      float playSpeed)
{
    clearPlayQueue();
    AnimationQueueNodePtr newNode(new AnimationQueueNode());
    newNode->animationNo = animationNo;
    newNode->animationFinished = animationFinished;
    newNode->playSpeed = playSpeed;
    _immediatelyPlayAnimation = newNode;
}

void AnimationInstance::enqueueNextAnimation(const std::string& animationHandle,
                                             AnimationFinishedDelegate animationFinished,
                                             float playSpeed)
{
    DEVEL_NOT_TRANSLATED;
    /*AnimationResolutionResults lookupResults = AnimationLibrary::lookupAnimationNo(_animationBase.getSpriteSheet(),
                                                                                   animationHandle);
    if (!lookupResults.doesNameExist)
    {
        ExecutionException error("Attempted to enqueue playing an animation using an invalid animationHandle.");
        error.addData("animationHandle", animationHandle);
        error.addData("animationBaseNo", toString(_animationBase.getAnimationBaseNo()));
        error.addData("animationBaseSpriteSheet", toString(_animationBase.getSpriteSheet()));
        throw error;
    }

    AnimationQueueNode newNode;
    newNode.animationNo = lookupResults.animationNo;
    newNode.animationFinished = animationFinished;
    newNode.playSpeed = playSpeed;
    _playQueue.push(newNode);*/
}

void AnimationInstance::enqueueNextAnimation(uint32_t animationNo,
                                             AnimationFinishedDelegate animationFinished,
                                             float playSpeed)
{
    AnimationQueueNode newNode;
    newNode.animationNo = animationNo;
    newNode.animationFinished = animationFinished;
    newNode.playSpeed = playSpeed;
    _playQueue.push(newNode);
}

// Member access functions (nothing too special beyond this point)
uint32_t AnimationInstance::getAnimationBaseNo() const
{
    return _animationBase.getAnimationBaseNo();
}

bool AnimationInstance::isPlaying() const
{
    return (_currentAnimation != nullptr);
}

uint32_t AnimationInstance::getCurrentAnimationNo() const
{
    if (_currentAnimation == nullptr)
    {
        return 0;
    }

    return _currentAnimation->getAnimationNo();
}

uint32_t AnimationInstance::getSpriteNo() const
{
    return _currentSpriteNo;
}

bool AnimationInstance::getDoesHaveOverlay() const
{
    return _currentHasOverlay;
}

uint32_t AnimationInstance::getOverlaySpriteNo() const
{
    return _currentOverlaySpriteNo;
}

bool AnimationInstance::isNewKeyframe() const
{
    return _isNewKeyframe;
}

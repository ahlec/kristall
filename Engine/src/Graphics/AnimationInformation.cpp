#include "Graphics/AnimationInformation.h"
#include "Data/BinaryReader.h"
#include "General/Math.h"

// Virtual function definitions
AnimationInformation::~AnimationInformation()
{

}

// Major class functions
AnimationInformation::Keyframe::Keyframe(const BinaryReaderPtr& reader) :
    spriteNo(reader->readUInt32()), overlaySpriteNo(reader->readUInt32()), timeOffset(reader->readUInt16())
{
}

AnimationInformation::AnimationInformation(uint32_t animationNo,
                                           BinaryReaderPtr reader) :
                                               _animationNo(animationNo)
{
    // Read header
    _isLooping = reader->readBool();
    _isDeadEnd = reader->readBool();
    _returnsToAnimationNo = reader->readUInt32();
    _hasSoundEffect = reader->readBool();
    _soundEffectNo = reader->readUInt16();
    _duration = reader->readUInt16();

    // Read frames
    uint16_t nFrames(reader->readUInt16());
    _frames.reserve(nFrames);
    for (size_t index = 0; index < nFrames; ++index)
    {
        _frames.push_back(Keyframe(reader));
    }
}

AnimationFramePtr AnimationInformation::getFrame(uint32_t atTime, uint32_t lastTime,
                                                 bool isZeroDurationAllowed) const
{
    if (atTime >= _duration && (_duration > 0 || !isZeroDurationAllowed))
    {
        return nullptr;
    }

    AnimationFramePtr frame(new AnimationFrame());
    if (atTime == 0)
    {
        frame->isFrameChanged = true;
        frame->spriteNo = _frames[0].spriteNo;
        frame->overlayNo = _frames[0].overlaySpriteNo;
        return frame;
    }

    int32_t currentFrame(-1);
    int32_t lastFrame(-1);
    for (size_t index = 0; index < _frames.size(); ++index)
    {
        if (currentFrame < 0 && atTime >= _frames[index].timeOffset)
        {
            currentFrame = index;
        }

        if (lastFrame < 0 && lastTime >= _frames[index].timeOffset)
        {
            lastFrame = index;
        }

        if (currentFrame >= 0 && lastFrame >= 0)
        {
            break;
        }
    }

    currentFrame = Math::max(0, currentFrame);
    lastFrame = Math::max(0, lastFrame);

    frame->isFrameChanged = (currentFrame != lastFrame);
    if (frame->isFrameChanged)
    {
        frame->spriteNo = _frames[currentFrame].spriteNo;
        frame->overlayNo = _frames[currentFrame].overlaySpriteNo;
    }

    return frame;
}

// Member access functions (nothing too special beyond this point)
uint32_t AnimationInformation::getAnimationNo() const
{
    return _animationNo;
}

bool AnimationInformation::getIsLooping() const
{
    return _isLooping;
}

bool AnimationInformation::getIsDeadEnd() const
{
    return _isDeadEnd;
}

uint32_t AnimationInformation::getReturnsToAnimationNo() const
{
    return _returnsToAnimationNo;
}

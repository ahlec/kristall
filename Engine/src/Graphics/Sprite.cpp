#include "Graphics/Sprite.h"
#include "Data/BinaryReader.h"

Sprite::Sprite()
{
}

Sprite::Sprite(BinaryReaderPtr& reader)
{
    topLeftCoords.x = reader->readUInt32();
    topLeftCoords.y = reader->readUInt32();

    topRightCoords.x = reader->readUInt32();
    topRightCoords.y = reader->readUInt32();

    bottomRightCoords.x = reader->readUInt32();
    bottomRightCoords.y = reader->readUInt32();

    bottomLeftCoords.x = reader->readUInt32();
    bottomLeftCoords.y = reader->readUInt32();

    width = reader->readUInt16();
    height = reader->readUInt16();

    ingrainedOffsetX = reader->readInt32();
    ingrainedOffsetY = reader->readInt32();
}

#include "Graphics/UniversalAnimation.h"
#include "Data/BinaryReader.h"

UniversalAnimation::AnimationFrame::AnimationFrame(const BinaryReaderPtr& reader) :
                spriteNo(reader->readUInt32()), timeOffset(reader->readUInt16())
{
}

UniversalAnimation::UniversalAnimation(const BinaryReaderPtr& reader) :
                        _spriteNo(reader->readUInt32()), _duration(reader->readUInt16()),
                        _currentTime(0), _currentFrame(0)
{
    uint16_t nFrames(reader->readUInt16());
    _frames.reserve(nFrames);
    for (size_t index = 0; index < nFrames; ++index)
    {
        _frames.push_back(AnimationFrame(reader));
    }
}

bool UniversalAnimation::update(uint32_t elapsedTime)
{
    _currentTime += elapsedTime;

    if (_currentFrame + 1 >= _frames.size())
    {
        if (_currentTime < _duration)
        {
            return false;
        }

        _currentTime = 0;
        _currentFrame = 0;
        return true;
    }
    else
    {
        if (_frames[_currentFrame + 1].timeOffset > _currentTime)
        {
            return false;
        }

        ++_currentFrame;
        return true;
    }
}

uint32_t UniversalAnimation::getCurrentSpriteNo() const
{
    return _frames[_currentFrame].spriteNo;
}

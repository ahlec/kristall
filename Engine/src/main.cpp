#include <curl/curl.h>
#include "Data/DataManager.h"
#include "Game.h"
#include "Data/BinaryReader.h"
#include "Interface/GameWindow.h"
#include "Characters/Character.h"
#include "Errors/ErrorHandling.h"
#include "Errors/Signals.h"
#include <SDL_ttf.h>

int main(int argc, char** argv)
{
    try
    {
        ErrorHandler::initializeErrorHandler();
        Signals::initializeHandlers();
        curl_global_init(CURL_GLOBAL_DEFAULT);

        SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS);
        TTF_Init();

        DataManager::initialize();

        // Initialize the Game
        Game::initialize();

        // Load the player game
        Game::getGameWindow()->initializePlayerGame();

        S_DeveloperSwitches->set(IS_GRID_VISIBLE, true);

        // Run the game
        Game::getGameWindow()->run();

        // Clean up
        Game::deinitialize();
        DataManager::deinitialize();
        curl_global_cleanup();
    }
    catch (ExecutionException const& error)
    {
        ErrorHandler::reportException(error);
        return 1;
    }
    return 0;
}

#include "World/MapBridgeTile.h"
#include "Data/BinaryReader.h"
#include "World/Map.h"

MapBridgeTile::MapBridgeTile(InternalMapPtr map, uint16_t x, uint16_t y, BinaryReaderPtr reader,
    PlayerGame* playerGame) : Tile(map, x, y, reader, playerGame)
{
}

MapBridgeTile::~MapBridgeTile()
{
}

void MapBridgeTile::readHeader(BinaryReaderPtr reader)
{
    _inwardArrow = reader->readDirection();
    _destinationMapNo = reader->readUInt32();
    _destinationTileX = reader->readUInt16();
    _destinationTileY = reader->readUInt16();
}

// Member access functions (nothing too special beyond this point)
TileType MapBridgeTile::getType() const
{
    return TileType::MapBridgeTile;
}

Direction MapBridgeTile::getInwardArrow() const
{
    return _inwardArrow;
}

Tile* MapBridgeTile::getDestination() const
{
    return Map::get(_destinationMapNo)->getTile(_destinationTileX, _destinationTileY);
}

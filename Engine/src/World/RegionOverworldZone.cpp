#include "World/RegionOverworldZone.h"
#include "Data/BinaryReader.h"
#include "World/OverworldEntry.h"
#include "General/Numbers.h"
#include "General/Math.h"

// RegionOverworldZone::Overflow
RegionOverworldZone::Overflow::Overflow(const BinaryReaderPtr& reader) :
                                spriteNo(reader->readUInt32())
{
    uint32_t nInstances(reader->readUInt32());
    Vector2 tempInstance;
    for (size_t index = 0; index < nInstances; ++index)
    {
        tempInstance.x = reader->readInt32();
        tempInstance.y = reader->readInt32();
        instances.push_back(tempInstance);
    }
}

// Class functions
RegionOverworldZone::RegionOverworldZone(const BinaryReaderPtr& reader) : _mapPtr(nullptr)
{
    _identityNo = reader->readInt64();
    _isOutdoor = reader->readBool();
    _boundingRectangle = reader->readRect();
    _primaryRectangle = reader->readRect();
    _tileWidth = reader->readUInt16();
    _tileHeight = reader->readUInt16();

    // Main
    _mainTextureNo = reader->readUInt32();
    //reader->readTexture(_mainTexture);
    uint32_t nMainOverflows(reader->readUInt32());
    _mainOverflows.reserve(nMainOverflows);
    for (uint32_t index = 0; index < nMainOverflows; ++index)
    {
        _mainOverflows.push_back(Overflow(reader));
    }

    // Superlayer
    _hasSuperlayer = reader->readBool();
    if (_hasSuperlayer)
    {
        _superlayerTextureNo = reader->readUInt32();
        //reader->readTexture(_superlayerTexture);
        uint32_t nSuperlayerOverflows(reader->readUInt32());
        _superlayerOverflows.reserve(nSuperlayerOverflows);
        for (uint32_t index = 0; index < nSuperlayerOverflows; ++index)
        {
            _superlayerOverflows.push_back(Overflow(reader));
        }
    }
}

bool RegionOverworldZone::isMap() const
{
    return (_identityNo >= 0);
}

uint32_t RegionOverworldZone::getMapNo() const
{
    if (_identityNo < 0)
    {
        ExecutionException error("Attempted to the get MapNo of a non-map RegionOverworldZone.");
        throw error;
    }

    return Numbers::as<uint32_t>(_identityNo);
}

bool RegionOverworldZone::isOnViewport(Rect& viewport) const
{
    return _boundingRectangle.intersects(viewport);
}

OverworldEntry RegionOverworldZone::toEntry(int32_t adjustmentX, int32_t adjustmentY,
                                            int32_t focusMapX, int32_t focusMapY,
                                            Rect& viewport) const
{
    int32_t localX(-adjustmentX + focusMapX + _boundingRectangle.x);
    int32_t localY(-adjustmentY + focusMapY + _boundingRectangle.y);
    int32_t textureX(Math::max(-localX, 0));
    int32_t textureY(Math::max(-localY, 0));

    return OverworldEntry(Math::max(localX, 0), Math::max(localY, 0), textureX, textureY,
                          Math::min(viewport.width + textureX,
                                    _boundingRectangle.width - textureX),
                          Math::min(viewport.height + textureY,
                                    _boundingRectangle.height - textureY),
                          this);
}

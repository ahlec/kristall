#include "World/Overworld.h"
#include "Player/PlayerGame.h"
#include "Characters/Character.h"
#include "World/Tile.h"
#include "World/Map.h"
#include "World/Region.h"
#include "Interface/GameWindow.h"
#include "Game.h"
#include "DeveloperSwitches.h"
#include "Graphics/SpriteBatch.h"
#include "Lua/OverworldLuaInterpretor.h"
#include "Characters/PlayerCharacter.h"
#include <SDL_mouse.h>

Overworld::Overworld() : _luaInterpretor(new OverworldLuaInterpretor(*this))
{
    //_occupantsSpriteBatch.setRenderSize(GameWindow::getWidth(), GameWindow::getHeight());
}

void Overworld::update(uint32_t elapsedTime, PlayerGame* currentPlayerGame)
{
    // Get the maps currently within the viewport
    Tile* playerCharacterCurrentTile(currentPlayerGame->getCharacter()->getParent());
    if (playerCharacterCurrentTile == nullptr)
    {
        _overworldEntriesThisTick.clear();
        return;
    }

    _mapsUpdatedThisTick.clear();
    _overworldEntriesThisTick = playerCharacterCurrentTile->getMap()->getRegion()->getOverworld(currentPlayerGame->getCharacter(),
                                                                                                Game::getGameWindow()->getWidth(),
                                                                                                Game::getGameWindow()->getHeight());

    // Update the maps
    for (OverworldEntry& overworldEntry : _overworldEntriesThisTick)
    {
        _mapsUpdatedThisTick.add(overworldEntry.update(elapsedTime, *this, currentPlayerGame));
    }
}

void Overworld::draw(SpriteBatch* spriteBatch, PlayerGame* currentPlayerGame)
{
    // Draw the overworld entries
    //bool isTileHoverEnabled = DeveloperSwitches::isOverworldTileHoverEnabled();
    int32_t mouseX(0);
    int32_t mouseY(0);
    SDL_GetMouseState(&mouseX, &mouseY);

    for (const OverworldEntry& overworldEntry : _overworldEntriesThisTick)
    {
        overworldEntry.drawBackground(spriteBatch);
    }

    for (const OverworldEntry& overworldEntry : _overworldEntriesThisTick)
    {
        overworldEntry.drawOccupants(spriteBatch);
    }

    for (const OverworldEntry& overworldEntry : _overworldEntriesThisTick)
    {
        overworldEntry.drawForeground(spriteBatch);
    }
}

// Member access functions (nothing too special beyond this point)
OverworldLuaInterpretor* Overworld::getLuaInterpretor() const
{
    return _luaInterpretor;
}

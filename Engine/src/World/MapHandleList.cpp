#include "World/MapHandleList.h"
#include "General/Numbers.h"

MapHandleList::MapHandleList()
{
}

void MapHandleList::add(int64_t mapNo)
{
    if (!Numbers::isValid<uint32_t>(mapNo))
    {
        return;
    }

    uint32_t validMapNo(Numbers::to<uint32_t>(mapNo));
    if (!_bst.has(validMapNo))
    {
        _bst.add(validMapNo);
    }
}

bool MapHandleList::contains(int64_t mapNo) const
{
    if (!Numbers::isValid<uint32_t>(mapNo))
    {
        return false;
    }

    return _bst.has(Numbers::to<uint32_t>(mapNo));
}

bool MapHandleList::contains(uint32_t mapNo) const
{
    return _bst.has(mapNo);
}

void MapHandleList::clear()
{
    _bst.clear();
}

std::vector<uint32_t> MapHandleList::getAll() const
{
    return _bst.getValues();
}

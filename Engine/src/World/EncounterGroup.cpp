#include "World/EncounterGroup.h"
#include "Data/BinaryReader.h"

EncounterGroup::EncounterGroup(BinaryReaderPtr reader)
{
    EncounterGroupNode tempNode;
    uint8_t nEntries = reader->readUInt8();
    for (size_t index = 0; index < nEntries; ++index)
    {
        tempNode.speciesNo = reader->readUInt32();
        tempNode.isElectricType = reader->readBool();
        tempNode.isSteelType = reader->readBool();
        tempNode.encounterRate = reader->readUInt8();
        tempNode.encounterMethod = static_cast<EncounterMethod>(reader->readUInt8());
        tempNode.levelRangeStart = reader->readUInt8();
        tempNode.levelRangeEnd = reader->readUInt8();
        tempNode.encounterTimes = static_cast<TimeOfDayFlags>(reader->readUInt8());

        _encounterNodes.push_back(tempNode);
    }

    _totalEncounterRate = reader->readUInt16();
    _isAffectedByStatic = reader->readBool();
    _isAffectedByMagnetPull = reader->readBool();
}

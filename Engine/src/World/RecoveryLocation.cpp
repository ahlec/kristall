#include "World/RecoveryLocation.h"

RecoveryLocation::RecoveryLocation(uint16_t tileX, uint16_t tileY,
                                   Direction arrivalDirection) : _tileX(tileX),
                                   _tileY(tileY), _arrivalDirection(arrivalDirection)
{
}

uint16_t RecoveryLocation::getTileX() const
{
    return _tileX;
}

uint16_t RecoveryLocation::getTileY() const
{
    return _tileY;
}

Direction RecoveryLocation::getArrivalDirection() const
{
    return _arrivalDirection;
}

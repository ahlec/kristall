#include "World/MapLocation.h"
#include "Data/BinaryReader.h"

// Class functions
MapLocation::MapLocation() : _mapNo(0), _tileX(0), _tileY(0)
{
}

MapLocation::MapLocation(uint32_t mapNo, uint16_t tileX, uint16_t tileY) : _mapNo(mapNo),
                        _tileX(tileX), _tileY(tileY)
{
}

MapLocation::MapLocation(const BinaryReaderPtr& reader) :
                    _mapNo(reader->readUInt32()), _tileX(reader->readUInt16()),
                    _tileY(reader->readUInt16())
{
}

bool MapLocation::operator==(const MapLocation& other) const
{
    return (_mapNo == other._mapNo && _tileX == other._tileX && _tileY == other._tileY);
}

bool MapLocation::operator!=(const MapLocation& other) const
{
    return !(*this == other);
}

// Member access functions (nothing too special beyond this point)
uint32_t MapLocation::getMapNo() const
{
    return _mapNo;
}

uint16_t MapLocation::getTileX() const
{
    return _tileX;
}

uint16_t MapLocation::getTileY() const
{
    return _tileY;
}

#include "World/OverworldEntry.h"
#include "World/Map.h"
#include "World/RegionOverworldZone.h"
#include "Graphics/SpriteBatch.h"
#include "World/OverworldOccupant.h"
#include "Characters/Character.h"
#include "World/Tile.h"
#include "Cornerstones.h"

OverworldEntry::OverworldEntry(int32_t screenX, int32_t screenY, int32_t textureX,
                               int32_t textureY, int32_t width, int32_t height,
                               const RegionOverworldZone* zone) :
                                   _screenX(screenX), _screenY(screenY),
                                   _textureX(textureX), _textureY(textureY),
                                   _width(width), _height(height), _zone(zone)
{

}

MapPtr OverworldEntry::getMap() const
{
    return Map::get(_zone->getMapNo());
}

int64_t OverworldEntry::update(uint32_t elapsedTime, Overworld& overworld, PlayerGame* currentPlayerGame)
{
    if (_zone->isMap())
    {
        Map::get(_zone->getMapNo())->update(elapsedTime, overworld, currentPlayerGame);
    }

    return _zone->_identityNo;
}

void OverworldEntry::drawBackground(SpriteBatch* spriteBatch) const
{
    spriteBatch->drawTexture(_zone->_mainTextureNo, _screenX, _screenY, _width, _height, _textureX,
                             _textureY);

    drawOverflows(spriteBatch, false);

    #if DEBUG
    if (S_DeveloperSwitches->get(IS_GRID_VISIBLE) && _zone->isMap())
    {
        int gridStartX = _screenX - (_textureX % TILE_SIZE);
        int gridEndX = _screenX + _width;
        int gridStartY = _screenY - (_textureY % TILE_SIZE);
        int gridEndY = _screenY + _height;

        for (int x = gridStartX; x <= gridEndX; x += TILE_SIZE)
        {
            spriteBatch->drawLine(x, gridStartY, x, gridEndY, Color::Black);
        }
        for (int y = gridStartY; y <= gridEndY; y += TILE_SIZE)
        {
            spriteBatch->drawLine(gridStartX, y, gridEndX, y, Color::Black);
        }
    }
    #endif
}

void OverworldEntry::drawOccupants(SpriteBatch* spriteBatch) const
{
    if (!_zone->isMap())
    {
        return;
    }

    int32_t xOffset(_textureX - _zone->_primaryRectangle.x + _zone->_boundingRectangle.x - TILE_SIZE);
    int32_t yOffset(_textureY - _zone->_primaryRectangle.y + _zone->_boundingRectangle.y - TILE_SIZE);
    int16_t offsetY;
    for (const OverworldOccupantPtr& occupant : Map::get(_zone->getMapNo())->getAllOccupants())
    {
        if (occupant != nullptr && occupant->getSpriteNo() > 0 && occupant->getIsVisible())
        {
            if (OverworldOccupantTypes::isCharacter(occupant->getType()))
            {
                offsetY = occupant->getOffsetY() + static_cast<Character&>(*occupant).getAdditionalYOffset();
            }
            else
            {
                offsetY = occupant->getOffsetY();
            }

            spriteBatch->drawSprite(occupant->getSpriteSheet(), occupant->getSpriteNo(), _screenX + occupant->getParent()->getX() * TILE_SIZE - xOffset + occupant->getOffsetX(),
                                    _screenY + occupant->getParent()->getY() * TILE_SIZE - yOffset + offsetY, LocationAnchor::BottomRight);
        }
    }
}

void OverworldEntry::drawForeground(SpriteBatch* spriteBatch) const
{
    if (!_zone->_hasSuperlayer)
    {
        return;
    }

    spriteBatch->drawTexture(_zone->_superlayerTextureNo, _screenX, _screenY, _width, _height,
                             _textureX, _textureY);
    drawOverflows(spriteBatch, true);
}

void OverworldEntry::drawOverflows(SpriteBatch* spriteBatch, bool isSuperlayer) const
{
    const std::vector<RegionOverworldZone::Overflow>& overflows((isSuperlayer ?
                                                                _zone->_superlayerOverflows :
                                                                _zone->_mainOverflows));
    if (overflows.empty())
    {
        return;
    }

    SpriteSheet spriteSheet(_zone->getAppropriateSpriteSheet());
    for (const RegionOverworldZone::Overflow& overflow : overflows)
    {
        if (overflow.spriteNo == 0)
        {
            continue;
        }

        for (const Vector2& instance : overflow.instances)
        {
            spriteBatch->drawSprite(spriteSheet, overflow.spriteNo, instance.x - _textureX + _screenX,
                                    instance.y - _textureY + _screenY);
        }
    }
}

#include "World/OverworldOccupant.h"
#include "World/Tile.h"
#include "World/Map.h"

OverworldOccupant::OverworldOccupant() :
    m_parent(nullptr),
    m_offsetX(0),
    m_offsetY(0),
    m_directionFacing(Direction::South),
    m_isVisible(true)
{
}

OverworldOccupant::~OverworldOccupant()
{

}

Tile* OverworldOccupant::getParent() const
{
    return m_parent;
}

int OverworldOccupant::getOffsetX() const
{
    return m_offsetX;
}

int OverworldOccupant::getOffsetY() const
{
    return m_offsetY;
}

Direction OverworldOccupant::getDirectionFacing() const
{
    return m_directionFacing;
}

bool OverworldOccupant::getIsVisible() const
{
    return m_isVisible;
}

void OverworldOccupant::setOffsetX(int newOffsetX)
{
    m_offsetX = newOffsetX;
}

void OverworldOccupant::setOffsetY(int newOffsetY)
{
    m_offsetY = newOffsetY;
}

void OverworldOccupant::turn(Direction newDirection)
{
    Direction oldDirection = m_directionFacing;
    m_directionFacing = newDirection;
    onTurned(oldDirection, newDirection);
}

void OverworldOccupant::onTurned(Direction oldDirection, Direction newDirection)
{

}

void OverworldOccupant::onParentChanged(Tile* oldParent, Tile* newParent)
{

}

void OverworldOccupant::setParent(Tile* newParent)
{
    Tile* oldParent(m_parent);
    bool isOldNull(oldParent == nullptr);
    bool isNewNull(newParent == nullptr);

    if (isOldNull && isNewNull)
    {
        return;
    }

    if (isOldNull || isNewNull || (!isOldNull && !isNewNull &&
                                   oldParent->_map->_mapNo != newParent->_map->_mapNo))
    {
        if (!isOldNull)
        {
            oldParent->_map->removeOccupant(this);
        }

        if (!isNewNull)
        {
            newParent->_map->addOccupant(this);
        }
    }

    m_parent = newParent;

    onParentChanged(oldParent, m_parent);
}

#include "World/Tile.h"
#include "World/Map.h"
#include "World/OverworldOccupant.h"
#include "Data/BinaryReader.h"
#include "Game.h"
#include "Items/OverworldItem.h"
#include "World/OverworldObjects/BuildingBlockOverworldObject.h"
#include "World/OverworldObjects/OverworldObject.h"
#include <sstream>

template<>
String toString<Tile>(Tile value)
{
    return "{X: " + toString(value.getX()) + ", Y: " + toString(value.getY()) + "}";
};

// Virtual function definitions
Tile::~Tile()
{
}

bool Tile::isPreparationRequiredForOccupation() const
{
    return false;
}

void Tile::readHeader(BinaryReaderPtr reader)
{
    // Do nothing for regular Tiles
}

void Tile::playerHasArrived(PlayerCharacter* character)
{
    _map->playerCharacterEntersATile(*this, character);
}

bool Tile::hasOccupant() const
{
    // The default meaning of this function
    return (_currentOccupant != nullptr);
}

bool Tile::isHandlingPlayerArrivalPersonally()
{
    return false;
}

void Tile::prepareForOccupant(OverworldOccupantPtr newOccupant,
                              OccupantMayEnterTileDelegate callback)
{
    if (callback != nullptr)
    {
        callback(this);
    }

    if (newOccupant != nullptr && newOccupant->getType() == OverworldOccupantType::PlayerCharacter &&
        !isHandlingPlayerArrivalPersonally())
    {
        /*((PlayerCharacter*)newOccupant)->.attachCallbackToCurrentMoveCycle([this, newOccupant]() {
                                    this->playerHasArrived((PlayerCharacter)*newOccupant));
        });*/
        WARN_DEVEL_NOT_TRANSLATED;
    }
}

void Tile::occupantHasLeft(OverworldOccupantPtr departedOccupant)
{
}

Direction Tile::getInwardArrow() const
{
    return Direction::South;
}

Tile* Tile::getDestination() const
{
    return nullptr;
}

// Major class functions
Tile::Tile(InternalMapPtr map, uint16_t x, uint16_t y, BinaryReaderPtr reader, PlayerGame* playerGame)
                    : _map(map), _x(x), _y(y),
                      _nonVisibleItem(nullptr), _hasAnimationInstance(false), _currentOccupant(nullptr)
{
    _nature.type = static_cast<TileNatureType>(reader->readUInt8());
    _nature.hasDirection = reader->readBool();
    _nature.direction = reader->readDirection();
    _terrain = static_cast<Terrain>(reader->readUInt8());
    _scriptNo = reader->readInt16();
    _encounterGroupNo = reader->readInt16();
    _triggeredOverworldEffect = static_cast<OverworldEffectType>(reader->readUInt8());
    _overworldEffectTriggerDirection = reader->readDirection();

    if (reader->readBool()) // hasNonVisibleItem
    {
        _nonVisibleItem = OverworldItemPtr(new OverworldItem(reader, *playerGame));
    }

    _hasAnimationInstance = reader->readBool();
    _animationInstanceIndex = reader->readUInt32();

    if (reader->readBool()) // hasOccupant
    {
        OverworldOccupantType occupantType = static_cast<OverworldOccupantType>(reader->readUInt8());
        switch (occupantType)
        {
            case OverworldOccupantType::BuildingBlock:
                {
                    OverworldOccupantPtr occupant(new BuildingBlockOverworldObject(reader));
                    setCurrentOccupant(occupant);
                    break;
                }
            case OverworldOccupantType::OverworldObject:
                {
                    OverworldOccupantPtr occupant(new OverworldObject(reader));
                    setCurrentOccupant(occupant);
                    break;
                }
            case OverworldOccupantType::NonPlayerCharacter:
            case OverworldOccupantType::TrainerCharacter:
            case OverworldOccupantType::RivalCharacter:
            case OverworldOccupantType::PlayerCharacter:
            case OverworldOccupantType::PokemonCharacter:
                {
                    DEVEL_NOT_TRANSLATED;
                }
            default:
                {
                    throw ExecutionException("Encountered an invalid occupant type.");
                }
        }
    }
}

void Tile::occupantLeaving(OverworldOccupantPtr oldOccupant)
{
    if (oldOccupant == nullptr || oldOccupant->getType() != OverworldOccupantType::PlayerCharacter ||
        !isOverworldEffectTrigger())
    {
        return;
    }

    WARN_DEVEL_NOT_TRANSLATED;
    /*PlayerCharacter* leavingPlayer = (PlayerCharacter*)oldOccupant;
    if (_overworldEffectTriggerDirection == leavingPlayer->getDirectionFacing() &&
        _triggeredOverworldEffect != GameWindow::getOverworld().getCurrentOverworldEffect())
    {
        GameWindow::getOverworld().beginOverworldEffect(_triggeredOverworldEffect);
    }*/
}

void Tile::occupantChanged(OverworldOccupantPtr oldOccupant, OverworldOccupantPtr newOccupant)
{
    if (newOccupant != nullptr && newOccupant->getType() == OverworldOccupantType::PlayerCharacter)
    {
        WARN_DEVEL_NOT_TRANSLATED;
        /*
        if (isOverworldEffectTrigger() && Directions::complement(_overworldEffectTriggerDirection) ==
            ((PlayerCharacter*)newOccupant)->getDirectionFacing())
        {
            GameWindow::getOverworld().dispelCurrentOverworldEffect();
        }*/
    }
}

void Tile::suppressNextOccupationAlert()
{
    _isNextOccupationAlertSuppressed = true;
}

class TilePlayAnimationFunctor
{
public:
    TilePlayAnimationFunctor(int32_t nLayers,
                             AnimationFinishedDelegate callback) :
                                 _nOutstandingLayers(nLayers),
                                 _callback(callback)
    {
    }

    void operator()()
    {
        if (_nOutstandingLayers-- <= 0)
        {
            if (_callback != nullptr)
            {
                _callback();
            }

            delete this;
        }
    }

private:
    int32_t _nOutstandingLayers;
    AnimationFinishedDelegate _callback;
};


void Tile::playAnimation(const std::string& animationHandle,
                       AnimationFinishedDelegate animationFinished)
{
    /*TilePlayAnimationFunctor* playFunctor = new TilePlayAnimationFunctor(_layers.size() + (_superLayer != nullptr ? 1 : 0),
                                                                         animationFinished);
    animationFinishedDelegate functorCallback = convertFunctorToFunctionPointer<TilePlayAnimationFunctor>(*playFunctor);

    for (Layer layer : _layers)
    {
        layer.playAnimation(animationHandle, functorCallback);
    }

    if (_superLayer != nullptr)
    {
        _superLayer->playAnimation(animationHandle, functorCallback);
    }*/

    DEVEL_NOT_TRANSLATED;
}

bool Tile::hasAnimation(const std::string& animationHandle) const
{
    DEVEL_NOT_TRANSLATED;
    return false;
}

void Tile::setCurrentOccupant(OverworldOccupantPtr newOccupant)
{
    // Detatch the old occupant and the new occupant from their parents
    if (_currentOccupant != nullptr)
    {
        _currentOccupant->setParent(nullptr);
    }

    if (newOccupant != nullptr && newOccupant->m_parent != nullptr)
    {
        newOccupant->m_parent->_currentOccupant = nullptr;
    }

    // Set the new current occupant
    _currentOccupant = newOccupant;

    if (_currentOccupant != nullptr)
    {
        _currentOccupant->setParent(this);
    }
}

bool Tile::operator==(const Tile& other) const
{
    if (_x != other._x || _y != other._y)
    {
        return false;
    }

    if (_map != other._map)
    {
        return false;
    }

    return true;
}

bool Tile::operator!=(const Tile& other) const
{
    return !(*this == other);
}

// Member access functions (nothing too special beyond this point)
TileType Tile::getType() const
{
    return TileType::Tile;
}

uint16_t Tile::getX() const
{
    return _x;
}

uint16_t Tile::getY() const
{
    return _y;
}

MapPtr Tile::getMap() const
{
    return Map::get(_map->getMapNo());
}

TileNature Tile::getNature() const
{
    return _nature;
}

Terrain Tile::getTerrain() const
{
    return _terrain;
}

bool Tile::hasScript() const
{
    return (_scriptNo >= 0);
}

LuaScriptPtr Tile::getScript() const
{
    if (!hasScript())
    {
        ExecutionException error("Attempted to get the MapScript off of a Tile which doesn't have a map script.");
        error.addData("map", toString(_map));
        error.addData("tile", toString(this));
        throw error;
    }

    return _map->getScript(static_cast<uint8_t>(_scriptNo));
}

bool Tile::hasEncounterGroup() const
{
    return (_encounterGroupNo >= 0);
}

EncounterGroupPtr Tile::getEncounterGroup() const
{
    if (!hasEncounterGroup())
    {
        ExecutionException error("Attempted to get the EncounterGroup off of a Tile which doesn't have an EncounterGroup.");
        error.addData("map", toString(_map));
        error.addData("tile", toString(this));
        throw error;
    }

    return _map->getEncounterGroup(static_cast<uint8_t>(_encounterGroupNo));
}

bool Tile::isOverworldEffectTrigger() const
{
    return (_triggeredOverworldEffect != OverworldEffectType::None);
}

OverworldEffectType Tile::getTriggeredOverworldEffect() const
{
    return _triggeredOverworldEffect;
}

Direction Tile::getOverworldEffectTriggerDirection() const
{
    return _overworldEffectTriggerDirection;
}

OverworldItemPtr Tile::getNonVisibleItem() const
{
    return _nonVisibleItem;
}

const OverworldOccupantPtr& Tile::getCurrentOccupant() const
{
    return _currentOccupant;
}

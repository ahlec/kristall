#include "World/OverworldOccupantType.h"

namespace OverworldOccupantTypes
{
    bool isCharacter(OverworldOccupantType occupantType)
    {
        switch (occupantType)
        {
            case OverworldOccupantType::NonPlayerCharacter:
            case OverworldOccupantType::PlayerCharacter:
            case OverworldOccupantType::PokemonCharacter:
            case OverworldOccupantType::RivalCharacter:
            case OverworldOccupantType::TrainerCharacter:
                {
                    return true;
                }
            default:
                {
                    return false;
                }
        }
    }

    bool isNonPlayerCharacter(OverworldOccupantType occupantType)
    {
        switch (occupantType)
        {
            case OverworldOccupantType::NonPlayerCharacter:
            case OverworldOccupantType::PokemonCharacter:
            case OverworldOccupantType::RivalCharacter:
            case OverworldOccupantType::TrainerCharacter:
                {
                    return true;
                }
            default:
                {
                    return false;
                }
        }
    }
}

#include "World/Map.h"
#include "Data/Cache.h"
#include "Game.h"
#include "Data/BinaryReader.h"
#include "World/RecoveryLocation.h"
#include "World/EncounterGroup.h"
#include "World/Tile.h"
#include "World/MapBridgeTile.h"
#include "World/DoorTile.h"
#include "World/OverworldOccupant.h"
#include "World/Region.h"
#include <sstream>

template<>
String toString<Map>(Map value)
{
    return String(value.getName() + " (mapNo: " + toString(value.getMapNo()) + ")");
};

static Cache<Map> s_cache(FileType::Map);
static PlayerGame* s_currentPlayerGame(nullptr);

// Class functions
MapPtr Map::get(uint32_t mapNo)
{
    if (s_currentPlayerGame == nullptr)
    {
        ExecutionException error("Attempted to load a map while there was no PlayerGame.");
        error.addData("mapNo", toString(mapNo));
        throw error;
    }

    MapPtr getPointer(s_cache.get(mapNo));
    if (getPointer != nullptr)
    {
        return getPointer;
    }

    BinaryReaderPtr reader(DataManager::openFileReader(FileType::Map, mapNo));
    return s_cache.add(mapNo, new Map(mapNo, reader, s_currentPlayerGame));
}

void Map::resetCache(PlayerGame* newPlayerGame)
{
    s_cache.clear();
    s_currentPlayerGame = newPlayerGame;
}

Map::Map(uint32_t mapNo, BinaryReaderPtr reader, PlayerGame* currentPlayerGame) : _mapNo(mapNo),
        _recoveryLocation(nullptr)
{
    _name = reader->readString();
    _width = reader->readUInt16();
    _height = reader->readUInt16();
    _regionNo = reader->readUInt32();
    _isInterior = reader->readBool();
    _hasParentMap = reader->readBool();
    _parentMapNo = reader->readUInt32();
    _isBuilding = reader->readBool();
    _isBikingAllowed = reader->readBool();
    _label = static_cast<MapLabel>(reader->readUInt8());
    _musicTrackNo = reader->readUInt16();
    _tinting.r = reader->readUInt8();
    _tinting.g = reader->readUInt8();
    _tinting.b = reader->readUInt8();
    _tinting.a = reader->readUInt8();
    _effectType = static_cast<OverworldEffectType>(reader->readUInt8());
    if (reader->readBool()) // isRecoveryLocation
    {
        _recoveryLocation = RecoveryLocationPtr(new RecoveryLocation(reader->readUInt16(), reader->readUInt16(),
                                                                     reader->readDirection()));
    }

    uint8_t nScripts = reader->readUInt8();
    for (size_t index = 0; index < nScripts; ++index)
    {
        _scripts.push_back(reader->readLuaScript());
    }

    _encounterRate = reader->readUInt8();
    uint8_t nEncounterGroups = reader->readUInt8();
    for (size_t index = 0; index < nEncounterGroups; ++index)
    {
        _encounterGroups.push_back(EncounterGroupPtr(new EncounterGroup(reader)));
    }

    uint16_t x;
    TileType tileType;
    _tiles = std::vector<std::vector<Tile*>>(_height);
    for (uint16_t y = 0; y < _height; ++y)
    {
        std::vector<Tile*>& row(_tiles[y]);
        for (x = 0; x < _width; ++x)
        {
            tileType = static_cast<TileType>(reader->readUInt8());
            switch (tileType)
            {
                case TileType::MapBridgeTile:
                    {
                        row.push_back(new MapBridgeTile(this, x, y, reader, currentPlayerGame));
                        break;
                    }

                case TileType::DoorTile:
                    {
                        row.push_back(new DoorTile(this, x, y, reader, currentPlayerGame));
                        break;
                    }

                case TileType::Tile:
                    {
                        row.push_back(new Tile(this, x, y, reader, currentPlayerGame));
                        break;
                    }
            }
            row[x]->readHeader(reader);
        }
    }

    uint32_t nTileAnimationInstances(reader->readUInt32());
    _tileAnimationInstances.reserve(nTileAnimationInstances);
    SpriteSheet mapSpriteSheet(_isInterior ? SpriteSheet::OverworldInterior : SpriteSheet::OverworldExterior);
    for (size_t index = 0; index < nTileAnimationInstances; ++index)
    {
        _tileAnimationInstances.push_back(TileAnimationInstance(reader, mapSpriteSheet));
    }
}

void Map::playerCharacterEntersATile(Tile& destination, PlayerCharacter* playerCharacter)
{
    WARN_DEVEL_NOT_TRANSLATED;
}

LuaScriptPtr Map::getScript(uint8_t scriptNo) const
{
    if (scriptNo >= _scripts.size())
    {
        ExecutionException error("ScriptNo out of bounds.");
        error.addData("map", toString(this));
        error.addData("scriptNo", toString(scriptNo));
        throw error;
    }

    return _scripts[scriptNo];
}

EncounterGroupPtr Map::getEncounterGroup(uint8_t encounterGroupNo) const
{
    if (encounterGroupNo >= _encounterGroups.size())
    {
        ExecutionException error("EncounterGroupNo out of bounds.");
        error.addData("map", toString(this));
        error.addData("encounterGroupNo", toString(encounterGroupNo));
        throw error;
    }

    return _encounterGroups[encounterGroupNo];
}

void Map::update(uint32_t elapsedTime, Overworld& overworld, PlayerGame* currentPlayerGame)
{
    for (auto occupant : _currentOccupants)
    {
        occupant->update(elapsedTime);
    }
}

Tile* Map::getTile(int32_t tileX, int32_t tileY) const
{
    if (tileX < 0 || tileY < 0 || tileX >= _width || tileY >= _height)
    {
        return nullptr;
    }

    return _tiles[tileY][tileX];
}

// Member access functions (nothing too special beyond this point)
const std::vector<OverworldOccupantPtr> Map::getAllOccupants() const
{
    return _currentOccupants;
}

uint32_t Map::getMapNo() const
{
    return _mapNo;
}

String Map::getName() const
{
    return _name;
}

uint16_t Map::getWidth() const
{
    return _width;
}

uint16_t Map::getHeight() const
{
    return _height;
}

RegionPtr Map::getRegion() const
{
    return Region::get(_regionNo);
}

bool Map::getIsInterior() const
{
    return _isInterior;
}

MapPtr Map::getParentMap() const
{
    if (!_hasParentMap)
    {
        ExecutionException error("Attempted to get the parent map of a parentless map.");
        error.addData("map", toString(this));
        throw error;
    }

    return Map::get(_parentMapNo);
}

bool Map::getIsBuilding() const
{
    return _isBuilding;
}

bool Map::getIsBikingAllowed() const
{
    return _isBikingAllowed;
}

MapLabel Map::getLabel() const
{
    return _label;
}

uint16_t Map::getMusicTrackNo() const
{
    return _musicTrackNo;
}

Color Map::getTinting() const
{
    return _tinting;
}

OverworldEffectType Map::getEffectType() const
{
    return _effectType;
}

MapLocationPtr Map::getFlightLocation() const
{
    DEVEL_NOT_TRANSLATED;
}

RecoveryLocationPtr Map::getRecoveryLocation() const
{
    DEVEL_NOT_TRANSLATED;
}

uint8_t Map::getEncounterRate() const
{
    return _encounterRate;
}

uint32_t Map::getRegionMapIndex() const
{
    RegionPtr region(Region::get(_regionNo));
    return region->getIndexOfMap(_mapNo);
    //_regionIndex = region->getIndexOfMap(_mapNo);
    //return _regionIndex;
}

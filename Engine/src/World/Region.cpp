#include "World/Region.h"
#include "Data/Cache.h"
#include "Data/BinaryReader.h"
#include "World/RegionOverworldZone.h"
#include "World/OverworldOccupant.h"
#include "World/Tile.h"
#include "World/Map.h"
#include "World/OverworldEntry.h"
#include "General/Math.h"

static Cache<Region> s_cache(FileType::Region);

RegionPtr Region::get(uint32_t regionNo)
{
    RegionPtr getPointer(s_cache.get(regionNo));
    if (getPointer != nullptr)
    {
        return getPointer;
    }

    BinaryReaderPtr reader(DataManager::openFileReader(FileType::Region, regionNo));
    return s_cache.add(regionNo, new Region(regionNo, reader));
}

uint32_t Region::getRegionNo() const
{
    return m_regionNo;
}

String Region::getName() const
{
    return m_name;
}

MapLocation Region::getMaleStartLocation() const
{
    return m_maleStartLocation;
}

MapLocation Region::getFemaleStartLocation() const
{
    return m_femaleStartLocation;
}

MapLocationPtr Region::getFlightLocation(int32_t index) const
{
    return MapLocationPtr(new MapLocation(m_flightLocations[index]));
}

std::vector<MapLocation> Region::getFlightLocations(Predicate<MapLocation> condition)
{
    if (condition == nullptr)
    {
        return std::vector<MapLocation>(m_flightLocations);
    }

    std::vector<MapLocation> predicatedFlightLocations;
    for (const MapLocation& flightLocation : m_flightLocations)
    {
        if (condition(flightLocation))
        {
            predicatedFlightLocations.push_back(flightLocation);
        }
    }

    return predicatedFlightLocations;
}

uint32_t Region::getIndexOfMap(uint32_t mapNo) const
{
    int64_t effectiveSize(m_regionMaps.size() / 2);
    int64_t currentIndex(effectiveSize);
    while (currentIndex >= 0 && currentIndex < m_regionMaps.size())
    {
        if (m_regionMaps[currentIndex].mapNo == mapNo)
        {
            return static_cast<uint32_t>(currentIndex);
        }

        if (effectiveSize <= 1)
        {
            break;
        }
        effectiveSize /= 2;

        if (m_regionMaps[currentIndex].mapNo < mapNo)
        {
            currentIndex += effectiveSize;
        }
        else
        {
            currentIndex -= effectiveSize;
        }
    }

    ExecutionException error("Could not locate the Region map index of the specified map.");
    error.addData("regionNo", m_regionNo);
    error.addData("mapNo", mapNo);
    throw error;
}

std::vector<OverworldEntry> Region::getOverworld(OverworldOccupant* focus,
                                                 int32_t viewportWidth,
                                                 int32_t viewportHeight) const
{
    Tile* focusCurrentTile(focus->getParent());
    if (focusCurrentTile == nullptr)
    {
        return std::vector<OverworldEntry>();
    }

    MapPtr focusCurrentMap(focusCurrentTile->getMap());

    int32_t usingHeight(focusCurrentMap->getHeight());
    if (usingHeight % 2 == 1)
    {
        ++usingHeight;
    }

    int32_t usingWidth(focusCurrentMap->getWidth());
    if (usingWidth % 2 == 1)
    {
        ++usingWidth;
    }

    int32_t tileX(focusCurrentTile->getX());
    int32_t tileY(focusCurrentTile->getY());
    int32_t focusMapX = floor((viewportWidth - usingWidth * TILE_SIZE) / 2.0 +
                      ((usingWidth / 2.0) - tileX) * TILE_SIZE -
                      focus->getOffsetX());
    int32_t focusMapY = (viewportHeight - usingHeight * TILE_SIZE) / 2 +
                      ((int)(usingHeight / 2) - tileY) * TILE_SIZE -
                      focus->getOffsetY();

    // If the focus is on an interior map, return just the interior (no adjacent maps)
    if (focusCurrentMap->getIsInterior())
    {
        DEVEL_NOT_TRANSLATED;
    }

    // The focus is on an exterior map, so we now collect all of the exterior entities that
    // would be visible
    std::vector<OverworldEntry> overworldEntries;
    int32_t adjustX(m_regionMaps[focusCurrentMap->getRegionMapIndex()].absoluteX);
    int32_t adjustY(m_regionMaps[focusCurrentMap->getRegionMapIndex()].absoluteY);
    Rect viewport(Math::max(adjustX - (int)focusMapX - TILE_SIZE, 0),
                  Math::max(adjustY - (int)focusMapY - TILE_SIZE, 0),
                  viewportWidth + TILE_SIZE * 2,
                  viewportHeight + TILE_SIZE * 2);

    for (const RegionOverworldZone& zone : m_zones)
    {
        if (zone.isOnViewport(viewport))
        {
            overworldEntries.push_back(zone.toEntry(adjustX, adjustY, focusMapX, focusMapY,
                                                    viewport));
        }
    }

    return overworldEntries;
}

bool Region::operator==(const Region& other) const
{
    return (other.m_regionNo == m_regionNo);
}

bool Region::operator!=(const Region& other) const
{
    return !(*this == other);
}

Region::RegionMap::RegionMap(const BinaryReaderPtr& reader)
{
    mapNo = reader->readUInt32();
    isOutdoor = reader->readBool();
    absoluteX = reader->readInt32();
    absoluteY = reader->readInt32();
}

Region::Region(uint32_t regionNo, const BinaryReaderPtr& reader) :
    m_regionNo(regionNo),
    m_maleStartLocation(0, 0, 0),
    m_femaleStartLocation(0, 0, 0)
{
    m_name = reader->readString();
    m_maleStartLocation = MapLocation(reader);
    m_femaleStartLocation = MapLocation(reader);

    uint16_t nFlightLocations(reader->readUInt16());
    for (size_t index = 0; index < nFlightLocations; ++index)
    {
        m_flightLocations.push_back(MapLocation(reader));
    }

    // Zones
    uint32_t nZones(reader->readUInt32());
    m_zones.reserve(nZones);
    for (size_t index = 0; index < nZones; ++index)
    {
        m_zones.push_back(RegionOverworldZone(reader));
    }

    // RegionMaps
    uint32_t nRegionMaps(reader->readUInt32());
    m_regionMaps.reserve(nRegionMaps);
    for (size_t index = 0; index < nRegionMaps; ++index)
    {
        m_regionMaps.push_back(RegionMap(reader));
    }
}

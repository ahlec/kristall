#include "World/OverworldObjects/OverworldObject.h"
#include "Data/BinaryReader.h"
#include "Game.h"
#include "Interface/GameWindow.h"
#include "Graphics/AnimationInstance.h"
#include "Graphics/SpriteSheetLibrary.h"

OverworldObject::OverworldObject(BinaryReaderPtr& reader)
{
    _hasAnimation = reader->readBool();
    if (_hasAnimation)
    {
        _animation = Game::getGameWindow()->getSpriteSheetLibrary().createAnimationInstance(SpriteSheet::OverworldObjects,
                                                               reader->readUInt32());
    }
    else
    {
        _spriteNo = reader->readUInt32();
    }
    _isMovable = reader->readBool();
    _isSign = reader->readBool();
    _isCounter = reader->readBool();
    m_offsetX = reader->readInt16();
    m_offsetY = reader->readInt16();
}

void OverworldObject::activate(Character& activator)
{
    DEVEL_NOT_TRANSLATED;
}

void OverworldObject::update(uint32_t elapsedTime)
{
    if (_hasAnimation)
    {
        _animation->update(elapsedTime);
    }

    // do nothing
}


// Member access functions (nothing too special beyond this point)
bool OverworldObject::getIsMovable() const
{
    return _isMovable;
}

bool OverworldObject::getIsCounter() const
{
    return _isCounter;
}

OverworldOccupantType OverworldObject::getType() const
{
    return OverworldOccupantType::OverworldObject;
}

SpriteSheet OverworldObject::getSpriteSheet() const
{
    return SpriteSheet::OverworldObjects;
}

uint32_t OverworldObject::getSpriteNo() const
{
    return (_hasAnimation ? _animation->getSpriteNo() : _spriteNo);
}

bool OverworldObject::getIsSign() const
{
    return _isSign;
}

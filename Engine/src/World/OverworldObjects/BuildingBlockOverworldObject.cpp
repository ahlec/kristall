#include "World/OverworldObjects/BuildingBlockOverworldObject.h"
#include "Data/BinaryReader.h"

BuildingBlockOverworldObject::BuildingBlockOverworldObject(BinaryReaderPtr& reader)
{
    _spriteNo = reader->readUInt32();
    _isOverhead = reader->readBool();
    _isSign = reader->readBool();
    m_offsetX = reader->readInt16();
    m_offsetY = reader->readInt16();
}

void BuildingBlockOverworldObject::activate(Character& activator)
{
    DEVEL_NOT_TRANSLATED;
}

void BuildingBlockOverworldObject::update(uint32_t elapsedTime)
{
    // do nothing
}


// Member access functions (nothing too special beyond this point)
bool BuildingBlockOverworldObject::getIsOverhead() const
{
    return _isOverhead;
}

OverworldOccupantType BuildingBlockOverworldObject::getType() const
{
    return OverworldOccupantType::BuildingBlock;
}

SpriteSheet BuildingBlockOverworldObject::getSpriteSheet() const
{
    return SpriteSheet::Buildings;
}

uint32_t BuildingBlockOverworldObject::getSpriteNo() const
{
    return _spriteNo;
}

bool BuildingBlockOverworldObject::getIsSign() const
{
    return _isSign;
}

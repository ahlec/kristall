#include "World/DoorTile.h"
#include "Data/BinaryReader.h"
#include "World/Map.h"

DoorTile::DoorTile(InternalMapPtr map, uint16_t x, uint16_t y, BinaryReaderPtr reader,
    PlayerGame* playerGame) : Tile(map, x, y, reader, playerGame)
{
}

DoorTile::~DoorTile()
{
    DEVEL_NOT_TRANSLATED;
}

void DoorTile::readHeader(BinaryReaderPtr reader)
{
    _inwardArrow = reader->readDirection();
    _isUsedOnExit = reader->readBool();
    _staircaseType = static_cast<StaircaseType>(reader->readUInt8());
    _soundEffectNo = reader->readUInt32();
    _destinationMapNo = reader->readUInt32();
    _destinationTileX = reader->readUInt16();
    _destinationTileY = reader->readUInt16();
}

bool DoorTile::hasOccupant() const
{
    return (Tile::hasOccupant() || _arrivingOccupant != nullptr);
}

bool DoorTile::isPreparationRequiredForOccupation() const
{
    DEVEL_NOT_TRANSLATED;
}

void DoorTile::prepareForOccupant(OverworldOccupantPtr newOccupant,
                                  OccupantMayEnterTileDelegate callback)
{
    DEVEL_NOT_TRANSLATED;
}

void DoorTile::occupantHasLeft(OverworldOccupantPtr departedOccupant)
{
    DEVEL_NOT_TRANSLATED;
}

// Member access functions (nothing too special beyond this point)
TileType DoorTile::getType() const
{
    return TileType::DoorTile;
}

Direction DoorTile::getInwardArrow() const
{
    return _inwardArrow;
}

Tile* DoorTile::getDestination() const
{
    return Map::get(_destinationMapNo)->getTile(_destinationTileX, _destinationTileY);
}

bool DoorTile::getIsUsedOnExit() const
{
    return _isUsedOnExit;
}

StaircaseType DoorTile::getStaircaseType() const
{
    return _staircaseType;
}

#include "World/TileAnimationInstance.h"
#include "Game.h"
#include "Interface/GameWindow.h"
#include "Data/BinaryReader.h"
#include "Graphics/SpriteSheetLibrary.h"

TileAnimationInstance::TileAnimationInstance(BinaryReaderPtr& reader, SpriteSheet mapSpriteSheet)
{
    _animationInstance = Game::getGameWindow()->getSpriteSheetLibrary().createAnimationInstance(mapSpriteSheet, 11);//reader->readUInt32());
    reader->readUInt32();
    _tileX = reader->readUInt16();
    _tileY = reader->readUInt16();
}

AnimationInstancePtr TileAnimationInstance::getAnimationInstance() const
{
    return _animationInstance;
}

uint16_t TileAnimationInstance::getTileX() const
{
    return _tileX;
}

uint16_t TileAnimationInstance::getTileY() const
{
    return _tileY;
}

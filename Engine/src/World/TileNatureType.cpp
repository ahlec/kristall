#include "World/TileNatureType.h"

namespace TileNatureTypes
{
    bool isSurfable(TileNatureType natureType)
    {
        switch (natureType)
        {
            case TileNatureType::Water:
            case TileNatureType::WaterCurrent:
            case TileNatureType::DeepWater:
            case TileNatureType::Seafloor:
                {
                    return true;
                }
            default:
                {
                    return false;
                }
        }
    }
}

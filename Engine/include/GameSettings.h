#ifndef KRISTALL_INCLUDE_GAMESETTINGS_H
#define KRISTALL_INCLUDE_GAMESETTINGS_H

#include "Data/Predefine.h"
#include "Input/Predefine.h"
#include "Interface/TextSpeed.h"
#include "General/Mutex.h"
#include "Graphics/Predefine.h"

class GameSettings
{
public:
    GameSettings(BinaryReaderPtr reader);
    ~GameSettings();

    uint32_t            getClientWidth() const;
    uint32_t            getClientHeight() const;
    bool                getIsMusicMuted() const;
    bool                getAreSoundEffectsMuted() const;
    bool                getShouldMuteInBackground() const;
    TextSpeed           getTextSpeed() const;
    bool                getShouldUseMouse() const;
    bool                getIsPostLatinEnabled() const;
    float               getMusicVolume() const;
    std::string         getFrameHandle() const;
    std::string         getTextboxFrameHandle() const;
    const KeyBindings&  getKeyBindings() const;
    FramePtr            getFrame() const;

private:
    Mutex _mutex;

    uint32_t _clientWidth;
    uint32_t _clientHeight;
    bool _isMusicMuted;
    bool _areSoundEffectsMuted;
    bool _shouldMuteInBackground;
    TextSpeed _textSpeed;
    bool _shouldUseMouse;
    bool _isPostLatinEnabled;
    float _musicVolume;
    std::string _frameHandle;
    std::string _textboxFrameHandle;
    KeyBindings* _keyBindings;
};

#endif

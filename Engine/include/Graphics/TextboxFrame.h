#ifndef GRAPHICS_TEXTBOXFRAME_H
#define GRAPHICS_TEXTBOXFRAME_H

class SpriteBatch;

class TextboxFrame
{
    friend class SpriteBatch;
public:
    TextboxFrame(BinaryReader& reader);
    ~TextboxFrame();

    const std::string& getHandle() const;

private:
    std::string _handle;
    sf::Texture _texture;
    bool _hasArrow;
    FontColor& _defaultFontColor;
    bool _isHorizontalTrackTiled;

    sf::Sprite _leftBorder;
    sf::Sprite _horizontalTrack;
    sf::Sprite _rightBorder;
    sf::Sprite* _arrow;
};

#endif

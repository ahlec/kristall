#ifndef KRISTALL_INCLUDE_GRAPHICS_SPRITESHEETLIBRARY_H
#define KRISTALL_INCLUDE_GRAPHICS_SPRITESHEETLIBRARY_H

#include "Cornerstones.h"
#include "Graphics/Predefine.h"
#include "Graphics/Sprite.h"
#include "Graphics/UniversalAnimation.h"
#include "Graphics/AnimationBase.h"
#include "Graphics/AnimationResolutionResults.h"

class SpriteSheetLibrary
{
public:
    SpriteSheetLibrary();
    ~SpriteSheetLibrary();

    void update(uint32_t elapsedTime);

    const Texture& getTexture(SpriteSheet spriteSheet);
    AnimationInstancePtr createAnimationInstance(SpriteSheet spriteSheet,
                                                        uint32_t animationBaseNo) const;
    AnimationResolutionResults lookupAnimationNo(SpriteSheet spriteSheet,
                                                 const std::string& animationHandle) const;
    uint32_t getTextureId(SpriteSheet spriteSheet) const;
    const Sprite& getSprite(SpriteSheet spriteSheet, uint32_t spriteNo) const;

private:
    class InternalSpriteSheet
    {
    public:
        InternalSpriteSheet();
        ~InternalSpriteSheet();

        void load(SpriteSheet name, BinaryReaderPtr&);

        uint32_t textureNo;
        std::vector<Sprite> sprites;
        std::vector<UniversalAnimation> universalAnimations;
        std::vector<std::string> animationNames;
        std::vector<AnimationBase> animationBases;
    };

    uint8_t _nSpriteSheets;
    InternalSpriteSheet* _spriteSheets;
};

#endif

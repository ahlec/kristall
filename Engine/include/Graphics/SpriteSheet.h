#ifndef KRISTALL_INCLUDE_GRAPHICS_SPRITESHEET_H
#define KRISTALL_INCLUDE_GRAPHICS_SPRITESHEET_H

#include "Cornerstones.h"

enum class SpriteSheet : uint8_t
{
    OverworldInterior,
    OverworldExterior,
    OverworldObjects,
    OverworldEffects,
    OverworldUserInterface,
    GeneralUserInterface,
    Buildings,
    Characters,
    CharactersEmotes,
    Items,
    BattleInterface,
    BattleTerrains,
    BattleTrainerMugshots,
    BattleCharacterSprites,
    BattleStatusAilments,
    PokeBalls,
    Pokemon
};

template<>
inline String toString<SpriteSheet>(SpriteSheet value)
{
    switch (value)
    {
        case SpriteSheet::OverworldInterior:
        {
            return String(L"OverworldInterior");
        }
        case SpriteSheet::OverworldExterior:
        {
            return String(L"OverworldExterior");
        }
        case SpriteSheet::OverworldObjects:
        {
            return String(L"OverworldObjects");
        }
        case SpriteSheet::OverworldEffects:
        {
            return String(L"OverworldEffects");
        }
        case SpriteSheet::OverworldUserInterface:
        {
            return String(L"OverworldUserInterface");
        }
        case SpriteSheet::GeneralUserInterface:
        {
            return String(L"GeneralUserInterface");
        }
        case SpriteSheet::Buildings:
        {
            return String(L"Buildings");
        }
        case SpriteSheet::Characters:
        {
            return String(L"Characters");
        }
        case SpriteSheet::CharactersEmotes:
        {
            return String(L"CharactersEmotes");
        }
        case SpriteSheet::Items:
        {
            return String(L"Items");
        }
        case SpriteSheet::BattleInterface:
        {
            return String(L"BattleInterface");
        }
        case SpriteSheet::BattleTerrains:
        {
            return String(L"BattleTerrains");
        }
        case SpriteSheet::BattleTrainerMugshots:
        {
            return String(L"BattleTrainerMugshots");
        }
        case SpriteSheet::BattleCharacterSprites:
        {
            return String(L"BattleCharacterSprites");
        }
        case SpriteSheet::BattleStatusAilments:
        {
            return String(L"BattleStatusAilments");
        }
        case SpriteSheet::PokeBalls:
        {
            return String(L"PokeBalls");
        }
        case SpriteSheet::Pokemon:
        {
            return String(L"Pokemon");
        }
    }

    return "<Unrecognized SpriteSheet: " + toString(static_cast<int32_t>(value)) + ">";
};

const int32_t NUMBER_SPRITESHEETS = 17;

#endif

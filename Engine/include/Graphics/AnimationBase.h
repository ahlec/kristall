#ifndef KRISTALL_INCLUDE_GRAPHICS_ANIMATIONBASE_H
#define KRISTALL_INCLUDE_GRAPHICS_ANIMATIONBASE_H

#include "Cornerstones.h"
#include "Graphics/SpriteSheet.h"
#include "Data/Predefine.h"
#include "Graphics/Predefine.h"

class AnimationBase
{
    friend class AnimationInstance;
public:
    AnimationBase(SpriteSheet spriteSheet, uint32_t animationBaseNo,
                  uint32_t nAnimations, BinaryReaderPtr reader);

    SpriteSheet getSpriteSheet() const;
    uint32_t getAnimationBaseNo() const;

    AnimationInstancePtr createInstance() const;
    bool hasAnimation(const std::string& animationHandle) const;
    bool hasAnimation(uint32_t animationNo) const;

    AnimationInformationPtr getAnimation(uint32_t animationNo) const;

private:
    SpriteSheet _spriteSheet;
    uint32_t _animationBaseNo;
    uint32_t _initialAnimationNo;
    std::vector<AnimationInformationPtr> _animations;
};

#endif

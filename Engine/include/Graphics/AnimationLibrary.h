#ifndef GRAPHICS_ANIMATIONLIBRARY_H
#define GRAPHICS_ANIMATIONLIBRARY_H

class AnimationLibrary
{
    friend class SpriteBatch;

public:
    static AnimationInstancePtr createAnimationInstance(SpriteSheets spriteSheet,
                                                        uint32 animationBaseNo);
    static AnimationResolutionResults lookupAnimationNo(SpriteSheets spriteSheet,
                                                        const std::string& animationHandle);
};

#endif

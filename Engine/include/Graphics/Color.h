#ifndef KRISTALL_INCLUDE_GRAPHICS_COLOR_H
#define KRISTALL_INCLUDE_GRAPHICS_COLOR_H

#include "Cornerstones.h"

class Color
{
public:
    Color();
    Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255);

    static const Color White;
    static const Color Black;
    static const Color Red;
    static const Color Green;
    static const Color Blue;
    static const Color Yellow;
    static const Color Magenta;
    static const Color Cyan;
    static const Color Gray;
    static const Color Orange;
    static const Color Pink;

    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
};

#endif

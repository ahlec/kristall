#ifndef KRISTALL_INCLUDE_GRAPHICS_ANIMATIONFRAME_H
#define KRISTALL_INCLUDE_GRAPHICS_ANIMATIONFRAME_H

#include "Cornerstones.h"
#include <memory>

struct AnimationFrame
{
    bool isFrameChanged;
    uint32_t spriteNo;
    uint32_t overlayNo;
};

typedef std::shared_ptr<AnimationFrame> AnimationFramePtr;

#endif

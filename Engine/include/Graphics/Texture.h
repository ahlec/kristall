#ifndef KRISTALL_INCLUDE_GRAPHICS_TEXTURE_H
#define KRISTALL_INCLUDE_GRAPHICS_TEXTURE_H

#include "Cornerstones.h"
#include <SDL.h>

class Texture
{
public:
    Texture(SDL_Texture* texture);
    ~Texture();

    inline SDL_Texture* getSdlTexture() const
    {
        return _texture;
    };

private:
    SDL_Texture* _texture;
};

#endif

#ifndef GRAPHICS_BUTTONSKIN_H
#define GRAPHICS_BUTTONSKIN_H

class SpriteBatch;

class ButtonSkin
{
    friend class SpriteBatch;
public:
    ButtonSkin(BinaryReader& reader);
    ~ButtonSkin();

private:
    sf::Texture _texture;
    bool _isHorizontalTrackTiled;
    int _leftPadding;
    int _rightPadding;
    bool _isTextOutlined;
    FontColor& _fontColor;
    int _minWidth;
    int _combinedHorizontalBordersWidth; // the combined sum of the width of
                                         // the widths of _leftBorder and
                                         // _rightBorder, plus _leftPadding
                                         // and _rightPadding.

    sf::Sprite _leftBorder;
    sf::Sprite _horizontalTrack;
    sf::Sprite _rightBorder;
    sf::Sprite _leftBorderSelected;
    sf::Sprite _horizontalTrackSelected;
    sf::Sprite _rightBorderSelected;
};

#endif

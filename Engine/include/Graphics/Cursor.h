#ifndef GRAPHICS_CURSOR_H
#define GRAPHICS_CURSOR_H

enum class Cursor
{
    None,
    Pointer,
    Holding
};

#endif

#ifndef KRISTALL_INCLUDE_GRAPHICS_USERINTERFACESPRITES_H
#define KRISTALL_INCLUDE_GRAPHICS_USERINTERFACESPRITES_H

namespace UserInterfaceSprites
{
    const uint16_t MENU_SELECTION_ARROW = 9;
    const uint16_t SINGLE_PIXEL         = 10;
};

#endif

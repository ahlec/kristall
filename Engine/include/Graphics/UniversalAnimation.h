#ifndef KRISTALL_INCLUDE_GRAPHICS_UNIVERSALANIMATION_H
#define KRISTALL_INCLUDE_GRAPHICS_UNIVERSALANIMATION_H

#include "Cornerstones.h"
#include "Data/Predefine.h"

class UniversalAnimation
{
    friend class SpriteSheetLibrary;
public:
    UniversalAnimation(const BinaryReaderPtr& reader);

    /// bool update(uint32_t elapsedTime)
    ///     Advances the UniversalAnimation forward by
    ///     the amount of time, adjusting the current
    ///     frame accordingly.
    ///
    ///     Returns: true - if the current sprite has changed
    ///              false - the current sprite remained the same
    bool update(uint32_t elapsedTime);

    uint32_t getCurrentSpriteNo() const;

private:
    class AnimationFrame
    {
    public:
        AnimationFrame(const BinaryReaderPtr& reader);

        uint32_t spriteNo;
        uint16_t timeOffset;
    };

    uint32_t _spriteNo;
    uint16_t _duration;
    uint32_t _currentTime;
    uint16_t _currentFrame;
    std::vector<AnimationFrame> _frames;
};

#endif

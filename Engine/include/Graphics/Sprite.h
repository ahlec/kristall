#ifndef KRISTALL_INCLUDE_GRAPHICS_SPRITE_H
#define KRISTALL_INCLUDE_GRAPHICS_SPRITE_H

#include "Cornerstones.h"
#include "Data/Predefine.h"
#include "General/Vector2.h"

class Sprite
{
public:
    Sprite();
    Sprite(BinaryReaderPtr& reader);

    Vector2 topLeftCoords;
    Vector2 topRightCoords;
    Vector2 bottomRightCoords;
    Vector2 bottomLeftCoords;
    uint16_t width;
    uint16_t height;
    int32_t ingrainedOffsetX;
    int32_t ingrainedOffsetY;
};

#endif

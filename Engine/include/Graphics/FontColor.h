#ifndef KRISTALL_INCLUDE_GRAPHICS_FONTCOLOR_H
#define KRISTALL_INCLUDE_GRAPHICS_FONTCOLOR_H

#include "Cornerstones.h"
#include "Graphics/Predefine.h"

enum class FontColor
{
    Black,
    White,
    Red,
    Blue,
    Pink,
    LightBlue,
    Orange,
    DarkBlack,
    Beige,
    Yellow,
    Crimson,
    Gold,
    TrueBlue,
    Mono,
    Gray
};

namespace FontColors
{
    Color getTextColor(FontColor color);
    Color getShadowColor(FontColor color);
};

#endif

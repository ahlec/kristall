#ifndef KRISTALL_INCLUDE_GRAPHICS_FRAME_H
#define KRISTALL_INCLUDE_GRAPHICS_FRAME_H

#include "Graphics/Predefine.h"
#include "Graphics/Sprite.h"

class Frame
{
    friend class SpriteBatch;
public:
    static FramePtr   get(uint16_t frameNo);

private:
    Frame(BinaryReaderPtr reader);

    uint32_t     m_topLeftSpriteNo;
    uint32_t     m_topSpriteNo;
    uint32_t     m_topRightSpriteNo;
    uint32_t     m_leftSpriteNo;
    uint32_t     m_centerSpriteNo;
    uint32_t     m_rightSpriteNo;
    uint32_t     m_bottomLeftSpriteNo;
    uint32_t     m_bottomSpriteNo;
    uint32_t     m_bottomRightSpriteNo;
};

#endif

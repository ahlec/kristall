#ifndef KRISTALL_INCLUDE_GRAPHICS_ANIMATIONINFORMATION_H
#define KRISTALL_INCLUDE_GRAPHICS_ANIMATIONINFORMATION_H

#include "Cornerstones.h"
#include "Data/Predefine.h"
#include "Graphics/Predefine.h"
#include "Graphics/AnimationFrame.h"

class AnimationInformation
{
    friend class AnimationBase;
public:
    AnimationInformation(uint32_t animationNo, BinaryReaderPtr reader);
    virtual ~AnimationInformation();

    uint32_t getAnimationNo() const;

    AnimationFramePtr getFrame(uint32_t atTime, uint32_t lastTime,
                               bool isZeroDurationAllowed = false) const;
    bool getIsLooping() const;
    bool getIsDeadEnd() const;
    uint32_t getReturnsToAnimationNo() const;

private:
    class Keyframe
    {
    public:
        Keyframe(const BinaryReaderPtr& reader);

        uint32_t spriteNo;
        uint32_t overlaySpriteNo;
        uint16_t timeOffset;
    };

    uint32_t _animationNo;
    bool _isLooping;
    bool _isDeadEnd; // true - there is no animation that this animation
                     //        returns to when it finishes
                     // false - this animation, when it finishes, has
                     //         an animation (specified by _returnsToAnimationNo)
                     //         that it "returns" to
    uint32_t _returnsToAnimationNo;
    bool _hasSoundEffect;
    uint16_t _soundEffectNo;
    uint16_t _duration;
    std::vector<Keyframe> _frames;

};

#endif

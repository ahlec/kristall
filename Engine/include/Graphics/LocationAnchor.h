#ifndef KRISTALL_INCLUDE_GRAPHICS_LOCATIONANCHOR_H
#define KRISTALL_INCLUDE_GRAPHICS_LOCATIONANCHOR_H

enum class LocationAnchor
{
    TopLeft,
    BottomRight
};

#endif

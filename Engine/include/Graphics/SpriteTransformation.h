#ifndef GRAPHICS_SPRITETRANSFORMATION_H
#define GRAPHICS_SPRITETRANSFORMATION_H

enum class SpriteTransformation : uint8_t
{
    None,
    Rotate90,
    Rotate180,
    Rotate270,
    Rotate45,
    Rotate225,
    FlipHorizontal,
    FlipVertical
};

#endif

#ifndef GRAPHICS_ANIMATIONRESOLUTIONRESULTS_H
#define GRAPHICS_ANIMATIONRESOLUTIONRESULTS_H

struct AnimationResolutionResults
{
    bool doesNameExist;
    uint32_t animationNo;
};

#endif

#ifndef KRISTALL_INCLUDE_GRAPHICS_PREDEFINE_H
#define KRISTALL_INCLUDE_GRAPHICS_PREDEFINE_H

#include <memory>
#include "Data/CachePointer.h"

class AnimationBase;
class AnimationInformation;
class AnimationInstance;
class Color;
class Frame;
class RenderText;
class SDL_Renderer;
class Sprite;
class SpriteBatch;
class SpriteSheetLibrary;
class Texture;

typedef std::shared_ptr<AnimationBase> AnimationBasePtr;
typedef std::shared_ptr<const AnimationBase> AnimationBaseCPtr;
typedef std::shared_ptr<AnimationInformation> AnimationInformationPtr;
typedef std::shared_ptr<AnimationInstance> AnimationInstancePtr;
typedef SpriteSheetLibrary* SpriteSheetLibraryPtr;
typedef CachePointer<Frame> FramePtr;

#endif

#ifndef KRISTALL_INCLUDE_GRAPHICS_SPRITEBATCH_H
#define KRISTALL_INCLUDE_GRAPHICS_SPRITEBATCH_H

#include "Cornerstones.h"
#include "Graphics/Predefine.h"
#include "Graphics/SpriteSheet.h"
#include "Graphics/LocationAnchor.h"
#include "General/Rect.h"
#include "Graphics/Color.h"
#include "Graphics/Font.h"
#include "Graphics/FontColor.h"
#include <forward_list>
#include <map>

/**  class SpriteBatch
 *
 * The cornerstone for rendering in the application. Exposes functions that allow for drawing to the screen.
 * The underlying architecture will batch together sprites and ensure that the calls for drawing are optimal
 * while also producing the intended result.
 *
 * NOTE: SpriteBatch is not thread-safe due to both concept (only the main frame should be doing drawing and
 * major updating) as well as by necessity (the amount of overhead trying to make this needlessly thread-safe
 * would be programmatically and computationally expensive). However, one should not need to tread with caution,
 * as only the main thread should be doing any rendering the in the first place (right? :P)
 **/

class SpriteBatch
{
    friend class GameWindow;
public:
    /** -------- Textures -------- **/
    void drawTexture(uint32_t textureNo, int screenX, int screenY, int width, int height, int textureX,
                     int textureY, const Color& tint = Color::White, uint16_t layer = 0);

    /** -------- Sprites -------- **/
    void drawSprite(SpriteSheet spriteSheet, uint32_t spriteNo, int x, int y,
        LocationAnchor anchor = LocationAnchor::TopLeft, const Color& tint = Color::White,
        uint16_t layer = 0);
    void tileSprite(SpriteSheet spriteSheet, uint32_t spriteNo, int x, int y, int tileWidth, int tileHeight,
        LocationAnchor anchor = LocationAnchor::TopLeft, const Color& tint = Color::White,
        uint16_t layer = 0);

    /** -------- Primitives -------- **/
    void drawLine(int x1, int y1, int x2, int y2, const Color& tint);
    void drawRectangle(Rect rectangle, const Color& tint);

    /** -------- Fonts/Text -------- **/
    void drawString(UnicodeString text, Font font, FontColor color, int x, int y, uint8_t alpha = 255);
    void drawString(std::string text, Font font, FontColor color, int x, int y, uint8_t alpha = 255);
    void drawOutlinedString(UnicodeString text, Font font, FontColor color, int x, int y, uint8_t alpha = 255);
    void drawOutlinedString(std::string text, Font font, FontColor color, int x, int y, uint8_t alpha = 255);

    /** -------- GUI elements -------- **/
    void drawFrame(FramePtr frame, Rect rectangle, const Color& tint = Color::White);

private:
    struct BatchNode
    {
        BatchNode* previous;
        BatchNode* next;

        uint32_t textureId;
        Rect sourceRect;
        Rect destRect;
        Color tinting;
    };
    struct SurfaceRender
    {
        SDL_Surface* surface;
        Rect destRect;
    };

    SpriteBatch(SpriteSheetLibrary* spriteSheetLibrary);
    void reset(); // Reset is ALWAYS called before drawing begins (even before first draw iteration)
    void addBatch(uint32_t textureId, const Rect& sourceRect, const Rect& destRect, const Color& tinting,
        uint16_t layer);

    /// ------------------------------------------------ Resources
    SpriteSheetLibrary*             m_spriteSheetLibrary;

    /// ------------------------------------------------ Batching
    std::vector<BatchNode>          m_nodePool;
    size_t                          m_nodePoolNextIndex;
    BatchNode*                      m_head;
    BatchNode*                      m_tail;
    std::map<uint16_t, BatchNode*>  m_layerLookup; // Pointers to the last nodes for each layer

    /// m_surfaces
    /// After batches have been drawn, surfaces will be iterated over and rendered. These are without layers,
    /// and so are drawn in whatever order the draw functions are called.
    std::vector<SurfaceRender>      m_surfaces;

};

#endif

#ifndef KRISTALL_INCLUDE_GRAPHICS_DEVELOPERCOLORSWATCH_H
#define KRISTALL_INCLUDE_GRAPHICS_DEVELOPERCOLORSWATCH_H

#include "Cornerstones.h"
#include "Graphics/Predefine.h"

class DeveloperColorSwatch
{
public:
    DeveloperColorSwatch(int offset = 0);

    Color getColor(int number) const;

private:
    int _offset;
};

#endif

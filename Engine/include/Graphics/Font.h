#ifndef KRISTALL_INCLUDE_GRAPHICS_FONT_H
#define KRISTALL_INCLUDE_GRAPHICS_FONT_H

#include "Cornerstones.h"
#include <SDL_ttf.h>

enum class Font
{
    Normal,
    Battle,
    SystemMessage
};

namespace Fonts
{
    TTF_Font* getTtfFont(Font font);
};

#endif

#ifndef KRISTALL_INCLUDE_GRAPHICS_ANIMATIONINSTANCE_H
#define KRISTALL_INCLUDE_GRAPHICS_ANIMATIONINSTANCE_H

#include "Cornerstones.h"
#include "Graphics/Predefine.h"
#include "Graphics/Delegates.h"

class AnimationInstance
{
public:
    AnimationInstance(const AnimationBase& base, uint32_t initialAnimation);

    uint32_t getAnimationBaseNo() const;
    bool isPlaying() const;
    uint32_t getCurrentAnimationNo() const;

    uint32_t getSpriteNo() const;
    bool getDoesHaveOverlay() const;
    uint32_t getOverlaySpriteNo() const;

    bool hasAnimation(const std::string& animationHandle) const;
    bool hasAnimation(uint32_t animationNo) const;

    void playAnimation(const std::string& animationHandle,
                       AnimationFinishedDelegate animationFinished = nullptr,
                       float playSpeed = 1.0f);
    void playAnimation(uint32_t animationNo,
                       AnimationFinishedDelegate animationFinished = nullptr,
                       float playSpeed = 1.0f);
    void enqueueNextAnimation(const std::string& animationHandle,
                              AnimationFinishedDelegate animationFinished = nullptr,
                              float playSpeed = 1.0f);
    void enqueueNextAnimation(uint32_t animationNo,
                              AnimationFinishedDelegate animationFinished = nullptr,
                              float playSpeed = 1.0f);

    /// AnimationInstance::update
    /// Parameters: uint32_t elapsedTime - the amount of time that has elapsed
    /// Returns: bool - True if the currently playing animation finishes; false if
    ///                 there is no currently playing animation, or if the animation
    ///                 does not finish during this call.
    /// Notes: While within this function, the AnimationInstance is volatile and
    ///        the class is not thread-safe. This function, as with all update functions,
    ///        needs to be used only within a critical section, and the instance
    ///        of the class should not be touched while this function is live.
    bool update(uint32_t elapsedTime);

    /// AnimationInstance::isNewKeyframe
    /// Returns: bool - True if the most immediate call to ::update() caused a change
    ///                 in the current frame; false otherwise. A new keyframe will
    ///                 most likely mean a change in current spriteNo, a change in
    ///                 overlay, or something similar.
    bool isNewKeyframe() const;

private:
    struct AnimationQueueNode
    {
        uint32_t animationNo;
        AnimationFinishedDelegate animationFinished;
        float playSpeed;
    };
    typedef std::shared_ptr<AnimationQueueNode> AnimationQueueNodePtr;

    inline void clearPlayQueue()
    {
        while (!_playQueue.empty())
        {
            _playQueue.pop();
        }
    };
    void playNextAnimationEnqueued();
    void beginAnimation(uint32_t animationNo, AnimationFinishedDelegate animationFinished,
                        float playSpeed);

    const AnimationBase& _animationBase;
    bool _isNewKeyframe;
    AnimationQueueNodePtr _immediatelyPlayAnimation;
    std::queue<AnimationQueueNode> _playQueue;

    AnimationInformationPtr _currentAnimation;
    uint32_t _currentSpriteNo;
    bool _currentHasOverlay;
    uint32_t _currentOverlaySpriteNo;
    uint32_t _elapsedTime;
    bool _isPlayingNormalSpeed;
    float _playSpeed;
    AnimationFinishedDelegate _animationFinished;
};

#endif

#ifndef KRISTALL_INCLUDE_GRAPHICS_ANIMATIONS_H
#define KRISTALL_INCLUDE_GRAPHICS_ANIMATIONS_H

#include "Cornerstones.h"
#include "Characters/TransportMode.h"
#include "General/Direction.h"

namespace Animations
{
    namespace Characters
    {
        // Idling (looking in the directions)
        const uint32_t NORTH = 1;
        const uint32_t SOUTH = 2;
        const uint32_t EAST = 3;
        const uint32_t WEST = 4;

        // Walking
        const uint32_t WALK_NORTH_LEFT_LEG = 5;
        const uint32_t WALK_NORTH_RIGHT_LEG = 7;
        const uint32_t WALK_SOUTH_LEFT_LEG = 9;
        const uint32_t WALK_SOUTH_RIGHT_LEG = 11;
        const uint32_t WALK_EAST_LEFT_LEG = 13;
        const uint32_t WALK_EAST_RIGHT_LEG = 15;
        const uint32_t WALK_WEST_LEFT_LEG = 17;
        const uint32_t WALK_WEST_RIGHT_LEG = 19;

        // Running
        const uint32_t RUN_NORTH_LEFT_LEG = 6;
        const uint32_t RUN_NORTH_RIGHT_LEG = 8;
        const uint32_t RUN_SOUTH_LEFT_LEG = 10;
        const uint32_t RUN_SOUTH_RIGHT_LEG = 12;
        const uint32_t RUN_EAST_LEFT_LEG = 14;
        const uint32_t RUN_EAST_RIGHT_LEG = 16;
        const uint32_t RUN_WEST_LEFT_LEG = 18;
        const uint32_t RUN_WEST_RIGHT_LEG = 20;

        // Surfing
        const uint32_t SURF_NORTH = 25;
        const uint32_t SURF_SOUTH = 26;
        const uint32_t SURF_EAST = 27;
        const uint32_t SURF_WEST = 28;

        // Biking
        const uint32_t BIKE_NORTH_IDLE = 29;
        const uint32_t BIKE_SOUTH_IDLE = 32;
        const uint32_t BIKE_EAST_IDLE = 35;
        const uint32_t BIKE_WEST_IDLE = 38;

        const uint32_t BIKE_NORTH_LEFT_LEG = 30;
        const uint32_t BIKE_NORTH_RIGHT_LEG = 31;
        const uint32_t BIKE_SOUTH_LEFT_LEG = 33;
        const uint32_t BIKE_SOUTH_RIGHT_LEG = 34;
        const uint32_t BIKE_EAST_LEFT_LEG = 36;
        const uint32_t BIKE_EAST_RIGHT_LEG = 37;
        const uint32_t BIKE_WEST_LEFT_LEG = 39;
        const uint32_t BIKE_WEST_RIGHT_LEG = 40;

        // Helpful functions
        uint32_t getIdleAnimation(TransportMode transportMode, Direction direction);
        uint32_t getMovementAnimation(TransportMode transportMode, Direction direction,
                                    bool isLeadingWithLeftLeg);
    };
};

#endif

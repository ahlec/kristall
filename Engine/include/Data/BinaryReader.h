#ifndef KRISTALL_INCLUDE_DATA_BINARYREADER_H
#define KRISTALL_INCLUDE_DATA_BINARYREADER_H

#include "General/ElementalType.h"
#include "General/TimeSpan.h"
#include "General/DateTime.h"
#include "Lua/Predefine.h"
#include "General/Direction.h"
#include "General/Function.h"
#include "Data/FileType.h"
#include "Graphics/Predefine.h"
#include "General/Rect.h"
#include <SDL.h>

class BinaryReader
{
    friend class DataManager;
public:
    ~BinaryReader();
    void close();

    String readString();
    std::string readStandardString();

    uint32_t readUInt32();
    int32_t readInt32();

    uint16_t readUInt16();
    int16_t readInt16();

    uint8_t readUInt8();
    int8_t readInt8();
    bool readBool();
    ElementalType readElementalType();
    Direction readDirection();

    LuaScriptPtr readLuaScript();

    uint64_t readUInt64();
    int64_t readInt64();

    float readFloat();

    void readTimeSpan(TimeSpan*);
    void readDateTime(DateTime*);

    Texture* readTexture(SDL_Renderer* renderer);

    Rect readRect();

protected:
    BinaryReader(FILE* file, FileType fileType, uint16_t fileNo,
                 uint32_t size, FunctionPtr<void> onClose = nullptr);

    SDL_Surface* readSdlSurface();

private:
    FILE* _file;
    FileType _fileType;
    uint16_t _fileNo;
    uint32_t _size;
    FunctionPtr<void> _closeCallback;
    uint32_t _sizeDataRead;

    void readRawData(void* data, size_t elementSize, size_t nElements);
};

#endif

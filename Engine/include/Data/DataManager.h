#ifndef KRISTALL_INCLUDE_DATA_DATAMANAGER_H
#define KRISTALL_INCLUDE_DATA_DATAMANAGER_H

#include "Cornerstones.h"
#include "Data/FileType.h"
#include "Data/Predefine.h"
#include "General/Mutex.h"
#include "General/Function.h"
#include "Data/FileEntry.h"

class DataManager
{
public:
    static void                 initialize();
    static void                 deinitialize();

    static uint16_t             getNumberFiles(FileType fileType);
    static BinaryReaderPtr      openFileReader(FileType fileType, uint16_t fileNo);

private:
    static FILE*                _file;
    static bool                 _isReaderOpen;

    static void readHeaderBlock(uint16_t* nFiles, FileEntry** fileEntries);
    static void onBinaryReaderClosed();
};

#endif

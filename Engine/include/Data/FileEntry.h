#ifndef KRISTALL_INCLUDE_DATA_FILEENTRY_H
#define KRISTALL_INCLUDE_DATA_FILEENTRY_H

struct FileEntry
{
    uint64_t    offset;
    uint32_t    size;
    uint8_t     trashBit0;
    uint8_t     trashBit1;
    uint8_t     trashBit2;
    uint8_t     trashBit3;
};

#endif

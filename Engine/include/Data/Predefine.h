#ifndef KRISTALL_INCLUDE_DATA_PREDEFINE_H
#define KRISTALL_INCLUDE_DATA_PREDEFINE_H

#include <memory>

class BinaryReader;
class BinaryWriter;
template <class T>
class Cache;

typedef std::shared_ptr<BinaryReader> BinaryReaderPtr;
typedef std::shared_ptr<BinaryWriter> BinaryWriterPtr;

#endif

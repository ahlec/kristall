#ifndef KRISTALL_INCLUDE_DATA_BINARYWRITER_H
#define KRISTALL_INCLUDE_DATA_BINARYWRITER_H

#include "Cornerstones.h"
#include "General/ElementalType.h"
#include "General/Direction.h"
#include "General/TimeSpan.h"
#include "General/DateTime.h"
#include "General/Rect.h"

class BinaryWriter
{
public:
    BinaryWriter(FILE* file);
    ~BinaryWriter();

    void close();

    uint64_t getBytesWrittenCount() const;
    void resetBytesWrittenCount();

    void writeString(String);
    void writeStandardString(std::string);

    void writeUInt16(uint16_t);
    void writeInt16(int16_t);

    void writeUInt32(uint32_t);
    void writeInt32(int32_t);

    void writeRect(Rect);

    void writeUInt8(uint8_t);
    void writeBool(bool);
    void writeElementalType(ElementalType);
    void writeDirection(Direction);

    void writeInt8(int8_t);
    void writeCharArray(const char*, uint32_t);

    void writeUInt64(uint64_t);
    void writeInt64(int64_t);

    void writeFloat(float);

    void writeTimeSpan(TimeSpan);
    void writeDateTime(DateTime);

    void writeRawBytes(const void*, size_t, size_t);

protected:
    void writeBytes(const void*, size_t, size_t);

private:
    FILE* _file;
    uint64_t _bytesWritten;

};

#endif

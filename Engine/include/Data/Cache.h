#ifndef KRISTALL_INCLUDE_DATA_CACHE_H
#define KRISTALL_INCLUDE_DATA_CACHE_H

#include "Data/CachePointer.h"
#include "Data/DataManager.h"
#include "General/MutexScopeLock.h"

template <class T>
class Cache
{
public:
    Cache(FileType fileType) : _fileType(fileType), _nFiles(0), _nodes(nullptr), _mutex(new Mutex())
    {
    }

    ~Cache()
    {
        if (_nodes != nullptr)
        {
            delete[] _nodes;
            _nodes = nullptr;
        }

        if (_mutex != nullptr)
        {
            delete _mutex;
        }
    }

    CachePointer<T> get(uint32_t handleNo)
    {
        if (_nodes == nullptr)
        {
            initialize();
        }

        return CachePointer<T>(_nodes[handleNo].pointer);
    }

    CachePointer<T> add(uint32_t handleNo, T* value)
    {
        if (_nodes == nullptr)
        {
            initialize();
        }

        if (_nodes[handleNo].pointer != nullptr)
        {
            ExecutionException error("Collision within the cache.");
            error.addData("handleNo", toString(handleNo));
            error.addData("fileType", toString(_fileType));
            throw error;
        }

        _nodes[handleNo].pointer = value;
        return CachePointer<T>(_nodes[handleNo].pointer);
    }

    void clear()
    {

    }

private:
    class Node
    {
    public:
        Node() : pointer(nullptr)
        {
        }

        ~Node()
        {
            if (pointer != nullptr)
            {
                delete pointer;
                pointer = nullptr;
            }
        }

        T* pointer;
    };

    FileType _fileType;
    uint32_t _nFiles;
    Node* _nodes;
    Mutex* _mutex;

    void initialize()
    {
        MutexScopeLock lock(_mutex);
        _nFiles = DataManager::getNumberFiles(_fileType);
        _nodes = new Node[_nFiles];
    }

};

#endif

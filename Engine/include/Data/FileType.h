#ifndef KRISTALL_INCLUDE_DATA_FILETYPE_H
#define KRISTALL_INCLUDE_DATA_FILETYPE_H

#include "Cornerstones.h"

enum class FileType : uint8_t
{
    SpriteSheet,
    GymBadge,
    PokemonMove,
    Item,
    PokemonSpecies,
    Map,
    Region,
    PlayerGame,
    GameSettings,
    Texture,
    Frame,

    // Obviously not a valid value to use with DataManager
    COUNT
};

#define IS_SPECIAL_FILE_TYPE(fileType)      ((fileType) == FileType::PlayerGame || (fileType) == FileType::GameSettings)

template<> String toString<FileType>(FileType type);

#endif

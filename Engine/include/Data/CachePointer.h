#ifndef KRISTALL_INCLUDE_DATA_CACHEPOINTER_H
#define KRISTALL_INCLUDE_DATA_CACHEPOINTER_H

#include "Data/Predefine.h"

template <class T>
class CachePointer
{
    friend class Cache<T>;
public:
    CachePointer() : _internalPointer(nullptr)
    {
    }

    CachePointer(std::nullptr_t nullVal) : _internalPointer(nullptr)
    {
    }

    CachePointer(const CachePointer<T>& other) : _internalPointer(other._internalPointer)
    {
    }

    ~CachePointer()
    {
        _internalPointer = nullptr;
    }

    T& operator*() const
    {
        return *_internalPointer;
    }

    T* operator->() const
    {
        return _internalPointer;
    }

    T* get() const
    {
        return _internalPointer;
    }


    CachePointer<T>& operator=(const CachePointer<T>& other)
    {
        _internalPointer = other._internalPointer;

        return *this;
    }

    CachePointer<T>& operator=(const std::nullptr_t other)
    {
        _internalPointer = nullptr;

        return *this;
    }

    bool operator==(const std::nullptr_t other) const
    {
        return (_internalPointer == nullptr);
    }

    bool operator==(const CachePointer<T>& other) const
    {
        return (other._internalPointer == _internalPointer);
    }

    bool operator!=(const std::nullptr_t other) const
    {
        return !(*this == other);
    }

    bool operator!=(const CachePointer<T>& other) const
    {
        return !(*this == other);
    }

private:
    CachePointer(T* pointer) : _internalPointer(pointer)
    {
    }

    T* _internalPointer;
};

#endif

#ifndef KRISTALL_INCLUDE_ERRORS_ERRORHANDLING_H
#define KRISTALL_INCLUDE_ERRORS_ERRORHANDLING_H

#include "Cornerstones.h"
#include "Errors/ExecutionException.h"

namespace ErrorHandler
{
    void initializeErrorHandler();
    void reportException(ExecutionException);
};

#endif

#ifndef KRISTALL_INCLUDE_ERRORS_ISSUE_H
#define KRISTALL_INCLUDE_ERRORS_ISSUE_H

#include "Cornerstones.h"
#include "Errors/Reproducability.h"
#include "Errors/IssueType.h"
#include "Errors/ErrorCategory.h"
#include "Errors/ExecutionException.h"

class Issue
{
public:
    Issue();
    void submit();

    void setTitle(String title);
    void setDescription(String description);
    void setReproduction(Reproducability reproducability, String steps);
    void setType(IssueType type);
    void setCategory(ErrorCategory category);
    void setException(ExecutionException error, bool isFatal);

private:
    String _title;
    String _description;
    String _reproductionSteps;
    IssueType _type;
    Reproducability _reproducability;
    ErrorCategory _category;
    ExecutionException* _exception;
    bool _hasException;
    bool _isFatalException;
};

#endif

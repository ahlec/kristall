#ifndef KRISTALL_INCLUDE_ERRORS_SIGNALS_H
#define KRISTALL_INCLUDE_ERRORS_SIGNALS_H

#include "Cornerstones.h"

namespace Signals
{
    void initializeHandlers();
};

#endif

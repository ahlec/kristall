#ifndef KRISTALL_INCLUDE_ERRORS_ISSUETYPE_H
#define KRISTALL_INCLUDE_ERRORS_ISSUETYPE_H

enum class IssueType
{
    BugReport,
    FeatureRequest,
    Enhancement,
    Task,
    UserStory,
    Idea,
    CrashReport
};

#endif

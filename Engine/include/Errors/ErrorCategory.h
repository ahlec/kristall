#ifndef KRISTALL_INCLUDE_ERRORS_ERRORCATEGORY_H
#define KRISTALL_INCLUDE_ERRORS_ERRORCATEGORY_H

#include "Cornerstones.h"

enum class ErrorCategory
{
    NotSpecified,
    General,
    Security,
    UserInterface
};

#endif

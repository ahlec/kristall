#ifndef ERRORS_CHASSIS_H
#define ERRORS_CHASSIS_H

enum class Chassis
{
    NotSpecified,
    Other,
    Unknown,
    Desktop,
    LowProfileDesktop,
    PizzaBox,
    MiniTower,
    Tower,
    Portable,
    Laptop,
    Notebook,
    HandHeld,
    DockingStation,
    AllInOne,
    SubNotebook,
    SpaceSaving,
    LunchBox,
    MainSystemChassis,
    ExpansionChassis,
    SubChassis,
    BusExpansionChassis,
    PeripheralChassis,
    StorageChassis,
    RackMountChassis,
    SealedCasePC
};

#endif

#ifndef KRISTALL_INCLUDE_ERRORS_REPRODUCABILITY_H
#define KRISTALL_INCLUDE_ERRORS_REPRODUCABILITY_H

enum class Reproducability
{
    NotSpecified,
    CantReproduce = 9,
    Rarely = 10,
    Often = 11,
    Always = 12
};

#endif

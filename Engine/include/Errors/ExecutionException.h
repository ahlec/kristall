#ifndef KRISTALL_INCLUDE_ERRORS_EXECUTIONEXCEPTION_H
#define KRISTALL_INCLUDE_ERRORS_EXECUTIONEXCEPTION_H

#include "Cornerstones.h"
#include <list>
#include <map>

class ExecutionException
{
    friend class Issue;
public:
    ExecutionException(String errorMessage);
    ExecutionException(const ExecutionException& other);
    ExecutionException& operator=(const ExecutionException& other);

    String getErrorMessage() const;
    String getStackTrace() const;
    std::map<String, String> getData() const;

    void addData(String key, String value);
    template<typename T>
    void addData(String key, T value)
    {
        addData(key, toString(value));
    };

private:
    struct DataNode
    {
        String key;
        String value;
    };

    String _message;
    std::string _stackTrace;
    std::list<DataNode> _data;
};

#endif

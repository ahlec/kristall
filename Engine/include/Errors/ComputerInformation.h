#ifndef KRISTALL_INCLUDE_ERRORS_COMPUTERINFORMATION_H
#define KRISTALL_INCLUDE_ERRORS_COMPUTERINFORMATION_H

#include "Cornerstones.h"

namespace ComputerInformation
{
    std::string getComputerName();
    std::string getOperatingSystemName();
    bool getIsComputer64Bit();
    std::string getComputerManufacturer();
    std::string getComputerModel();
    std::string getComputerType();
    std::string getSystemMemory();
};

#endif

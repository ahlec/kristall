#ifndef KRISTALL_INCLUDE_WORLD_TILENATURE_H
#define KRISTALL_INCLUDE_WORLD_TILENATURE_H

#include "World/TileNatureType.h"
#include "General/Direction.h"

struct TileNature
{
    TileNatureType type;
    bool hasDirection;
    Direction direction;
};

#endif

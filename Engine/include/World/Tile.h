#ifndef KRISTALL_INCLUDE_WORLD_TILE_H
#define KRISTALL_INCLUDE_WORLD_TILE_H

#include "Cornerstones.h"
#include "World/Predefine.h"
#include "World/Delegates.h"
#include "World/TileType.h"
#include "World/TileNature.h"
#include "World/Terrain.h"
#include "Lua/Predefine.h"
#include "World/Effects/OverworldEffectType.h"
#include "Items/Predefine.h"
#include "Graphics/Delegates.h"
#include "Characters/Predefine.h"
#include "Player/Predefine.h"

class Tile
{
    friend class Map;
    friend class Overworld;
    friend class OverworldOccupant;
public:
    virtual ~Tile();

    void suppressNextOccupationAlert();

    virtual bool isPreparationRequiredForOccupation() const;
    virtual void prepareForOccupant(OverworldOccupantPtr newOccupant,
                                    OccupantMayEnterTileDelegate callback);
    virtual void occupantHasLeft(OverworldOccupantPtr departedOccupant);

    virtual TileType getType() const;
    uint16_t getX() const;
    uint16_t getY() const;
    MapPtr getMap() const;
    TileNature getNature() const;
    Terrain getTerrain() const;
    bool hasScript() const;
    LuaScriptPtr getScript() const;
    bool hasEncounterGroup() const;
    EncounterGroupPtr getEncounterGroup() const;
    bool isOverworldEffectTrigger() const;
    OverworldEffectType getTriggeredOverworldEffect() const;
    Direction getOverworldEffectTriggerDirection() const;
    OverworldItemPtr getNonVisibleItem() const;

    const OverworldOccupantPtr& getCurrentOccupant() const;
    virtual bool hasOccupant() const;
    void setCurrentOccupant(OverworldOccupantPtr newOccupant);

    void playAnimation(const std::string& animationHandle,
                       AnimationFinishedDelegate animationFinished = nullptr);
    void playAnimation(uint16_t animationNo,
                       AnimationFinishedDelegate animationFinished = nullptr);
    bool hasAnimation(const std::string& animationHandle) const;
    bool hasAnimation(uint16_t animationNo) const;

    virtual Direction getInwardArrow() const;
    virtual Tile* getDestination() const;

    bool operator==(const Tile& other) const;
    bool operator!=(const Tile& other) const;

protected:
    Tile(InternalMapPtr map, uint16_t x, uint16_t y, BinaryReaderPtr reader, PlayerGame* playerGame);

    virtual void readHeader(BinaryReaderPtr reader);

    virtual void playerHasArrived(PlayerCharacter* character);
    virtual bool isHandlingPlayerArrivalPersonally();

private:
    InternalMapPtr _map;
    uint16_t _x;
    uint16_t _y;
    TileNature _nature;
    Terrain _terrain;
    int16_t _scriptNo;
    int16_t _encounterGroupNo;
    OverworldEffectType _triggeredOverworldEffect;
    Direction _overworldEffectTriggerDirection;
    OverworldItemPtr _nonVisibleItem;
    bool _hasAnimationInstance;
    uint32_t _animationInstanceIndex;

    OverworldOccupantPtr _currentOccupant;
    bool _isNextOccupationAlertSuppressed;

    void occupantLeaving(OverworldOccupantPtr oldOccupant);
    void occupantChanged(OverworldOccupantPtr oldOccupant,
                         OverworldOccupantPtr newOccupant);

};

template<> String toString<Tile>(Tile value);

#endif

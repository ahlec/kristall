#ifndef KRISTALL_INCLUDE_WORLD_OVERWORLDENTRY_H
#define KRISTALL_INCLUDE_WORLD_OVERWORLDENTRY_H

#include "Cornerstones.h"
#include "World/Predefine.h"
#include "Player/Predefine.h"
#include "Graphics/Predefine.h"

class OverworldEntry
{
public:
    OverworldEntry(int32_t screenX, int32_t screenY, int32_t textureX,
                   int32_t textureY, int32_t width, int32_t height,
                   const RegionOverworldZone* zone);

    MapPtr getMap() const;

    int64_t update(uint32_t elapsedTime, Overworld& overworld, PlayerGame* currentPlayerGame);
    void drawBackground(SpriteBatch* spriteBatch) const;
    void drawOccupants(SpriteBatch* spriteBatch) const;
    void drawForeground(SpriteBatch* spriteBatch) const;

private:
    int32_t _screenX;
    int32_t _screenY;
    int32_t _textureX;
    int32_t _textureY;
    int32_t _width;
    int32_t _height;
    const RegionOverworldZone* _zone;

    void drawOverflows(SpriteBatch* spriteBatch, bool isSuperlayer = false) const;
};

#endif

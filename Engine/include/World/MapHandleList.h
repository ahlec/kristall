#ifndef KRISTALL_INCLUDE_WORLD_MAPHANDLELIST_H
#define KRISTALL_INCLUDE_WORLD_MAPHANDLELIST_H

#include "Cornerstones.h"
#include "General/BinarySearchTree.h"

class MapHandleList
{
public:
    MapHandleList();

    void add(int64_t mapNo);
    bool contains(int64_t mapNo) const;
    bool contains(uint32_t mapNo) const;
    void clear();
    std::vector<uint32_t> getAll() const;

private:
    BinarySearchTree<uint32_t> _bst;
};

#endif

#ifndef KRISTALL_INCLUDE_WORLD_TILENATURETYPE_H
#define KRISTALL_INCLUDE_WORLD_TILENATURETYPE_H

#include "Cornerstones.h"

enum class TileNatureType : uint8_t
{
    Normal,
    Solid,
    Ledge,
    BikePath,
    Water,
    WaterCurrent,
    DeepWater,
    Seafloor,
    Ice,
    Spinner,
    Stopper
};

namespace TileNatureTypes
{
    bool isSurfable(TileNatureType natureType);
};

#endif

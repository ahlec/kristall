#ifndef KRISTALL_INCLUDE_WORLD_DOORTILE_H
#define KRISTALL_INCLUDE_WORLD_DOORTILE_H

#include "World/Tile.h"
#include "World/StaircaseType.h"

class DoorTile : public Tile
{
    friend class Map;
public:
    virtual ~DoorTile() override;

    bool getIsUsedOnExit() const;
    StaircaseType getStaircaseType() const;

    // Tile
    virtual TileType getType() const override;
    virtual bool hasOccupant() const override;
    virtual bool isPreparationRequiredForOccupation() const override;
    virtual void prepareForOccupant(OverworldOccupantPtr newOccupant,
                                    OccupantMayEnterTileDelegate callback) override;
    virtual void occupantHasLeft(OverworldOccupantPtr departedOccupant) override;
    virtual Direction getInwardArrow() const override;
    virtual Tile* getDestination() const override;

protected:
    DoorTile(InternalMapPtr map, uint16_t x, uint16_t y, BinaryReaderPtr reader, PlayerGame* playerGame);

    // Tile
    virtual void readHeader(BinaryReaderPtr reader) override;

private:
    Direction _inwardArrow;
    bool _isUsedOnExit;
    StaircaseType _staircaseType;
    uint32_t _soundEffectNo;
    uint32_t _destinationMapNo;
    uint16_t _destinationTileX;
    uint16_t _destinationTileY;

    OverworldOccupantPtr _arrivingOccupant;

    void characterHasArrived(Character& arrivedCharacter);

};

#endif

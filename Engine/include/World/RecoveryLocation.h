#ifndef KRISTALL_INCLUDE_WORLD_RECOVERYLOCATION_H
#define KRISTALL_INCLUDE_WORLD_RECOVERYLOCATION_H

#include "Cornerstones.h"
#include "General/Direction.h"

class RecoveryLocation
{
public:
    RecoveryLocation(uint16_t tileX, uint16_t tileY, Direction arrivalDirection);

    uint16_t getTileX() const;
    uint16_t getTileY() const;
    Direction getArrivalDirection() const;

private:
    uint16_t _tileX;
    uint16_t _tileY;
    Direction _arrivalDirection;
};

#endif

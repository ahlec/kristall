#ifndef KRISTALL_INCLUDE_WORLD_OVERWORLDOBJECTS_OVERWORLDOBJECT_H
#define KRISTALL_INCLUDE_WORLD_OVERWORLDOBJECTS_OVERWORLDOBJECT_H

#include "Data/Predefine.h"
#include "World/OverworldOccupant.h"
#include "Graphics/Predefine.h"

class OverworldObject : public OverworldOccupant
{
public:
    OverworldObject(BinaryReaderPtr& reader);

    bool getIsMovable() const;
    bool getIsCounter() const;

    // OverworldOccupant
    virtual OverworldOccupantType getType() const final;
    virtual SpriteSheet getSpriteSheet() const final;
    virtual uint32_t getSpriteNo() const final;
    virtual bool getIsSign() const final;
    virtual void activate(Character& activator) final;

protected:
    //OverworldOccupant
    virtual void update(uint32_t elapsedTime) final;

private:
    bool _hasAnimation;
    AnimationInstancePtr _animation;
    uint32_t _spriteNo;
    bool _isMovable;
    bool _isSign;
    bool _isCounter;

};

#endif

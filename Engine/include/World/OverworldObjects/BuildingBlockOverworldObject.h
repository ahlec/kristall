#ifndef KRISTALL_INCLUDE_WORLD_OVERWORLDOBJECTS_BUILDINGBLOCKOVERWORLDOBJECT_H
#define KRISTALL_INCLUDE_WORLD_OVERWORLDOBJECTS_BUILDINGBLOCKOVERWORLDOBJECT_H

#include "World/OverworldOccupant.h"

class BuildingBlockOverworldObject : public OverworldOccupant
{
public:
    BuildingBlockOverworldObject(BinaryReaderPtr& reader);

    bool getIsOverhead() const;

    // OverworldOccupant
    virtual OverworldOccupantType getType() const final;
    virtual SpriteSheet getSpriteSheet() const final;
    virtual uint32_t getSpriteNo() const final;
    virtual bool getIsSign() const final;
    virtual void activate(Character& activator) final;

protected:
    //OverworldOccupant
    virtual void update(uint32_t elapsedTime) final;

private:
    uint32_t _spriteNo;
    bool _isOverhead;
    bool _isSign;

};

#endif

#ifndef KRISTALL_INCLUDE_WORLD_MAPBRIDGETILE_H
#define KRISTALL_INCLUDE_WORLD_MAPBRIDGETILE_H

#include "World/Tile.h"

class MapBridgeTile : public Tile
{
    friend class Map;
public:
    virtual ~MapBridgeTile() override;

    // Tile
    virtual TileType getType() const override;
    virtual Direction getInwardArrow() const;
    virtual Tile* getDestination() const;

protected:
    MapBridgeTile(InternalMapPtr map, uint16_t x, uint16_t y, BinaryReaderPtr reader, PlayerGame* playerGame);

    // Tile
    virtual void readHeader(BinaryReaderPtr reader) override;

private:
    Direction _inwardArrow;
    uint32_t _destinationMapNo;
    uint16_t _destinationTileX;
    uint16_t _destinationTileY;

};

#endif

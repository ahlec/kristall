#ifndef KRISTALL_INCLUDE_WORLD_STAIRCASETYPE_H
#define KRISTALL_INCLUDE_WORLD_STAIRCASETYPE_H

enum class StaircaseType : uint8_t
{
    NotAStaircase,
    UpwardStaircase,
    DownwardStaircase
};

#endif

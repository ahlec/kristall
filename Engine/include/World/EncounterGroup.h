#ifndef KRISTALL_INCLUDE_WORLD_ENCOUNTERGROUP_H
#define KRISTALL_INCLUDE_WORLD_ENCOUNTERGROUP_H

#include "Cornerstones.h"
#include "Data/Predefine.h"
#include "Pokemon/Predefine.h"
#include "World/Predefine.h"
#include "World/EncounterMethod.h"
#include "Pokemon/PokemonAbility.h"
#include "General/TimeOfDayFlags.h"

class EncounterGroup
{
public:
    EncounterGroup(BinaryReaderPtr reader);

    WildPokemonPtr nextWildPokemon(const Tile& tile, EncounterMethod encounterMethod,
                                   PokemonAbility leadingPokemonAbility);

private:
    struct EncounterGroupNode
    {
        uint32_t speciesNo;
        bool isElectricType;
        bool isSteelType;
        uint8_t encounterRate;
        EncounterMethod encounterMethod;
        uint8_t levelRangeStart;
        uint8_t levelRangeEnd;
        TimeOfDayFlags encounterTimes;
    };

    uint16_t _totalEncounterRate;
    bool _isAffectedByStatic;
    bool _isAffectedByMagnetPull;

    std::vector<EncounterGroupNode> _encounterNodes;
};

#endif

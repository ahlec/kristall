#ifndef KRISTALL_INCLUDE_WORLD_PREDEFINE_H
#define KRISTALL_INCLUDE_WORLD_PREDEFINE_H

#include "Data/CachePointer.h"
#include <memory>

class DoorTile;
class EncounterGroup;
class Map;
class MapLocation;
class Overworld;
class OverworldEffect;
class OverworldEntry;
class OverworldOccupant;
class RecoveryLocation;
class Region;
class RegionOverworldZone;
class Tile;

typedef std::shared_ptr<EncounterGroup> EncounterGroupPtr;
typedef Map* InternalMapPtr;
typedef std::shared_ptr<MapLocation> MapLocationPtr;
typedef CachePointer<Map> MapPtr;
typedef std::shared_ptr<OverworldEffect> OverworldEffectPtr;
typedef OverworldOccupant* OverworldOccupantPtr;
typedef std::shared_ptr<RecoveryLocation> RecoveryLocationPtr;
typedef CachePointer<Region> RegionPtr;

#endif

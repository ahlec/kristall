#ifndef KRISTALL_INCLUDE_WORLD_TILETYPE_H
#define KRISTALL_INCLUDE_WORLD_TILETYPE_H

enum class TileType : uint8_t
{
    Tile,
    MapBridgeTile,
    DoorTile
};

#endif

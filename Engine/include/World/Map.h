#ifndef KRISTALL_INCLUDE_WORLD_MAP_H
#define KRISTALL_INCLUDE_WORLD_MAP_H

#include "Cornerstones.h"
#include "World/Predefine.h"
#include "World/TileAnimationInstance.h"
#include "General/Predicate.h"
#include "World/MapLabel.h"
#include "Player/Predefine.h"
#include "World/Effects/OverworldEffectType.h"
#include "Lua/Predefine.h"
#include "Characters/Predefine.h"
#include "Graphics/Color.h"
#include <algorithm>

class Map
{
    friend class Tile;
    friend class OverworldOccupant;
    friend class OverworldEntry;
public:
    static MapPtr   get(uint32_t mapNo);
    /// static void Map::resetCache(PlayerGame*)
    /// To be called when a new PlayerGame has been loaded. Informs the cache to reset and use
    /// the new PlayerGame when loading all future maps.
    static void     resetCache(PlayerGame* newPlayerGame);

    Tile* getTile(int32_t tileX, int32_t tileY) const;
    std::vector<Tile*> getAllTiles(Predicate<Tile> condition) const;

    DoorTile& getFirstDoorToMap(Map& destinationMap) const;

    const std::vector<OverworldOccupantPtr> getAllOccupants() const;

    uint32_t getMapNo() const;
    String getName() const;
    uint16_t getWidth() const;
    uint16_t getHeight() const;
    RegionPtr getRegion() const;
    bool getIsInterior() const;
    MapPtr getParentMap() const;
    bool getIsBuilding() const;
    bool getIsBikingAllowed() const;
    MapLabel getLabel() const;
    uint16_t getMusicTrackNo() const;
    Color getTinting() const;
    OverworldEffectType getEffectType() const;
    MapLocationPtr getFlightLocation() const;
    RecoveryLocationPtr getRecoveryLocation() const;
    LuaScriptPtr getScript(uint8_t scriptNo) const;
    uint8_t getEncounterRate() const;
    EncounterGroupPtr getEncounterGroup(uint8_t encounterGroupNo) const;

    uint32_t getRegionMapIndex() const;

    bool operator==(const Map& other) const;
    bool operator!=(const Map& other) const;

private:
    Map(uint32_t mapNo, BinaryReaderPtr reader, PlayerGame* currentPlayerGame);

    uint32_t _mapNo;
    String _name;
    uint16_t _width;
    uint16_t _height;
    uint32_t _regionNo;
    bool _isInterior;
    bool _hasParentMap;
    uint32_t _parentMapNo;
    bool _isBuilding;
    bool _isBikingAllowed;
    MapLabel _label;
    uint16_t _musicTrackNo;
    Color _tinting;
    OverworldEffectType _effectType;
    RecoveryLocationPtr _recoveryLocation;
    std::vector<LuaScriptPtr> _scripts;
    uint8_t _encounterRate;
    std::vector<EncounterGroupPtr> _encounterGroups;
    std::vector<std::vector<Tile*>> _tiles;
    std::vector<TileAnimationInstance> _tileAnimationInstances;
    std::vector<OverworldOccupantPtr> _currentOccupants;

    uint32_t _regionIndex;
    void update(uint32_t elapsedTime, Overworld& overworld, PlayerGame* currentPlayerGame);
    void playerCharacterEntersATile(Tile& destination, PlayerCharacter* playerCharacter);

    inline void removeOccupant(OverworldOccupantPtr occupant)
    {
        _currentOccupants.erase(std::remove(_currentOccupants.begin(),
                                            _currentOccupants.end(),
                                            occupant), _currentOccupants.end());
    }
    inline void addOccupant(OverworldOccupantPtr occupant)
    {
        _currentOccupants.push_back(occupant);
    }
};

// String conversion functions
template<> String toString<Map>(Map value);

#endif

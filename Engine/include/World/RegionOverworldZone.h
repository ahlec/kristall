#ifndef KRISTALL_INCLUDE_WORLD_REGIONOVERWORLDZONE_H
#define KRISTALL_INCLUDE_WORLD_REGIONOVERWORLDZONE_H

#include "Cornerstones.h"
#include "Data/Predefine.h"
#include "World/Predefine.h"
#include "Graphics/SpriteSheet.h"
#include "General/Vector2.h"
#include "General/Rect.h"

class RegionOverworldZone
{
    friend class OverworldEntry;
public:
    RegionOverworldZone(const BinaryReaderPtr& reader);

    bool isMap() const;
    uint32_t getMapNo() const;
    bool isOnViewport(Rect& viewport) const;
    OverworldEntry toEntry(int32_t adjustmentX, int32_t adjustmentY,
                           int32_t focusMapX, int32_t focusMapY,
                           Rect& viewport) const;

private:
    class Overflow
    {
    public:
        Overflow(const BinaryReaderPtr& reader);

        uint32_t spriteNo;
        std::vector<Vector2> instances;
    };

    int64_t _identityNo;  // -1 if not a map, otherwise, the MapNo
    bool _isOutdoor;
    Rect _boundingRectangle;
    Rect _primaryRectangle;

    uint16_t _tileWidth;
    uint16_t _tileHeight;
    uint32_t _mainTextureNo;
    std::vector<Overflow> _mainOverflows;

    bool _hasSuperlayer;
    uint32_t _superlayerTextureNo;
    std::vector<Overflow> _superlayerOverflows;

    MapPtr _mapPtr;

    inline SpriteSheet getAppropriateSpriteSheet() const
    {
        return (_isOutdoor ? SpriteSheet::OverworldExterior : SpriteSheet::OverworldInterior);
    }
};

#endif

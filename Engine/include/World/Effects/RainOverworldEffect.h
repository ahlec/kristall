#ifndef KRISTALL_INCLUDE_WORLD_EFFECTS_RAINOVERWORLDEFFECT_H
#define KRISTALL_INCLUDE_WORLD_EFFECTS_RAINOVERWORLDEFFECT_H

#include "World/Effects/OverworldEffect.h"

class RainOverworldEffect : public OverworldEffect
{
public:
    RainOverworldEffect();
    ~RainOverworldEffect();

protected:
    virtual void draw(SpriteBatch& spriteBatch, const Overworld& overworld,
        std::vector<OverworldEntry&> overworldEntries, const PlayerGame& currentPlayerGame) override;
    virtual void update(milliseconds elapsedTime, const Overworld& overworld,
        std::vector<OverworldEntry&> overworldEntries, const PlayerGame& currentPlayerGame) override;
    virtual void dispel(overworldEffectDispelledDelegate onDispel) override;

private:
    struct Raindrop
    {
        Vector2 position;
        AnimationInstance animation;
        Vector2 destination;
        float time;
        bool hasReached;
        bool isDone;
        uint16_t mapHandleNo;
    };

    std::vector<Raindrop> _raindrops;
    sf::Color _overlayColor;
    float _rainNextBatch;
    uint16_t _rainBatch;

    bool _isIntroduced;
    float _introduceTime;
    float _introduceDuration;

    bool _isDispelling;
    float _dispelTime;
    float _dispelDuration;

};

#endif

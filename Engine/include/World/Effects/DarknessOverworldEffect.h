#ifndef KRISTALL_INCLUDE_WORLD_EFFECTS_DARKNESSOVERWORLDEFFECT_H
#define KRISTALL_INCLUDE_WORLD_EFFECTS_DARKNESSOVERWORLDEFFECT_H

#include "World/Effects/OverworldEffect.h"

class DarknessOverworldEffect : public OverworldEffect
{
public:
    DarknessOverworldEffect();
    ~DarknessOverworldEffect();

protected:
    virtual void draw(SpriteBatch& spriteBatch, const Overworld& overworld,
        std::vector<OverworldEntry&> overworldEntries, const PlayerGame& currentPlayerGame) override;
    virtual void update(milliseconds elapsedTime, const Overworld& overworld,
        std::vector<OverworldEntry&> overworldEntries, const PlayerGame& currentPlayerGame) override;
    virtual void dispel(overworldEffectDispelledDelegate onDispel) override;

private:
    bool _isIntroduced;
    bool _isDispelling;
    float _time;
    float _duration;
};

#endif

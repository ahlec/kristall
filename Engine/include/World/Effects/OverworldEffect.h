#ifndef KRISTALL_INCLUDE_WORLD_EFFECTS_OVERWORLDEFFECT_H
#define KRISTALL_INCLUDE_WORLD_EFFECTS_OVERWORLDEFFECT_H

class OverworldEffect
{
    friend class Overworld;
public:
    virtual ~OverworldEffect();
    OverworldEffectType getType() const;

protected:
    OverworldEffect(OverworldEffectType type);

    virtual void draw(SpriteBatch& spriteBatch, const Overworld& overworld,
        std::vector<OverworldEntry&> overworldEntries, PlayerGame* currentPlayerGame) = 0;
    virtual void update(milliseconds elapsedTime, const Overworld& overworld,
        std::vector<OverworldEntry&> overworldEntries, PlayerGame* currentPlayerGame) = 0;
    virtual void dispel(overworldEffectDispelledDelegate onDispel);


private:
    OverworldEffectType _type;
};

#endif

#ifndef KRISTALL_INCLUDE_WORLD_EFFECTS_OVERWORLDEFFECTTYPE_H
#define KRISTALL_INCLUDE_WORLD_EFFECTS_OVERWORLDEFFECTTYPE_H

#include "Cornerstones.h"

enum class OverworldEffectType : uint8_t
{
    None,
    Rain,
    Darkness,
    Snow,
    Sandstorm
};

namespace OverworldEffectTypes
{
    bool isWeather(OverworldEffectType effectType);
};

#endif

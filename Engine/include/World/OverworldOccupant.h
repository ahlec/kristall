#ifndef KRISTALL_INCLUDE_WORLD_OVERWORLDOCCUPANT_H
#define KRISTALL_INCLUDE_WORLD_OVERWORLDOCCUPANT_H

#include "Cornerstones.h"
#include "Characters/Predefine.h"
#include "World/Predefine.h"
#include "World/OverworldOccupantType.h"
#include "Graphics/SpriteSheet.h"
#include "General/Direction.h"

class OverworldOccupant
{
    friend class Tile;
    friend class Map;
public:
    OverworldOccupant();
    virtual ~OverworldOccupant();

    /// ----------------------------------------------------- Map data
    virtual OverworldOccupantType               getType() const = 0;
    Tile*                                       getParent() const;
    int                                         getOffsetX() const;
    int                                         getOffsetY() const;
    virtual Direction                           getDirectionFacing() const;
    virtual bool                                getIsVisible() const;
    virtual bool                                getIsSign() const = 0;

    virtual void                                setOffsetX(int newOffsetX);
    virtual void                                setOffsetY(int newOffsetY);

    /// ----------------------------------------------------- Graphics
    virtual SpriteSheet                         getSpriteSheet() const = 0;
    virtual uint32_t                            getSpriteNo() const = 0;

    /// ----------------------------------------------------- Interactivity
    virtual void                                activate(Character& activator) = 0;
    virtual void                                turn(Direction newDirection);

protected:
    Tile*                   m_parent;
    int                     m_offsetX;
    int                     m_offsetY;
    Direction               m_directionFacing;
    bool                    m_isVisible;

    virtual void        update(uint32_t elapsedTime) = 0;
    virtual void        onTurned(Direction oldDirection, Direction newDirection);
    virtual void        onParentChanged(Tile* oldParent, Tile* newParent);

private:
    void setParent(Tile* newParent);
};

#endif

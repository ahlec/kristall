#ifndef KRISTALL_INCLUDE_WORLD_TILEANIMATIONINSTANCE_H
#define KRISTALL_INCLUDE_WORLD_TILEANIMATIONINSTANCE_H

#include "Cornerstones.h"
#include "Graphics/SpriteSheet.h"
#include "Graphics/Predefine.h"
#include "Data/Predefine.h"

class TileAnimationInstance
{
public:
    TileAnimationInstance(BinaryReaderPtr& reader, SpriteSheet mapSpriteSheet);

    AnimationInstancePtr getAnimationInstance() const;
    uint16_t getTileX() const;
    uint16_t getTileY() const;

private:
    AnimationInstancePtr _animationInstance;
    uint16_t _tileX;
    uint16_t _tileY;
};

#endif

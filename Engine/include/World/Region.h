#ifndef KRISTALL_INCLUDE_WORLD_REGION_H
#define KRISTALL_INCLUDE_WORLD_REGION_H

#include "Cornerstones.h"
#include "World/Predefine.h"
#include "General/Predicate.h"
#include "Data/Predefine.h"
#include "World/MapLocation.h"
#include "World/RegionOverworldZone.h"

class Region
{
public:
    static RegionPtr get(uint32_t regionNo);

    /// ----------------------------------------------------- Information
    uint32_t                        getRegionNo() const;
    String                          getName() const;
    MapLocation                     getMaleStartLocation() const;
    MapLocation                     getFemaleStartLocation() const;

    /// ----------------------------------------------------- Transportation
    MapLocationPtr                  getFlightLocation(int32_t index) const;
    std::vector<MapLocation>        getFlightLocations(Predicate<MapLocation> condition = nullptr);

    /// ----------------------------------------------------- Mapping / Rendering
    uint32_t                        getIndexOfMap(uint32_t mapNo) const;
    std::vector<OverworldEntry>     getOverworld(OverworldOccupant* focus,
                                                 int32_t viewportWidth,
                                                 int32_t viewportHeight) const;

    /// ----------------------------------------------------- Class
    bool                            operator==(const Region& other) const;
    bool                            operator!=(const Region& other) const;

private:
    class RegionMap
    {
    public:
        RegionMap(const BinaryReaderPtr& reader);

        uint32_t mapNo;
        bool isOutdoor;
        int32_t absoluteX;
        int32_t absoluteY;
    };

    Region(uint32_t regionNo, const BinaryReaderPtr& reader);

    uint32_t                            m_regionNo;
    String                              m_name;
    MapLocation                         m_maleStartLocation;
    MapLocation                         m_femaleStartLocation;
    std::vector<MapLocation>            m_flightLocations;
    std::vector<RegionOverworldZone>    m_zones;
    std::vector<RegionMap>              m_regionMaps;
};

#endif

#ifndef KRISTALL_INCLUDE_WORLD_OVERWORLDOCCUPANTTYPE_H
#define KRISTALL_INCLUDE_WORLD_OVERWORLDOCCUPANTTYPE_H

enum class OverworldOccupantType
{
    NonPlayerCharacter,
    TrainerCharacter,
    RivalCharacter,
    PlayerCharacter,
    PokemonCharacter,
    BuildingBlock,
    OverworldObject
};

namespace OverworldOccupantTypes
{
    bool isCharacter(OverworldOccupantType);
    bool isNonPlayerCharacter(OverworldOccupantType);
}

#endif

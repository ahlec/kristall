#ifndef KRISTALL_INCLUDE_WORLD_OVERWORLD_H
#define KRISTALL_INCLUDE_WORLD_OVERWORLD_H

#include "Cornerstones.h"
#include "World/MapHandleList.h"
#include "Lua/Predefine.h"
#include "World/Predefine.h"
#include "World/Effects/OverworldEffectType.h"
#include "World/OverworldEntry.h"
#include "Player/Predefine.h"
#include "Graphics/Predefine.h"
#include "General/Mutex.h"

class GameWindow;

class Overworld
{
    friend class GameWindow;
public:
    Overworld();

    OverworldLuaInterpretor* getLuaInterpretor() const;

    OverworldEffectType getCurrentOverworldEffect();
    void killCurrentOverworldEffect();
    void dispelCurrentOverworldEffect();
    void beginOverworldEffect(OverworldEffectType newEffectType);

private:
    OverworldLuaInterpretor* _luaInterpretor;

    MapHandleList _mapsUpdatedThisTick;
    std::vector<OverworldEntry> _overworldEntriesThisTick;

    Mutex _effectMutex;
    OverworldEffectPtr _currentEffect;
    OverworldEffectPtr _dispellingEffect;

    float _stillwaterTime;

    void update(uint32_t elapsedTime, PlayerGame* currentPlayerGame);
    void draw(SpriteBatch* spriteBatch, PlayerGame* currentPlayerGame);
};

#endif

#ifndef KRISTALL_INCLUDE_WORLD_ENCOUNTERMETHOD_H
#define KRISTALL_INCLUDE_WORLD_ENCOUNTERMETHOD_H

#include "Cornerstones.h"

enum class EncounterMethod : uint8_t
{
    Wandering,
    OldRod,
    GoodRod,
    SuperRod,
    Headbutt
};

#endif

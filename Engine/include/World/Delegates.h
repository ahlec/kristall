#ifndef KRISTALL_INCLUDE_WORLD_DELEGATES_H
#define KRISTALL_INCLUDE_WORLD_DELEGATES_H

#include "General/Function.h"

typedef void (* DoorFullyVacatedDelegate)();
typedef FunctionPtr<void, Tile*> OccupantMayEnterTileDelegate;

#endif

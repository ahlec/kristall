#ifndef KRISTALL_INCLUDE_WORLD_TERRAIN_H
#define KRISTALL_INCLUDE_WORLD_TERRAIN_H

enum class Terrain : uint8_t
{
    Dirt,
    Snow,
    Water,
    Ice,
    Grass,
    Sand,
    Rocky,
    Cave,
    Interior,
    Mud,
    Puddle
};

namespace Terrains
{
    uint32_t getBattleBackdrop(Terrain terrain);
};

#endif

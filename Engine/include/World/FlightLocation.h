#ifndef KRISTALL_INCLUDE_WORLD_FLIGHTLOCATION_H
#define KRISTALL_INCLUDE_WORLD_FLIGHTLOCATION_H

class FlightLocation
{
public:
    FlightLocation(BinaryReader& reader);

    uint16_t getTileX() const;
    uint16_t getTileY() const;
    const std::string& getMapHandle() const;

    bool operator== (const FlightLocation& other) const;
    bool operator!= (const FlightLocation& other) const;

private:
    uint16_t _mapNo;
    uint16_t _x;
    uint16_t _y;
};

#endif

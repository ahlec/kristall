#ifndef KRISTALL_INCLUDE_WORLD_MAPLABEL_H
#define KRISTALL_INCLUDE_WORLD_MAPLABEL_H

enum class MapLabel : uint8_t
{
    Generic,
    City,
    Town,
    CityEdge,
    Mountains,
    Forest,
    Ocean,
    Countryside,
    Lake
};

#endif

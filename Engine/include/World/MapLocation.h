#ifndef KRISTALL_INCLUDE_WORLD_MAPLOCATION_H
#define KRISTALL_INCLUDE_WORLD_MAPLOCATION_H

#include "Cornerstones.h"
#include "Data/Predefine.h"

class MapLocation
{
public:
    MapLocation();
    MapLocation(uint32_t mapNo, uint16_t tileX, uint16_t tileY);
    MapLocation(const BinaryReaderPtr& reader);

    uint32_t getMapNo() const;
    uint16_t getTileX() const;
    uint16_t getTileY() const;

    bool operator==(const MapLocation& other) const;
    bool operator!=(const MapLocation& other) const;

private:
    uint32_t _mapNo;
    uint16_t _tileX;
    uint16_t _tileY;
};

#endif

#ifndef AUDIO_SOUNDS_H
#define AUDIO_SOUNDS_H

class Sounds
{
    STATIC_GLOBAL_CLASS_FRIEND_DECLARATION
public:
    static void play(SoundLibraries soundLibrary, uint16_t soundNo,
                     soundFinishedPlayingDelegate onSoundFinished,
                     uint32_t playbackDuration = -1);

private:
    static void initialize(BinaryReader& reader);
    static void deinitialize();

    static SoundLibrary* _soundEffects;
    static SoundLibrary* _pokemonCries;
    static SoundLibrary* _battleSoundEffects;
};

#endif

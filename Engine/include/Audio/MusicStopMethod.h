#ifndef AUDIO_MUSICSTOPMETHOD_H
#define AUDIO_MUSICSTOPMETHOD_H

enum class MusicStopMethod
{
    StopImmediately,
    FadeOut
};

#endif

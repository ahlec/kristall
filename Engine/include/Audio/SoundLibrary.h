#ifndef AUDIO_SOUNDLIBRARY_H
#define AUDIO_SOUNDLIBRARY_H

class Sounds;

class SoundLibrary
{
    friend class Sounds;
public:
    SoundLibrary(BinaryReader& reader);
    virtual ~SoundLibrary();

private:
    uint16_t _nSounds;
    sf::SoundBuffer* _soundBuffers;
    float* _soundVolumes;
};

#endif

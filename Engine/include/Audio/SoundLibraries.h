#ifndef AUDIO_SOUNDLIBRARIES_H
#define AUDIO_SOUNDLIBRARIES_H

enum class SoundLibraries
{
    SoundEffects,
    PokemonCries,
    BattleSoundEffects
};

#endif

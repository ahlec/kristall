#ifndef AUDIO_MUSICPLAYER_H
#define AUDIO_MUSICPLAYER_H

class MusicPlayer
{
    STATIC_GLOBAL_CLASS_FRIEND_DECLARATION
public:
    static void playTrack(uint16_t trackNo);
    static void transitionToTrack(uint16_t trackNo);

    static void pauseCurrentTrack();
    static void resumeCurrentTrack();

    static void stopCurrentTrack();
    static void fadeOutCurrentTrack(milliseconds fadeDuration);

    static uint16_t getCurrentTrackNo();

private:
    static void initialize(BinaryReader& reader);
    static void deinitialize();

    static sf::Mutex _mutex;
    static uint16_t _nTracks;
    static MusicTrack* _tracks;

    static MusicTrackPlayInstance* _currentTrack;

};

#endif

#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONEVOLUTIONSTIMULUS_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONEVOLUTIONSTIMULUS_H

enum class PokemonEvolutionStimulus : uint8_t
{
    Leveling,
    ItemUse,
    Friendship,
    LuaFunction
};

#endif

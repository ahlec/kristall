#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONEGGGROUP_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONEGGGROUP_H

enum class PokemonEggGroup : uint8_t
{
    None,
    Monster,
    Water1,
    Bug,
    Flying,
    Field,
    Fairy,
    Grass,
    Humanlike,
    Water3,
    Mineral,
    Amorphous,
    Water2,
    Ditto,
    Dragon,
    Undiscovered
};

#endif

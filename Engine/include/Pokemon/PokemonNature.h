#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONNATURE_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONNATURE_H

#include "Pokemon/PokemonStat.h"
#include "Items/BerryFlavor.h"

enum class PokemonNature
{
    Hardy,
    Lonely,
    Brave,
    Adamant,
    Naughty,
    Bold,
    Docile,
    Relaxed,
    Impish,
    Lax,
    Timid,
    Hasty,
    Serious,
    Jolly,
    Naive,
    Modest,
    Mild,
    Quiet,
    Bashful,
    Rash,
    Calm,
    Gentle,
    Sassy,
    Careful,
    Quirky
};

namespace PokemonNatures
{
    bool isStatBoosting(PokemonNature nature);
    PokemonStat getBoostedStat(PokemonNature nature);
    PokemonStat getLoweredStat(PokemonNature nature);
    bool hasBerryFlavorPreference(PokemonNature nature);
    BerryFlavor getDislikedBerryFlavor(PokemonNature nature);
    BerryFlavor getLikedBerryFlavor(PokemonNature nature);
};

#endif

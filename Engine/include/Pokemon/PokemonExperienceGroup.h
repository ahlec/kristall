#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONEXPERIENCEGROUP_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONEXPERIENCEGROUP_H

enum class PokemonExperienceGroup : uint8_t
{
    Erratic,
    Fast,
    MediumFast,
    MediumSlow,
    Slow,
    Fluctuating
};

namespace PokemonExperienceGroups
{
    uint32_t calculateExpForLevel(uint8_t level, PokemonExperienceGroup expGroup);
};

#endif

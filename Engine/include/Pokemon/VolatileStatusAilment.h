#ifndef KRISTALL_INCLUDE_POKEMON_VOLATILESTATUSAILMENT_H
#define KRISTALL_INCLUDE_POKEMON_VOLATILESTATUSAILMENT_H

enum class VolatileStatusAilment
{
    None = 0 ,
    Confusion = 1,
    Curse = 2,
    Encore = 4,
    Flinch = 8,
    Identify = 16,
    Infatuation = 32,
    Nightmare = 64,
    PartiallyTrapped = 128,
    PerishSong = 256,
    Seeding = 512,
    Taunt = 1024,
    Torment = 2048,
    Wrapped = 4096,
    Disable = 8192,
    Invincible = 16384,
    SemiInvulnerable = 32768
};

#endif

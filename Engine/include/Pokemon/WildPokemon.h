#ifndef POKEMON_WILDPOKEMON_H
#define POKEMON_WILDPOKEMON_H

class Tile;

class WildPokemon : public BattleTeamMember
{
public:
    WildPokemon(uint16_t speciesNo, uint8_t level, EncounterMethod encounterMethod,
                Tile& encounterTile, std::string battleAI = std::string(""),
                bool isPassive = false);
    ~WildPokemon();

private:
    PokemonPtr _pokemon;
    EncounterMethod _encounterMethod;
    Tile& _encounterTile;
    bool _isPassive;

    static std::string getDefaultBattleAi(PokemonSpeciesPtr species,
                                          uint8_t level,
                                          EncounterMethod encounterMethod);
};

typedef std::shared_ptr<WildPokemon> WildPokemonPtr;

#endif

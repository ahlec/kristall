#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONSPECIESLEARNSET_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONSPECIESLEARNSET_H

class PokemonSpeciesLearnset
{
public:
    PokemonSpeciesLearnset(BinaryReader& reader);
    ~PokemonSpeciesLearnset();

    bool canKnowMove(const PokemonMovePtr move) const;
    bool canKnowMove(uint16_t moveHandleNo) const;

    std::vector<PokemonMovePtr> getMovesLearnedAtLevel(uint8_t level) const;

    PokemonMoveset createMovesetForLevel(uint8_t level) const;

private:
    uint16_t _nEntries;
    PokemonSpeciesLearnsetEntry* _entries;
};

#endif

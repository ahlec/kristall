#ifndef KRISTALL_INCLUDE_POKEMON_PARTYPOKEMONLOCATION_H
#define KRISTALL_INCLUDE_POKEMON_PARTYPOKEMONLOCATION_H

#include "Pokemon/PokemonLocation.h"

class PartyPokemonLocation : public PokemonLocation
{
public:
    PartyPokemonLocation(uint16_t index);
    virtual PokemonLocationType getType() const override;

    virtual uint16_t getIndex() const override;

private:
    uint16_t _index;
};

#endif

#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONMOVEACQUISITIONMODE_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONMOVEACQUISITIONMODE_H

enum class PokemonMoveAcquisitionMode : uint8_t
{
    Leveling,
    MoveMachineTM,
    MoveMachineHM,
    Tutoring,
    Egg
};

#endif

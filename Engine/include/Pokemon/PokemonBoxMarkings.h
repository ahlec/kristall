#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONBOXMARKINGS_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONBOXMARKINGS_H

enum class PokemonBoxMarkings
{
    None = 0,
    Circle = 1,
    Triangle = 2,
    Square = 4,
    Heart = 8,
    Star = 16,
    Diamond = 32
};

#endif

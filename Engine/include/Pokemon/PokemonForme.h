#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONFORME_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONFORME_H

class PokemonForme
{
public:
    PokemonForme(BinaryReader& reader, uint16_t speciesNo);
    ~PokemonForme();

    const std::string& getName() const;
    uint16_t getSpeciesNo() const;
    bool getIsDefaultForme() const;
    bool getIsBattleOnlyForme() const;

    bool getHasCustomTypes() const;
    ElementalType getCustomType1() const;
    ElementalType getCustomType2() const;

    bool getHasCustomAbilities() const;
    uint8_t getNumberCustomAbilities() const;
    PokemonAbility getCustomAbility(uint8_t index) const;

    bool getHasCustomStats() const;
    uint8_t getCustomStat(PokemonStat stat) const;

    bool operator==(const PokemonForme& other) const;
    bool operator!=(const PokemonForme& other) const;

private:
    uint16_t _speciesNo;
    std::string _name;
    bool _isDefaultForme;
    bool _isBattleOnlyForme;

    bool _hasCustomTypes;
    ElementalType _type1;
    ElementalType _type2;

    bool _hasCustomAbilities;
    PokemonAbility _ability1;
    PokemonAbility _ability2;

    bool _hasCustomStats;
    uint8_t _customStats[6];
};

#endif

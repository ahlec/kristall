#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONSPECIESWILDHELDITEMSCOLLECTION_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONSPECIESWILDHELDITEMSCOLLECTION_H

class PokemonSpeciesWildHeldItemsCollection
{
public:
    PokemonSpeciesWildHeldItemsCollection(BinaryReader& reader);

    ItemPtr next(bool isCompoundEyesActive);

private:
    bool _isHoldingItemsWhenWild;
    uint16_t _oneRarityItemHandleNo;
    uint16_t _fiveRarityItemHandleNo;
    uint16_t _fiftyRarityItemHandleNo;
    uint16_t _hundredRarityItemHandleNo;

};

#endif

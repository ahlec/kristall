#ifndef POKEMON_BREEDING_H
#define POKEMON_BREEDING_H

namespace Breeding
{
    uint8_t getBreedingRate(const PokemonPtr pokemonA, const PokemonPtr pokemonB);
    uint16_t getEggSpecies(const PokemonPtr pokemonA, const PokemonPtr pokemonB);
    bool canBreed(const PokemonPtr pokemonA, const PokemonPtr pokemonB);
};

#endif

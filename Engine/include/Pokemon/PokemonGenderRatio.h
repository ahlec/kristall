#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONGENDERRATIO_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONGENDERRATIO_H

enum class PokemonGenderRatio : uint8_t
{
    MaleOnly,
    OneFemaleSevenMale,
    OneFemaleThreeMale,
    OneFemaleOneMale,
    ThreeFemaleOneMale,
    FemaleOnly,
    Genderless
};

#endif

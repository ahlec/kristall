#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONACQUISITIONMETHOD_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONACQUISITIONMETHOD_H

enum class PokemonAcquisitionMethod
{
    Unacquired,
    Hatched,
    FatefulEvent,
    Caught,
    Received        // NOTE: This is used for eggs that have not yet been hatched.
};

#endif

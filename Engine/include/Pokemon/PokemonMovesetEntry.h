#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONMOVESETENTRY_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONMOVESETENTRY_H

struct PokemonMovesetEntry
{
    uint16_t moveHandleNo;
    uint8_t currentPowerPoints;
    uint8_t totalPowerPoints;
};

#endif

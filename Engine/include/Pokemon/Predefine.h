#ifndef KRISTALL_INCLUDE_POKEMON_PREDEFINE_H
#define KRISTALL_INCLUDE_POKEMON_PREDEFINE_H

#include "Data/CachePointer.h"

class PartyPokemonLocation;
class PokemonAcquisitionInformation;
class Pokemon;
class PokemonForme;
class PokemonLocation;
class PokemonMove;
class PokemonMoveset;
class PokemonSpecies;
class WildPokemon;

typedef std::shared_ptr<Pokemon> PokemonPtr;
typedef std::shared_ptr<PokemonForme> PokemonFormePtr;
typedef std::shared_ptr<PokemonLocation> PokemonLocationPtr;
typedef CachePointer<PokemonMove> PokemonMovePtr;
typedef CachePointer<PokemonSpecies> PokemonSpeciesPtr;
typedef std::shared_ptr<WildPokemon> WildPokemonPtr;

#endif

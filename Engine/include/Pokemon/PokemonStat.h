#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONSTAT_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONSTAT_H

enum class PokemonStat
{
    HP,
    Attack,
    Defense,
    SpecialAttack,
    SpecialDefense,
    Speed
};

#endif

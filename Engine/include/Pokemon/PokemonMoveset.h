#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONMOVESET_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONMOVESET_H

class PokemonMoveset
{
public:
    PokemonMoveset(BinaryReader& reader);
    ~PokemonMoveset();

    uint8_t getNumberMovesKnown() const;

    bool isMoveKnown(const PokemonMovePtr move) const;
    bool isMoveKnown(uint16_t moveHandleNo) const;

    PokemonMovesetEntry get(int32_t slotNo) const;
    PokemonMovesetEntry get(const PokemonMovePtr move) const;

    void teach(const PokemonMovePtr move);
    void teach(uint16_t moveHandleNo);

    void forget(const PokemonMovePtr move);
    void forget(uint16_t moveHandleNo);

    void decrementCurrentPowerPoints(const PokemonMovePtr move,
                                     uint8_t decrementAmount);
    void decrementCurrentPowerPoints(uint16_t moveHandleNo,
                                     uint8_t decrementAmount);
    void incrementCurrentPowerPoints(const PokemonMovePtr move,
                                     uint8_t incrementAmount);
    void incrementCurrentPowerPoints(uint16_t moveHandleNo,
                                     uint8_t incrementAmount);

    void incrementTotalPowerPoints(const PokemonMovePtr move,
                                   uint8_t incrementAmount);
    void incrementTotalPowerPoints(uint16_t moveHandleNo, uint8_t incrementAmount);

private:
    uint8_t _nMovesKnown;
    uint16_t _moveHandleNos[4];
    uint8_t _maxPowerPoints[4];
    uint8_t _currentPowerPoints[4];

};

#endif

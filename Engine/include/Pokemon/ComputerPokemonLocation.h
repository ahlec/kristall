#ifndef KRISTALL_INCLUDE_POKEMON_COMPUTERPOKEMONLOCATION_H
#define KRISTALL_INCLUDE_POKEMON_COMPUTERPOKEMONLOCATION_H

#include "Pokemon/PokemonLocation.h"

class ComputerPokemonLocation : public PokemonLocation
{
public:
    ComputerPokemonLocation(uint8_t boxNo, uint16_t index);
    virtual PokemonLocationType getType() const override;

    virtual uint16_t getIndex() const override;
    uint8_t getBoxNo() const;

private:
    uint16_t _index;
    uint8_t _boxNo;
};

#endif

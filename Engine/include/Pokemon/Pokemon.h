#ifndef KRISTALL_INCLUDE_POKEMON_POKEMON_H
#define KRISTALL_INCLUDE_POKEMON_POKEMON_H

#include "Pokemon/Predefine.h"
#include "Characters/Predefine.h"
#include "Items/Predefine.h"
#include "Pokemon/Delegates.h"
#include "Pokemon/PokemonBoxMarkings.h"
#include "Pokemon/NonVolatileStatusAilment.h"
#include "General/Gender.h"
#include "Pokemon/PokemonNature.h"
#include "Pokemon/ContestCondition.h"

class Pokemon
{
    friend class EggHatchingScreen;
    friend class ExperienceDistributionBattleTurnAction;
    friend class PlayerCharacter;
    friend class PokemonTrainer;
public:
    static PokemonPtr breed(const PokemonPtr parentA, const PokemonPtr parentB,
                            const std::string& daycareNpcName);
    Pokemon(uint16_t speciesNo, uint8_t level);
    Pokemon(BinaryReader& reader, PokemonTrainer* trainer);
    ~Pokemon();

    void setName(std::string newName);
    ElementalType determineType1() const;
    ElementalType determineType2() const;
    bool hasType(ElementalType type) const;
    float calculateExpToNextLevelPercent() const;
    void giveExpPoints(int32_t exp, PokemonLeveledDelegate onLevel,
                       PokemonWantsToLearnMoveDelegate onWantsMove,
                       PokemonEvolutionTriggeredDelegate onEvolutionTriggered);
    void restoreCompletely(PokemonRevivedDelegate onRevive);
    void setBoxMarkings(PokemonBoxMarkings newMarkings);
    void setHeldItem(ItemPtr newHeldItem);
    void setForme(PokemonFormePtr newForme);
    void modifyFriendship(int8_t amount);
    void setFriendship(uint8_t amount);
    uint16_t giveHP(uint16_t amountToGive, PokemonRevivedDelegate onRevive);
    uint16_t takeHP(uint16_t amountToTake, PokemonFaintedDelegate onFaint);
    void setNonVolatileStatus(NonVolatileStatusAilment newStatus,
                              uint8_t newStatusData);
    void setNonVolatileStatusData(uint8_t newStatusData);

    const std::string& getName() const;
    bool getIsNicknamed() const;
    PokemonSpeciesPtr getSpecies() const;
    uint8_t getLevel();
    Gender getGender() const;
    PokemonAbility getAbility() const;
    PokemonNature getNature() const;
    bool getIsShiny() const;
    uint8_t getFriendship() const;
    PokemonBoxMarkings getBoxMarkings() const;
    PokemonFormePtr getCurrentForme() const;
    PokemonTrainer* getTrainer() const;
    PokemonAcquisitionInformation* getAcquisitionInformation() const;
    PokemonMoveset* getMoveset() const;
    uint8_t getEffortValue(PokemonStat stat) const;
    uint8_t getIndividualValue(PokemonStat stat) const;
    uint16_t getStat(PokemonStat stat) const;
    uint16_t getCurrentHP() const;
    uint8_t getContestStat(ContestCondition contestCondition) const;
    NonVolatileStatusAilment getNonVolatileStatus() const;
    uint8_t getNonVolatileStatusData() const;
    uint32_t getTotalExperience() const;
    uint32_t getExperienceToNextLevel() const;
    bool getIsEgg() const;
    int32_t getRemainingEggCycles() const;
    ItemPtr getHeldItem() const;

private:
    std::string _name;
    bool _isNicknamed;
    PokemonSpeciesPtr _species;
    uint8_t _level;
    Gender _gender;
    PokemonAbility _ability;
    PokemonNature _nature;
    bool _isShiny;
    uint8_t _friendship;
    PokemonBoxMarkings _boxMarkings;
    PokemonTrainer* _trainer;
    PokemonAcquisitionInformation* _acquisitionInformation;
    uint8_t _formeNo;

    PokemonMoveset* _moveset;

    uint8_t _effortValues[6];
    uint8_t _individualValues[6];
    uint16_t _stats[6];
    uint8_t _contestStats[5];

    uint16_t _currentHP;
    NonVolatileStatusAilment _nonVolatileStatus;
    uint8_t _nonVolatileStatusData;
    uint32_t _totalExp;
    uint32_t _expToNextLevel;

    bool _isEgg;
    int32_t _remainingEggCycles;

    ItemPtr _heldItem;

    void joinTrainer(PokemonTrainer* newTrainer,
                     PokemonAcquisitionInformation acquisitionInfo,
                     uint16_t pokeballNo,
                     PokemonJoinsTrainerDelegate onJoin);
    bool markEggCycleComplete(uint8_t cycleCount, bool canEggHatch,
                              EggHatchingDelegate onHatching);
    void hatchEgg(EggHatchedDelegate onHatched);
    void evolveInto(PokemonSpeciesPtr evolution,
                    PokemonEvolvedDelegate onEvolved);
    void calculateStats();

};

#endif

#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONACQUISITIONINFORMATION_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONACQUISITIONINFORMATION_H

class PokemonAcquisitionInformation
{
public:
    PokemonAcquisitionInformation(PokemonAcquisitionMethod method,
                                  uint16_t mapAcquiredNo,
                                  uint8_t levelAcquired,
                                  uint16_t originalTrainerNo,
                                  std::string originalTrainerName,
                                  uint16_t pokeballNo,
                                  std::string daycareNpcName,
                                  DateTime dateEggReceived);
    PokemonAcquisitionInformation(BinaryReader& reader);

    PokemonAcquisitionMethod getMethod() const;
    uint16_t getMapNoAcquiredOn() const;
    uint8_t getLevelAcquired() const;
    uint16_t getOriginalTrainerNo() const;
    const std::string& getOriginalTrainerName() const;
    uint16_t getPokeballNoContaining() const;
    DateTime getDateAcquired() const;
    const std::string& getDayCareNpcName() const;
    DateTime getDateEggReceived() const;

private:
    PokemonAcquisitionMethod _method;
    uint16_t _mapAcquired;
    uint8_t _levelAcquired;
    uint16_t _originalTrainerNo;
    std::string _originalTrainerName;
    uint16_t _pokeballNo;
    DateTime _dateAcquired;
    std::string _daycareNpcName;
    DateTime _dateEggReceived;

};

#endif

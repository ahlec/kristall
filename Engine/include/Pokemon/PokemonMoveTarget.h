#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONMOVETARGET_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONMOVETARGET_H

enum class PokemonMoveTarget : uint8_t
{
    SelectedPokemon,
    RandomOpponent,
    Ally,
    Self,
    SelfOrAlly,
    OwnField,
    OpponentField,
    AllOthers,
    AllOpponents,
    EntireField,
    Special
};

#endif

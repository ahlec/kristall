#ifndef KRISTALL_INCLUDE_POKEMON_EVOLUTIONTREE_H
#define KRISTALL_INCLUDE_POKEMON_EVOLUTIONTREE_H

class EvolutionTree
{
public:
    EvolutionTree(BinaryReader& reader);
    ~EvolutionTree();

    int32_t getNumberEvolutions() const;
    uint16_t getBaseSpeciesNo() const;

    bool hasBranch(uint16_t speciesNo) const;
    bool usesStimulus(PokemonEvolutionStimulus stimulus,
                      uint16_t stimulusData) const;
    PokemonSpeciesPtr getEvolution(PokemonEvolutionStimulus stimulus,
                                   uint16_t stimulusData) const;

private:
    struct Node
    {
        uint16_t speciesNo;
        PokemonEvolutionStimulus stimulus;
        void* stimulusData;
    };

    uint16_t _baseSpeciesNo;
    int32_t _nEvolutions;

    uint16_t _breedingIncenseItemHandleNo;
    uint16_t _breedingIncenseBabySpeciesNo;

    uint16_t _nNodes;
    Node* _nodes;
};

#endif

#ifndef KRISTALL_INCLUDE_POKEMON_NONVOLATILESTATUSAILMENT_H
#define KRISTALL_INCLUDE_POKEMON_NONVOLATILESTATUSAILMENT_H

enum class NonVolatileStatusAilment
{
    None,
    Burn,
    Freeze,
    Paralysis,
    Poison,
    BadPoison,
    Sleep,
    Faint
};

#endif

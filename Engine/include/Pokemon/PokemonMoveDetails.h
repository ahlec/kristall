#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONMOVEDETAILS_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONMOVEDETAILS_H

enum class PokemonMoveDetails : uint32_t
{
    None = 0,
    HasRecoil = 1,
    IsPunch = 2,
    IsSoundBased = 4,
    MakesContact = 8,
    AffectedByProtect = 16,
    AffectedByKingsRock = 32,
    MirrorMoveCopied = 64,
    MagicBounceReflectable = 128,
    Snatchable = 256,
    HasChargingTurn = 512,
    CanUseWhenAsleep = 1024,
    CanUseWhenFrozen = 2048,
    OneHitKO = 4096,
    HitsAirborne = 8192,
    HitsUnderwater = 16384,
    HitsUnderground = 32768,
    Heals = 65536,
    Explodes = 131072,
    IsBiting = 262144,
    IsBallOrBomb = 524288,
    IsAuraOrPulse = 1048576
};

inline PokemonMoveDetails operator|(PokemonMoveDetails detailA, PokemonMoveDetails detailB)
{
    return static_cast<PokemonMoveDetails>(static_cast<uint32_t>(detailA) | static_cast<uint32_t>(detailB));
}

#endif

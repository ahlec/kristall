#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONLOCATIONTYPE_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONLOCATIONTYPE_H

enum class PokemonLocationType
{
    PartyPokemonLocation,
    ComputerPokemonLocation
};

#endif

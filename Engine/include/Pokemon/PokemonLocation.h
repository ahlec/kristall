#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONLOCATION_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONLOCATION_H

class PokemonLocation
{
public:
    virtual ~PokemonLocation();

    virtual PokemonLocationType getType() const=0;
    virtual uint16_t getIndex() const=0;
};

#endif

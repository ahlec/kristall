#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONSPECIES_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONSPECIES_H

#include "Pokemon/Predefine.h"

class PokemonSpecies
{
public:
    static PokemonSpeciesPtr get(uint16_t speciesNo);
    ~PokemonSpecies();

    uint16_t getSpeciesNo() const;
    const std::string& getName() const;
    const std::string& getSpecies() const;
    const std::string& getPokedexEntry() const;
    ElementalType getType1() const;
    ElementalType getType2() const;
    uint8_t getBaseStat(PokemonStat stat) const;
    PokemonExperienceGroup getExperienceGroup() const;
    PokemonGenderRatio getGenderRatio() const;
    PokemonSpeciesLearnset& getLearnset() const;
    EvolutionTreePtr getEvolutionTree() const;
    uint16_t getBaseExpYield() const;
    uint8_t getEvYield(PokemonStat stat) const;
    PokemonSpeciesWildHeldItemsCollection& getWildHeldItems() const;
    uint16_t getBaseCatchRate() const;
    uint8_t getBaseFriendship() const;
    bool getIsLarge() const;
    PokemonSpeciesColor getColor() const;
    float getWeightKilograms() const;
    float getHeightMeters() const;
    bool getHasFormes() const;
    uint8_t getNumberFormes() const;
    PokemonFormePtr getForme(uint8_t formeNo) const;
    PokemonEggGroup getEggGroup1() const;
    PokemonEggGroup getEggGroup2() const;
    uint8_t getNumberCyclesToHatchEgg() const;
    uint8_t getNumberAbilities() const;
    PokemonAbility getAbility(uint8_t index) const;

    bool operator==(const PokemonSpecies& other) const;
    bool operator!=(const PokemonSpecies& other) const;

private:
    PokemonSpecies(uint16_t speciesNo);

    uint16_t _speciesNo;
    std::string _name;
    std::string _species;
    std::string _pokedexEntry;
    ElementalType _type1;
    ElementalType _type2;
    PokemonAbility _ability1;
    PokemonAbility _ability2;
    uint8_t _baseStats[6];
    uint8_t _baseFriendship;
    uint16_t _baseCatchRate;
    PokemonExperienceGroup _expGroup;
    uint16_t _baseExpYield;
    uint8_t _evYield[6];
    PokemonGenderRatio _genderRatio;
    PokemonSpeciesLearnset _learnset;

    bool _isLarge;
    PokemonSpeciesColor _color;
    float _weightKilograms;
    float _heightMeters;
    PokemonEggGroup _eggGroup1;
    PokemonEggGroup _eggGroup2;
    uint8_t _nEggCyclesToHatch;
    EvolutionTreePtr _evolutionTree;

    PokemonSpeciesWildHeldItemsCollection _wildHeldItems;

    bool _hasFormes;
    uint8_t _nFormes;
    PokemonFormePtr* _formes;

};

#endif

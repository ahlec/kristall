#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONSPECIESLEARNSETENTRY_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONSPECIESLEARNSETENTRY_H

class PokemonSpeciesLearnsetEntry
{
public:
    PokemonSpeciesLearnsetEntry(BinaryReader& reader);

    uint16_t getMoveHandleNo() const;
    PokemonMoveAcquisitionMode getAcquisitionMode() const;
    uint16_t getAcquisitionCondition() const;

private:
    uint16_t _moveHandleNo;
    PokemonMoveAcquisitionMode _acquisitionMode;
    uint16_t _acquisitionCondition;
};

#endif

#ifndef KRISTALL_INCLUDE_POKEMON_CONTESTCONDITION_H
#define KRISTALL_INCLUDE_POKEMON_CONTESTCONDITION_H

enum class ContestCondition
{
    Cool,
    Tough,
    Beauty,
    Smart,
    Cute
};

#endif

#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONMOVE_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONMOVE_H

#include "Cornerstones.h"
#include "Data/Predefine.h"
#include "General/ElementalType.h"
#include "Pokemon/PokemonMoveDamageCategory.h"
#include "Pokemon/PokemonMoveTarget.h"
#include "Pokemon/PokemonMoveDetails.h"
#include "Data/CachePointer.h"

class PokemonMove;
typedef CachePointer<PokemonMove> PokemonMovePtr;

class PokemonMove
{
public:
    static PokemonMovePtr get(uint16_t handleNo);
    ~PokemonMove();

    uint16_t getHandleNo() const;
    const std::string& getName() const;
    const std::string& getDescription() const;
    ElementalType getElementalType() const;
    bool getIsElementalTypeIgnored() const;
    PokemonMoveDamageCategory getDamageCategory() const;
    PokemonMoveTarget getTarget() const;
    PokemonMoveDetails getDetails() const;

    bool getIsHmMove() const;
    bool getIsTmMove() const;
    uint16_t getMoveMachineNo() const;

    uint8_t getBasePowerPoints() const;
    bool getHasBasePower() const;
    uint8_t getBasePower() const;
    bool getHasAccuracy() const;
    uint8_t getBaseAccuracy() const;
    int32_t getPriority() const;

    const std::string& getBattlePreparationCode() const;
    const std::string& getBattleCode() const;

    bool getIsUsableOutsideBattle() const;
    bool getHasTargetWhenUsedOutsideBattle() const;
    const std::string& getOutsideBattleUsabilityCode() const;
    const std::string& getOutsideBattleCode() const;

    bool operator==(const PokemonMove& other) const;
    bool operator!=(const PokemonMove& other) const;

private:
    PokemonMove(BinaryReader& reader);

    uint16_t _handleNo;
    String _name;
    String _description;
    ElementalType _elementalType;
    bool _isTypeIgnoredInBattle;
    PokemonMoveDamageCategory _damageCategory;
    uint8_t _basePowerPoints;
    bool _isHmMove;
    bool _isTmMove;
    uint16_t _moveMachineNo;

    bool _hasBasePower;
    uint8_t _basePower;
    bool _hasAccuracy;
    uint8_t _baseAccuracy;
    int8_t _priority;
    PokemonMoveDetails _details;
    PokemonMoveTarget _target;
    std::string _battlePreparationCode;
    std::string _battleCode;

    bool _isUsableOutsideBattle;
    bool _hasTargetWhenUsedOutsideBattle;
    std::string _outsideBattleUsabilityCode;
    std::string _outsideBattleCode;

};

#endif

#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONSPECIESCOLOR_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONSPECIESCOLOR_H

enum class PokemonSpeciesColor : uint8_t
{
    White,
    Black,
    Blue,
    Brown,
    Gray,
    Green,
    Pink,
    Purple,
    Red,
    Yellow
};

#endif

#ifndef POKEMON_COMPUTERBOXPOKEMON_H
#define POKEMON_COMPUTERBOXPOKEMON_H

class ComputerBoxPokemon
{
public:
    ComputerBoxPokemon(uint8_t boxNo, uint8_t slotNo, PokemonPtr pokemon);
    ~ComputerBoxPokemon();

    uint8_t getBoxNo() const;
    uint8_t getSlotNo() const;
    PokemonPtr getPokemon() const;

private:
    uint8_t _boxNo;
    uint8_t _slotNo;
    PokemonPtr _pokemon;
};

#endif

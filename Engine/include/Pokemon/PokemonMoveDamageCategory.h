#ifndef KRISTALL_INCLUDE_POKEMON_POKEMONMOVEDAMAGECATEGORY_H
#define KRISTALL_INCLUDE_POKEMON_POKEMONMOVEDAMAGECATEGORY_H

enum class PokemonMoveDamageCategory : uint8_t
{
    Physical,
    Special,
    Status
};

#endif

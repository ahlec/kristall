#ifndef KRISTALL_INCLUDE_POKEMON_DELEGATES_H
#define KRISTALL_INCLUDE_POKEMON_DELEGATES_H

#include "Pokemon/Predefine.h"
#include "Characters/Predefine.h"

typedef void (* EggHatchingDelegate)(Pokemon&);
typedef void (* EggHatchedDelegate)(Pokemon&);
typedef void (* PokemonEvolutionTriggeredDelegate)(const Pokemon&, const PokemonSpeciesPtr);
typedef void (* PokemonEvolvedDelegate)(Pokemon&);
typedef void (* PokemonFaintedDelegate)(Pokemon&);
typedef void (* PokemonFormeChangedDelegate)(Pokemon&);
typedef void (* PokemonJoinsTrainerDelegate)(Pokemon&, PokemonTrainer&);
typedef void (* PokemonLeveledDelegate)(Pokemon&, char, unsigned short[6]);
typedef void (* PokemonRevivedDelegate)(Pokemon&);
typedef void (* PokemonWantsToLearnMoveDelegate)(const Pokemon&, const PokemonMovePtr);
typedef void (* PokemonMoveSelectedDelegate)(const Pokemon&, const PokemonMovePtr);

#endif

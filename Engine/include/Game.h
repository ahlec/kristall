#ifndef KRISTALL_INCLUDE_GAME_H
#define KRISTALL_INCLUDE_GAME_H

#include "GameSettings.h"
#include "Cornerstones.h"
#include "Player/Predefine.h"
#include "Interface/Predefine.h"
#include "General/Mutex.h"

class Game
{
public:
    static void initialize();
    static void deinitialize();

    static const GameWindowPtr& getGameWindow();

    static const GameSettings& getSettings();

    static void lockMutex(Mutex* mutex);

private:
    static GameWindowPtr s_gameWindow;
    static GameSettings* s_gameSettings;
};

#endif

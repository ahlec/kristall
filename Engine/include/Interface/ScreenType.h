#ifndef KRISTALL_INCLUDE_INTERFACE_SCREENTYPE_H
#define KRISTALL_INCLUDE_INTERFACE_SCREENTYPE_H

/* enum class ScreenType
 *
 * This enumeration is only to be used when checking for type equality.
 * The values here are not in any hard-coded order, and the order of this
 * enumeration can be changed at will. This enumeration is not meant for any
 * sort of public consumption outside the the use of checking for type
 * equality.
 */

enum class ScreenType
{
    AREA_NAME_SCREEN,
    BAG_ITEM_FRAME_SCREEN,
    BAG_SCREEN,
    BLACK_OUT_SCREEN,
    CHARACTER_PARTY_SCREEN,
    COMPUTER_BOX_SCREEN,
    EGG_HATCHING_SCREEN,
    EVOLUTION_SCREEN,
    FLIGHT_MAP_SCREEN,
    GYM_BADGE_RECEIVED_SCREEN,
    IN_GAME_MENU_SCREEN,
    PARTY_MEMBER_SELECTION_SCREEN,
    POKEDEX_SCREEN,
    POKEMON_MOVE_OVERWORLD_USE_SCREEN,
    POKEMON_MOVE_SELECTION_SCREEN,
    REGION_MAP_SCREEN,
    SAVE_SCREEN,
    SETTINGS_SCREEN,
    STORE_BUY_SCREEN,
    STORE_SCREEN,
    TEXTBOX_SCREEN,
    TEXT_INPUT_SCREEN
};

#endif

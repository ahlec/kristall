#ifndef KRISTALL_INCLUDE_INTERFACE_SCREEN_H
#define KRISTALL_INCLUDE_INTERFACE_SCREEN_H

#include "Cornerstones.h"
#include "Interface/ScreenType.h"
#include "Player/Predefine.h"
#include "Input/Predefine.h"
#include "Graphics/Predefine.h"
#include "General/Function.h"
#include "Graphics/UserInterfaceSprites.h"

class Screen
{
public:
    Screen(bool doesTakeInput, bool doesPauseGameplay, bool doesDrawOverworld);
    virtual ~Screen();

    virtual ScreenType getScreenType() const = 0;
    bool doesTakeInput() const;
    bool isGameplayPaused() const;
    bool isOverworldDrawn() const;
    bool isClosed() const;
    bool isInitialized() const;

    virtual void update(uint32_t elapsedTime, PlayerGame* playerGame, const ProcessedInput& input)=0;
    virtual void draw(SpriteBatch* spriteBatch, PlayerGame* playerGame)=0;

    void registerCloseCallback(FunctionPtr<void, Screen*> callback);
    void close();
    void initialize(PlayerGame* playerGame, SpriteBatch* spriteBatch);

protected:

    virtual void onInitialize(PlayerGame* playerGame, SpriteBatch* spriteBatch);
    virtual void onClose();

private:
    bool m_doesTakeInput;
    bool m_isGameplayPaused;
    bool m_isOverworldDrawn;
    bool m_isClosed;
    bool m_isInitialized;

    std::vector< FunctionPtr<void, Screen*> > m_closeCallbacks;
};

#endif

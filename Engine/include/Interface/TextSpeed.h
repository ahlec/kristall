#ifndef KRISTALL_INCLUDE_INTERFACE_TEXTSPEED_H
#define KRISTALL_INCLUDE_INTERFACE_TEXTSPEED_H

#include "Cornerstones.h"

enum class TextSpeed
{
    Slow,
    Medium,
    Fast
};

namespace TextSpeeds
{
    inline int32_t getCharacterRate(TextSpeed textSpeed)
    {
        switch (textSpeed)
        {
            case TextSpeed::Slow:
                {
                    return 50;
                }
            case TextSpeed::Medium:
                {
                    return 20;
                }
            case TextSpeed::Fast:
                {
                    return 7;
                }
        }
    };
}

#endif

#ifndef KRISTALL_INCLUDE_INTERFACE_GAMEWINDOW_H
#define KRISTALL_INCLUDE_INTERFACE_GAMEWINDOW_H

#include "Cornerstones.h"
#include "World/Predefine.h"
#include "Input/Predefine.h"
#include "Graphics/Predefine.h"
#include "Interface/Predefine.h"
#include "Player/Predefine.h"
#include <map>

class SDL_Window;
class SDL_Renderer;
class SDL_Surface;
class Mutex;

class GameWindow
{
    friend class Game;
public:

    /// ----------------------------------------------------- Game functionality
    GameWindow();
    #if DEBUG
    void initializePlayerGame();    /// to be replaced with the SystemScreen main menu
    #endif
    void run();

    /// ----------------------------------------------------- Window
    int32_t                     getWidth();
    int32_t                     getHeight();
    bool                        hasFocus();

    Overworld*                  getOverworld();
    const SpriteSheetLibrary&   getSpriteSheetLibrary() const;

    /// ----------------------------------------------------- UI
    void                        openScreen(Screen* screen);

private:
    void update(uint32_t elapsedTime);
    void draw();

    void                        removeScreenFromList(Screen* screen, bool doDeleteScreen = true);
    void                        onInGameMenuScreenClosed(Screen* screen);
    void                        onScreenClosed(Screen* screen);

    SDL_Window*                     m_window;
    SDL_Surface*                    m_windowSurface;
    SDL_Renderer*                   m_renderer;
    bool                            m_isActive;
    Overworld*                      m_overworld;
    SpriteSheetLibrary*             m_spriteSheetLibrary;
    SpriteBatch*                    m_spriteBatch;
    std::map<uint32_t, Texture*>    m_textures;
    InputController*                m_inputController;
    PlayerGame*                     m_currentPlayerGame;

    SystemScreen*                   m_systemScreen;
    bool                            m_doScreensAllowOverworldDrawing;
    std::vector<Screen*>            m_screens;
    Mutex*                          m_screensMutex;

    #if DEBUG
    class GameWindowDebugData;
    GameWindowDebugData*            m_debugData;
    #endif
};

#endif

#ifndef KRISTALL_INCLUDE_INTERFACE_PREDEFINE_H
#define KRISTALL_INCLUDE_INTERFACE_PREDEFINE_H

#include <memory>

class GameWindow;
class Screen;
class SystemScreen;

typedef std::shared_ptr<GameWindow> GameWindowPtr;

#endif

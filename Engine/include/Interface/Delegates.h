#ifndef INTERFACE_DELEGATES_H
#define INTERFACE_DELEGATES_H

#include "World/Predefine.h"
#include "Interface/Predefine.h"

typedef void (* ScreenClosedDelegate)();
typedef void (* OverworldOccupantOpensScreenDelegate)(OverworldOccupant&, Screen&);

#endif

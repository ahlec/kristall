#ifndef KRISTALL_INCLUDE_INTERFACE_SCREENS_INGAMEMENUSCREEN_H
#define KRISTALL_INCLUDE_INTERFACE_SCREENS_INGAMEMENUSCREEN_H

#include "Interface/Screen.h"
#include "Player/Predefine.h"
#include "General/Vector2.h"
#include "General/Rect.h"
#include "Interface/GameWindow.h"
#include "Graphics/Predefine.h"

class InGameMenuScreen : public Screen
{
public:
    InGameMenuScreen(GameWindow* window, PlayerGame* playerGame);
    virtual ~InGameMenuScreen();

    virtual ScreenType getScreenType() const final { return ScreenType::IN_GAME_MENU_SCREEN; };
    virtual void update(uint32_t elapsedTime, PlayerGame* playerGame, const ProcessedInput& input) final;
    virtual void draw(SpriteBatch* spriteBatch, PlayerGame* playerGame) final;

private:
    class MenuItem;

    std::vector<MenuItem*>      m_menuItems;
    int                         m_menuHeight;
    Vector2                     m_menuOffset;
    Rect                        m_menuRectangle;
    FramePtr                    m_menuFrame;
    int                         m_currentSelectionIndex;
};

#endif

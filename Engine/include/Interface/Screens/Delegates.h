#ifndef INTERFACE_SCREENS_DELEGATES_H
#define INTERFACE_SCREENS_DELEGATES_H

typedef void (* flightLocationSelectedDelegate)(const Region&, const FlightLocation&);
typedef void (* partyMembersSelectedDelegate)(PlayerCharacter*, Pokemon* const);
typedef void (* textInputScreenValueConfirmedDelegate)(const std::string&);

#endif

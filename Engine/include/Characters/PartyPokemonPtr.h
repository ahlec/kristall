#ifndef KRISTALL_INCLUDE_CHARACTERS_PARTYPOKEMONPTR_H
#define KRISTALL_INCLUDE_CHARACTERS_PARTYPOKEMONPTR_H

class PartyPokemonPtr
{
    friend class PlayerCharacter;
public:
    PartyPokemonPtr(PokemonPtr pokemon);
    ~PartyPokemonPtr();

    Pokemon& operator*() const;
    PokemonPtr operator->() const;

private:
    PokemonPtr _pokemon;
    uint8* _nStepsTakenWith;
    uint8* _nAcknowledgements;

    void markStepTakenWith();
};

#endif

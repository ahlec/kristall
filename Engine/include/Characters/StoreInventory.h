#ifndef CHARACTERS_STOREINVENTORY_H
#define CHARACTERS_STOREINVENTORY_H

class StoreInventory
{
public:
    StoreInventory(BinaryReader& reader);
    ~StoreInventory();

    std::vector<ItemPtr> getAvailableInventory(const PlayerGame& currentPlayerGame) const;

private:
    struct StockItem
    {
        uint16_t itemHandleNo;
        uint8_t nBadgesRequired;
    };

    int32_t _nInventory;
    StockItem* _inventory;
};

#endif

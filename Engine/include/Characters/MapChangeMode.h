#ifndef KRISTALL_INCLUDE_CHARACTERS_MAPCHANGEMODE_H
#define KRISTALL_INCLUDE_CHARACTERS_MAPCHANGEMODE_H

enum class MapChangeMode
{
    ExplicitPlacement,
    MapBridge,
    Door
};

#endif

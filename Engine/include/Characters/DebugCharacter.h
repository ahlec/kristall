#ifndef KRISTALL_INCLUDE_CHARACTERS_DEBUGCHARACTER_H
#define KRISTALL_INCLUDE_CHARACTERS_DEBUGCHARACTER_H

#include "Cornerstones.h"
#include "Characters/HumanCharacter.h"

#if DEBUG

class DebugCharacter : public HumanCharacter
{
public:
    DebugCharacter();

    void                beginDebug();

    // OverworldOccupant
    virtual OverworldOccupantType getType() const override;

    // Character
    virtual String getName() const override;

    virtual void activate(Character& activator) override;

protected:
    virtual void onUpdate(uint32_t elapsedTime) override;

private:
    class DebugCharacterData;

    /// ----------------------------------------------------- Character

    /// ----------------------------------------------------- Debug
    DebugCharacterData*             m_debugData;

};

#endif // DEBUG

#endif // Header guard

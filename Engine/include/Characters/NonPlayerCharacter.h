#ifndef KRISTALL_INCLUDE_CHARACTERS_NONPLAYERCHARACTER_H
#define KRISTALL_INCLUDE_CHARACTERS_NONPLAYERCHARACTER_H

#include "Cornerstones.h"
#include "Characters/Predefine.h"
#include "Characters/HumanCharacter.h"

class NonPlayerCharacter : public HumanCharacter
{
public:
    NonPlayerCharacter(BinaryReader& reader, const PlayerGame& currentPlayerGame);
    virtual ~NonPlayerCharacter();

    // OverworldOccupant
    virtual OverworldOccupantType getType() const override;

    // Character
    virtual String getName() const override;

    void destroy();

    bool getHasSight() const;
    uint8_t getSightDistance() const;
    void processSight(PlayerCharacter& currentPlayerCharacter);

    bool hasStoreInventory() const;
    StoreInventoryPtr getStoreInventory() const;

    virtual void activate(Character& activator) override;

protected:
    std::string _name;

    virtual void readHeader(BinaryReader& reader,
                            const PlayerGame& currentPlayerGame);
    virtual void onUpdate(milliseconds elapsedTime) override;
    virtual void onPlayerCharacterSighted(PlayerCharacter& playerCharacter);

    void setMovementPatternDisabled(bool isMovementPatternDisabled);

    virtual bool isSightActive(const PlayerCharacter& currentPlayerCharacter) const;

    void executeOverworldLua(const std::string& fieldName, const std::string& luaCode,
                             PlayerCharacter& playerCharacter,
                             luaExecutionCompleteDelegate executionComplete = nullptr);

private:
    bool _isInteracting;
    bool _shouldTurnOnActivation;
    bool _shouldResumeDirectionOnDeactivation;
    std::string _interactLuaCode;

    bool _hasSight;
    uint8_t _sightDistance;
    std::string _sightedLuaCode;

    MovementPatternPtr _movementPattern;
    bool _isMovementPatternDisabled;

    StoreInventoryPtr _storeInventory;

};

#endif

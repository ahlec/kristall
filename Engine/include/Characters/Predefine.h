#ifndef KRISTALL_INCLUDE_CHARACTERS_PREDEFINE_H
#define KRISTALL_INCLUDE_CHARACTERS_PREDEFINE_H

#include <memory>

class Character;
class DayCare;
class EmotionDisplay;
class PlayerCharacter;
class PokemonTrainer;
class PokemonTrainerMusicSelection;

typedef std::shared_ptr<EmotionDisplay> EmotionDisplayPtr;

#endif

#ifndef KRISTALL_INCLUDE_CHARACTERS_CHARACTER_H
#define KRISTALL_INCLUDE_CHARACTERS_CHARACTER_H

#include "Cornerstones.h"
#include "World/OverworldOccupant.h"
#include "General/Gender.h"
#include "Graphics/Delegates.h"
#include "Interface/Delegates.h"
#include "Characters/Emote.h"
#include "Characters/TransportMode.h"
#include "General/Mutex.h"
#include "Characters/Delegates.h"
#include "Characters/MapChangeMode.h"
#include "General/Comparable.h"
#include "World/StaircaseType.h"

class Character : public OverworldOccupant
{
    friend class TilePreparationFunctor;
public:
    Character();
    virtual ~Character();

    /// ----------------------------------------------------- Character
    virtual String                  getName() const = 0;
    Gender                          getGender() const;

    /// ----------------------------------------------------- Map
    Tile*                           getCurrentlyFacedTile() const;
    void                            setAt(uint32_t mapNo, uint16_t tileX, uint16_t tileY);
    void                            setAt(Tile* tile);
    void                            warpTo(Tile& tile);

    /// ----------------------------------------------------- Actions
    void                            emote(Emote emotion);
    Emote                           getCurrentEmotion() const;
    void                            speak(const std::string& dialogue,
                                          OverworldOccupantOpensScreenDelegate screenOpened = nullptr);

    /// ----------------------------------------------------- Rendering / Graphics
    int16_t                         getAdditionalYOffset() const;
    void                            setIsVisible(bool isVisible);
    virtual void                    playAnimation(const std::string& animationHandle,
                                                  AnimationFinishedDelegate animationFinished) = 0;

    /// ----------------------------------------------------- Movement
    bool                            getIsJumping() const;
    Direction                       getLastDirectionWalked() const;
    bool                            isMoving() const;
    bool                            canMoveForward() const;
    TransportMode                   getCurrentTransportMode() const;
    void                            setTransportMode(TransportMode newMode);
    void                            moveForward(bool isFollowerIgnored = false,
                                                Mutex* threadMutex = nullptr);
    void                            moveTo(MapLocation destination,
                                           CharacterDelegate onArrival = nullptr);
    void                            moveTo(OverworldOccupant& target,
                                           CharacterDelegate onArrival = nullptr);
    void                            pushForward();
    void                            moveInPlace();
    void                            attachCallbackToCurrentMoveCycle(CharacterDelegate callback);
    virtual void                    turn(Direction newDirection) override;

    /// ----------------------------------------------------- Followers / Leaders
    Character*                      getFollower() const;
    void                            setFollower(Character* newFollower);
    Character*                      getLeader() const;
    void                            setLeader(Character* newLeader);

    /// ----------------------------------------------------- Misc
    virtual bool                    getIsSign() const final; // via OverworldEntry

protected:
    enum class TileEntrancePermissions
    {
        CannotEnter,
        CanEnter,
        CanEnterUsingStrength
    };

    Gender _gender;
    Direction _lastDirectionWalked;
    uint32_t _spinTimeElapsed;
    Direction _currentSpinningDirection;
    bool _isMoving;
    bool _isFakeMoving;
    bool _isColliding;
    bool _isSlidingOnIce;
    bool _isSpinningBecauseOfSpinner;

    virtual void update(uint32_t elapsedTime) final;
    virtual void updateAnimation(uint32_t elapsedTime) = 0;
    virtual void onUpdate(uint32_t elapsedTime) = 0;
    virtual void animateMoveForward(bool isLeadingLeftLeg) = 0;
    virtual void animateStandingIdle() = 0;

    virtual void onParentChanged(Tile* oldParent, Tile* newParent) final;
    virtual void onTransportModeChanged(TransportMode oldMode, TransportMode newMode);
    virtual void onMovingBegun(Tile* destination);
    virtual void onMovingFinished();
    virtual void onMapChanged(MapPtr oldMap, MapPtr newMap,
                              MapChangeMode changeMode);
    virtual void onJump();

    virtual bool isStrengthActive() const;

    virtual TileEntrancePermissions calculateTileEntrance(int32_t tileX, int32_t tileY,
                                                          TransportMode transportation,
                                                          bool isStrengthAllowed) const;

private:
    class PathfindingTile : public Comparable
    {
    public:
        PathfindingTile(int x, int y, int widthOfMap, int heightOfMap);

        int tileX;
        int tileY;
        int mapWidth;
        int mapHeight;

        virtual int compareTo(const Comparable& other) const override;
    };

    class PathfindingInformation
    {
    public:
        std::stack<PathfindingTile> path;
        PathfindingTile* currentStep;
        CharacterDelegate onArrivalCallback;
    };

    class TilePreparationFunctor : Functor<void, Tile*>
    {
        friend class Character;
    public:
        TilePreparationFunctor(Character* character, bool areFollowersIgnored,
                               TileEntrancePermissions entrancePermissions);
        virtual void operator()(Tile* tile) final;

    private:
        Character* _character;
        bool _areFollowersIgnored;
        TileEntrancePermissions _entrancePermissions;
    };

    Character* _leader;
    Character* _follower;

    bool _isJumping;
    bool _isSpeaking;

    int16_t _additionalYOffset;

    EmotionDisplayPtr _currentEmotion;

    TransportMode _currentTransportMode;

    bool _isPreparingToMove;
    bool _isFinishingMove;
    bool _isLeadingWithLeftLeg;
    uint32_t _moveTimeRemaining;
    uint32_t _moveTimeDuration;
    Mutex* _heldMoveMutex;
    Tile* _lastParent;
    MapPtr _lastMap;
    MapChangeMode _moveCurrentMapChangeMode;
    StaircaseType _currentlyUsedStaircase;
    bool _isUsingStaircaseAndAlreadyChangedMaps;
    CharacterDelegate _moveFinishedCallback;

    PathfindingInformation* _pathfindingInfo;

    bool isActuallyMoving() const;
    void move(bool areChecksIgnored, bool isFollowerIgnored, bool isStrengthAllowed,
              Mutex* threadMutex);
    void collide();
    void moveCharacterIntoTile(Tile* destination, bool isFollowerIgnored,
                               TileEntrancePermissions enterPermissions);
};

#endif

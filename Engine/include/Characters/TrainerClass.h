#ifndef KRISTALL_INCLUDE_CHARACTERS_TRAINERCLASS_H
#define KRISTALL_INCLUDE_CHARACTERS_TRAINERCLASS_H

#include "General/Gender.h"
#include "Characters/Predefine.h"

enum class TrainerClass
{
    Youngster,
    BugCatcher,
    Lass,
    Sailor,
    Camper,
    Picnicker,
    PokeManiac,
    SuperNerd,
    Hiker,
    Biker,
    Burglar,
    Engineer,
    Juggler,
    Fisherman,
    Swimmer,
    Roughneck,
    PI,
    Beauty,
    Psychic,
    Rocker,
    Tamer,
    BirdKeeper,
    BlackBelt,
    Scientist,
    Grunt,
    AceTrainer,
    Gentleman,
    Channeler,
    Sage,
    Firebreather,
    PokeFan,
    Twins,
    Medium,
    KimonoGirl,
    Commander,
    Skier,
    Policeman,
    Guitarist,
    Teacher,
    AromaLady,
    YoungCouple,
    Collector,
    BattleGirl,
    PokemonBreeder,
    DragonTamer,
    NinjaBoy,
    Tuber,
    Interviewers,
    RichBoy,
    Lady,
    Cyclist,
    Artist,
    Jogger,
    BelleAndPa,
    Cameraman,
    Clown,
    Cowgirl,
    DoubleTeam,
    Idol,
    PokeKid,
    Rancher,
    Reporter,
    Socialite,
    Veteran,
    Waiter,
    Waitress,
    Worker,
    Elder,
    Maid,
    Infielder,
    Striker,
    Hoopster,
    Linebacker,
    Smasher,
    Backers,
    GymLeader,
    EliteFour,
    Champion,
    PKMNTrainer,
    Rival,
    Boarder
};

namespace TrainerClasses
{
    uint16_t getBattleSpriteNo(TrainerClass trainerClass, Gender gender);
    uint16_t getOverworldSpriteNo(TrainerClass trainerClass, Gender gender);
    uint16_t getBasePayout(TrainerClass trainerClass, Gender gender);
    std::string getClassName(TrainerClass trainerClass, Gender gender);
    PokemonTrainerMusicSelection getMusicSelection(TrainerClass trainerClass,
                                                   Gender gender);
    std::string getBattleAi(TrainerClass trainerClass, Gender gender);
};

#endif

#ifndef KRISTALL_INCLUDE_CHARACTERS_DELEGATES_H
#define KRISTALL_INCLUDE_CHARACTERS_DELEGATES_H

#include "General/Function.h"
#include "Characters/Predefine.h"
#include "World/Predefine.h"
#include "Characters/Emote.h"

typedef FunctionPtr<void, Character*> CharacterDelegate;
typedef void (* CharacterHasSpottedDelegate)(Character&, PlayerCharacter&);
typedef void (* CharacterArrivedAtDestinationDelegate)(Character&, OverworldOccupant&);

typedef void (* CharacterEmotionDelegate)(Character&, Emote);

#endif

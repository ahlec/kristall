#ifndef KRISTALL_INCLUDE_CHARACTERS_POKEMONTRAINER_H
#define KRISTALL_INCLUDE_CHARACTERS_POKEMONTRAINER_H

#include "Characters/TrainerClass.h"
#include "Pokemon/Predefine.h"
#include "Pokemon/PokemonAcquisitionMethod.h"
#include "Items/Predefine.h"
#include "General/Predicate.h"

class PokemonTrainer
{
public:
    virtual uint16_t getTrainerNo() const=0;
    virtual uint16_t getSecretNo() const=0;
    bool getIsPassiveBattler() const;
    virtual TrainerClass getTrainerClass() const=0;
    virtual const PokemonTrainerMusicSelection& getMusicSelection() const=0;
    virtual const std::string& getTextOnDefeat() const=0;
    virtual uint16_t getBasePayout() const=0;

    virtual int32_t getTotalNumberPokemon() const=0;
    virtual PokemonLocationPtr givePokemon(Pokemon& pokemonGiven,
                                PokemonAcquisitionMethod acquisitionMethod,
                                PokeballItem containingPokeball)=0;

    virtual std::vector<PokemonPtr> getParty()=0;
    virtual PokemonPtr getFirstPokemonInParty(Predicate<Pokemon> condition = nullptr)=0;

    virtual PokemonPtr getPokemon(PokemonLocationPtr location)=0;

protected:
    bool _isPassiveBattler;
};

#endif

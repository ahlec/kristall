#ifndef KRISTALL_INCLUDE_CHARACTERS_POKEMONTRAINERMUSICSELECTION_H
#define KRISTALL_INCLUDE_CHARACTERS_POKEMONTRAINERMUSICSELECTION_H

class PokemonTrainerMusicSelection
{
public:
    PokemonTrainerMusicSelection(uint16_t spottedTrackNo,
                                 uint16_t battleTrackNo,
                                 uint16_t defeatedTrackNo);

    uint16_t getSpottedTrackNo() const;
    uint16_t getBattleTrackNo() const;
    uint16_t getDefeatedTrackNo() const;

private:
    uint16_t _spottedTrackNo;
    uint16_t _battleTrackNo;
    uint16_t _defeatedTrackNo;
};

#endif

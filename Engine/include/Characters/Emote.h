#ifndef KRISTALL_INCLUDE_CHARACTERS_EMOTE_H
#define KRISTALL_INCLUDE_CHARACTERS_EMOTE_H

enum class Emote
{
    None,
    Notice,
    Confusion
};

#endif

#ifndef KRISTALL_INCLUDE_CHARACTERS_DAYCARE_H
#define KRISTALL_INCLUDE_CHARACTERS_DAYCARE_H

#include "Cornerstones.h"
#include "Characters/Predefine.h"
#include "Pokemon/Predefine.h"
#include "General/Money.h"

class DayCare
{
    friend class PlayerCharacter;
public:
    DayCare(BinaryReader& reader, PlayerCharacter& currentPlayerCharacter);
    ~DayCare();

    int32_t getDayCareNo() const;
    int32_t getCapacity() const;
    const std::string& getNpcName() const;
    int getOccupationCount() const;

    bool isBreedingPossible() const;
    int getBreedingRate() const;
    bool hasEgg() const;

    PokemonSpeciesPtr getPokemonASpecies() const;
    std::string getPokemonAName() const;
    int getPokemonALevelGain() const;

    PokemonSpeciesPtr getPokemonBSpecies() const;
    std::string getPokemonBName() const;
    int getPokemonBLevelGain() const;

    void givePokemon(Pokemon& pokemonDroppedOff);
    Pokemon& takePokemon(int slotNo);
    Money calculatePickupFee(int slotNo);

    bool operator==(const DayCare& other) const;
    bool operator!=(const DayCare& other) const;

private:
    int32_t _daycareNo;
    int32_t _capacity;
    std::string _npcName;

    int32_t _breedingRate;

    PokemonPtr _pokemonA;
    uint8_t _pokemonAEntryLevel;
    PokemonPtr _pokemonB;
    uint8_t _pokemonBEntryLevel;
    PokemonPtr _egg;

    void markPlayerStepTaken();
    void markEggCycleComplete();
};

#endif

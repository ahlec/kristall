#ifndef CHARACTERS_RIVALCHARACTER_H
#define CHARACTERS_RIVALCHARACTER_H

class RivalCharacter : public TrainerCharacter
{
public:
    RivalCharacter(BinaryReader& reader, const PlayerGame& currentPlayerGame,
                   uint16_t rivalNo);
    ~RivalCharacter();

    static std::string getRivalName(const PlayerGame& currentPlayerGame,
                               uint16_t rivalNo);

    // OverworldOccupant
    virtual OverworldOccupantType getType() const final;

    //Character
    virtual String getName() const override;

protected:

private:
    uint16_t _rivalNo;
    const PlayerGame& _currentPlayerGame;

};

#endif

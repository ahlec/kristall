#ifndef KRISTALL_INCLUDE_CHARACTERS_PLAYERCHARACTER_H
#define KRISTALL_INCLUDE_CHARACTERS_PLAYERCHARACTER_H

#include "Characters/HumanCharacter.h"
#include "Characters/PokemonTrainer.h"
#include "Battles/BattleTeamMember.h"
#include "Player/Predefine.h"
#include "World/EncounterMethod.h"
#include "Characters/Predefine.h"
#include "Input/Predefine.h"

class PlayerCharacter : public HumanCharacter, public PokemonTrainer, public BattleTeamMember
{
    friend class BlackOutScreen;
    friend class DayCare;
    friend class GameWindow;
public:
    PlayerCharacter(BinaryReaderPtr& reader, PlayerGame* playerGame);
    PlayerCharacter(PlayerGame& playerGame, Gender gender,
                    const Region& startingRegion);
    ~PlayerCharacter();

    void setInputDisabled(bool isInputDisabled);
    void activateRepel(int32_t nSteps);
    void fish(EncounterMethod encounterMethod);
    void interact();
    void usePokemonMoveOnOverworld(PartyPokemonLocation activatingPartyMember,
                                   const PokemonMove& moveActivated);

    int32_t getNumberPartyPokemon() const;
    int32_t getNumberComputerPokemon() const;

    void swapPokemon(PokemonLocationPtr locationA, PokemonLocationPtr locationB);
    void putPokemonAt(PokemonLocationPtr destination, Pokemon& pokemon);
    PokemonPtr removePokemonFrom(PokemonLocationPtr source);
    bool canRemovePokemonFromParty(PartyPokemonLocation pokemonLocation,
                                   const PokemonPtr replacementPokemon = nullptr);

    DayCare& getDayCare(int32_t dayCareNo);

    bool canUseItemInOverworld(const Item& item,
                               PokemonLocationPtr targetPokemon = nullptr);
    void useItemInOverworld(const Item& item,
                            PokemonLocationPtr targetPokemon = nullptr);

    // OverworldOccupant
    virtual OverworldOccupantType getType() const final;

    // Character
    virtual String getName() const override;
    virtual void activate(Character& activator) override;

    // PokemonTrainer
    virtual uint16_t getTrainerNo() const override;
    virtual uint16_t getSecretNo() const override;
    virtual TrainerClass getTrainerClass() const override;
    virtual const PokemonTrainerMusicSelection& getMusicSelection() const override;
    virtual const std::string& getTextOnDefeat() const override;
    virtual uint16_t getBasePayout() const override;
    virtual int32_t getTotalNumberPokemon() const override;
    virtual PokemonLocationPtr givePokemon(Pokemon& pokemonGiven,
                                PokemonAcquisitionMethod acquisitionMethod,
                                PokeballItem containingPokeball) override;
    virtual std::vector<PokemonPtr> getParty() override;
    virtual PokemonPtr getFirstPokemonInParty(Predicate<Pokemon> condition = nullptr) override;
    virtual PokemonPtr getPokemon(PokemonLocationPtr location) override;

protected:
    // Character
    virtual void onUpdate(uint32_t elapsedTime) override;
    virtual void onMapChanged(MapPtr oldMap, MapPtr newMap,
                              MapChangeMode changeMode) override;
    virtual void onTurned(Direction oldDirection, Direction newDirection) override;
    virtual void onMovingBegun(Tile* destination) override;
    virtual void onMovingFinished() override;
    virtual void onTransportModeChanged(TransportMode oldMode,
                                        TransportMode newMode) override;
    virtual bool isStrengthActive() const override;

private:
    class PartyPokemon
    {
    public:
        PartyPokemon(PokemonPtr pokemon, uint8_t nStepsTakenWith);
        PokemonPtr getPokemon() const;
        uint8_t getNStepsTakenWith() const;
        void markStepTaken();
    private:
        PokemonPtr _pokemon;
        uint8_t _nStepsTakenWith;
    };

    PlayerGame* _playerGame;
    Mutex _mutex;
    uint16_t _trainerNo;
    uint16_t _secretNo;
    bool _isInputDisabled;
    MapPtr _lastMapWithRecoveryLocation;
    bool _isInputDirectionQueued;
    Direction _queuedInputDirection;
    bool _hasTurnedSinceLastMove;

    PartyPokemon* _party;
    uint8_t _nPartyPokemonCurrently;

    std::vector<DayCare> _activeDayCares;

    bool _isRepelActive;
    int _nStepsRepelRemaining;

    int _nEncounterQualifiedStepsTaken;

    void interpretInput(const ProcessedInput& input);
    void chanceWildEncounter();

    void openDayCare(DayCare& dayCare);
    void closeDayCare(DayCare& dayCare);
};

#endif

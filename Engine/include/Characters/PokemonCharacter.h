#ifndef KRISTALL_INCLUDE_CHARACTERS_POKEMONCHARACTER_H
#define KRISTALL_INCLUDE_CHARACTERS_POKEMONCHARACTER_H

class PokemonCharacter : public Character
{
public:
    PokemonCharacter(const PokemonTrainer& trainer);
    PokemonCharacter(BinaryReader& reader, const PlayerGame& currentPlayerGame,
                     uint16_t tileX, uint16_t tileY);
    ~PokemonCharacter();

    void setIsInPokeball(bool isInPokeball);
    bool getIsInPokeball() const;

    // OverworldOccupant
    virtual OverworldOccupantType getType() const final;

    // Character
    virtual String getName() const override;
    virtual uint32_t getSpriteNo() const override;
    virtual SpriteSheet getSpriteSheet() const override;
    virtual void activate(Character& activator) override;
    virtual void playAnimation(const std::string& animationHandle,
                               animationFinishedDelegate animationFinished) override;
protected:
    // Character
    virtual void updateAnimation(milliseconds elapsedTime) override;
    virtual void onUpdate(milliseconds elapsedTime) override;
    virtual void onTurned(Direction oldDirection, Direction newDirection) override;
    virtual void onTransportModeChanged(TransportMode oldMode,
                                        TransportMode newMode) override;

private:
    AnimationInstancePtr _animation;
    WanderMovementAction* _wanderAction;
    PokemonPtr _pokemon;
    bool _isInPokeball;
    milliseconds _bounceTime;
    milliseconds _bounceDuration;
    std::string _activationLuaCode;

    bool _isWildPokemonOnlyOne;
    uint16_t _wildPokemonOnlyOneHandle;

};

#endif

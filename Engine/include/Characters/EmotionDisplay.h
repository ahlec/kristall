#ifndef KRISTALL_INCLUDE_CHARACTERS_EMOTIONDISPLAY_H
#define KRISTALL_INCLUDE_CHARACTERS_EMOTIONDISPLAY_H

class EmotionDisplay
{
    friend class Character;
public:
    EmotionDisplay(Character& emotee, Emote emotion);

    Emote getEmotion() const;
    uint16_t getCurrentSpriteNo() const;

private:
    Character& _emotee;
    Emote _emotion;
    bool _isEmoting;
    AnimationInstancePtr _animation;

    void update(milliseconds elapsedTime);
    void animationFinished();
};

#endif

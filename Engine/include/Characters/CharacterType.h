#ifndef KRISTALL_INCLUDE_CHARACTERS_CHARACTERTYPE_H
#define KRISTALL_INCLUDE_CHARACTERS_CHARACTERTYPE_H

enum class CharacterType
{
    NonPlayerCharacter,
    TrainerCharacter,
    RivalCharacter,
    PlayerCharacter,
    PokemonCharacter
};

#endif

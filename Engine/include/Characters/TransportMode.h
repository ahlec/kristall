#ifndef KRISTALL_INCLUDE_CHARACTERS_TRANSPORTMODE_H
#define KRISTALL_INCLUDE_CHARACTERS_TRANSPORTMODE_H

#include "Cornerstones.h"

enum class TransportMode
{
    Walk,
    Run,
    Bike,
    Surf
};


template<> String toString<TransportMode>(TransportMode value);

namespace TransportModes
{
    uint32_t getJumpDuration();
    uint32_t getMoveDuration(TransportMode mode);
};

#endif

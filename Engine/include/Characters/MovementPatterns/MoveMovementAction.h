#ifndef CHARACTERS_MOVEMENTPATTERNS_MOVEMOVEMENTACTION_H
#define CHARACTERS_MOVEMENTPATTERNS_MOVEMOVEMENTACTION_H

class MoveMovementAction : public MovementAction
{
public:
    MoveMovementAction(uint8_t numberTiles, Direction forwardDirection);
    virtual bool isFinished(const Character& subject) const override;

private:
    uint8_t _nTilesToWalk;
    Direction _forwardDirection;

    int _nTilesLeftToWalk;

    virtual void update(milliseconds elapsedTime, Character& subject) override;
    virtual void reset() override;
};

#endif

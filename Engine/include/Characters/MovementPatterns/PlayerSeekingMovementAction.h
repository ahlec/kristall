#ifndef CHARACTERS_MOVEMENTPATTERNS_PLAYERSEEKINGMOVEMENTACTION_H
#define CHARACTERS_MOVEMENTPATTERNS_PLAYERSEEKINGMOVEMENTACTION_H

class PlayerSeekingMovementAction : public MovementAction
{
public:
    PlayerSeekingMovementAction();
    virtual bool isFinished(const Character& subject) const override;

private:
    virtual void update(milliseconds elapsedTime, Character& subject) override;
    virtual void reset() override;
};

#endif

#ifndef CHARACTERS_MOVEMENTPATTERNS_WAITMOVEMENTACTION_H
#define CHARACTERS_MOVEMENTPATTERNS_WAITMOVEMENTACTION_H

class WaitMovementAction : public MovementAction
{
public:
    WaitMovementAction(int32_t duration);
    virtual bool isFinished(const Character& subject) const override;

private:
    milliseconds _currentTime;
    int32_t _duration;

    virtual void update(milliseconds elapsedTime, Character& subject) override;
    virtual void reset() override;
};

#endif

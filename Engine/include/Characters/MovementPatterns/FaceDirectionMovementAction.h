#ifndef CHARACTERS_MOVEMENTPATTERNS_FACEDIRECTIONMOVEMENTACTION_H
#define CHARACTERS_MOVEMENTPATTERNS_FACEDIRECTIONMOVEMENTACTION_H

class FaceDirectionMovementAction : public MovementAction
{
public:
    FaceDirectionMovementAction(Direction directionToFace);
    virtual bool isFinished(const Character& subject) const override;

private:
    Direction _directionToFace;

    virtual void update(milliseconds elapsedTime, Character& subject) override;
    virtual void reset() override;
};

#endif

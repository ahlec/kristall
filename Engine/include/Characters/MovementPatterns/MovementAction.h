#ifndef CHARACTERS_MOVEMENTPATTERNS_MOVEMENTACTION_H
#define CHARACTERS_MOVEMENTPATTERNS_MOVEMENTACTION_H

class Character;
class MovementPattern;

class MovementAction
{
    friend class MovementPattern;
public:
    virtual bool isFinished(const Character& subject) const=0;

private:
    virtual void update(milliseconds elapsedTime, Character& subject)=0;
    virtual void reset()=0;
};

#endif

#ifndef CHARACTERS_MOVEMENTPATTERNS_IDLEMOVEMENTACTION_H
#define CHARACTERS_MOVEMENTPATTERNS_IDLEMOVEMENTACTION_H

class IdleMovementAction : public MovementAction
{
public:
    IdleMovementAction();
    virtual bool isFinished(const Character& subject) const override;

private:
    static RandomGenerator _randomGenerator;

    milliseconds _time;
    milliseconds _duration;

    virtual void update(milliseconds elapsedTime, Character& subject) override;
    virtual void reset() override;
};

#endif

#ifndef CHARACTERS_MOVEMENTPATTERNS_WANDERMOVEMENTACTION_H
#define CHARACTERS_MOVEMENTPATTERNS_WANDERMOVEMENTACTION_H

class WanderMovementAction : public MovementAction
{
public:
    WanderMovementAction();
    WanderMovementAction(int32_t minTileX, int32_t minTileY, int32_t maxTileX,
                         int32_t maxTileY);
    virtual bool isFinished(const Character& subject) const override;

private:
    static RandomGenerator _randomGenerator;
    static const int32_t _maxNumberTimesToWalk;

    bool _hasWanderBounds;
    int32_t _minX;
    int32_t _minY;
    int32_t _maxX;
    int32_t _maxY;

    int32_t _nTimesWalked;

    bool _isWaiting;
    milliseconds _currentWaitTime;
    static const int32_t _waitDuration;

    bool _hasJustTurned;

    void turn(Character& subject);

    virtual void update(milliseconds elapsedTime, Character& subject) override;
    virtual void reset() override;
};

#endif

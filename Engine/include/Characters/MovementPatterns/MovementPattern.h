#ifndef CHARACTERS_MOVEMENTPATTERNS_MOVEMENTPATTERN_H
#define CHARACTERS_MOVEMENTPATTERNS_MOVEMENTPATTERN_H

class Character;

class MovementPattern
{
    friend class Character;
public:
    MovementPattern(BinaryReader& reader);
    ~MovementPattern();

private:
    int32_t _nActions;
    MovementAction* _actions;

    int32_t _currentActionIndex;

    void update(milliseconds elapsedTime, Character& subject);
};

typedef std::shared_ptr<MovementPattern> MovementPatternPtr;

#endif

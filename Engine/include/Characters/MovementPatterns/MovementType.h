#ifndef KRISTALL_INCLUDE_CHARACTERS_MOVEMENTPATTERNS_MOVEMENTTYPE_H
#define KRISTALL_INCLUDE_CHARACTERS_MOVEMENTPATTERNS_MOVEMENTTYPE_H

enum class MovementType
{
    Wait,
    MoveForward,
    Face,
    Wander,
    Idle,
    PlayerSeeking
};

#endif

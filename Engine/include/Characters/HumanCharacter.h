#ifndef KRISTALL_INCLUDE_CHARACTERS_HUMANCHARACTER_H
#define KRISTALL_INCLUDE_CHARACTERS_HUMANCHARACTER_H

/**
 * abstract class HumanCharacter
 * An abstract refinement class for all Characters which are humanoid (NOTE: This class
 * does not imply that the character is played by a human -- only that the character "is"
 * a human and can do "human" things and is rendered using NPC spritesheets rather than
 * other spritesheets.
 **/

#include "Characters/Character.h"
#include "Graphics/Predefine.h"
#include "Graphics/Delegates.h"

class HumanCharacter : public Character
{
public:
    //Character
    virtual uint32_t getSpriteNo() const final;
    virtual SpriteSheet getSpriteSheet() const final;
    virtual void playAnimation(const std::string& animationHandle,
                               AnimationFinishedDelegate animationFinished) final;

protected:
    void setAnimationBaseNo(uint32_t animationBaseNo);

    // Character
    virtual void updateAnimation(uint32_t elapsedTime) final;
    virtual void animateMoveForward(bool isLeadingLeftLeg) final;
    virtual void animateStandingIdle() final;

private:
    AnimationInstancePtr _animation;
};

#endif

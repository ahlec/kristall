#ifndef CHARACTERS_TRAINERCHARACTER_H
#define CHARACTERS_TRAINERCHARACTER_H

// I don't think this is done; I just feel that I should return to this at a later time.

class TrainerCharacter : public NonPlayerCharacter, public PokemonTrainer, public BattleTeamMember
{
public:
    virtual ~TrainerCharacter();

    void reselectCurrentParty();
    void beginBattle(PlayerCharacter& playerCharacter, bool isBlackoutEnabled = true);

    // OverworldOccupant
    virtual OverworldOccupantType getType() const override;

    // PokemonTrainer
    virtual uint16_t getTrainerNo() const override;
    virtual uint16_t getSecretNo() const override;
    virtual TrainerClass getTrainerClass() const override;
    virtual const PokemonTrainerMusicSelection& getMusicSelection() const override;
    virtual const std::string& getTextOnDefeat() const override;
    virtual uint16_t getBasePayout() const override;
    virtual int32_t getTotalNumberPokemon() const override;
    virtual PokemonLocationPtr givePokemon(Pokemon& pokemonGiven,
                                PokemonAcquisitionMethod acquisitionMethod,
                                PokeballItem containingPokeball) override;
    virtual std::vector<PokemonPtr> getParty() override;
    virtual PokemonPtr getFirstPokemonInParty(Predicate<Pokemon> condition = nullptr) override;
    virtual PokemonPtr getPokemon(PokemonLocationPtr location) override;

protected:
    TrainerClass _trainerClass;
    uint16_t _basePayout;

    // NonPlayerCharacter
    virtual bool isSightActive(const PlayerCharacter& currentPlayerCharacter) const override;
    virtual void onPlayerCharacterSighted(PlayerCharacter& playerCharacter) override;

private:
    bool _canRebattle;
    bool _isPassive;
    bool _isBattleTypeForced;
    bool _isDoubleBattleForced;

    int32_t _currentPartyNo;
    std::string _partySelectionLuaCode;
    std::string _postBattleLuaCode;

    int32_t _nInitialBattleParties;
    uint8* _initialBattlePartySizes;
    PokemonPtr** _initialBattleParties;

    int32_t _nRebattleParties;
    uint8* _rebattlePartySizes;
    PokemonPtr** _rebattleParties;

};

typedef std::shared_ptr<TrainerCharacter> TrainerCharacterPtr;

#endif

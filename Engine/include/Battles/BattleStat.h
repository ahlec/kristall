#ifndef BATTLES_BATTLESTAT_H
#define BATTLES_BATTLESTAT_H

enum class BattleStat
{
    Attack,
    Defense,
    SpecialAttack,
    SpecialDefense,
    Speed,
    Accuracy,
    Evasion,
    CriticalHits
};

#endif

#ifndef BATTLES_WEATHERCONDITION_H
#define BATTLES_WEATHERCONDITION_H

enum class WeatherCondition
{
    None,
    IntenseSunlight,
    HeavyRain,
    Sandstorm,
    Hail,
    Fog
};

#endif

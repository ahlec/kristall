#ifndef BATTLES_CATCHRATEINFORMATION_H
#define BATTLES_CATCHRATEINFORMATION_H

struct CatchRateInformation
{
    uint16_t pokemonCatchRate;
    float pokeballEffectiveness;
};

#endif

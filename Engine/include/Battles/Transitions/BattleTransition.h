#ifndef BATTLES_TRANSITIONS_BATTLETRANSITION_H
#define BATTLES_TRANSITIONS_BATTLETRANSITION_H

class BattleTransition
{
public:
    static BattleTransition create(String battleTransitionName,
                                   sf::Image& overworldSnapshot);
};

#endif

#ifndef BATTLES_TRANSITIONS_DELEGATES_H
#define BATTLES_TRANSITIONS_DELEGATES_H

typedef void (* battleTransitionFinishedDelegate)();
typedef void (* binaryPromptChosenDelegate)(bool);

#endif

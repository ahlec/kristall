#ifndef BATTLES_COMPETITORS_DELEGATES_H
#define BATTLES_COMPETITORS_DELEGATES_H

class BattleCompetitor;
class PokemonBattleFighter;
class ActiveBattleCompetitor;

typedef void (* battleFighterChangedDelegate)(BattleCompetitor&,
                                              PokemonBattleFighter&);   // newActive

typedef void (* battleFighterFaintedDelegate)(ActiveBattleCompetitor&,  // competitor
                                              PokemonBattleFighter&,    // faintee
                                              PokemonBattleFighter&);   // victor
typedef void (* turnFinishedDelegate)(const BattleTurnChoice&);

#endif

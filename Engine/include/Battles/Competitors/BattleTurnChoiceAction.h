#ifndef BATTLES_COMPETITORS_BATTLETURNCHOICEACTION_H
#define BATTLES_COMPETITORS_BATTLETURNCHOICEACTION_H

enum BattleTurnChoiceAction
{
    BattleTurnChoiceActionDoNothing,
    BattleTurnChoiceActionAttack,
    BattleTurnChoiceActionUseItem,
    BattleTurnChoiceActionSwitchPokemon,
    BattleTurnChoiceActionAttemptFlee
};

#endif

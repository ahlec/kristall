#ifndef KRISTALL_INCLUDE_BATTLES_BATTLEDAMAGE_H
#define KRISTALL_INCLUDE_BATTLES_BATTLEDAMAGE_H

#include "Cornerstones.h"

struct BattleDamage
{
    uint16_t damage;
    float damageRatio;
};

#endif

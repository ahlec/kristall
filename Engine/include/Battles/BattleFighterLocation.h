#ifndef BATTLES_BATTLEFIGHTERLOCATION_H
#define BATTLES_BATTLEFIGHTERLOCATION_H

enum class BattleFighterLocation
{
    ChallengerFirst,
    OpponentFirst,
    ChallengerSecond,
    OpponentSecond
};

#endif

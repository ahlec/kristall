#ifndef BATTLES_SEMIINVULNERABLEBATTLESTAGE_H
#define BATTLES_SEMIINVULNERABLEBATTLESTAGE_H

enum class SemiInvulnerableBattleStage
{
    Underground,
    Underwater,
    Airborne
};

#endif

#ifndef BATTLES_DELEGATES_H
#define BATTLES_DELEGATES_H

class BattleDrawComponent;

typedef void (* battleDrawComponentOpenedDelegate)(BattleDrawComponent&);
typedef void (* battleTextboxFinishedWritingDelegate)();

#endif

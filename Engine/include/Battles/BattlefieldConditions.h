#ifndef BATTLES_BATTLEFIELDCONDITIONS_H
#define BATTLES_BATTLEFIELDCONDITIONS_H

enum BattlefieldConditions
{
    BattlefieldConditionsNone,
    BattlefieldConditionsReflect = 1,
    BattlefieldConditionsLightScreen = 2,
    BattlefieldConditionsGravity = 4
};

#endif

#ifndef BATTLES_BATTLETYPE_H
#define BATTLES_BATTLETYPE_H

enum class BattleType
{
    OneVOne,
    TwoVTwo,
    OneVTwo,
    TwoVOne
};

#endif

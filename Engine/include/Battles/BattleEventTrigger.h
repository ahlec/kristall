#ifndef BATTLES_BATTLEEVENTTRIGGER_H
#define BATTLES_BATTLEEVENTTRIGGER_H

enum class BattleEventTrigger
{
    BattleStart,
    FirstDamageTaken,
    FirstDamageGiven,
    LastPokemonSentOut,
    LastPokemonHalfHP
};

#endif

#ifndef BATTLES_BATTLETEAMMEMBERTYPE_H
#define BATTLES_BATTLETEAMMEMBERTYPE_H

enum class BattleTeamMemberType
{
    None,
    PlayerCharacter,
    TrainerCharacter,
    WildPokemon
};

#endif

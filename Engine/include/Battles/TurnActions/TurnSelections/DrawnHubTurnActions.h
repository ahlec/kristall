#ifndef BATTLES_TURNACTIONS_TURNSELECTIONS_DRAWNHUBTURNACTIONS_H
#define BATTLES_TURNACTIONS_TURNSELECTIONS_DRAWNHUBTURNACTIONS_H

enum DrawnHubTurnActions
{
    DrawnHubTurnActionsNothing,
    DrawnHubTurnActionsMoveSelection,
    DrawnHubTurnActionsMoveTargetSelection,
    DrawnHubTurnActionsPartySwitch,
    DrawnHubTurnActionsBagOpen,
    DrawnHubTurnActionsAttemptFlee,
    DrawnHubTurnActionsBagPocket,
    DrawnHubTurnActionsBagItem
};

#endif

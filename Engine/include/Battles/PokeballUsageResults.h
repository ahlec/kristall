#ifndef BATTLES_POKEBALLUSAGERESULTS_H
#define BATTLES_POKEBALLUSAGERESULTS_H

struct PokeballUsageResults
{
    bool isSuccessful;
    uint8_t pokeballShakesNo;
};

#endif

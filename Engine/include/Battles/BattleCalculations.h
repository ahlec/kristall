#ifndef BATTLES_BATTLECALCULATIONS_H
#define BATTLES_BATTLECALCULATIONS_H

#include "Battles/BattleDamage.h"
#include "Battles/Competitors/PokemonBattleFighter.h"
#include "Pokemon/PokemonMove.h"
#include "Battles/WeatherCondition.h"
#include "Battles/BattlefieldConditions.h"
#include "Battles/BattleStat.h"
#include "Pokemon/PokemonAbility.h"
#include "General/ElementalType.h"
#include "General/TypeEffectiveness.h"
#include "Pokemon/Pokemon.h"
#include "Battles/PokeballUsageResults.h"
#include "Battles/Competitors/ActiveBattleCompetitor.h"
#include "Battles/Battle.h"

namespace BattleCalculations
{
    BattleDamage calculateBattleDamage(PokemonBattleFighter& attackingPokemon,
                                       PokemonBattleFighter& defendingPokemon,
                                       PokemonMove& pokemonMove, int32_t movePower,
                                       WeatherCondition weather,
                                       BattlefieldConditions battlefield,
                                       bool isCriticalHit);

    float translateBattleStatModifier(BattleStat stat, uint8_t value, PokemonAbility ability);

    TypeEffectiveness getTypeEffectiveness(ElementalType attackingType, ElementalType defendingType,
                                           PokemonAbility attackersAbility);

    uint32_t* calculateBattleExperienceGain(PokemonBattleFighter& victorPokemon,
                                          PokemonBattleFighter& defeatedPokemon);

    PokeballUsageResults calculatePokeballUse(Pokemon& wildPokemon,
                                              uint16_t pokemonCatchRate,
                                              float pokeballEffectiveness);

    uint32_t calculateBattleCompetitorPayout(ActiveBattleCompetitor& loser);

    uint16_t calculatePokemonBattleFighterSpeed(PokemonBattleFighter& fighter,
                                              Battle& battle);
};

#endif

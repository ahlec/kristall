#ifndef PLAYER_BATTLERECORD_H
#define PLAYER_BATTLERECORD_H

class BattleEndBattleTurnAction;

class BattleRecord
{
    friend class BattleEndBattleTurnAction;
public:
    BattleRecord(BinaryReader& reader);
    ~BattleRecord();

    bool hasBattled(uint16_t trainerNo) const;
    bool hasBattled(const TrainerCharacterPtr trainer) const;

private:
    class BattleRecordEntry : public Comparable
    {
    public:
        BattleRecordEntry(uint16_t trainerNo);
        ~BattleRecordEntry();

        virtual int compareTo(const Comparable& other) const override;

    private:
        uint16_t _trainerNo;
    };

    //BinarySearchTree _record;
    #warning "return here!"
    void markTrainerBattled(const TrainerCharacterPtr trainer);

};

#endif

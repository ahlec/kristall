#ifndef KRISTALL_INCLUDE_PLAYER_PREDEFINE_H
#define KRISTALL_INCLUDE_PLAYER_PREDEFINE_H

#include <memory>

class PlayerGame;

typedef std::shared_ptr<PlayerGame> PlayerGamePtr;

#endif

#ifndef KRISTALL_INCLUDE_PLAYER_PLAYERGAME_H
#define KRISTALL_INCLUDE_PLAYER_PLAYERGAME_H

#include "Cornerstones.h"
#include "Characters/Predefine.h"
#include "Data/Predefine.h"
#include "World/Predefine.h"

class PlayerCharacter;

class PlayerGame
{
    friend class Overworld;
public:
    PlayerGame();
    PlayerGame(BinaryReaderPtr& reader, MapLocation& characterLocation);

    String getName() const;
    uint16_t getTrainerId() const;
    uint16_t getSecretId() const;

    PlayerCharacter* getCharacter() const;

private:
    String _name;
    uint16_t _trainerId;
    uint16_t _secretId;
    //Region _region;
    PlayerCharacter* _character;
    #warning "return here!!!"
};

#endif

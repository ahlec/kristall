#ifndef KRISTALL_INCLUDE_PLAYER_POKEDEXENTRYSTATE_H
#define KRISTALL_INCLUDE_PLAYER_POKEDEXENTRYSTATE_H

enum class PokedexEntryState
{
    Unseen,
    Seen,
    Owned
};

#endif

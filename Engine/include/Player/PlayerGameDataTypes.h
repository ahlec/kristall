#ifndef KRISTALL_INCLUDE_PLAYER_PLAYERGAMEDATATYPES_H
#define KRISTALL_INCLUDE_PLAYER_PLAYERGAMEDATATYPES_H

enum class PlayerGameDataTypes
{
    Bool,
    String,
    Double,
    Null
};

#endif

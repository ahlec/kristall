#ifndef KRISTALL_INCLUDE_DATATYPES_H
#define KRISTALL_INCLUDE_DATATYPES_H

#include <chrono>
#include <memory>
#include <stack>
#include <queue>
#include "unicode/unistr.h"

typedef icu::UnicodeString String;

/// ------------------------------------------------------------- toString(...)
size_t getCStrLength(const char*);
template<typename T,
         class = typename std::enable_if<!std::is_pointer<T>::value>::type>
String toString(T value)
{
    return L"<UNDEFINED PARAMETER FOR toString>";
};
template<typename T,
         class = typename std::enable_if<std::is_pointer<T>::value>::type,
         class = typename std::enable_if<!std::is_same<T, char*>::value>::type,
         class = typename std::enable_if<!std::is_same<T, const char*>::value>::type>
String toString(T value)
{
    if (value == nullptr)
    {
        return "<nullptr>";
    }
    return toString(*value);
};
template<class T,
         class = typename std::enable_if<std::is_pointer<T>::value>::type,
         class = typename std::enable_if<std::is_same<T, char*>::value ||
                                         std::is_same<T, const char*>::value>::type>
String toString(T value)
{
    if (value == nullptr)
    {
        return L"<nullptr>";
    }
    int32_t strLength(getCStrLength(value));
    return String(value, strLength);
};
template<> String toString<String>(String value);
template<> String toString<const std::string&>(const std::string& value);
template<> String toString<bool>(bool value);
template<> String toString<int8_t>(int8_t value);
template<> String toString<uint8_t>(uint8_t value);
template<> String toString<int16_t>(int16_t value);
template<> String toString<uint16_t>(uint16_t value);
template<> String toString<int32_t>(int32_t value);
template<> String toString<uint32_t>(uint32_t value);
template<> String toString<int64_t>(int64_t value);
template<> String toString<uint64_t>(uint64_t value);
template<> String toString<float>(float value);
template<> String toString<char>(char value);

// Test sizes of built-in datatypes that have implementation-specific data sizes,
// but all the same have a VERY standard size by convention. Ensure that these
// datatypes DO in fact adhere to these standard conventions.
static_assert(sizeof(char) == 1, "char is not 1 byte.");
static_assert(sizeof(bool) == 1, "bool is not 1 byte.");
static_assert(sizeof(std::time_t) == sizeof(int32_t),   "time_t is not an int32_t.");
static_assert(std::is_integral<std::time_t>::value,     "time_t is not an int32_t.");
static_assert(std::is_signed<std::time_t>::value,       "time_t is not an int32_t.");

#endif

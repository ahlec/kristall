#ifndef KRISTALL_INCLUDE_ITEMS_ITEMOVERWORLDOBJECTFORM_H
#define KRISTALL_INCLUDE_ITEMS_ITEMOVERWORLDOBJECTFORM_H

#include "Cornerstones.h"

enum class ItemOverworldObjectForm : uint8_t
{
    Visible,
    Obscured,
    Hidden
};

#endif

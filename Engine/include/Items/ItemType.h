#ifndef KRISTALL_INCLUDE_ITEMS_ITEMTYPE_H
#define KRISTALL_INCLUDE_ITEMS_ITEMTYPE_H

#include "Cornerstones.h"

enum class ItemType : uint8_t
{
    Item,
    Medicine,
    PokeBall,
    MoveMachine,
    Berry,
    Mail,
    BattleItem,
    KeyItem
};

namespace ItemTypes
{
    String getPocketName(ItemType itemType);
};

#endif

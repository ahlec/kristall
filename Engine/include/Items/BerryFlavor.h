#ifndef KRISTALL_INCLUDE_ITEMS_BERRYFLAVOR_H
#define KRISTALL_INCLUDE_ITEMS_BERRYFLAVOR_H

enum class BerryFlavor
{
    Spicy,
    Sour,
    Dry,
    Bitter,
    Sweet
};

#endif

#ifndef KRISTALL_INCLUDE_ITEMS_ITEM_H
#define KRISTALL_INCLUDE_ITEMS_ITEM_H

#include "Cornerstones.h"
#include "Items/Predefine.h"
#include "Pokemon/PokemonStat.h"
#include "General/IndefiniteArticle.h"
#include "Items/ItemType.h"
#include "General/Money.h"

class Item
{
public:
    virtual ~Item();
    static ItemPtr get(uint16_t handleNo);

    bool isEvEnhancing(PokemonStat& enhancedStat) const;

    const std::string& getSingularName() const;
    IndefiniteArticle getIndefiniteArticle() const;
    const std::string& getPluralName() const;
    ItemType getItemType() const;
    uint16_t getHandleNo() const;
    bool getIsBuyable() const;
    Money getBuyPrice() const;
    bool getIsSellable() const;
    Money getSellPrice() const;
    const std::string& getDescription() const;
    uint16_t getSpriteNo() const;
    bool getIsUsableInBattle() const;
    bool getIsTargetingInBattle() const;
    const std::string& getBattleUsabilityCode() const;
    const std::string& getBattleCode() const;
    bool getIsUsableOutsideBattle() const;
    bool getIsTargetingOutsideBattle() const;
    const std::string& getOutsideBattleUsabilityCode() const;
    const std::string& getOutsideBattleCode() const;

    bool operator==(const Item& other) const;
    bool operator!=(const Item& other) const;

protected:
    Item();
    virtual void readHeader(BinaryReader& reader);

private:
    uint16_t _handleNo;
    String _singularName;
    IndefiniteArticle _indefiniteArticle;
    String _pluralName;
    String _description;
    ItemType _type;
    uint16_t _spriteNo;
    bool _isBuyable;
    Money _buyPrice;
    bool _isSellable;
    Money _sellPrice;

    bool _isUsableInBattle;
    bool _isTargetingInBattle; // whether or not this item targets a pokemon in battle
    std::string _battleUsabilityCode;
    std::string _battleCode;

    bool _isUsableOutsideBattle;
    bool _isTargetingOutsideBattle; // whether or not this item targets a pokemon outside battle
    std::string _outsideBattleUsabilityCode;
    std::string _outsideBattleCode;
};

#endif

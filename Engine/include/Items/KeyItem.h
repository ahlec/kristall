#ifndef KRISTALL_INCLUDE_ITEMS_KEYITEM_H
#define KRISTALL_INCLUDE_ITEMS_KEYITEM_H

#include "Cornerstones.h"
#include "Items/Item.h"

class KeyItem : public Item
{
    friend class Item;
public:
    bool getCanBeRegistered() const;

protected:
    KeyItem();
    void readHeader(BinaryReader& reader) override;

private:
    bool _canBeRegistered;
};

#endif

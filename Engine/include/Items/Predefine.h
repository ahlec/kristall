#ifndef KRISTALL_INCLUDE_ITEMS_PREDEFINE_H
#define KRISTALL_INCLUDE_ITEMS_PREDEFINE_H

#include "Data/CachePointer.h"

class GymBadge;
class Item;
class OverworldItem;
class PokeballItem;

typedef CachePointer<Item> ItemPtr;
typedef std::shared_ptr<OverworldItem> OverworldItemPtr;

#endif

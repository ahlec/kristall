#ifndef KRISTALL_INCLUDE_ITEMS_BERRYITEM_H
#define KRISTALL_INCLUDE_ITEMS_BERRYITEM_H

#include "Cornerstones.h"
#include "Items/Item.h"
#include "Items/BerryUseTrigger.h"

class BerryItem : public Item
{
    friend class Item;
public:
    uint8_t getSpicyFlavor() const;
    uint8_t getDryFlavor() const;
    uint8_t getSourFlavor() const;
    uint8_t getBitterFlavor() const;
    uint8_t getSweetFlavor() const;
    BerryUseTrigger getUseTrigger() const;
    uint8_t getUseData() const;
    const std::string& getBattleHeldActivationCode() const;

protected:
    BerryItem();
    void readHeader(BinaryReader& reader) override;

private:
    uint8_t _spicy;
    uint8_t _dry;
    uint8_t _sour;
    uint8_t _bitter;
    uint8_t _sweet;
    BerryUseTrigger _useTrigger;
    uint8_t _useData;
    std::string _battleHeldActivationCode;
};

#endif

#ifndef KRISTALL_INCLUDE_ITEMS_GIVEITEMMETHOD_H
#define KRISTALL_INCLUDE_ITEMS_GIVEITEMMETHOD_H

enum class GiveItemMethod
{
    Obtain,
    Receive,
    Given
};

#endif

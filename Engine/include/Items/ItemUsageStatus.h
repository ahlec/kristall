#ifndef KRISTALL_INCLUDE_ITEMS_ITEMUSAGESTATUS_H
#define KRISTALL_INCLUDE_ITEMS_ITEMUSAGESTATUS_H

enum class ItemUsageStatus
{
    CannotUse,
    NoEffect,
    OK
};

#endif

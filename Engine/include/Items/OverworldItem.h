#ifndef KRISTALL_INCLUDE_ITEMS_OVERWORLDITEM_H
#define KRISTALL_INCLUDE_ITEMS_OVERWORLDITEM_H

#include "Cornerstones.h"
#include "Data/Predefine.h"
#include "Player/Predefine.h"
#include "Characters/Predefine.h"
#include "Items/ItemOverworldObjectForm.h"

class OverworldItem
{
public:
    OverworldItem(BinaryReaderPtr reader, PlayerGame& playerGame);

    bool claim(Character& claimer);

    uint32_t getItemNo() const;
    uint32_t getRegistrationCode() const;
    ItemOverworldObjectForm getForm() const;
    bool getHasBeenClaimed() const;

private:
    uint32_t _itemNo;
    uint32_t _registrationCode;
    ItemOverworldObjectForm _form;
    bool _hasBeenClaimed;
};

#endif

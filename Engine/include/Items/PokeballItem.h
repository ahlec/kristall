#ifndef KRISTALL_INCLUDE_ITEMS_POKEBALLITEM_H
#define KRISTALL_INCLUDE_ITEMS_POKEBALLITEM_H

#include "Items/Item.h"

class PokeballItem : public Item
{
    friend class Item;
public:
    uint16_t getPokeballAnimationNo() const;
    const std::string& getPokeballEffectivenessCode() const;

protected:
    PokeballItem();
    void readHeader(BinaryReader& reader) override;

private:
    uint16_t _pokeballAnimationNo;
    std::string _effectivenessCode;
};

#endif

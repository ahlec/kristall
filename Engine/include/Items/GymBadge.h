#ifndef KRISTALL_INCLUDE_ITEMS_GYMBADGE_H
#define KRISTALL_INCLUDE_ITEMS_GYMBADGE_H

#include "Cornerstones.h"

class GymBadge
{
public:
    static GymBadge get(uint16_t handleNo);

    uint16_t getHandleNo() const;
    const std::string& getName() const;
    bool getDoesAllowHmUse() const;
    uint16_t getAllowedHm() const;
    uint8_t getObedienceLevel() const;

    bool operator==(const GymBadge& other) const;
    bool operator!=(const GymBadge& other) const;

private:
    GymBadge();

    uint16_t _handleNo;
    String _name;
    bool _doesAllowHmUse;
    uint16_t _allowedHm;
    uint8_t _obedienceLevel;
};

#endif

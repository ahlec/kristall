#ifndef KRISTALL_INCLUDE_ITEMS_BAG_H
#define KRISTALL_INCLUDE_ITEMS_BAG_H

#include "Cornerstones.h"
#include "Items/Predefine.h"
#include "General/Mutex.h"

class Bag
{
public:
    bool has(const ItemPtr item);
    bool has(uint16_t itemHandleNo);

    int32_t count(const ItemPtr item);
    int32_t count(uint16_t itemHandleNo);

    void give(const ItemPtr item, uint16_t amount = 1);
    void give(uint16_t itemHandleNo, uint16_t amount = 1);

    void take(const ItemPtr item, uint16_t amount = 1);
    void take(uint16_t itemHandleNo, uint16_t amount = 1);

    void swapPositions(const ItemPtr itemA, const ItemPtr itemB);

    int getTotalItemCount() const;
    int getUniqueItemsCount() const;

private:
    Bag();
    ~Bag();

    struct Node
    {
        uint16_t itemHandleNo;
        int amount;
        Node* next;
    };

    Mutex _mutex;
    int _totalCount;
    int _uniqueItemsCount;
    Node* _head;
};

#endif

#ifndef KRISTALL_INCLUDE_ITEMS_GYMBADGECASE_H
#define KRISTALL_INCLUDE_ITEMS_GYMBADGECASE_H

#include "Cornerstones.h"
#include "Items/Predefine.h"
#include "Items/Delegates.h"
#include "General/Mutex.h"
#include "Items/GymBadge.h"

class GymBadgeCase
{
public:
    GymBadgeCase();
    ~GymBadgeCase();

    bool has(const GymBadge& badge) const;
    bool has(uint16_t badgeHandleNo) const;

    void give(const GymBadge& badge);
    void give(uint16_t badgeHandleNo);

    bool canUseHm(uint16_t hmNo) const;

    void setOnBadgeReceived(GymBadgeReceivedDelegate delegate);

    uint8_t getObedienceLevel() const;
    uint16_t getCount() const;

private:
    class Node
    {
    public:
        Node(GymBadge gymBadge);
        GymBadge badge;
        Node* next;
    };

    Mutex _mutex;
    GymBadgeReceivedDelegate _onBadgeReceived;
    Node* _head;
    uint8_t _obedienceLevel;
    uint16_t _count;
};

#endif

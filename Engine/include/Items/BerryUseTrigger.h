#ifndef KRISTALL_INCLUDE_ITEMS_BERRYUSETRIGGER_H
#define KRISTALL_INCLUDE_ITEMS_BERRYUSETRIGGER_H

enum class BerryUseTrigger : uint8_t
{
    None,
    LowHP,
    LowPP,
    NonVolatileStatus,
    VolatileStatus,
    SuperEffectiveAttack,
    AnyAttack
};

#endif

#ifndef KRISTALL_INCLUDE_INPUT_INPUTBUTTONSTATE_H
#define KRISTALL_INCLUDE_INPUT_INPUTBUTTONSTATE_H

enum class InputButtonState
{
    Up,
    Down,
    Pressed
};

#endif

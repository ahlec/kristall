#ifndef KRISTALL_INCLUDE_INPUT_INPUTCONTROLLER_H
#define KRISTALL_INCLUDE_INPUT_INPUTCONTROLLER_H

#include "Cornerstones.h"
#include "Input/Predefine.h"
#include "Graphics/Predefine.h"
#include "Input/InputButton.h"

class InputController
{
public:
    InputController();
    ~InputController();

    ProcessedInput          getProcessedInput(uint32_t elapsedTime, bool hasFocus, bool useMouse,
                                              AnimationInstance* cursorAnimation);
    bool                    isInInitialState() const;
    void                    reset();
    void                    delayButton(InputButton button, uint16_t delayDuration,
                                        bool isReleaseRequired = true);
    void                    requireButtonRelease(InputButton button);
    void                    setButtonDelayLength(InputButton button, uint16_t newDelayLength);

private:

    class ButtonInfo
    {
    public:
        ButtonInfo();
        ButtonInfo(InputButton inputButton, uint16_t defaultDelayLength);

        InputButton button;
        uint16_t defaultLength;
        uint16_t currentLength;
        bool isDepressed;
        uint16_t depressionLength;
        bool isBlocked;
        uint16_t delayTime;
    };

    ButtonInfo* _buttons;
    bool _isInInitialState;
    bool _isMouseLeftDown;
    bool _isMouseRightDown;
    bool _isPrintScreenDown;
    bool _isHelpButtonDown;
    bool _isReportButtonDown;
    int32_t _lastMouseX;
    int32_t _lastMouseY;
};

#endif

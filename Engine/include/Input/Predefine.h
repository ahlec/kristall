#ifndef KRISTALL_INCLUDE_INPUT_PREDEFINE_H
#define KRISTALL_INCLUDE_INPUT_PREDEFINE_H

#include <memory>

class CursorData;
class InputController;
class KeyBindings;
class ProcessedInput;

typedef std::shared_ptr<CursorData> CursorDataPtr;
typedef InputController* InputControllerPtr;

#endif

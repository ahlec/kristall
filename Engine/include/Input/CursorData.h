#ifndef INPUT_CURSORDATA_H
#define INPUT_CURSORDATA_H

class CursorData
{
public:
    CursorData(const CursorData& other);
    CursorData(int positionX, int positionY, bool isLeftPressed, bool isLeftDown,
               bool isRightPressed, bool isRightDown, bool isOverWindow,
               bool hasMoved);

    const int x;
    const int y;
    const bool isLeftButtonPressed;
    const bool isRightButtonPressed;
    const bool isLeftButtonDown;
    const bool isRightButtonDown;
    const bool isCursorOverWindow;
    const bool hasCursorMoved;

    CursorData& operator=(const CursorData& other);

    const static CursorData NONE;
};

#endif

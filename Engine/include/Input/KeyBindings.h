#ifndef INPUT_KEYBINDINGS_H
#define INPUT_KEYBINDINGS_H

#include "Data/Predefine.h"
#include "Input/InputButton.h"
#include "Input/Key.h"

class KeyBindings
{
public:
    KeyBindings(BinaryReaderPtr reader);

    Key operator() (InputButton button) const;

private:
    std::vector<Key> _bindings;
};

#endif

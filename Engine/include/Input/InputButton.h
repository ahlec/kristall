#ifndef KRISTALL_INCLUDE_INPUT_INPUTBUTTON_H
#define KRISTALL_INCLUDE_INPUT_INPUTBUTTON_H

enum class InputButton
{
    None,
    Up,
    Down,
    Left,
    Right,
    A,
    B,
    Start,
    Select,
    LeftShoulder,
    RightShoulder
};

#define LENGTH_InputButtons 11

#endif

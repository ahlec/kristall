#ifndef KRISTALL_INCLUDE_INPUT_PROCESSEDINPUT_H
#define KRISTALL_INCLUDE_INPUT_PROCESSEDINPUT_H

#include "Cornerstones.h"
#include "Input/Predefine.h"

/// Notice! There has been a switch from C# to C++. The old functions
/// "IsAPressed" are now "isADown", while the old functions "A" are
/// now "isAPressed," to be consistant with language terminology (such as
/// javascript).

struct ProcessedInput
{
    ProcessedInput(); // Simply initialises all variables to nullptr/false

    InputControllerPtr inputController;
    CursorDataPtr cursor;

    bool isUpPressed;
    bool isDownPressed;
    bool isLeftPressed;
    bool isRightPressed;
    bool isAPressed;
    bool isBPressed;
    bool isStartPressed;
    bool isSelectPressed;
    bool isLeftShoulderPressed;
    bool isRightShoulderPressed;
    bool isAnyMovementButtonPressed;

    bool isScreenshotButtonPressed;
    bool isHelpButtonPressed;
    bool isReportButtonPressed;
    bool isCtrlPressed;

    bool isUpDown;
    bool isDownDown;
    bool isLeftDown;
    bool isRightDown;
    bool isADown;
    bool isBDown;
    bool isStartDown;
    bool isSelectDown;
    bool isLeftShoulderDown;
    bool isRightShoulderDown;
    bool isAnyMovementButtonDown;
};

#endif

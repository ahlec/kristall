#ifndef KRISTALL_INCLUDE_DEVELOPER_H
#define KRISTALL_INCLUDE_DEVELOPER_H

/// ---------------------------------------------------------- Logging ( DEVEL_LOG / DEVEL_LOGF )
#if DEBUG

    namespace DebugLogger
    {
        void writeLine(String line);
        void writeLinef(const char* format, ...);

        void writeLine(const char* line);
    };

    #define DEVEL_LOG(...)              DebugLogger::writeLine(__VA_ARGS__);
    #define DEVEL_LOGF(format, ...)     DebugLogger::writeLinef(format, __VA_ARGS__);
#else
    #define DEVEL_LOG(...)              (__VA_ARGS__)
    #define DEVEL_LOGF(...)             (__VA_ARGS__)
#endif

/// ---------------------------------------------------------- Not implemented warning ( DEVEL_NOT_IMPLEMENTED /
///                                                                                      DEVEL_NOT_TRANSLATED /
///                                                                                      WARN_DEVEL_NOT_TRANSLATED )
/// DEVEL_NOT_IMPLEMENTED     : should be used in functions where the declaration is provided but definition isn't
/// DEVEL_NOT_TRANSLATED      : should be used when the definition of a function hasn't been translated from C#. Fatal.
/// WARN_DEVEL_NOT_TRANSLATED : should be used when the definition of a function hasn't been translated from C#. Not fatal in DEBUG
#if DEBUG

    #define DEVEL_NOT_IMPLEMENTED       { \
                                            ExecutionException develNotImplementedExcept("[DEVEL] Not implemented"); \
                                            throw develNotImplementedExcept; \
                                        }

    #define DEVEL_NOT_TRANSLATED        { \
                                            ExecutionException develNotTranslatedExcept("[DEVEL] Not translated from C# code"); \
                                            throw develNotTranslatedExcept; \
                                        }
    #define WARN_DEVEL_NOT_TRANSLATED

#else
    #define DEVEL_NOT_IMPLEMENTED       static_assert(false, "[DEVEL] Function not implemented (cannot ship)");
    #define DEVEL_NOT_TRANSLATED        static_assert(false, "[DEVEL] Function not translated from C# code (cannot ship)");
    #define WARN_DEVEL_NOT_TRANSLATED   static_assert(false, "[DEVEL] Function not translated from C# code (cannot ship)");
#endif

#endif

#ifndef KRISTALL_INCLUDE_GENERAL_TYPEEFFECTIVENESS_H
#define KRISTALL_INCLUDE_GENERAL_TYPEEFFECTIVENESS_H

#include "Cornerstones.h"

enum class TypeEffectiveness : uint8_t
{
    One,
    Double,
    Half,
    Zero,
    Quadruple,
    Quarter
};

TypeEffectiveness operator*(TypeEffectiveness a, TypeEffectiveness b);

#endif

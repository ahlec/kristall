#ifndef KRISTALL_INCLUDE_GENERAL_VECTOR2_H
#define KRISTALL_INCLUDE_GENERAL_VECTOR2_H

#include "Cornerstones.h"

class Vector2
{
public:
    Vector2();
    Vector2(int32_t x, int32_t y);

    int32_t x;
    int32_t y;

    bool operator==(const Vector2& other) const;
    bool operator!=(const Vector2& other) const;
};

#endif

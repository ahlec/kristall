#ifndef KRISTALL_INCLUDE_GENERAL_RANDOMGENERATOR_H
#define KRISTALL_INCLUDE_GENERAL_RANDOMGENERATOR_H

#include "Cornerstones.h"
#include <random>

class RandomGenerator
{
public:
    RandomGenerator(int32_t minValue, int32_t maxValue);

    int32_t next();

private:
    std::default_random_engine _engine;
    std::uniform_int_distribution<int32_t> _distribution;

};

#endif

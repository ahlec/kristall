#ifndef GENERAL_ELEMENTALTYPE_H
#define GENERAL_ELEMENTALTYPE_H

// While the term "ElementalType" isn't 100% canon with the Pokemon franchise,
// using "Type" conflicted with the C# class of the name System.Type. When
// Kristall was rewritten in C++, the naming scheme was already thoroughly
// propogated throughout the code, and to allow for no confusion as to what
// "Type" may apply to, this naming scheme was kept as a suitable substitute.

#include "General/TypeEffectiveness.h"

enum class ElementalType : uint8_t
{
    None,
    Normal,
    Fire,
    Fighting,
    Water,
    Flying,
    Grass,
    Poison,
    Electric,
    Ground,
    Psychic,
    Rock,
    Ice,
    Bug,
    Dragon,
    Ghost,
    Dark,
    Steel,
    Indeterminate,  // The indeterminate type "???"
    Fairy
};

namespace ElementalTypes
{
    TypeEffectiveness getEffectiveness(ElementalType attackingType, ElementalType defendingType);
};

#endif

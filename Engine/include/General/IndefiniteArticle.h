#ifndef KRISTALL_INCLUDE_GENERAL_INDEFINITEARTICLE_H
#define KRISTALL_INCLUDE_GENERAL_INDEFINITEARTICLE_H

#include "Cornerstones.h"

enum class IndefiniteArticle
{
    A,
    An
};

template<> String toString<IndefiniteArticle>(IndefiniteArticle value);

#endif

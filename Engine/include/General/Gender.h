#ifndef KRISTALL_INCLUDE_GENERAL_GENDER_H
#define KRISTALL_INCLUDE_GENERAL_GENDER_H

#include "Cornerstones.h"

enum class Gender : uint8_t
{
    Genderless,
    Male,
    Female
};

template<>
inline String toString<Gender>(Gender value)
{
    switch (value)
    {
        case Gender::Genderless:
        {
            return L"Genderless";
        }
        case Gender::Male:
        {
            return L"Male";
        }
        case Gender::Female:
        {
            return L"Female";
        }
    }

    return "<Unrecognized Gender: " + toString(static_cast<int32_t>(value)) + ">";
};

#endif

#ifndef KRISTALL_INCLUDE_GENERAL_ARRAYS_H
#define KRISTALL_INCLUDE_GENERAL_ARRAYS_H

template <class T> void pushOnArray(T* arr, int arrayLength, T value);
template <class T> T popOffArray(T* arr, int arrayLength);
template <class T> void removeFirstInArray(T* arr, int arrayLength, T value);
template <class T> void appendToArray(T* arr, int arrayLength, T value, ...);

#endif

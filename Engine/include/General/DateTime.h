#ifndef KRISTALL_INCLUDE_GENERAL_DATETIME_H
#define KRISTALL_INCLUDE_GENERAL_DATETIME_H

#include "Cornerstones.h"
#include "General/DayOfWeek.h"
#include "General/TimeOfDay.h"
#include "General/Month.h"

class DateTime
{
public:
    DateTime(int32_t ticks);
    static DateTime now();

    int8_t hour;
    int8_t minute;
    int8_t second;
    TimeOfDay timeOfDay;
    bool isTwilight;

    int8_t day;
    DayOfWeek dayOfWeek;
    Month month;
    int32_t year;

    int32_t getTicks() const;

private:
    int32_t _ticks;

};

template<> String toString<DateTime>(DateTime value);

#endif

#ifndef GENERAL_BINARYSEARCHTREE_H
#define GENERAL_BINARYSEARCHTREE_H

#include "Errors/ExecutionException.h"

/* NUMERIC IMPLEMENTATION */
template <typename T,
          class = typename std::enable_if<std::is_integral<T>::value>::type,
          class = typename std::enable_if<!std::is_same<T, bool>::value>::type>
class BinarySearchTree
{
public:
    BinarySearchTree() : _count(0), _root(nullptr)
    {
    }

    uint32_t getCount() const
    {
        return _count;
    }

    T getMinValue() const
    {
        if (_count == 0)
        {
            ExecutionException error("There is no minimum value for an empty BinarySearchTree.");
            throw error;
        }

        return _min;
    }

    T getMaxValue() const
    {
        if (_count == 0)
        {
            ExecutionException error("There is no maximum value for an empty BinarySearchTree.");
            throw error;
        }

        return _max;
    }

    void add(T item)
    {
        if (_root == nullptr)
        {
            _root = std::shared_ptr<Node>(new Node(item, nullptr, false));
        }
        else
        {
            _root->add(item, *this);
        }

        if (_count == 0)
        {
            _count = 1;
            _min = item;
            _max = item;
        }
        else
        {
            ++_count;
            if (item < _min)
            {
                _min = item;
            }
            if (item > _max)
            {
                _max = item;
            }
        }
    }

    bool has(T item) const
    {
        if (_count == 0)
        {
            return false;
        }

        return _root->contains(item);
    }

    void remove(T item)
    {
        if (_count == 0)
        {
            return;
        }

        _root->remove(item);
        --_count;

        if (item == _min && _count > 0)
        {
            _min = _root->getMin();
        }

        if (item == _max && _count > 0)
        {
            _max = _root->getMax();
        }
    }

    void clear()
    {
        _count = 0;
        if (_root != nullptr)
        {
            _root->vacate();
        }
    }

    std::vector<T> getValues() const
    {
        if (_count == 0)
        {
            return std::vector<T>();
        }

        std::vector<T> values;
        values.reserve(_count);
        _root->outputValues(values);
        return values;
    }

private:
    class Node
    {
    public:
        Node(T number, std::shared_ptr<Node> parent, bool isLeft) : _value(number), _isOccupied(true),
                                    _left(nullptr), _right(nullptr), _parent(parent), _isLeftNode(isLeft)
        {
        }

        void add(T number, BinarySearchTree<T>& bst)
        {
            if (!_isOccupied)
            {
                _value = number;
                _isOccupied = true;
                if (_left != nullptr)
                {
                    _left->_isOccupied = false;
                }
                if (_right != nullptr)
                {
                    _right->_isOccupied = false;
                }

                return;
            }

            if (number < _value)
            {
                if (_left == nullptr)
                {
                    _left = std::shared_ptr<Node>(new Node(number, getSelfPtr(bst), true));
                }
                else
                {
                    _left->add(number, bst);
                }
                return;
            }
            else if (number > _value)
            {
                if (_right == nullptr)
                {
                    _right = std::shared_ptr<Node>(new Node(number, getSelfPtr(bst), false));
                }
                else
                {
                    _right->add(number, bst);
                }
                return;
            }

            ExecutionException error("A BinarySearchTree cannot contain multiple copies of the same value.");
            error.addData("number", toString(number));
            throw error;
        }

        bool contains(T number) const
        {
            if (!_isOccupied)
            {
                return false;
            }

            if (number == _value)
            {
                return true;
            }
            else if (number < _value)
            {
                if (!hasLeft())
                {
                    return false;
                }

                return _left->contains(number);
            }
            else
            {
                if (!hasRight())
                {
                    return false;
                }

                return _right->contains(number);
            }
        }

        void remove(T number)
        {
            if (number < _value)
            {
                if (!hasLeft())
                {
                    return;
                }

                _left->remove(number);
                return;
            }
            else if (number > _value)
            {
                if (!hasRight())
                {
                    return;
                }

                _right->remove(number);
                return;
            }

            cascadeRemoveMe();
        }

        T getMax() const
        {
            if (!hasRight())
            {
                return _value;
            }

            return _right->getMax();
        }

        T getMin() const
        {
            if (!hasLeft())
            {
                return _value;
            }

            return _left->getMin();
        }

        void outputValues(std::vector<T>& arr)
        {
            if (hasLeft())
            {
                _left->outputValues(arr);
            }
            arr.push_back(_value);
            if (hasRight())
            {
                _right->outputValues(arr);
            }
        }

        void vacate()
        {
            _isOccupied = false;
        }

    private:
        inline bool hasLeft() const
        {
            return (_left != nullptr && _left->_isOccupied);
        }

        inline bool hasRight() const
        {
            return (_right != nullptr && _right->_isOccupied);
        }

        inline std::shared_ptr<Node>& getSelfPtr(BinarySearchTree<T>& bst) const
        {
            if (_parent == nullptr)
            {
                return bst._root;
            }

            return (_isLeftNode ? _parent->_left : _parent->_right);
        }

        void cascadeRemoveMe()
        {
            if (hasLeft())
            {
                _value = _left->removeMax();
            }
            else if (hasRight())
            {
                _value = _right->removeMin();
            }
            else
            {
                _isOccupied = false;
            }
        }

        void cascadeUp()
        {
            // Never let this function run on the current _root
            // Also, only let this function run on a node that has only one side of its
            // tree occupied

            if (!_isOccupied)
            {
                _parent->_isOccupied = false;
                return;
            }

            _parent->_isOccupied = true;
            _parent->_value = _value;

            if (hasLeft())
            {
                _left->cascadeUp();
            }
            else if (hasRight())
            {
                _right->cascadeUp();
            }
            else
            {
                _isOccupied = false;
            }
        }

        T removeMin() const
        {
            if (!hasLeft())
            {
                if (hasRight())
                {
                    T originalVal(_value);
                    _right->cascadeUp();
                    return originalVal;
                }
                else
                {
                    _isOccupied = false;
                    return _value;
                }
            }
            else
            {
                return _left->removeMin();
            }
        }

        T removeMax() const
        {
            if (!hasRight())
            {
                if (hasLeft())
                {
                    T originalVal(_value);
                    _left->cascadeUp();
                    return originalVal;
                }
                else
                {
                    _isOccupied = false;
                    return _value;
                }
            }
            else
            {
                return _right->removeMax();
            }
        }

        T _value;
        bool _isOccupied;
        std::shared_ptr<Node> _left;
        std::shared_ptr<Node> _right;
        std::shared_ptr<Node> _parent;
        bool _isLeftNode;
    };

    uint32_t _count;
    std::shared_ptr<Node> _root;
    T _min;
    T _max;
};

#endif

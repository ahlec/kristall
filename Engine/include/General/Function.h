#ifndef KRISTALL_INCLUDE_GENERAL_FUNCTION_H
#define KRISTALL_INCLUDE_GENERAL_FUNCTION_H

#include "Cornerstones.h"
#include <memory>

// Function (abstract template class)
template <typename ReturnType, class... Args>
class Function
{
public:
    virtual ReturnType operator()(Args ...)=0;
};

// GlobalFunction
template <typename ReturnType, class... Args>
class GlobalFunction : public Function<ReturnType, Args...>
{
public:
    GlobalFunction(ReturnType (*func)(Args ...)) : _func(func)
    {
    }

    virtual ReturnType operator()(Args ... arguments) override
    {
        return _func(std::forward<Args>(arguments)...);
    }

private:
    ReturnType (* _func)(Args ...);
};

// MemberFunction
template <class Object, typename ReturnType, class... Args>
class MemberFunction : public Function<ReturnType, Args...>
{
public:
    MemberFunction(ReturnType (Object::* func)(Args ...), std::shared_ptr<Object> obj) :
        _func(func), _sharedPtr(obj), _nativePtr(nullptr)
    {
    }

    MemberFunction(ReturnType (Object::* func)(Args ...), Object* obj) :
        _func(func), _sharedPtr(nullptr), _nativePtr(obj)
    {
    }

    virtual ReturnType operator()(Args ... arguments) override
    {
        if (_nativePtr != nullptr)
        {
            return (_nativePtr->*_func)(std::forward<Args>(arguments)...);
        }
        else
        {
            return (_sharedPtr.get()->*_func)(std::forward<Args>(arguments)...);
        }
    }

private:
    ReturnType (Object::* _func)(Args ...);
    std::shared_ptr<Object> _sharedPtr;
    Object* _nativePtr;
};

// Functor abstract class
template <typename ReturnType, class... Args>
class Functor
{
public:
    virtual ReturnType operator()(Args ... arguments)=0;
};

// FunctorFunction
template <typename ReturnType, class... Args>
class FunctorFunction : public Function<ReturnType, Args...>
{
public:
    FunctorFunction(std::shared_ptr<Functor<ReturnType, Args...>> functor) : _sharedPtr(functor)
    {
    }

    FunctorFunction(Functor<ReturnType, Args...>* functor) : _sharedPtr(functor)
    {

    }

    virtual ReturnType operator()(Args ... arguments) override
    {
        return (*_sharedPtr)(std::forward<Args>(arguments)...);
    }

private:
    std::shared_ptr<Functor<ReturnType, Args...>> _sharedPtr;
};

// FunctionPtr
template <typename ReturnType, class... Args>
class FunctionPtr : public std::shared_ptr<Function<ReturnType, Args...>>
{
private:
    typedef std::shared_ptr<Function<ReturnType, Args...>> BaseClass;
    typedef ReturnType (* NativeFunctionPtr)(Args ...);

public:
    FunctionPtr(NativeFunctionPtr nativeFunction) : BaseClass(new GlobalFunction<ReturnType, Args...>(nativeFunction))
    {
    }

    FunctionPtr(std::shared_ptr<Function<ReturnType, Args...>> functionPtr) : BaseClass(functionPtr)
    {
    }

    FunctionPtr(Functor<ReturnType, Args...>* functor) : BaseClass(new FunctorFunction<ReturnType, Args...>(functor))
    {
    }

    FunctionPtr(std::nullptr_t nullValue) : BaseClass(nullValue)
    {
    }

    FunctionPtr() : BaseClass(nullptr)
    {
    }

    template <class ObjectClass>
    static FunctionPtr<ReturnType, Args...> fromMember(ObjectClass* const object, ReturnType (ObjectClass::* memberFunction)(Args ...))
    {
        if (object == nullptr || memberFunction == nullptr)
        {
            return FunctionPtr(nullptr);
        }

        return FunctionPtr<ReturnType, Args...>(std::shared_ptr<Function<ReturnType, Args...>>(new MemberFunction<ObjectClass, ReturnType, Args...>(memberFunction, object)));
    }

    ReturnType operator()(Args ... arguments)
    {
        return (*BaseClass::get())(std::forward<Args>(arguments)...);
    }

    FunctionPtr& operator=(NativeFunctionPtr& nativeFunction)
    {
        BaseClass other(new GlobalFunction<ReturnType, Args...>(nativeFunction));
        return BaseClass::operator=(other);
    }

    FunctionPtr& operator=(std::nullptr_t& nullValue)
    {
        throw ExecutionException("HI");
        //return BaseClass::operator=(nullValue);
    }

};

/*
using FunctionPtr = std::shared_ptr<Function<ReturnType, Args...>>;

// Global conversion operators
template <typename ReturnType, class... Args>
using NativeFunctionPtr = ReturnType (*)(Args ...);

template <class ReturnType, class... Args>
FunctionPtr<ReturnType, Args...>& operator=(FunctionPtr<ReturnType, Args...>& dest, NativeFunctionPtr<ReturnType, Args...> src)
{
    printf("NativeFunctionPtr operator=\n");
    dest = FunctionPtr(new GlobalFunction(src));
    return dest;
}
*/

#endif

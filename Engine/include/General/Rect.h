#ifndef KRISTALL_INCLUDE_GENERAL_RECT_H
#define KRISTALL_INCLUDE_GENERAL_RECT_H

#include "Cornerstones.h"
#include "General/Vector2.h"

class Rect
{
public:
    Rect();
    Rect(int32_t x, int32_t y, int32_t width, int32_t height);

    bool intersects(const Rect& other) const;
    bool contains(int32_t x, int32_t y) const;
    bool contains(Vector2 point) const;

    int32_t x;
    int32_t y;
    int32_t width;
    int32_t height;

    bool operator==(const Rect& other) const;
    bool operator!=(const Rect& other) const;
};

template<> String toString<Rect>(Rect value);

#endif

#ifndef KRISTALL_INCLUDE_GENERAL_MUTEXSCOPELOCK_H
#define KRISTALL_INCLUDE_GENERAL_MUTEXSCOPELOCK_H

#include "General/Mutex.h"

/* class MutexScopeLock
 * A simple mutex scope locking class. Locks a mutex when constructed, unlocks when destructed.
 *
 * Usage: use as above, with impunity. This is safe to take nullptr (but ask yourself why). This
 * will break if the mutex is destroyed while in use, but that's not going to happen because that would
 * be incredibly stupid.
 */

class MutexScopeLock
{
public:
    MutexScopeLock(Mutex* mutex);
    ~MutexScopeLock();

private:
    Mutex* m_mutex;
};

#endif

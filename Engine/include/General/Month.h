#ifndef KRISTALL_INCLUDE_GENERAL_MONTH_H
#define KRISTALL_INCLUDE_GENERAL_MONTH_H

#include "Cornerstones.h"

enum class Month : uint8_t
{
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December
};

template<> String toString<Month>(Month value);

namespace Months
{
    Month current();

    Month get(uint8_t month);
};

#endif

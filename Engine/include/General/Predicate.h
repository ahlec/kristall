#ifndef KRISTALL_INCLUDE_GENERAL_PREDICATE_H
#define KRISTALL_INCLUDE_GENERAL_PREDICATE_H

#include "General/Function.h"

template <typename T>
using Predicate = FunctionPtr<bool, const T&>;

#endif

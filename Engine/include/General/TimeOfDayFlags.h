#ifndef GENERAL_TIMEOFDAYFLAGS_H
#define GENERAL_TIMEOFDAYFLAGS_H

#include "Cornerstones.h"

enum class TimeOfDayFlags : uint8_t
{
    None = 0,
    Morning = 1,
    Day = 2,
    Night = 4
};

inline TimeOfDayFlags operator|(TimeOfDayFlags a, TimeOfDayFlags b)
{
    return static_cast<TimeOfDayFlags>(static_cast<uint8_t>(a) | static_cast<uint8_t>(b));
}


#endif

#ifndef KRISTALL_INCLUDE_GENERAL_TIMEOFDAY_H
#define KRISTALL_INCLUDE_GENERAL_TIMEOFDAY_H

#include "Cornerstones.h"

enum class TimeOfDay : uint8_t
{
    Morning,    /// From 04:00 to 09:59
    Day,        /// From 10:00 to 19:59
    Night       /// From 20:00 to 03:59
};

template<> String toString<TimeOfDay>(TimeOfDay value);

namespace TimesOfDay
{
    TimeOfDay getNow();
    bool isNowTwilight();

    TimeOfDay get(uint8_t hour);
    bool isTwilight(uint8_t hour);
};

#endif

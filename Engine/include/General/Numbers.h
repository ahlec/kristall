#ifndef KRISTALL_INCLUDE_GENERAL_NUMBERS_H
#define KRISTALL_INCLUDE_GENERAL_NUMBERS_H

#include "Cornerstones.h"
#include <limits>
#include "Errors/ExecutionException.h"

/// Numbers NAMESPACE
///
///     There are typically three functions for each numeric datatype within
///     the Numbers namespace:
///         - "isValid[Y]([X])": determines if the X-type value is a valid Y-type value
///         - "to[Y]([X])": transforms the X-type value into an always-valid Y-type value
///                         (if the X-type value is not a valid Y-type value, it is made into
///                          a valid Y-type value automatically)
///         - "as[Y]([X])": transforms the X-type value into a Y-type value; if the
///                         X-type value is not a valid Y-type value, an exception is thrown.
namespace Numbers
{
    String convertIntToLonghand(int32_t number);

    template<typename Dest, typename Src,
             class = typename std::enable_if<std::is_integral<Dest>::value>::type,
             class = typename std::enable_if<std::is_integral<Src>::value>::type,
             class = typename std::enable_if<!std::is_same<Dest, bool>::value>::type,
             class = typename std::enable_if<!std::is_same<Src, bool>::value>::type>
    bool isValid(Src number)
    {
        if (std::is_unsigned<Dest>::value &&
            std::is_unsigned<Src>::value)
        {
            if (sizeof(Dest) > sizeof(Src))
            {
                return true;
            }

            return (number <= std::numeric_limits<Dest>::max());
        }
        else if (std::numeric_limits<Dest>::is_signed)
        {
            if (number < 0)
            {
                return false;
            }

            return (number <= std::numeric_limits<Dest>::max());
        }
        else if (std::numeric_limits<Src>::is_signed)
        {
            return (number <= std::numeric_limits<Dest>::max());
        }
        else
        {
            if (sizeof(Dest) > sizeof(Src))
            {
                return true;
            }

            return (number >= std::numeric_limits<Dest>::min() &&
                    number <= std::numeric_limits<Dest>::max());
        }
    }


    template<typename Dest, typename Src>
    Dest to(Src number)
    {
        if (isValid<Dest, Src>(number))
        {
            return (Dest)number;
        }

        return (number < std::numeric_limits<Dest>::max() ? std::numeric_limits<Dest>::min() : std::numeric_limits<Dest>::max());
    }

    template<typename Dest, typename Src>
    Dest as(Src number)
    {
        if (!isValid<Dest, Src>(number))
        {
            ExecutionException error("The provided number is unable to be casted from its original datatype to the requested datatype.");
            error.addData("number", toString(number));
            error.addData("Dest", toString(typeid(Dest).name()));
            error.addData("Src", toString(typeid(Src).name()));
            throw error;
        }

        return (Dest)number;
    }
};

#endif

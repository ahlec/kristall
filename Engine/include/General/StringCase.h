#ifndef KRISTALL_INCLUDE_GENERAL_STRINGCASE_H
#define KRISTALL_INCLUDE_GENERAL_STRINGCASE_H

#include "Cornerstones.h"

enum class StringCase
{
    TitleCase,
    UpperCase,
    LowerCase,
    StartCase
};

template<>
String toString<StringCase>(StringCase value)
{
    switch (value)
    {
        case StringCase::TitleCase:
        {
            return L"TitleCase";
        }
        case StringCase::UpperCase:
        {
            return L"UpperCase";
        }
        case StringCase::LowerCase:
        {
            return L"LowerCase";
        }
        case StringCase::StartCase:
        {
            return L"StartCase";
        }
    }

    return "<Unrecognized StringCase: " + toString(static_cast<int32_t>(value)) + ">";
};

#endif

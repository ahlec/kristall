#ifndef KRISTALL_INCLUDE_GENERAL_MONEY_H
#define KRISTALL_INCLUDE_GENERAL_MONEY_H

#include "Cornerstones.h"

class Money
{
public:
    Money();
    Money(uint32_t amount);

private:
    uint32_t _amount;
};

#endif

#ifndef KRISTALL_INCLUDE_GENERAL_DIRECTION_H
#define KRISTALL_INCLUDE_GENERAL_DIRECTION_H

#include "Cornerstones.h"
#include "General/Vector2.h"
#include "World/Predefine.h"

enum class Direction
{
    North,
    East,
    South,
    West
};

template<> String toString<Direction>(Direction value);

namespace Directions
{
    Direction complement(Direction direction);
    Direction fromUnitVector(Vector2 unitVector);
    Vector2 toUnitVector(Direction direction);
    Direction calculate(int32_t initialX, int32_t initialY,
                        int32_t finalX, int32_t finalY);
    Direction calculate(Tile initialLocation, Tile finalLocation);
    void applyDirectionToTileLocation(Direction direction, int32_t& tileX, int32_t& tileY);
};

#endif

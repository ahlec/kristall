#ifndef KRISTALL_INCLUDE_GENERAL_MUTEX_H
#define KRISTALL_INCLUDE_GENERAL_MUTEX_H

#include "Cornerstones.h"
#include <SDL_mutex.h>

/* class Mutex
 *
 * A wrapper for a cross-platform mutex.
 *
 * For an analysis on the cost of locking/unlocking a mutex, consider the following StackOverflow answer:
 * http://stackoverflow.com/questions/3652056/how-efficient-is-locking-an-unlocked-mutex-how-much-does-a-mutex-costs
 */

class Mutex
{
public:
    Mutex();
    ~Mutex();

    void unlock();
    void lock();

private:
    SDL_mutex* _mutex;
};

#endif

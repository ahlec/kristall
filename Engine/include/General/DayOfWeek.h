#ifndef KRISTALL_INCLUDE_GENERAL_DAYOFWEEK_H
#define KRISTALL_INCLUDE_GENERAL_DAYOFWEEK_H

#include "Cornerstones.h"

enum class DayOfWeek : uint8_t
{
    Sunday,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday
};

template<> String toString<DayOfWeek>(DayOfWeek value);

namespace DaysOfWeek
{
    DayOfWeek today();

    DayOfWeek get(uint8_t day);
};

#endif

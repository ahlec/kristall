#ifndef GENERAL_COMPARABLE_H
#define GENERAL_COMPARABLE_H

class Comparable
{
public:
    Comparable();
    virtual ~Comparable();
    virtual int compareTo(const Comparable& other) const=0;
};

#endif

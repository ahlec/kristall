#ifndef KRISTALL_INCLUDE_GENERAL_STRINGS_H
#define KRISTALL_INCLUDE_GENERAL_STRINGS_H

#include "Cornerstones.h"

namespace Strings
{
    std::string trimRight(const std::string& str);
}

#endif

#ifndef KRISTALL_INCLUDE_GENERAL_MATH_H
#define KRISTALL_INCLUDE_GENERAL_MATH_H

#include "Cornerstones.h"
#include <cmath>

namespace Math
{
    // CONSTANTS
    const float PI = 3.1415;

    int32_t clamp(int32_t value, int32_t minValue, int32_t maxValue);

    inline int32_t max(int32_t a, int32_t b)
    {
        return (a > b ? a : b);
    }

    inline int32_t min(int32_t a, int32_t b)
    {
        return (a < b ? a : b);
    }
};

#endif

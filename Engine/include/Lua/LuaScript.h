#ifndef KRISTALL_INCLUDE_LUA_LUASCRIPT_H
#define KRISTALL_INCLUDE_LUA_LUASCRIPT_H

#include "Cornerstones.h"

class LuaScript
{
    friend class BinaryReader;
public:
    ~LuaScript();

private:
    LuaScript();

    uint32_t _nBytes;
    char* _codeData;
};

#endif

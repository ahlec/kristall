#ifndef KRISTALL_INCLUDE_LUA_DELEGATES_H
#define KRISTALL_INCLUDE_LUA_DELEGATES_H

typedef void (* BattleCompetitorLuaFinishedDelegate)();
typedef void (* LuaExecutionCompleteDelegate)();

#endif

#ifndef KRISTALL_INCLUDE_LUA_BATTLECOMPETITORLUAINTERPRETOR_H
#define KRISTALL_INCLUDE_LUA_BATTLECOMPETITORLUAINTERPRETOR_H

#include "Lua/LuaInterpretor.h"

class BattleCompetitorLuaInterpretor : public LuaInterpretor
{
public:
    BattleCompetitorLuaInterpretor(Battle& battle, BattleCompetitor& competitor,
                                   const std::string& battleAi);

    void setupMove(AttackAction& attackAction, PokemonBattleFighter& fighter,
                   std::vector<PokemonBattleFighter&> targets,
                   std::vector<BattleTurnChoiceAction> opponentActions,
                   PokemonMove& move);
    void executeMove(AttackAction& attackAction, PokemonBattleFighter& fighter,
                     std::vector<PokemonBattleFighter&> targets,
                     PokemonMove& move, int priorTurns);
    void cancelMove();

    CatchRateInformation calculateCatchRate(const PokeballItem& pokeball,
                                            const WildPokemon& wildPokemon);

    ItemUsageStatus determineItemUsage(const Item& item,
                                       const PokemonBattleFighter* const target);
    void useItem(const Item& item, const BattleFighter* const target);

    void activateBerry(const BerryItem& berry, const PokemonBattleFighter& target);

    BattleTurnChoice determineTurnChoice(const BattleFighter& fighter,
                                         PassiveTurnSelection* const passiveTurnSelection = nullptr);

private:
    Battle& _battle;
    BattleCompetitor& _competitor;
    bool _isPassiveCompetitor;
    bool _hasBattleAi;

    bool _isExecutingMove;
    bool _negativeMoveEffects;

    bool _isExecutingItem;
};

#endif

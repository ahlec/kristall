#ifndef KRISTALL_INCLUDE_LUA_LUAINTERPRETOR_H
#define KRISTALL_INCLUDE_LUA_LUAINTERPRETOR_H

#include "Cornerstones.h"
#include "Lua/Delegates.h"
#include "General/Mutex.h"
#include <SDL_thread.h>
class lua_State;

class LuaInterpretor
{
public:
    LuaInterpretor(std::string name);
    virtual ~LuaInterpretor();

    bool getIsCurrentlyExecuting();

    bool evaluate(const std::string& luaCode);

protected:
    void execute(const std::string& luaCode, const std::string& fileName,
                 const std::string& fieldName,
                 LuaExecutionCompleteDelegate completedDelegate = nullptr);
    void stopExecution();
    virtual void onExecutionCompleted();

private:
    void executeBody();

    lua_State* _lua;
    Mutex _mutex;
    Mutex _isExecutingMutex;
    SDL_Thread* _executionThread;
    std::string _name;

    bool _isCurrentlyExecuting;
    std::string _currentLuaCode;

    LuaExecutionCompleteDelegate _executionComplete;
};

#endif

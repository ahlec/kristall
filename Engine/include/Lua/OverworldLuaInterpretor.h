#ifndef KRISTALL_INCLUDE_LUA_OVERWORLDLUAINTERPRETOR_H
#define KRISTALL_INCLUDE_LUA_OVERWORLDLUAINTERPRETOR_H

#include "Lua/LuaInterpretor.h"
#include "Items/ItemUsageStatus.h"
#include "World/Predefine.h"
#include "Items/Predefine.h"
#include "Pokemon/Predefine.h"

class OverworldLuaInterpretor : public LuaInterpretor
{
public:
    OverworldLuaInterpretor(const Overworld& overworld);
    ~OverworldLuaInterpretor();

    void execute(const std::string& luaCode, const std::string& fileName,
                 const std::string& fieldName, OverworldOccupant& executor);
    ItemUsageStatus determineItemUsage(const Item& item, const Pokemon* const target);

private:

};

#endif

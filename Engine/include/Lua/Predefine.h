#ifndef KRISTALL_INCLUDE_LUA_PREDEFINE_H
#define KRISTALL_INCLUDE_LUA_PREDEFINE_H

#include <memory>

class LuaScript;
class OverworldLuaInterpretor;

typedef std::shared_ptr<LuaScript> LuaScriptPtr;

#endif

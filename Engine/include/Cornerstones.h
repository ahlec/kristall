#ifndef KRISTALL_INCLUDE_CORNERSTONES_H
#define KRISTALL_INCLUDE_CORNERSTONES_H

#include "DataTypes.h"
#include "Config.h"
#include "Errors\ExecutionException.h"
#include "Developer.h"
#include "DeveloperSwitches.h"

// DON'T MODIFY BEYOND THIS LINE (everything is automatic)
const int TILE_SIZE_DOUBLE = (TILE_SIZE * 2);
const int TILE_SIZE_HALF = (TILE_SIZE / 2);
const int TARGET_TICKS_PER_FRAME = 1000 / TARGET_FPS;

#endif

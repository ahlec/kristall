#ifndef KRISTALL_INCLUDE_DEVELOPERSWITCHES_H
#define KRISTALL_INCLUDE_DEVELOPERSWITCHES_H

#include "Cornerstones.h"

#if DEBUG

enum DeveloperSwitch
{
    IS_GRID_VISIBLE,
    ARE_MAP_BORDERS_TINTED,
    IS_OVERWORLD_TILE_HOVER_ENABLED,

    COUNT_DEVELOPER_SWITCHES
};

class DeveloperSwitches
{
public:
    static DeveloperSwitches* getInstance();

    bool get(DeveloperSwitch devSwitch);
    void set(DeveloperSwitch devSwitch, bool value);

private:
    DeveloperSwitches();
    std::vector<bool> m_switches;
};

#define S_DeveloperSwitches (DeveloperSwitches::getInstance())

#endif

#endif

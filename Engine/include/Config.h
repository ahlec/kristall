#ifndef KRISTALL_INCLUDE_CONFIG_H
#define KRISTALL_INCLUDE_CONFIG_H

#if DEBUG
    #define DEVEL_LOGGER_OUTPUT_FILE    ("kristall_debug.log")
#endif

const int TILE_SIZE     = 16;
const int SCREEN_WIDTH  = 1024;
const int SCREEN_HEIGHT = 768;
const int TARGET_FPS    = 60;

#endif

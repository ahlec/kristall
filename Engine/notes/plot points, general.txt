PLOT POINTS:
- Player character is much older than 10 (late teens, perhaps)
- Player character is not designated at the start to be a pokemon trainer; actually has another job (delivery of packages?),
  but fateful encounter forces him/her to be a trainer
    - doesn't *want* to be a trainer, hesitant to start the adventure of being one
- While out between towns at the start (delivering packages?), is jumped by the villains, who gang up on him/her, not for
  any reason necessarily, but just because they can
    - a wild pokemon (random, from the starter group*) sees the hero under attack, jumps in between the hero and the attackers
	- the wild pokemon is thoroughly thrashed, faints
	- the villains all the same attack the hero, take the package/whatever, leave
	- the hero, though really doesn't like pokemon, naturally feels a duty to help the fallen pokemon
	  - takes the pokemon back to town, to heal
	  - the hero, when the pokemon is awake, tries to leave, but the pokemon keeps trying (weakly) to follow, to its own
	    harm (still weak/hurt)
		- can't simply recover, because not the damage from a regular battle, but rather from human abuse
	  - the hero, seeing that the pokemon is going to kill itself if it keeps trying to follow him, reluctantly stays with it
	    "until it is better, and can stay on its own, and then the hero can leave it"
	  - even when better, the pokemon won't leave the hero alone, because it feels it needs to protect the hero, after seeing
	    what happened to him/her
	- the pokemon follows the hero around, and after a while, the hero reluctantly accepts it, although refuses to battle trainers or
	  act like a trainer him/herself
	- is able to avoid battling with trainers, until reaches another town, where a gym leader(?) or police(?) sees that the hero
	  has a pokemon, and attempts to arrest or enforce the law/regulations about pokemon trainers
	    - the hero tries to explain, but the person won't listen
		- the aggressor sends out pokemon to capture the hero and the pokemon, but the starter pokemon jumps into battle and begins
		  to attack
		- the hero has no choice but to help the starter pokemon, who is trying to help him so much
			- alternatively, the starter pokemon hasn't been following the hero around, the aggressor is acting only on the knowledge
			  that the hero had been with a pokemon (and looks like had become a trainer, waiting to heal up the pokemon, etc), and
			  the pokemon now jumps out from hiding to protect the hero (<-- I like this idea better, I think)
		- the hero has a battle, but is losing; finally, another trainer joins the battle, and they are able to push back the aggressor
			- this trainer will become the heros' friend, probably follows around on adventures
	- the hero has to go to the first gym, because of some compelling reason (such as, needing to get back something that was stolen,
	  needing to reclaim the other pokemon trainer's pokemon who was "confiscated" after the battle, etc)
		- doesn't battle with the trainers in there because it's a "gym," but because the hero needs to in order to ensure their own
		  protection
		- after defeating the first gym leader, the friend trainer convinces the hero that the only way now to ensure that this doesn't
		  continue for the hero, and doesn't happen to other people, is to change the system (ie, bring down the corrupt system)
			- this causes the hero to be very jaded towards gym leaders, elite four, etc (even certain types of trainers at first)
				- not willing at first to believe/accept the aid of the one or two gym leaders who are actually good, want to help
			- the trainer friend follows the hero, helps out occasionally (perhaps?)
- because the hero isn't an "official" pokemon trainer, doesn't have the gear of one (namely, pokedex); the pokedex that the hero has
  access to is owned by the trainer friend
	- pokedexes are not like they are in the main, official series; they aren't given out in order to fill their pages for the first
	  time. pokedexes already have information in them, which can be used without having to encounter the pokemon for the first time(?)
	- pokedexes contain more information than just pokemon (moves, perhaps -- like, a movedex?)
	- pokedexes can be used during battle (because, really -- that's when you would actually need a pokedex)
	
GENERAL IDEAS
- this is a BIG if, but: sexuality? for pokemon? like, each individual pokemon would have a value, and certain male pokemon would
  resist attract from females but be influenced by males, etc
- potential for relationship between hero and a character (regardless of gender -- based upon actions and choices between the hero and
  friends, etc)
- **starter groups: each home town/starter area would have a "starter group" of three pokemon which would normally exist in the area logically
  (ie, pidgey in a forest, or cacnea in a desert), which have an evolution line of three (or, in certain instances, something that can be
  an acceptable replacement, ie eevee).
  
PROGRAMMING
- certain things can be moved from Lua directly into the engine, for speed, power, and because the engine is meant for the game, not for
  general consumption
  - input screens, START SCREENS, intro, etc
battles could be based off of the animations of special classes known as "EntityAnimations." These compose a single being (player,
pokemon, etc) that will animate throughout the battle. They have the ability to play animations, stop, etc. But they are composed
of various frames and positions of body parts, instead of whole animations. Each frame, these limbs are updated with the appropriate
frames, and when a `draw` is called on EntityAnimations, each limb is drawn until there are no more limbs/nested limbs. Thus, it appears
to everything outside of EntityAnimations that it is all one sprite, but in actuality is many. Definitions of these animations would be
special, but would be capable of controlling each limb individually, as needed.

For a good illustration, see the "back challenger throwing pokeball template" in the dropbox folder
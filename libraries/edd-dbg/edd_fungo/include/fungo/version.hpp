// Copyright Edd Dawson 2010
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef VERSION_HPP_0040_15102010
#define VERSION_HPP_0040_15102010

#define FUNGO_VERSION_MAJOR 0
#define FUNGO_VERSION_MINOR 2
#define FUNGO_VERSION_TWEAK 4

#endif // VERSION_HPP_0040_15102010

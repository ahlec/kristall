// Copyright Edd Dawson 2010
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef FUNGO_HPP_1228_16102010
#define FUNGO_HPP_1228_16102010

#include <fungo/catcher.hpp>
#include <fungo/exception_cage.hpp>
#include <fungo/exceptions.hpp>
#include <fungo/policies.hpp>
#include <fungo/raise.hpp>
#include <fungo/version.hpp>

#endif // FUNGO_HPP_1228_16102010

project('test_o_matic')

flags = cfgvar('test_o_matic.flags', 'flags', description='list of abstract build flags (see doozer docs)')
variadic_macros = cfgvar('test_o_matic.use_variadic_macros', default='auto', description='whether test-o-matic should enable the use of variadic macros')

def add_defines(opt):
    if variadic_macros != 'auto':
        if variadic_macros:
            opt.defines += ['TEST_O_MATIC_USE_VARIADIC_MACROS=1']
        else:
            opt.defines += ['TEST_O_MATIC_DONT_USE_VARIADIC_MACROS=1']

# Builds a static library for test_o_matic.
@target
def staticlib(kit):
    opt = kit.cpp.opt(*flags)
    opt.sources += here/'*.cpp'
    opt.includes += [here]
    add_defines(opt)

    return properties(
        libs = [kit.cpp.lib('test_o_matic', opt)],
        includes = [here]
    )

@target
def test_objects(kit):
    tomlib = staticlib(kit)

    opt = kit.cpp.opt(*flags)
    opt.sources += here/'self_test/*.cpp'
    opt.sources.erase(here/'self_test/main.cpp')
    opt.includes += [here/'self_test'] + tomlib.includes
    add_defines(opt)

    return properties(
        objs = kit.cpp.objs(opt),
        includes = tomlib.includes,
        libs = tomlib.libs,
        syslibs = tomlib.syslibs
    )

@target
def testlibs(kit):
    tomlib = staticlib(kit)
    testobjs = test_objects(kit)

    opt = kit.cpp.opt(*flags)
    opt.objects += testobjs.objs
    opt.includes += testobjs.includes + tomlib.includes
    opt.libs += testobjs.syslibs + tomlib.libs
    opt.syslibs += testobjs.syslibs + tomlib.syslibs

    return [kit.cpp.shlib('test_o_matic_testlib', opt)]

@target
def tests(kit):
    tomlib = staticlib(kit)
    testobjs = test_objects(kit)

    opt = kit.cpp.opt(*flags)
    opt.objects += testobjs.objs
    opt.sources.append(here/'self_test/main.cpp')
    opt.includes += testobjs.includes + tomlib.includes
    opt.libs += testobjs.syslibs + tomlib.libs
    opt.syslibs += testobjs.syslibs + tomlib.syslibs

    return [process(kit.cpp.exe('test_o_matic_tests', opt))]

@target
def run_tests(kit):
    for proc in tests(kit):
        proc.test()

@target
def default(kit):
    run_tests(kit)

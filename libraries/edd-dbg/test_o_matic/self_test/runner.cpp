// Copyright 2008 Edd Dawson.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <test_o_matic.hpp>

namespace
{
    using namespace test_o_matic;

    struct first_event_logger : public logger
    {
        virtual void record(const std::string &event, const strmap &) { if (first_event.empty()) first_event = event; }
        std::string first_event;
    };

    struct last_event_logger : public logger
    {
        virtual void record(const std::string &event, const strmap &) { last_event = event; }
        std::string last_event;
    };

    struct fixture
    {
        runner rnr;
        first_event_logger fe_lgr;
        last_event_logger le_lgr;
        test throwing_test;

        static void throwing_test_func(logger &) { throw 1; }

        fixture() : 
            throwing_test("temp test", __FILE__, __LINE__, __DATE__, __TIME__, &throwing_test_func)
        {
        }
    };
}

TESTFIX("test_o_matic::runner issues pre_test event before running test", fixture)
{
    TRY(rnr.run(throwing_test, fe_lgr));
    CHECK(fe_lgr.first_event == "pre_test");
}

TESTFIX("test_o_matic::runner reports exceptions thrown in tests", fixture)
{
    TRY(rnr.run(throwing_test, le_lgr));
    CHECK(le_lgr.last_event == "post_test");
}

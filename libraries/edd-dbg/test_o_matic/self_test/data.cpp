// Copyright 2008 Edd Dawson.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <test_o_matic.hpp>

namespace
{
    struct blah { };
}

namespace test_o_matic
{
    template<>
    struct stringize<blah> { static std::string convert(const blah &) { return "BLAH"; } };
}

using namespace test_o_matic;

TEST("test_o_matic::data default constructor")
{
    data d;
    strmap &m = d;
    CHECK(m.empty());
}

TEST("test_o_matic::data constructor")
{
    data d("key", "value");
    strmap &m = d;
    CHECK(m["key"] == "value");
}

TEST("test_o_matic::data conversion operator to strmap")
{
    data d("key", "value");
    const strmap &cm = d;

    REQUIRE(cm.find("key") != cm.end());
    CHECK(cm.find("key")->second == "value");

    strmap &m = d;
    m["colour"] = "red";

    REQUIRE(cm.find("colour") != cm.end());
    CHECK(cm.find("colour")->second == "red");
}

TEST("test_o_matic::data operator()")
{
    data d;

    CHECK(&d("meaning of life", 42) == &d);
    strmap &m = d;
    REQUIRE(m.find("meaning of life") != m.end());
    CHECK(m.find("meaning of life")->second == "42");
}

TEST("test_o_matic::data operator() can be chained")
{
    CHECK(static_cast<strmap &>(data("colour", "red")("answer", "42")).count("answer") == 1);
}

TEST("test_o_matic::data uses stringize specializations")
{
    blah b;
    data d("a", b);
    d("b", b);

    strmap &m = d;

    CHECK(m["a"] == "BLAH");
    CHECK(m["b"] == "BLAH");
}

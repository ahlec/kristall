// Copyright 2008 Edd Dawson.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <test_o_matic.hpp>

const char *test_name = "test_o_matic::test construction and registration";
static const std::size_t line_before_test = __LINE__;
TEST(test_name)
{
    using std::string;

    const test_o_matic::test *t = test_o_matic::first_test();
    CHECK(t != 0);

    for ( ; t; t = t->next)
        if (string(t->name) == test_name)
            break;

    REQUIRE(t != 0);
    CHECK(string(t->file) == __FILE__);
    CHECK(string(t->time) == __TIME__);
    CHECK(string(t->date) == __DATE__);
    CHECK(t->line == line_before_test + 1); // The line number on which this test is defined
}


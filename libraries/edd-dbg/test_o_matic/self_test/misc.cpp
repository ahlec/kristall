// Copyright 2011 Edd Dawson.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <test_o_matic.hpp>
#include <vector>

namespace
{
    using namespace test_o_matic;

    struct custom { };

    template<typename T, typename U>
    struct same_type { enum { value = false }; };

    template<typename T>
    struct same_type<T, T> { enum { value = true }; };
}

namespace test_o_matic
{
    template<>
    struct stringize<custom> { static std::string convert(const custom &) { return "custom"; } };
}

TEST("test_o_matic::stringize on output-streamable types")
{
    CHECK(stringize<int>::convert(-5) == "-5");
    CHECK(stringize<const char *>::convert("hello, world!") == "hello, world!");
    CHECK(stringize<std::string>::convert("hello, world!") == "hello, world!");
    CHECK(stringize<double>::convert(42) == "42");
}

TEST("test_o_matic::stringize specialized for custom type")
{
    custom c;
    CHECK(stringize<custom>::convert(c) == "custom");
}

TEST("test_o_matic::detail::to_const_ref")
{
    using test_o_matic::detail::to_const_ref;

    {
        typedef to_const_ref<int>::type int_const_ref;
        CHECK((same_type<int_const_ref, const int &>::value));
    }

    {
        typedef to_const_ref<const int>::type int_const_ref;
        CHECK((same_type<int_const_ref, const int &>::value));
    }

    {
        typedef to_const_ref<const int &>::type int_const_ref;
        CHECK((same_type<int_const_ref, const int &>::value));
    }
}

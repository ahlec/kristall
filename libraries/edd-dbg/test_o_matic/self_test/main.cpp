// Copyright 2008 Edd Dawson.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <test_o_matic.hpp>
#include <iostream>
#include <cstring>

// Can't use namespace assignment on dmc
namespace tom { using namespace test_o_matic; }

int main(int argc, char **argv)
{
    const char *match = 0;
    if (argc == 2) match = argv[1];

    tom::simple_logger lgr(std::cout, true);
    tom::runner rnr;

    for (const tom::test *t = tom::first_test(); t; t = t->next)
    {
        if (!match || std::strstr(t->name, match))
            tom::run_test(*t, lgr, rnr);
    }

    return lgr.summary(std::cout);
}

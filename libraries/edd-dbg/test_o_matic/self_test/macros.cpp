// Copyright 2008 Edd Dawson.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <test_o_matic.hpp>

namespace
{
    using namespace test_o_matic;

    template<typename A, typename B, typename C>
    struct always_true
    {
        enum { value = 1 };
        static bool func() { return true; }
    };

    void harmless() { }

    // get_local_tom_logger() is called by macros such as CHECK and THROWS. Usually, access to this function is
    // provided by TEST and TESTFIX. However, we can provide a different one for testing purposes by making a
    // override_local_logger named get_local_tom_logger inside a test definition.
    class override_local_logger
    {
        public:
            override_local_logger() : lgr_(out_, false) { }

            test_o_matic::simple_logger &operator() () { return lgr_; }

        private:
            std::ostringstream out_;
            test_o_matic::simple_logger lgr_;
    };

    void no_op() { }

    void do_check(bool pass, override_local_logger &get_local_tom_logger)
    {
        CHECK(pass);
    }

    void do_require(bool pass, override_local_logger &get_local_tom_logger, bool &finished)
    {
        try
        {
            REQUIRE(pass);
            finished = true; 
        }
        catch (const data &) { }
    }

    void do_abort(override_local_logger &/*get_local_tom_logger*/, bool &finished)
    {
        try
        {
            ABORT();
            finished = true; 
        }
        catch (const data &) { }
    }

    void do_try(bool pass, override_local_logger &get_local_tom_logger)
    {
        if (pass) TRY(no_op());
        else TRY(throw 1);
    }

    void do_throws(bool pass, override_local_logger &get_local_tom_logger)
    {
        if (pass) THROWS(throw 1, int);
        else THROWS(throw "oops", int);
    }
}

TEST("test_o_matic CHECK() macro")
{
    using namespace test_o_matic;

    override_local_logger lgr;

    do_check(true, lgr);
    std::size_t s1 = lgr().successes;
    std::size_t f1 = lgr().failures;

    do_check(false, lgr);
    std::size_t s2 = lgr().successes;
    std::size_t f2 = lgr().failures;
    
    CHECK(s1 == 1);
    CHECK(f1 == 0);
    CHECK(s2 == 1);
    CHECK(f2 == 1);
}

TEST("test_o_matic CHECK() macro applies side effects only once")
{
    int n = 1;

    {
        override_local_logger get_local_tom_logger;
        CHECK(n++ == 1);
    }

    CHECK(n == 2);
}

#if defined(TEST_O_MATIC_USE_VARIADIC_MACROS)
TEST("test_o_matic CHECK() variadic usage")
{
    CHECK(always_true<bool, bool, bool>::value);
}
#endif

TEST("test_o_matic REQUIRE() macro")
{
    using namespace test_o_matic;

    bool finished1 = false;
    bool finished2 = false;

    override_local_logger lgr;

    do_require(true, lgr, finished1);
    std::size_t s1 = lgr().successes;
    std::size_t f1 = lgr().failures;

    do_require(false, lgr, finished2);
    std::size_t s2 = lgr().successes;
    std::size_t f2 = lgr().failures;

    CHECK(finished1);
    CHECK(s1 == 1);
    CHECK(f1 == 0);

    CHECK(!finished2);
    CHECK(s2 == 1);
    CHECK(f2 == 1);
}

TEST("test_o_matic REQUIRE() macro applies side effects only once")
{
    int n = 1;

    {
        override_local_logger get_local_tom_logger;
        REQUIRE(n++ == 1);
    }

    CHECK(n == 2);
}

#if defined(TEST_O_MATIC_USE_VARIADIC_MACROS)
TEST("test_o_matic REQUIRE() variadic usage")
{
    REQUIRE(always_true<bool, bool, bool>::value);
}
#endif

TEST("test_o_matic TRY() macro")
{
    using namespace test_o_matic;

    std::size_t s1 = 0;
    std::size_t f1 = 0;
    std::size_t s2 = 0;
    std::size_t f2 = 0;
    {
        override_local_logger get_local_tom_logger;

        TRY(harmless());
        s1 = get_local_tom_logger().successes;
        f1 = get_local_tom_logger().failures;

        TRY(throw 1);
        s2 = get_local_tom_logger().successes;
        f2 = get_local_tom_logger().failures;
    }
    CHECK(s1 == 1);
    CHECK(f1 == 0);
    CHECK(s2 == 1);
    CHECK(f2 == 1);
}

TEST("test_o_matic TRY() macro applies side effects only once")
{
    int n = 1;

    {
        override_local_logger get_local_tom_logger;
        TRY(n++);
    }

    CHECK(n == 2);
}

#if defined(TEST_O_MATIC_USE_VARIADIC_MACROS)
TEST("test_o_matic TRY() variadic usage")
{
    TRY(always_true<bool, bool, bool>::func());
}
#endif

TEST("test_o_matic TRY() macro works outside of TEST/TESTFIX")
{
    override_local_logger lgr;
    do_try(true, lgr);
    CHECK(lgr().successes == 1);

    do_try(false, lgr);
    CHECK(lgr().failures == 1);
}

TEST("test_o_matic THROWS() macro")
{
    using namespace test_o_matic;

    std::size_t s1 = 0;
    std::size_t f1 = 0;
    std::size_t s2 = 0;
    std::size_t f2 = 0;
    std::size_t s3 = 0;
    std::size_t f3 = 0;
    {
        override_local_logger get_local_tom_logger;

        THROWS(throw 1, int);
        s1 = get_local_tom_logger().successes;
        f1 = get_local_tom_logger().failures;

        THROWS(harmless(), int);
        s2 = get_local_tom_logger().successes;
        f2 = get_local_tom_logger().failures;

        THROWS(throw 1, double);
        s3 = get_local_tom_logger().successes;
        f3 = get_local_tom_logger().failures;
    }
    CHECK(s1 == 1);
    CHECK(f1 == 0);
    CHECK(s2 == 1);
    CHECK(f2 == 1);
    CHECK(s3 == 1);
    CHECK(f3 == 2);
}

TEST("test_o_matic THROWS() macro applies side effects only once")
{
    int n = 1;

    {
        override_local_logger get_local_tom_logger;
        THROWS(n++, int);
    }

    CHECK(n == 2);
}

#if defined(TEST_O_MATIC_USE_VARIADIC_MACROS)
TEST("test_o_matic THROWS() variadic usage")
{
    always_true<bool, bool, bool> a;
    THROWS(throw a, always_true<bool, bool, bool>);
}
#endif

TEST("test_o_matic THROWS() macro works outside of TEST/TESTFIX")
{
    override_local_logger lgr;
    do_throws(true, lgr);
    CHECK(lgr().successes == 1);

    do_throws(false, lgr);
    CHECK(lgr().failures == 1);
}

TEST("test_o_matic ABORT() macro")
{
    override_local_logger lgr;
    bool finished = false;
    do_abort(lgr, finished);

    CHECK(!finished);
}

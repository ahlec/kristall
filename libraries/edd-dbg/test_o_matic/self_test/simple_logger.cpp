// Copyright 2011 Edd Dawson.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <test_o_matic.hpp>
#include <sstream>
#include <vector>

namespace
{
    using namespace test_o_matic;

    template<bool Verbose>
    struct fixture
    {
        std::ostringstream out;
        simple_logger lgr;

        fixture() : lgr(out, Verbose) { }
    };

    typedef fixture<true> verbose_fixture;
    typedef fixture<false> quiet_fixture;
}

TESTFIX("test_o_matic::simple_logger() handles 'check' events", verbose_fixture)
{
    lgr.record("check", data("result", "ok"));
    CHECK(lgr.successes == 1);

    lgr.record("check", data("result", "fail"));
    CHECK(lgr.failures == 1);
}

TESTFIX("test_o_matic::simple_logger() handles 'throws' events", verbose_fixture)
{
    lgr.record("throws", data("result", "ok"));
    CHECK(lgr.successes == 1);

    lgr.record("throws", data("result", "fail"));
    CHECK(lgr.failures == 1);
}

TESTFIX("test_o_matic::simple_logger() handles 'try' events", verbose_fixture)
{
    lgr.record("try", data("result", "ok"));
    CHECK(lgr.successes == 1);

    lgr.record("throws", data("result", "fail"));
    CHECK(lgr.failures == 1);
}

TESTFIX("test_o_matic::simple_logger() handles 'pre_test' events", verbose_fixture)
{
    // pre_test doesn't update the stats, so we have to look to see if it generates
    // any output in order to tell whether or not the associated handler has been called.
    lgr.record("pre_test", data());
    CHECK(!out.str().empty());
}

TESTFIX("test_o_matic::simple_logger() handles 'post_test' events", verbose_fixture)
{
    lgr.record("post_test", data("result", "ok"));
    CHECK(lgr.complete == 1);

    lgr.record("post_test", data("result", "fail"));
    CHECK(lgr.aborted == 1);
}

TESTFIX("test_o_matic::simple_logger::summary() mentions successes, failures and aborted tests", verbose_fixture)
{
    using namespace std;

    // successes: 1
    lgr.record("check", data("result", "ok"));

    // failures: 2
    lgr.record("check", data("result", "fail"));
    lgr.record("check", data("result", "fail"));

    // aborted: 3
    lgr.record("post_test", data("result", "fail"));
    lgr.record("post_test", data("result", "fail"));
    lgr.record("post_test", data("result", "fail"));

    ostringstream text;
    lgr.summary(text);

    vector<string> lines;
    istringstream iss(text.str());

    for (string line; getline(iss, line); )
        if (!line.empty())
            lines.push_back(line);

    REQUIRE(lines.size() == 3);

    string::size_type not_found = string::npos;

    CHECK(lines[0].find("successes") != not_found);
    CHECK(lines[0].find("1") != not_found);

    CHECK(lines[1].find("failures") != not_found);
    CHECK(lines[1].find("2") != not_found);

    CHECK(lines[2].find("aborted") != not_found);
    CHECK(lines[2].find("3") != not_found);
}

TESTFIX("test_o_matic::simple_logger doesn't say anything on successes when verbose=false", quiet_fixture)
{
    lgr.record("check", data("result", "ok"));
    lgr.record("throws", data("result", "ok"));
    lgr.record("try", data("result", "ok"));

    CHECK(lgr.successes == 3);
    CHECK(out.str().empty());

    lgr.record("throws", data("result", "fail"));
    CHECK(!out.str().empty());
}

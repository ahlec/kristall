#include "KristallCompiler.hpp"
#include "Pokemon/PokemonMoveDamageCategory.h"
#include "Pokemon/PokemonMoveDetails.h"
#include "Pokemon/PokemonMoveTarget.h"
#include "Pokemon/PokemonAbility.h"
#include "Pokemon/PokemonExperienceGroup.h"
#include "Pokemon/PokemonGenderRatio.h"
#include "Pokemon/PokemonSpeciesColor.h"
#include "Items/BerryUseTrigger.h"
#include "Pokemon/PokemonMoveAcquisitionMode.h"
#include "Pokemon/PokemonEggGroup.h"
#include "Pokemon/PokemonEvolutionStimulus.h"
#include "General/TimeOfDayFlags.h"
#include "Graphics/SpriteSheet.h"
#include "World/TileNatureType.h"
#include "World/MapLabel.h"
#include "World/Effects/OverworldEffectType.h"
#include "Components/MapBorder.h"
#include "World/EncounterMethod.h"
#include "World/TileType.h"
#include "World/Terrain.h"
#include "Items/ItemOverworldObjectForm.h"
#include "General/IndefiniteArticle.h"
#include <algorithm>
#include <inttypes.h>
#include "CompilerVersions.h"
#include "DebugFlags.h"

void callFinalizer()
{
    Finalizer::call();
}

template<class T>
T* transformContentFile(UnicodeString fileName)
{
    fileName.remove(fileName.length() - 4, 4);
    T* filePtr(new T(fileName));
    if (filePtr->isValid())
    {
        return filePtr;
    }
    delete filePtr;
    return nullptr;
};

template<class T>
bool sortContentFiles(T* a, T* b)
{
    return (a->getUId() < b->getUId());
};

template<class T>
uint8_t compileDirectory(UnicodeString directoryName, KristallBin& kristallBin)
{
    uint64_t contentSectionNo(KristallCompilerLog::createNewSection(0));
    KristallCompilerLog::setSectionText(contentSectionNo, "[" + directoryName + "]");
    printf("[%s]\n", UnicodeStrings::toStdString(directoryName).c_str());
    std::vector<T*> files = Directory::getAllFiles("content\\" + directoryName,
                                                   FunctionPtr<T*, UnicodeString>(transformContentFile<T>));
    std::sort(files.begin(), files.end(), sortContentFiles<T>);
    for (T* file : files)
    {
        if (file != nullptr)
        {
            file->update(kristallBin, contentSectionNo);
            kristallBin.addFile(file);
        }
    }

    printf("\n");
    return 1;
};

void declareEnums()
{
    printf("[DECLARING ENUMS]\n");
    // ElementalType
    {
        Enums::ValueMap<ElementalType> values;
        values["None"] = ElementalType::None;
        values["Normal"] = ElementalType::Normal;
        values["Fire"] = ElementalType::Fire;
        values["Fighting"] = ElementalType::Fighting;
        values["Water"] = ElementalType::Water;
        values["Flying"] = ElementalType::Flying;
        values["Grass"] = ElementalType::Grass;
        values["Poison"] = ElementalType::Poison;
        values["Electric"] = ElementalType::Electric;
        values["Ground"] = ElementalType::Ground;
        values["Psychic"] = ElementalType::Psychic;
        values["Rock"] = ElementalType::Rock;
        values["Ice"] = ElementalType::Ice;
        values["Bug"] = ElementalType::Bug;
        values["Dragon"] = ElementalType::Dragon;
        values["Ghost"] = ElementalType::Ghost;
        values["Dark"] = ElementalType::Dark;
        values["Steel"] = ElementalType::Steel;
        values["Indeterminate"] = ElementalType::Indeterminate;
        values["Fairy"] = ElementalType::Fairy;
        Enums::declare<ElementalType>(values);
    }

    // Move Damage Categories
    {
        Enums::ValueMap<PokemonMoveDamageCategory> values;
        values["Physical"] = PokemonMoveDamageCategory::Physical;
        values["Special"] = PokemonMoveDamageCategory::Special;
        values["Status"] = PokemonMoveDamageCategory::Status;
        Enums::declare<PokemonMoveDamageCategory>(values);
    }

    // PokemonMoveDetails
    {
        Enums::ValueMap<PokemonMoveDetails> values;
        values["None"] = PokemonMoveDetails::None;
        values["HasRecoil"] = PokemonMoveDetails::HasRecoil;
        values["IsPunch"] = PokemonMoveDetails::IsPunch;
        values["IsSoundBased"] = PokemonMoveDetails::IsSoundBased;
        values["MakesContact"] = PokemonMoveDetails::MakesContact;
        values["AffectedByProtect"] = PokemonMoveDetails::AffectedByProtect;
        values["AffectedByKingsRock"] = PokemonMoveDetails::AffectedByKingsRock;
        values["MirrorMoveCopied"] = PokemonMoveDetails::MirrorMoveCopied;
        values["MagicBounceReflectable"] = PokemonMoveDetails::MagicBounceReflectable;
        values["Snatchable"] = PokemonMoveDetails::Snatchable;
        values["HasChargingTurn"] = PokemonMoveDetails::HasChargingTurn;
        values["CanUseWhenAsleep"] = PokemonMoveDetails::CanUseWhenAsleep;
        values["CanUseWhenFrozen"] = PokemonMoveDetails::CanUseWhenFrozen;
        values["OneHitKO"] = PokemonMoveDetails::OneHitKO;
        values["HitsAirborne"] = PokemonMoveDetails::HitsAirborne;
        values["HitsUnderwater"] = PokemonMoveDetails::HitsUnderwater;
        values["HitsUnderground"] = PokemonMoveDetails::HitsUnderground;
        values["Heals"] = PokemonMoveDetails::Heals;
        values["Explodes"] = PokemonMoveDetails::Explodes;
        Enums::declare<PokemonMoveDetails>(values);
    }

     // Move Targets
    {
        Enums::ValueMap<PokemonMoveTarget> values;
        values["SelectedPokemon"] = PokemonMoveTarget::SelectedPokemon;
        values["RandomOpponent"] = PokemonMoveTarget::RandomOpponent;
        values["Ally"] = PokemonMoveTarget::Ally;
        values["Self"] = PokemonMoveTarget::Self;
        values["SelfOrAlly"] = PokemonMoveTarget::SelfOrAlly;
        values["OwnField"] = PokemonMoveTarget::OwnField;
        values["OpponentField"] = PokemonMoveTarget::OpponentField;
        values["AllOthers"] = PokemonMoveTarget::AllOthers;
        values["AllOpponents"] = PokemonMoveTarget::AllOpponents;
        values["EntireField"] = PokemonMoveTarget::EntireField;
        values["Special"] = PokemonMoveTarget::Special;
        Enums::declare<PokemonMoveTarget>(values);
    }

    // Pokemon Abilities
    {
        Enums::ValueMap<PokemonAbility> values;
        values["None"] = PokemonAbility::None;
        values["Adaptability"] = PokemonAbility::Adaptability;
        values["Aftermath"] = PokemonAbility::Aftermath;
        values["AirLock"] = PokemonAbility::AirLock;
        values["Analytic"] = PokemonAbility::Analytic;
        values["AngerPoint"] = PokemonAbility::AngerPoint;
        values["Anticipation"] = PokemonAbility::Anticipation;
        values["ArenaTrap"] = PokemonAbility::ArenaTrap;
        values["BadDreams"] = PokemonAbility::BadDreams;
        values["BattleArmor"] = PokemonAbility::BattleArmor;
        values["BigPecks"] = PokemonAbility::BigPecks;
        values["Blaze"] = PokemonAbility::Blaze;
        values["Cacophony"] = PokemonAbility::Cacophony;
        values["Chlorophyll"] = PokemonAbility::Chlorophyll;
        values["ClearBody"] = PokemonAbility::ClearBody;
        values["CloudNine"] = PokemonAbility::CloudNine;
        values["ColorChange"] = PokemonAbility::ColorChange;
        values["Compoundeyes"] = PokemonAbility::Compoundeyes;
        values["Contrary"] = PokemonAbility::Contrary;
        values["CursedBody"] = PokemonAbility::CursedBody;
        values["CuteCharm"] = PokemonAbility::CuteCharm;
        values["Damp"] = PokemonAbility::Damp;
        values["Defeatist"] = PokemonAbility::Defeatist;
        values["Defiant"] = PokemonAbility::Defiant;
        values["Download"] = PokemonAbility::Download;
        values["Drizzle"] = PokemonAbility::Drizzle;
        values["Drought"] = PokemonAbility::Drought;
        values["DrySkin"] = PokemonAbility::DrySkin;
        values["EarlyBird"] = PokemonAbility::EarlyBird;
        values["EffectSpore"] = PokemonAbility::EffectSpore;
        values["Filter"] = PokemonAbility::Filter;
        values["FlameBody"] = PokemonAbility::FlameBody;
        values["FlareBoost"] = PokemonAbility::FlareBoost;
        values["FlashFire"] = PokemonAbility::FlashFire;
        values["FlowerGift"] = PokemonAbility::FlowerGift;
        values["Forecast"] = PokemonAbility::Forecast;
        values["Forewarn"] = PokemonAbility::Forewarn;
        values["FriendGuard"] = PokemonAbility::FriendGuard;
        values["Frisk"] = PokemonAbility::Frisk;
        values["Gluttony"] = PokemonAbility::Gluttony;
        values["Guts"] = PokemonAbility::Guts;
        values["Harvest"] = PokemonAbility::Harvest;
        values["Healer"] = PokemonAbility::Healer;
        values["Heatproof"] = PokemonAbility::Heatproof;
        values["HeavyMetal"] = PokemonAbility::HeavyMetal;
        values["HoneyGather"] = PokemonAbility::HoneyGather;
        values["HugePower"] = PokemonAbility::HugePower;
        values["Hustle"] = PokemonAbility::Hustle;
        values["Hydration"] = PokemonAbility::Hydration;
        values["HyperCutter"] = PokemonAbility::HyperCutter;
        values["IceBody"] = PokemonAbility::IceBody;
        values["Illuminate"] = PokemonAbility::Illuminate;
        values["Illusion"] = PokemonAbility::Illusion;
        values["Immunity"] = PokemonAbility::Immunity;
        values["Imposter"] = PokemonAbility::Imposter;
        values["Infiltrator"] = PokemonAbility::Infiltrator;
        values["InnerFocus"] = PokemonAbility::InnerFocus;
        values["Insomnia"] = PokemonAbility::Insomnia;
        values["Intimidate"] = PokemonAbility::Intimidate;
        values["IronBarbs"] = PokemonAbility::IronBarbs;
        values["IronFist"] = PokemonAbility::IronFist;
        values["Justified"] = PokemonAbility::Justified;
        values["KeenEye"] = PokemonAbility::KeenEye;
        values["Klutz"] = PokemonAbility::Klutz;
        values["LeafGuard"] = PokemonAbility::LeafGuard;
        values["Levitate"] = PokemonAbility::Levitate;
        values["LightMetal"] = PokemonAbility::LightMetal;
        values["Lightningrod"] = PokemonAbility::Lightningrod;
        values["Limber"] = PokemonAbility::Limber;
        values["LiquidOoze"] = PokemonAbility::LiquidOoze;
        values["MagicBounce"] = PokemonAbility::MagicBounce;
        values["MagicGuard"] = PokemonAbility::MagicGuard;
        values["MagmaArmor"] = PokemonAbility::MagmaArmor;
        values["MagnetPull"] = PokemonAbility::MagnetPull;
        values["MarvelScale"] = PokemonAbility::MarvelScale;
        values["Minus"] = PokemonAbility::Minus;
        values["MoldBreaker"] = PokemonAbility::MoldBreaker;
        values["Moody"] = PokemonAbility::Moody;
        values["MotorDrive"] = PokemonAbility::MotorDrive;
        values["Moxie"] = PokemonAbility::Moxie;
        values["Multiscale"] = PokemonAbility::Multiscale;
        values["Multitype"] = PokemonAbility::Multitype;
        values["Mummy"] = PokemonAbility::Mummy;
        values["NaturalCure"] = PokemonAbility::NaturalCure;
        values["NoGuard"] = PokemonAbility::NoGuard;
        values["Normalize"] = PokemonAbility::Normalize;
        values["Oblivious"] = PokemonAbility::Oblivious;
        values["Overcoat"] = PokemonAbility::Overcoat;
        values["Overgrow"] = PokemonAbility::Overgrow;
        values["OwnTempo"] = PokemonAbility::OwnTempo;
        values["Pickpocket"] = PokemonAbility::Pickpocket;
        values["Pickup"] = PokemonAbility::Pickup;
        values["Plus"] = PokemonAbility::Plus;
        values["PoisonHeal"] = PokemonAbility::PoisonHeal;
        values["PoisonPoint"] = PokemonAbility::PoisonPoint;
        values["PoisonTouch"] = PokemonAbility::PoisonTouch;
        values["Prankster"] = PokemonAbility::Prankster;
        values["Pressure"] = PokemonAbility::Pressure;
        values["PurePower"] = PokemonAbility::PurePower;
        values["QuickFeet"] = PokemonAbility::QuickFeet;
        values["RainDish"] = PokemonAbility::RainDish;
        values["Rattled"] = PokemonAbility::Rattled;
        values["Reckless"] = PokemonAbility::Reckless;
        values["Regenerator"] = PokemonAbility::Regenerator;
        values["Rivalry"] = PokemonAbility::Rivalry;
        values["RockHead"] = PokemonAbility::RockHead;
        values["RoughSkin"] = PokemonAbility::RoughSkin;
        values["RunAway"] = PokemonAbility::RunAway;
        values["SandForce"] = PokemonAbility::SandForce;
        values["SandRush"] = PokemonAbility::SandRush;
        values["SandStream"] = PokemonAbility::SandStream;
        values["SandVeil"] = PokemonAbility::SandVeil;
        values["SapSipper"] = PokemonAbility::SapSipper;
        values["Scrappy"] = PokemonAbility::Scrappy;
        values["SereneGrace"] = PokemonAbility::SereneGrace;
        values["ShadowTag"] = PokemonAbility::ShadowTag;
        values["ShedSkin"] = PokemonAbility::ShedSkin;
        values["SheerForce"] = PokemonAbility::SheerForce;
        values["ShellArmor"] = PokemonAbility::ShellArmor;
        values["ShieldDust"] = PokemonAbility::ShieldDust;
        values["Simple"] = PokemonAbility::Simple;
        values["SkillLink"] = PokemonAbility::SkillLink;
        values["SlowStart"] = PokemonAbility::SlowStart;
        values["Sniper"] = PokemonAbility::Sniper;
        values["SnowCloak"] = PokemonAbility::SnowCloak;
        values["SnowWarning"] = PokemonAbility::SnowWarning;
        values["SolarPower"] = PokemonAbility::SolarPower;
        values["SolidRock"] = PokemonAbility::SolidRock;
        values["Soundproof"] = PokemonAbility::Soundproof;
        values["SpeedBoost"] = PokemonAbility::SpeedBoost;
        values["Stall"] = PokemonAbility::Stall;
        values["Static"] = PokemonAbility::Static;
        values["Steadfast"] = PokemonAbility::Steadfast;
        values["Stench"] = PokemonAbility::Stench;
        values["StickyHold"] = PokemonAbility::StickyHold;
        values["StormDrain"] = PokemonAbility::StormDrain;
        values["Sturdy"] = PokemonAbility::Sturdy;
        values["SuctionCups"] = PokemonAbility::SuctionCups;
        values["SuperLuck"] = PokemonAbility::SuperLuck;
        values["Swarm"] = PokemonAbility::Swarm;
        values["SwiftSwim"] = PokemonAbility::SwiftSwim;
        values["Synchronize"] = PokemonAbility::Synchronize;
        values["TangledFeet"] = PokemonAbility::TangledFeet;
        values["Technician"] = PokemonAbility::Technician;
        values["Telepathy"] = PokemonAbility::Telepathy;
        values["Teravolt"] = PokemonAbility::Teravolt;
        values["ThickFat"] = PokemonAbility::ThickFat;
        values["TintedLens"] = PokemonAbility::TintedLens;
        values["Torrent"] = PokemonAbility::Torrent;
        values["ToxicBoost"] = PokemonAbility::ToxicBoost;
        values["Trace"] = PokemonAbility::Trace;
        values["Truant"] = PokemonAbility::Truant;
        values["Turboblaze"] = PokemonAbility::Turboblaze;
        values["Unaware"] = PokemonAbility::Unaware;
        values["Unburden"] = PokemonAbility::Unburden;
        values["Unnerve"] = PokemonAbility::Unnerve;
        values["VictoryStar"] = PokemonAbility::VictoryStar;
        values["VitalSpirit"] = PokemonAbility::VitalSpirit;
        values["VoltAbsorb"] = PokemonAbility::VoltAbsorb;
        values["WaterAbsorb"] = PokemonAbility::WaterAbsorb;
        values["WaterVeil"] = PokemonAbility::WaterVeil;
        values["WeakArmor"] = PokemonAbility::WeakArmor;
        values["WhiteSmoke"] = PokemonAbility::WhiteSmoke;
        values["WonderGuard"] = PokemonAbility::WonderGuard;
        values["WonderSkin"] = PokemonAbility::WonderSkin;
        values["ZenMode"] = PokemonAbility::ZenMode;
        values["Aerilate"] = PokemonAbility::Aerilate;
        values["AromaVeil"] = PokemonAbility::AromaVeil;
        values["AuraBreak"] = PokemonAbility::AuraBreak;
        values["Bulletproof"] = PokemonAbility::Bulletproof;
        values["CheekPouch"] = PokemonAbility::CheekPouch;
        values["Competitive"] = PokemonAbility::Competitive;
        values["DarkAura"] = PokemonAbility::DarkAura;
        values["FairyAura"] = PokemonAbility::FairyAura;
        values["FurCoat"] = PokemonAbility::FurCoat;
        values["GaleWings"] = PokemonAbility::GaleWings;
        values["Gooey"] = PokemonAbility::Gooey;
        values["GrassPelt"] = PokemonAbility::GrassPelt;
        values["Magician"] = PokemonAbility::Magician;
        values["MegaLauncher"] = PokemonAbility::MegaLauncher;
        values["ParentalBond"] = PokemonAbility::ParentalBond;
        values["Pixilate"] = PokemonAbility::Pixilate;
        values["Protean"] = PokemonAbility::Protean;
        values["Refrigerate"] = PokemonAbility::Refrigerate;
        values["StanceChange"] = PokemonAbility::StanceChange;
        values["StrongJaw"] = PokemonAbility::StrongJaw;
        values["Symbiosis"] = PokemonAbility::Symbiosis;
        values["ToughClaws"] = PokemonAbility::ToughClaws;
        Enums::declare<PokemonAbility>(values);
    }

    // PokemonExperienceGroups
    {
        Enums::ValueMap<PokemonExperienceGroup> values;
        values["Erratic"] = PokemonExperienceGroup::Erratic;
        values["Fast"] = PokemonExperienceGroup::Fast;
        values["MediumFast"] = PokemonExperienceGroup::MediumFast;
        values["MediumSlow"] = PokemonExperienceGroup::MediumSlow;
        values["Slow"] = PokemonExperienceGroup::Slow;
        values["Fluctuating"] = PokemonExperienceGroup::Fluctuating;
        Enums::declare<PokemonExperienceGroup>(values);
    }

    // PokemonGenderRatio
    {
        Enums::ValueMap<PokemonGenderRatio> values;
        values["MaleOnly"] = PokemonGenderRatio::MaleOnly;
        values["OneFemaleSevenMale"] = PokemonGenderRatio::OneFemaleSevenMale;
        values["OneFemaleThreeMale"] = PokemonGenderRatio::OneFemaleThreeMale;
        values["OneFemaleOneMale"] = PokemonGenderRatio::OneFemaleOneMale;
        values["ThreeFemaleOneMale"] = PokemonGenderRatio::ThreeFemaleOneMale;
        values["FemaleOnly"] = PokemonGenderRatio::FemaleOnly;
        values["Genderless"] = PokemonGenderRatio::Genderless;
        Enums::declare<PokemonGenderRatio>(values);
    }

    // PokemonSpeciesColor
    {
        Enums::ValueMap<PokemonSpeciesColor> values;
        values["White"] = PokemonSpeciesColor::White;
        values["Black"] = PokemonSpeciesColor::Black;
        values["Blue"] = PokemonSpeciesColor::Blue;
        values["Brown"] = PokemonSpeciesColor::Brown;
        values["Gray"] = PokemonSpeciesColor::Gray;
        values["Green"] = PokemonSpeciesColor::Green;
        values["Pink"] = PokemonSpeciesColor::Pink;
        values["Purple"] = PokemonSpeciesColor::Purple;
        values["Red"] = PokemonSpeciesColor::Red;
        values["Yellow"] = PokemonSpeciesColor::Yellow;
        Enums::declare<PokemonSpeciesColor>(values);
    }

    // ItemType
    {
        Enums::ValueMap<ItemType> values;
        values["Item"] = ItemType::Item;
        values["Medicine"] = ItemType::Medicine;
        values["PokeBall"] = ItemType::PokeBall;
        values["MoveMachine"] = ItemType::MoveMachine;
        values["Berry"] = ItemType::Berry;
        values["Mail"] = ItemType::Mail;
        values["BattleItem"] = ItemType::BattleItem;
        values["KeyItem"] = ItemType::KeyItem;
        Enums::declare<ItemType>(values);
    }

    // BerryUseTrigger
    {
        Enums::ValueMap<BerryUseTrigger> values;
        values["None"] = BerryUseTrigger::None;
        values["LowHP"] = BerryUseTrigger::LowHP;
        values["LowPP"] = BerryUseTrigger::LowPP;
        values["NonVolatileStatus"] = BerryUseTrigger::NonVolatileStatus;
        values["VolatileStatus"] = BerryUseTrigger::VolatileStatus;
        values["SuperEffectiveAttack"] = BerryUseTrigger::SuperEffectiveAttack;
        values["AnyAttack"] = BerryUseTrigger::AnyAttack;
        Enums::declare<BerryUseTrigger>(values);
    }

    // PokemonMoveAcquisitionMode
    {
        Enums::ValueMap<PokemonMoveAcquisitionMode> values;
        values["Leveling"] = PokemonMoveAcquisitionMode::Leveling;
        values["MoveMachineTM"] = PokemonMoveAcquisitionMode::MoveMachineTM;
        values["MoveMachineHM"] = PokemonMoveAcquisitionMode::MoveMachineHM;
        values["Tutoring"] = PokemonMoveAcquisitionMode::Tutoring;
        values["Egg"] = PokemonMoveAcquisitionMode::Egg;
        Enums::declare<PokemonMoveAcquisitionMode>(values);
    }

    // PokemonEggGroup
    {
        Enums::ValueMap<PokemonEggGroup> values;
        values["None"] = PokemonEggGroup::None;
        values["Monster"] = PokemonEggGroup::Monster;
        values["Water1"] = PokemonEggGroup::Water1;
        values["Bug"] = PokemonEggGroup::Bug;
        values["Flying"] = PokemonEggGroup::Flying;
        values["Field"] = PokemonEggGroup::Field;
        values["Fairy"] = PokemonEggGroup::Fairy;
        values["Grass"] = PokemonEggGroup::Grass;
        values["Humanlike"] = PokemonEggGroup::Humanlike;
        values["Water3"] = PokemonEggGroup::Water3;
        values["Mineral"] = PokemonEggGroup::Mineral;
        values["Amorphous"] = PokemonEggGroup::Amorphous;
        values["Water2"] = PokemonEggGroup::Water2;
        values["Ditto"] = PokemonEggGroup::Ditto;
        values["Dragon"] = PokemonEggGroup::Dragon;
        values["Undiscovered"] = PokemonEggGroup::Undiscovered;
        Enums::declare<PokemonEggGroup>(values);
    }

    // PokemonEvolutionStimulus
    {
        Enums::ValueMap<PokemonEvolutionStimulus> values;
        values["Leveling"] = PokemonEvolutionStimulus::Leveling;
        values["ItemUse"] = PokemonEvolutionStimulus::ItemUse;
        values["Friendship"] = PokemonEvolutionStimulus::Friendship;
        values["LuaFunction"] = PokemonEvolutionStimulus::LuaFunction;
        Enums::declare<PokemonEvolutionStimulus>(values);
    }

    // TimeOfDayFlags
    {
        Enums::ValueMap<TimeOfDayFlags> values;
        values["None"] = TimeOfDayFlags::None;
        values["Morning"] = TimeOfDayFlags::Morning;
        values["Day"] = TimeOfDayFlags::Day;
        values["Night"] = TimeOfDayFlags::Night;
        Enums::declare<TimeOfDayFlags>(values);
    }

    // SpriteSheet
    {
        Enums::ValueMap<SpriteSheet> values;
        values["OverworldInterior"] = SpriteSheet::OverworldInterior;
        values["OverworldExterior"] = SpriteSheet::OverworldExterior;
        values["OverworldObjects"] = SpriteSheet::OverworldObjects;
        values["OverworldEffects"] = SpriteSheet::OverworldEffects;
        values["OverworldUserInterface"] = SpriteSheet::OverworldUserInterface;
        values["GeneralUserInterface"] = SpriteSheet::GeneralUserInterface;
        values["Buildings"] = SpriteSheet::Buildings;
        values["Characters"] = SpriteSheet::Characters;
        values["CharactersEmotes"] = SpriteSheet::CharactersEmotes;
        values["Items"] = SpriteSheet::Items;
        values["BattleInterface"] = SpriteSheet::BattleInterface;
        values["BattleTerrains"] = SpriteSheet::BattleTerrains;
        values["BattleTrainerMugshots"] = SpriteSheet::BattleTrainerMugshots;
        values["BattleCharacterSprites"] = SpriteSheet::BattleCharacterSprites;
        values["BattleStatusAilments"] = SpriteSheet::BattleStatusAilments;
        values["PokeBalls"] = SpriteSheet::PokeBalls;
        values["Pokemon"] = SpriteSheet::Pokemon;
        Enums::declare<SpriteSheet>(values);
    }

    // TileNatureType
    {
        Enums::ValueMap<TileNatureType> values;
        values["Normal"] = TileNatureType::Normal;
        values["Solid"] = TileNatureType::Solid;
        values["Ledge"] = TileNatureType::Ledge;
        values["BikePath"] = TileNatureType::BikePath;
        values["Water"] = TileNatureType::Water;
        values["WaterCurrent"] = TileNatureType::WaterCurrent;
        values["DeepWater"] = TileNatureType::DeepWater;
        values["Seafloor"] = TileNatureType::Seafloor;
        values["Ice"] = TileNatureType::Ice;
        values["Spinner"] = TileNatureType::Spinner;
        values["Stopper"] = TileNatureType::Stopper;
        Enums::declare<TileNatureType>(values);
    }

    // MapLabel
    {
        Enums::ValueMap<MapLabel> values;
        values["Generic"] = MapLabel::Generic;
        values["City"] = MapLabel::City;
        values["Town"] = MapLabel::Town;
        values["CityEdge"] = MapLabel::CityEdge;
        values["Mountains"] = MapLabel::Mountains;
        values["Forest"] = MapLabel::Forest;
        values["Ocean"] = MapLabel::Ocean;
        values["Countryside"] = MapLabel::Countryside;
        values["Lake"] = MapLabel::Lake;
        Enums::declare<MapLabel>(values);
    }

    // OverworldEffectType
    {
        Enums::ValueMap<OverworldEffectType> values;
        values["None"] = OverworldEffectType::None;
        values["Rain"] = OverworldEffectType::Rain;
        values["Darkness"] = OverworldEffectType::Darkness;
        values["Snow"] = OverworldEffectType::Snow;
        values["Sandstorm"] = OverworldEffectType::Sandstorm;
        Enums::declare<OverworldEffectType>(values);
    }

    // MapBorder
    {
        Enums::ValueMap<MapBorder> values;
        values["None"] = MapBorder::None;
        values["Black"] = MapBorder::Black;
        values["Trees"] = MapBorder::Trees;
        values["Ocean"] = MapBorder::Ocean;
        values["Mountain"] = MapBorder::Mountain;
        values["Grass"] = MapBorder::Grass;
        values["WhiteFence"] = MapBorder::WhiteFence;
        values["DoubleTierMountain"] = MapBorder::DoubleTierMountain;
        values["OddTrees"] = MapBorder::OddTrees;
        Enums::declare<MapBorder>(values);
    }

    // EncounterMethod
    {
        Enums::ValueMap<EncounterMethod> values;
        values["Wandering"] = EncounterMethod::Wandering;
        values["OldRod"] = EncounterMethod::OldRod;
        values["GoodRod"] = EncounterMethod::GoodRod;
        values["SuperRod"] = EncounterMethod::SuperRod;
        values["Headbutt"] = EncounterMethod::Headbutt;
        Enums::declare<EncounterMethod>(values);
    }

    // TileType
    {
        Enums::ValueMap<TileType> values;
        values["Tile"] = TileType::Tile;
        values["MapBridgeTile"] = TileType::MapBridgeTile;
        values["DoorTile"] = TileType::DoorTile;
        Enums::declare<TileType>(values);
    }

    // Terrain
    {
        Enums::ValueMap<Terrain> values;
        values["Dirt"] = Terrain::Dirt;
        values["Snow"] = Terrain::Snow;
        values["Water"] = Terrain::Water;
        values["Ice"] = Terrain::Ice;
        values["Grass"] = Terrain::Grass;
        values["Sand"] = Terrain::Sand;
        values["Rocky"] = Terrain::Rocky;
        values["Cave"] = Terrain::Cave;
        values["Interior"] = Terrain::Interior;
        values["Mud"] = Terrain::Mud;
        values["Puddle"] = Terrain::Puddle;
        Enums::declare<Terrain>(values);
    }

    // ItemOverworldObjectForm
    {
        Enums::ValueMap<ItemOverworldObjectForm> values;
        values["Visible"] = ItemOverworldObjectForm::Visible;
        values["Obscured"] = ItemOverworldObjectForm::Obscured;
        values["Hidden"] = ItemOverworldObjectForm::Hidden;
        Enums::declare<ItemOverworldObjectForm>(values);
    }

    // IndefiniteArticle
    {
        Enums::ValueMap<IndefiniteArticle> values;
        values["A"] = IndefiniteArticle::A;
        values["An"] = IndefiniteArticle::An;
        Enums::declare<IndefiniteArticle>(values);
    }
};

void outputException(ExecutionException& error)
{
    uint64_t sectionNo(KristallCompilerLog::createNewSection());
    KristallCompilerLog::setSectionText(sectionNo, "[EXCEPTION]", KristallCompilerLogColor::Red);

    uint64_t messageNo(KristallCompilerLog::createNewSection(sectionNo));
    KristallCompilerLog::setSectionText(messageNo, error.getErrorMessage());

    uint64_t div1No(KristallCompilerLog::createNewSection(sectionNo));
    KristallCompilerLog::setSectionText(div1No, "--------------------------------------------------");

    uint64_t stackNo(KristallCompilerLog::createNewSection(sectionNo));
    KristallCompilerLog::setSectionText(stackNo, error.getStackTrace());

    uint64_t div2No(KristallCompilerLog::createNewSection(sectionNo));
    KristallCompilerLog::setSectionText(div2No, "--------------------------------------------------");

    printf("\n\n");
    printf("[EXCEPTION]\n");
    std::string stdString;
    error.getErrorMessage().toUTF8String(stdString);
    printf("%s\n", stdString.c_str());
    printf("--------------------------------------------------\n");
    error.getStackTrace().toUTF8String(stdString);
    printf("%s", stdString.c_str());
    printf("--------------------------------------------------\n");

    uint64_t dataNo;
    std::map<String, String> data(error.getData());
    std::string stdString2;
    for (auto dataNode : data)
    {
        dataNo = KristallCompilerLog::createNewSection(sectionNo);
        KristallCompilerLog::appendSectionText(dataNo, "'" + dataNode.first + "': '" +
                                               dataNode.second + "'");

        dataNode.first.toUTF8String(stdString);
        dataNode.second.toUTF8String(stdString2);
        printf("'%s': '%s'\n", stdString.c_str(), stdString2.c_str());
    }

}

String getSignalName(int signalParam)
{
    if (signalParam == SIGSEGV)
    {
        return "SIGSEGV";
    }
    else if (signalParam == SIGFPE)
    {
        return "SIGFPE";
    }
    else if (signalParam == SIGILL)
    {
        return "SIGILL";
    }
    else if (signalParam == SIGINT)
    {
        return "SIGINT";
    }
    else if (signalParam == SIGABRT)
    {
        return "SIGABRT";
    }
    else if (signalParam == SIGBREAK)
    {
        return "SIGBREAK";
    }

    return toString(signalParam);
}

void handleSignals(int signalParam)
{
    static bool s_isHandlingSignal = false;

    if (s_isHandlingSignal)
    {
        ExecutionException newError("Encountered another signal while handling the initial signal.");
        newError.addData("signalParam", getSignalName(signalParam));
        throw newError;
    }
    s_isHandlingSignal = true;

    try
    {
        ExecutionException error("A signal was caught during execution.");
        error.addData("signal", getSignalName(signalParam));
        outputException(error);
    }
    catch(...)
    {
        printf("\n\n");
        printf("[SIGNAL]\n");
        printf("Encountered a signal during execution; additionally, construction of an ExecutionException object was impossible.");
        printf("'signalParam': %d.\n", signalParam);
    }

    KristallCompilerLog::done();
    exit(EXIT_FAILURE);
}

void ensureDirectoryExists(UnicodeString relativePath)
{
    if (!Directory::exists(relativePath))
    {
        std::string stdstrRelativePath;
        relativePath.toUTF8String(stdstrRelativePath);
        printf("Creating required directory structure ./%s\n", stdstrRelativePath.c_str());

        Directory::create(relativePath);
    }
};

int main(int argc, char **argv)
{
    KristallCompilerLog::start();
    SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS);

    try
    {
        if (signal(SIGSEGV, handleSignals) == SIG_IGN)
        {
            signal(SIGSEGV, SIG_IGN);
        }

        if (signal(SIGFPE, handleSignals) == SIG_IGN)
        {
            signal(SIGFPE, SIG_IGN);
        }

        if (signal(SIGILL, handleSignals) == SIG_IGN)
        {
            signal(SIGILL, SIG_IGN);
        }

        printf("    __ __        _        __          __ __ ______                          _  __           \n");
        printf("   / //_/ _____ (_)_____ / /_ ____ _ / // // ____/____   ____ ___   ____   (_)/ /___   _____\n");
        printf("  / ,<   / ___// // ___// __// __ `// // // /    / __ \\ / __ `__ \\ / __ \\ / // // _ \\ / ___/\n");
        printf(" / /| | / /   / /(__  )/ /_ / /_/ // // // /___ / /_/ // / / / / // /_/ // // //  __// /    \n");
        printf("/_/ |_|/_/   /_//____/ \\__/ \\__,_//_//_/ \\____/ \\____//_/ /_/ /_// .___//_//_/ \\___//_/     \n");
        printf("                                                                /_/                         \n");

        UnicodeString beganUnicodeStr(UnicodeStrings::getCurrentDateTime());
        std::string beganStr(UnicodeStrings::toStdString(beganUnicodeStr));
        printf("Began:               %s\n", beganStr.c_str());
        printf("Compiler compiled:   %s %s\n", __DATE__, __TIME__);
        printf("Versions:            File:            %I64u\n", COMPILER_VERSION_MAIN);
        printf("                     SpriteSheets:    %I64u\n", COMPILER_VERSION_FILE_SPRITESHEET);
        printf("                     GymBadges:       %I64u\n", COMPILER_VERSION_FILE_GYMBADGE);
        printf("                     PokemonMoves:    %I64u\n", COMPILER_VERSION_FILE_POKEMONMOVE);
        printf("                     Items:           %I64u\n", COMPILER_VERSION_FILE_ITEM);
        printf("                     PokemonSpecies:  %I64u\n", COMPILER_VERSION_FILE_POKEMONSPECIES);
        printf("                     Maps:            %I64u\n", COMPILER_VERSION_FILE_MAP);
        printf("                     Regions:         %I64u\n", COMPILER_VERSION_FILE_REGION);
        printf("                     Textures:        %I64u\n", COMPILER_VERSION_FILE_TEXTURE);
        printf("                     Frames:          %I64u\n", COMPILER_VERSION_FILE_FRAME);
        printf("\n");

        declareEnums();
        printf("\n");

        ensureDirectoryExists("dat");
        ensureDirectoryExists("dat/textures");
        ensureDirectoryExists("dat/clipboards");
        ensureDirectoryExists("reports");
        ensureDirectoryExists("reports/reference");

        KristallBin kristallBin;

        uint8_t fileTypeCount = 0;
        fileTypeCount += compileDirectory<SpriteSheetFile>("catalogs", kristallBin);
        fileTypeCount += compileDirectory<GymBadgeFile>("gym-badges", kristallBin);
        fileTypeCount += compileDirectory<PokemonMoveFile>("moves", kristallBin);
        fileTypeCount += compileDirectory<ItemFile>("items", kristallBin);
        fileTypeCount += compileDirectory<PokemonSpeciesFile>("pokemon", kristallBin);
        fileTypeCount += compileDirectory<MapFile>("maps", kristallBin);
        fileTypeCount += compileDirectory<RegionFile>("regions", kristallBin);

        TextureManager::compile(kristallBin);
        fileTypeCount += 1;

        fileTypeCount += compileDirectory<FrameFile>("gui/frames", kristallBin);
        /*
        // Doesn't work right now because doesn't take into account the special FileTypes like
        // PlayerGame or GameSettings which aren't compiled here.

        if (fileTypeCount != static_cast<uint8_t>(FileType::COUNT))
        {
            printf("Missing some file type compilations\n");
            ExecutionException error("Missing some file type compilations");
            error.addData("Number of compiled files", toString(fileTypeCount));
            throw error;
        }*/

        printf("[COMPILE]\n");
        kristallBin.compile();
        kristallBin.close();
        printf("[COMPLETE]\n");
        callFinalizer();
        printf("[COMPILER FINALIZED]\n");
    }
    catch (ExecutionException& ex)
    {
        outputException(ex);
    }

    KristallCompilerLog::done();

    // Open the output log file
    SHELLEXECUTEINFO ShExecInfo;
    ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
    ShExecInfo.fMask = NULL;
    ShExecInfo.hwnd = NULL;
    ShExecInfo.lpVerb = "open";
    ShExecInfo.lpFile = KRISTALLCOMPILER_LOG_FILE;
    ShExecInfo.lpParameters = NULL;
    ShExecInfo.lpDirectory = NULL;
    ShExecInfo.nShow = SW_MAXIMIZE;
    ShExecInfo.hInstApp = NULL;

    ShellExecuteEx(&ShExecInfo);

    // Done
    return 0;
}

Pages.WriteLine("It's been a while since you've last played. Would you like to review your key configurations before you play?");
if Pages.Prompt({"Yes", "No"}) == "Yes" then
	Pages.New();
	local keybindings = Settings.KeyBindings;
	Pages.WriteLine("Let's review your key configurations.");
	Pages.WriteLine("");
	Pages.WriteLine("[Blue]Movement:[/Blue]");
	Pages.WriteLine("  Up: " .. keytostring(keybindings[InputButtons.Up]));
	Pages.WriteLine("  Down: " .. keytostring(keybindings[InputButtons.Down]));
	Pages.WriteLine("  Left: " .. keytostring(keybindings[InputButtons.Left]));
	Pages.WriteLine("  Right: " .. keytostring(keybindings[InputButtons.Right]));
	Pages.WriteLine("");
	Pages.WriteLine("[Blue]Interaction:[/Blue]");
	Pages.WriteLine("  A: " .. keytostring(keybindings[InputButtons.A]));
	Pages.WriteLine("  B: " .. keytostring(keybindings[InputButtons.B]));
	Pages.WriteLine("");
	Pages.WriteLine("[Blue]System:[/Blue]");
	Pages.WriteLine("  Start: " .. keytostring(keybindings[InputButtons.Start]));
	Pages.WriteLine("  Select: " .. keytostring(keybindings[InputButtons.Select]));
	Pages.WriteLine("  Left Shoulder: " .. keytostring(keybindings[InputButtons.LeftShoulder]));
	Pages.WriteLine("  Right Shoulder: " .. keytostring(keybindings[InputButtons.RightShoulder]));
	Pages.WriteLine("");
	Pages.WriteLine("[Blue]Special:[/Blue]");
	Pages.WriteLine("  Screenshot: PrintScreen");
	Pages.WriteLine("  Screenshot (without UI): PrintScreen + Ctrl");
	Pages.WriteLine("  Report: F10");
	Pages.WriteLine("  Help: F1");
end
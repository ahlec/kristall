$(document).ready(function () {
	$("span.toggle").click(function () {
		var toggleButton = $(this);
		if (toggleButton.hasClass("collapsed")) {
			toggleButton.removeClass("collapsed");
			toggleButton.addClass("expanded");
			toggleButton.parent().children("ul").removeClass("collapsed");
		}
		else {
			toggleButton.removeClass("expanded");
			toggleButton.addClass("collapsed");
			toggleButton.parent().children("ul").addClass("collapsed");
		}
	});
});
#include "KristallCompiler.hpp"
#include "CompilerVersions.h"

static XmlDocument s_log("html");
static XmlElementPtr s_bodyNode(nullptr);
static XmlElementPtr s_mainNode(nullptr);
static uint64_t s_nextSectionNo(0);
std::vector<XmlElementPtr> s_sections;
static UDate s_startDate;
static XmlElementPtr s_metaTable(nullptr);

UnicodeString toString(KristallCompilerLogColor color)
{
    switch (color)
    {
        case KristallCompilerLogColor::Black:
            {
                return "Black";
            }
        case KristallCompilerLogColor::Green:
            {
                return "Green";
            }
        case KristallCompilerLogColor::Red:
            {
                return "Red";
            }
        case KristallCompilerLogColor::Yellow:
            {
                return "Yellow";
            }
        case KristallCompilerLogColor::Orange:
            {
                return "Orange";
            }
    }
}

uint64_t KristallCompilerLog::createNewSection(uint64_t parentSection)
{
    XmlElementPtr newSection(nullptr);
    if (parentSection == 0)
    {
        newSection = s_bodyNode->appendChild("li");
    }
    else
    {
        if (s_sections.size() < parentSection)
        {
            ExecutionException error("Attempted to add a section to a parent which doesn't exist.");
            error.addData("parentSection", toString(parentSection));
            KristallCompilerLog::done();
            throw error;
        }
        XmlElementPtr parentLi(s_sections[parentSection - 1]);

        if (!parentLi->hasChild("ul"))
        {
            XmlElementPtr parentSpan(parentLi->firstChild("span"));
            parentSpan->setAttribute("class", "toggle collapsed");
            parentLi->appendChild("ul")->setAttribute("class", "collapsed");
        }

        newSection = parentLi->firstChild("ul")->appendChild("li");
    }

    uint64_t newSectionNo(++s_nextSectionNo);
    newSection->setAttribute("data-sectionno", newSectionNo);

    XmlElementPtr toggleButton = newSection->appendChild("span");
    toggleButton->setValue("toggle");

    newSection->appendChild("div");
    s_sections.push_back(newSection);
    return newSectionNo;
}

void KristallCompilerLog::setSectionText(uint64_t sectionNo, UnicodeString text, KristallCompilerLogColor color)
{
    if (sectionNo == 0)
    {
        ExecutionException error("The section text may not be set for the base section (0).");
        throw error;
    }

    if (s_sections.size() < sectionNo)
    {
        ExecutionException error("Attempted to add a section to a parent which doesn't exist.");
        error.addData("sectionNo", toString(sectionNo));
        KristallCompilerLog::done();
        throw error;
    }
    XmlElementPtr sectionLi(s_sections[sectionNo - 1]);

    sectionLi->firstChild("div")->empty();
    XmlElementPtr appendNode(sectionLi->firstChild("div")->appendChild("span"));
    appendNode->setAttribute("class", "color " + toString(color).toLower());
    appendNode->setValue(text);
}

void KristallCompilerLog::appendSectionText(uint64_t sectionNo, UnicodeString text, KristallCompilerLogColor color)
{
    if (sectionNo == 0)
    {
        ExecutionException error("The section text may not be set for the base section (0).");
        throw error;
    }

    if (s_sections.size() < sectionNo)
    {
        ExecutionException error("Attempted to add a section to a parent which doesn't exist.");
        error.addData("sectionNo", toString(sectionNo));
        KristallCompilerLog::done();
        throw error;
    }
    XmlElementPtr sectionLi(s_sections[sectionNo - 1]);

    XmlElementPtr appendNode(sectionLi->firstChild("div")->appendChild("span"));
    appendNode->setAttribute("class", "color " + toString(color).toLower());
    appendNode->setValue(text);
}

void KristallCompilerLog::appendSectionLink(uint64_t sectionNo, UnicodeString url, UnicodeString text)
{
    if (sectionNo == 0)
    {
        ExecutionException error("The section text may not be altered for the base section (0).");
        throw error;
    }

    if (s_sections.size() < sectionNo)
    {
        ExecutionException error("Attempted to add to a parent which doesn't exist.");
        error.addData("sectionNo", toString(sectionNo));
        KristallCompilerLog::done();
        throw error;
    }
    XmlElementPtr sectionLi(s_sections[sectionNo - 1]);

    XmlElementPtr appendNode(sectionLi->firstChild("div")->appendChild("a"));
    appendNode->setAttribute("href", url);
    appendNode->setAttribute("target", "_blank");
    appendNode->setValue(text);
}

void appendMetaRow(XmlElementPtr metaTable, UnicodeString colA, UnicodeString colB, UnicodeString colC)
{
    XmlElementPtr tr(metaTable->appendChild("tr"));
    tr->appendChild("td", colA);

    XmlElementPtr tdB(tr->appendChild("td", colB));
    if (colC.length() == 0)
    {
        tdB->setAttribute("colspan", 2);
    }
    else
    {
        tr->appendChild("td", colC);
    }
}

void KristallCompilerLog::start()
{
    // <head>
    XmlElementPtr headElement = s_log.appendChild("head");
    headElement->appendChild("title", "KristallCompiler.log");
    XmlElementPtr cssElement = headElement->appendChild("link");
    cssElement->setAttribute("rel", "stylesheet");
    cssElement->setAttribute("href", "log.css");
    XmlElementPtr jqueryElement = headElement->appendChild("script");
    jqueryElement->setAttribute("src", "jquery-2.0.3.min.js");
    jqueryElement->setAttribute("type", "text/javascript");
    jqueryElement->setValue("'Javascript'");
    XmlElementPtr scriptElement = headElement->appendChild("script");
    scriptElement->setAttribute("src", "log.js");
    scriptElement->setAttribute("type", "text/javascript");
    scriptElement->setValue("'Javascript'");

    // <body>
    s_bodyNode = s_log.appendChild("body");
    XmlElementPtr headerElement = s_bodyNode->appendChild("div");
    headerElement->setAttribute("class", "header");
    headerElement->setValue("    __ __        _        __          __ __ ______                          _  __           \n"
                            "   / //_/ _____ (_)_____ / /_ ____ _ / // // ____/____   ____ ___   ____   (_)/ /___   _____\n"
                            "  / ,<   / ___// // ___// __// __ `// // // /    / __ \\ / __ `__ \\ / __ \\ / // // _ \\ / ___/\n"
                            " / /| | / /   / /(__  )/ /_ / /_/ // // // /___ / /_/ // / / / / // /_/ // // //  __// /    \n"
                            "/_/ |_|/_/   /_//____/ \\__/ \\__,_//_//_/ \\____/ \\____//_/ /_/ /_// .___//_//_/ \\___//_/     \n"
                            "                                                                /_/                         \n");

    s_mainNode = s_bodyNode->appendChild("ul");
    s_mainNode->setAttribute("class", "log");

    s_metaTable = s_bodyNode->appendChild("table");
    s_metaTable->setAttribute("class", "meta");
    appendMetaRow(s_metaTable, "Began:", UnicodeStrings::getCurrentDateTime(), "");
    appendMetaRow(s_metaTable, "Compiler compiled:", UnicodeStrings::getCompileDateTime(), "");
    appendMetaRow(s_metaTable, "Versions:", "File:", UnicodeStrings::fromInt64(COMPILER_VERSION_MAIN));
    appendMetaRow(s_metaTable, "", "SpriteSheets:", UnicodeStrings::fromInt64(COMPILER_VERSION_FILE_SPRITESHEET));
    appendMetaRow(s_metaTable, "", "GymBadges:", UnicodeStrings::fromInt64(COMPILER_VERSION_FILE_GYMBADGE));
    appendMetaRow(s_metaTable, "", "PokemonMoves:", UnicodeStrings::fromInt64(COMPILER_VERSION_FILE_POKEMONMOVE));
    appendMetaRow(s_metaTable, "", "Items:", UnicodeStrings::fromInt64(COMPILER_VERSION_FILE_ITEM));
    appendMetaRow(s_metaTable, "", "PokemonSpecies:", UnicodeStrings::fromInt64(COMPILER_VERSION_FILE_POKEMONSPECIES));
    appendMetaRow(s_metaTable, "", "Maps:", UnicodeStrings::fromInt64(COMPILER_VERSION_FILE_MAP));
    appendMetaRow(s_metaTable, "", "Regions:", UnicodeStrings::fromInt64(COMPILER_VERSION_FILE_REGION));
    appendMetaRow(s_metaTable, "", "Textures:", UnicodeStrings::fromInt64(COMPILER_VERSION_FILE_TEXTURE));

    s_startDate = Calendar::getNow();
}

void KristallCompilerLog::done()
{
    // Calculate duration
    UDate endDate = Calendar::getNow();
    double durationRaw(endDate - s_startDate);
    int32_t durationHour(std::max(0.0, floor(durationRaw / U_MILLIS_PER_HOUR)));
    durationRaw -= durationHour * U_MILLIS_PER_HOUR;
    int32_t durationMinute(std::max(0.0, floor(durationRaw / U_MILLIS_PER_MINUTE)));
    durationRaw -= durationMinute * U_MILLIS_PER_MINUTE;
    int32_t durationSecond(std::max(0.0, floor(durationRaw / U_MILLIS_PER_SECOND)));
    durationRaw -= durationSecond * U_MILLIS_PER_SECOND;
    int32_t duration(std::max(0.0, ceil(durationRaw)));

    // Output duration
    bool forceField(false);
    UnicodeString durationStr("");
    printf("\nDuration:");
    if (durationHour == 1)
    {
        durationStr += "1 hour ";
        printf("1 hour ");
        forceField = true;
    }
    else if (durationHour > 1)
    {
        durationStr += UnicodeStrings::fromInt64(durationHour) + " hours ";
        printf("%d hours ", durationHour);
        forceField = true;
    }

    if (durationMinute == 1)
    {
        durationStr += "1 minute";
        printf("1 minute");
        forceField = true;
    }
    else if (durationMinute > 1 || forceField)
    {
        durationStr += UnicodeStrings::fromInt64(durationMinute) + " minutes ";
        printf("%d minutes ", durationMinute);
        forceField = true;
    }

    if (durationSecond == 1)
    {
        durationStr += "1 second ";
        printf("1 second ");
        forceField = true;
    }
    else if (durationSecond > 1 || forceField)
    {
        durationStr += UnicodeStrings::fromInt64(durationSecond) + " seconds ";
        printf("%d seconds ", durationSecond);
        forceField = true;
    }

    if (duration == 1)
    {
        durationStr += "1 millisecond";
        printf("1 millisecond");
    }
    else if (duration > 1 || forceField)
    {
        durationStr += UnicodeStrings::fromInt64(duration) + " milliseconds";
        printf("%d milliseconds", duration);
    }

    appendMetaRow(s_metaTable, "Duration:", durationStr, "");
    printf("\n");

    s_log.save(KRISTALLCOMPILER_LOG_FILE);
}

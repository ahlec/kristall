#include "KristallCompiler.hpp"

static std::vector<FunctionPtr<void>> s_functions;

void Finalizer::chain(FunctionPtr<void> func)
{
    s_functions.push_back(func);
}

void Finalizer::call()
{
    for (FunctionPtr<void>& func : s_functions)
    {
        func();
    }
}

#include "KristallCompiler.hpp"
#include "CompilerVersions.h"

//////////////////////////////
// VERSIONING
//////////////////////////////
uint64_t File::getCompilerVersion() const
{
    return COMPILER_VERSION_MAIN;
}

/////////////////////////////
// CLASS METHODS
/////////////////////////////
File::File(UnicodeString handle) : _handle(handle)
{
}

File::~File()
{
}

bool File::isValid()
{
    return true;
}

bool File::isCurrent() const
{
    // Open the file
    FILE* binaryFile = openBinaryFile(false);
    if (binaryFile == nullptr)
    {
        return false;
    }

    fseek(binaryFile, -sizeof(uint64_t), SEEK_END);
    uint64_t footerSize;
    if (fread(&footerSize, sizeof(uint64_t), 1, binaryFile) != 1)
    {
        ExecutionException error("Unable to read the size of the footer from the file's footer.");
        throw error;
    }

    fseek(binaryFile, -footerSize, SEEK_END);
    CompilerBinaryReader reader(binaryFile, footerSize);

    // Check compiler version
    if (reader.readUInt64() != getCompilerVersion())
    {
        reader.close();
        return false;
    }

    // Check file type version
    if (reader.readUInt64() != getFileTypeVersion())
    {
        reader.close();
        return false;
    }

    // Check the source files
    uint32_t nSourceFiles(reader.readUInt32());
    UnicodeString sourceFile;
    for (size_t index = 0; index < nSourceFiles; ++index)
    {
        sourceFile = reader.readString();
        int32_t length(sourceFile.length());
        if (Files::getLastModifiedTimestamp(sourceFile) != reader.readUInt64())
        {
            reader.close();
            return false;
        }
    }

    // Wrap up
    reader.close();
    return true;
}

void File::update(const KristallBin& kristallBin, uint64_t contentSectionNo)
{
    _fileSectionNo = KristallCompilerLog::createNewSection(contentSectionNo);
    KristallCompilerLog::setSectionText(_fileSectionNo, "'" + UnicodeString(getBinaryFileName()) + "'... ");
    printf("\t'%s'... ", getBinaryFileName());

    // Determine if an update is necessary
    if (isCurrent())
    {
        KristallCompilerLog::appendSectionText(_fileSectionNo, "CURRENT", KristallCompilerLogColor::Green);
        printf("CURRENT\n");

        // Open the file
        FILE* populateFile = openBinaryFile(false);
        if (populateFile == nullptr)
        {
            ExecutionException error("Encountered an error when attempting to open a compiled binary file for population.");
            throw error;
        }

        // Determine the lengths of the file
        fseek(populateFile, -sizeof(uint64_t) * 2, SEEK_END);

        // Read the length of the binary file
        uint64_t binarySize = 0;
        if (fread(&binarySize, sizeof(uint64_t), 1, populateFile) != 1)
        {
            fclose(populateFile);
            ExecutionException error("Unable to read the binary file size from the file footer upon population.");
            throw error;
        }

        // Move to the appropriate location and write
        fseek(populateFile, 0, SEEK_SET);

        // Create the BinaryReader and populate
        CompilerBinaryReader reader(populateFile, binarySize);
        populateData(reader);
        reader.close();

        return;
    }

    // Open the file
    FILE* binaryFile = openBinaryFile(true);
    CompilerBinaryWriter writer(binaryFile);
    if (hasSourceXml())
    {
        addSourceFilename("content\\" + getContentDirectoryName() + "\\" + _handle + ".xml");
    }

    // Perform the file update
    try
    {
        performUpdate(writer, kristallBin);
    }
    catch (ExecutionException& ex)
    {
        writer.close();
        remove(getBinaryFileName());

        ex.addData("_handle", _handle);
        ex.addData("fileType", getFileTypePrefix());
        throw;
    }

    // Write the file footer
    uint64_t fileDataSize(writer.getBytesWrittenCount());
    writer.resetBytesWrittenCount();
    writer.writeUInt64(getCompilerVersion());
    writer.writeUInt64(getFileTypeVersion());
    writer.writeUInt32(_sourceFilenames.size());
    for (const UnicodeString& sourceFile : _sourceFilenames)
    {
        writer.writeString(sourceFile);
        writer.writeUInt64(Files::getLastModifiedTimestamp(sourceFile));
    }
    writer.writeUInt64(fileDataSize);
    uint64_t fileFooterSize(writer.getBytesWrittenCount() + sizeof(uint64_t));
    writer.writeUInt64(fileFooterSize);

    // Close the file
    KristallCompilerLog::appendSectionText(_fileSectionNo, "UPDATED", KristallCompilerLogColor::Orange);
    printf("UPDATED\n");
    writer.close();
}

uint64_t File::getBinarySize() const
{
    // Open the file
    FILE* binaryFile = openBinaryFile(false);
    if (binaryFile == nullptr)
    {
        ExecutionException error("Attempted to get the binary size of a file which hasn't been compiled yet.");
        throw error;
    }

    // Seek to the appropriate location
    fseek(binaryFile, -2 * sizeof(uint64_t), SEEK_END);

    // Read the binary size from the file footer
    uint64_t binarySize = 0;
    if (fread(&binarySize, sizeof(uint64_t), 1, binaryFile) != 1)
    {
        fclose(binaryFile);
        ExecutionException error("Unable to read the binary size from the file footer.");
        throw error;
    }

    // Wrap up
    fclose(binaryFile);
    return binarySize;
}

void File::write(CompilerBinaryWriter& writer) const
{
    // Open the file
    FILE* binaryFile = openBinaryFile(false);
    if (binaryFile == nullptr)
    {
        ExecutionException error("Encountered an error when attempting to open a compiled binary file.");
        throw error;
    }

    // Determine the lengths of the file
    fseek(binaryFile, -sizeof(uint64_t) * 2, SEEK_END);

    // Read the length of the binary file
    uint64_t binarySize = 0;
    if (fread(&binarySize, sizeof(uint64_t), 1, binaryFile) != 1)
    {
        fclose(binaryFile);
        ExecutionException error("Unable to read the binary file size from the file footer upon writing.");
        throw error;
    }

    // Move to the appropriate location and write
    fseek(binaryFile, 0, SEEK_SET);
    const uint64_t BUFFER_SIZE = 8192;
    char buffer[BUFFER_SIZE];
    uint64_t bytesRead = 0;
    uint64_t bytesRemaining = binarySize;
    size_t bufferRead = fread(buffer, 1, std::min(BUFFER_SIZE, bytesRemaining), binaryFile);
    while (bytesRead < binarySize)
    {
        if (bufferRead > 0)
        {
            writer.writeRawBytes(buffer, 1, bufferRead);
            bytesRead += bufferRead;
            bytesRemaining -= bufferRead;
        }

        if (bytesRead == binarySize)
        {
            break;
        }
        else if (bytesRead > binarySize)
        {
            ExecutionException error("Read too many bytes!");
            error.addData("bytesRead", toString(bytesRead));
            error.addData("binarySize", toString(binarySize));
            error.addData("uid", toString(getUId()));
            error.addData("filetypePrefix", getFileTypePrefix());
            throw error;
        }

        bufferRead = fread(buffer, 1, std::min(BUFFER_SIZE, bytesRemaining), binaryFile);
    }

    // Wrap up
    fclose(binaryFile);
}

UnicodeString File::getCompiledFileName() const
{
    return UnicodeString(getBinaryFileName());
}

void File::addSourceFilename(UnicodeString filename)
{
    if (std::find(_sourceFilenames.begin(), _sourceFilenames.end(), filename) == _sourceFilenames.end())
    {
        _sourceFilenames.push_back(filename);
    }
}

void File::populateData(CompilerBinaryReader& reader)
{

}

XmlDocument File::openSourceXml() const
{
    return XmlDocument::load("content\\" + getContentDirectoryName() + "\\" + _handle + ".xml");
}

UnicodeString File::getHandle() const
{
    return _handle;
}

bool File::hasSourceXml() const
{
    return true;
}

inline bool writeNextAdditionalReportInfo(std::vector<ReportInfo>::iterator& current,
                                          std::vector<ReportInfo>::iterator endPos,
                                          UFILE* file)
{
    if (current == endPos)
    {
        return false;
    }

    UnicodeString str("<td class=\"label\">" + current->label +
                      "</td><td>" + current->value + "</td>");
    ++current;
    u_file_write(str.getTerminatedBuffer(), str.length(), file);
    return true;
}

inline bool writeNextReportAction(std::vector<ReportActionPtr>::iterator& current,
                                  std::vector<ReportActionPtr>::iterator endPos,
                                  UFILE* file)
{
    UnicodeString str;
    if (current == endPos)
    {
        str = "<td colspan=\"2\"></td></td>";
        u_file_write(str.getTerminatedBuffer(), str.length(), file);
        return false;
    }

    str = "<td class=\"label\">" + current->get()->outputLabel() +
                      "</td><td>" + current->get()->outputDom() + "</td>";
    ++current;
    u_file_write(str.getTerminatedBuffer(), str.length(), file);
    return true;
}

void File::writeReport(Report& report) const
{
    // Open the file
    std::string fileNameStd(UnicodeStrings::toStdString("reports\\" + report.getFilename()));
    UFILE* file = u_fopen(fileNameStd.c_str(), "w", NULL, "UTF-8");
    if (file == nullptr)
    {
        ExecutionException error("Could not open the report file to load.");
        error.addData("fileName", toString(fileNameStd.c_str()));
        throw error;
    }
    u_fputs(L"<!DOCTYPE html>", file);
    u_fputs(L"<html>", file);

    std::vector<ReportActionPtr> actions(report.getActions());

    // Header
    u_fputs(L"    <head>", file);
    UnicodeString title("        <title>" + report.getTitle() + "</title>\n");
    u_file_write(title.getTerminatedBuffer(), title.length(), file);
    u_fputs(L"        <script type=\"text/javascript\" src=\"../jquery-2.0.3.min.js\"></script>\n", file);
    u_fputs(L"        <style>", file);
    u_fputs(L"            html {", file);
    u_fputs(L"                font-family:\"Lucida Sans Unicode\", \"Lucida Grande\", sans-serif;", file);
    u_fputs(L"            }", file);
    u_fputs(L"            body {", file);
    u_fputs(L"                min-width:900px;", file);
    u_fputs(L"                min-height:700px;", file);
    u_fputs(L"                margin:0px;", file);
    u_fputs(L"            }", file);
    u_fputs(L"            body > div {", file);
    u_fputs(L"                position:fixed;", file);
    u_fputs(L"                top:0px;", file);
    u_fputs(L"                left:0px;", file);
    u_fputs(L"                right:0px;", file);
    u_fputs(L"                overflow:auto;", file);
    u_fputs(L"            }", file);
    u_fputs(L"            body > table {", file);
    u_fputs(L"                position:fixed;", file);
    u_fputs(L"                bottom:0px;", file);
    u_fputs(L"                left:0px;", file);
    u_fputs(L"                right:0px;", file);
    u_fputs(L"                border-top:1px solid black;", file);
    u_fputs(L"                background-color:rgba(0, 0, 0, 0.6);", file);
    u_fputs(L"                border-collapse:collapse;", file);
    u_fputs(L"                color:white;", file);
    u_fputs(L"                display:block;", file);
    u_fputs(L"                font-size:13px;", file);
    u_fputs(L"            }", file);
    u_fputs(L"            body > table tr > td.label {", file);
    u_fputs(L"                font-weight:600;", file);
    u_fputs(L"                width:16.667%;", file);
    u_fputs(L"                min-width:150px;", file);
    u_fputs(L"                text-align:right;", file);
    u_fputs(L"                vertical-align:top;", file);
    u_fputs(L"                padding-right:6px;", file);
    u_fputs(L"            }", file);
    u_fputs(L"            body > table note {", file);
    u_fputs(L"                font-style:italic;", file);
    u_fputs(L"                font-size:10px;", file);
    u_fputs(L"            }", file);
    u_fputs(L"        </style>", file);
    u_fputs(L"        <script type=\"text/javascript\">", file);
    u_fputs(L"            $(document).ready(function () {", file);
    u_fputs(L"                $(window).resize(function () {", file);
    u_fputs(L"                    $(\"body > div\").css(\"bottom\", $(\"body > table\").outerHeight() + \"px\");", file);
    u_fputs(L"                }).resize();", file);
    u_fputs(L"            });", file);
    u_fputs(L"        </script>", file);
    UnicodeString header(report.outputHeader());
    u_file_write(header.getTerminatedBuffer(), header.length(), file);
    for (const ReportActionPtr& action : actions)
    {
        header = action->outputHead();
        if (!header.isEmpty())
        {
            u_file_write(header.getTerminatedBuffer(), header.length(), file);
        }
    }
    u_fputs(L"    </head>", file);

    // Body
    u_fputs(L"    <body>", file);
    u_fputs(L"        <div>", file);
    UnicodeString body(report.outputBody());
    u_file_write(body.getTerminatedBuffer(), body.length(), file);
    u_fputs(L"        </div>", file);

    // Info bar
    u_fputs(L"        <table>", file);
    std::vector<ReportInfo> customReportInfos(report.getAdditionalInfo());
    auto currentReportInfo(customReportInfos.begin());
    auto currentAction(actions.begin());

    u_fputs(L"            <tr><td class=\"label\">Generated:</td><td>", file);
    UnicodeString dateTime(UnicodeStrings::getCurrentDateTime());
    u_file_write(dateTime.getTerminatedBuffer(), dateTime.length(), file);
    u_fputs(L"</td>", file);
    writeNextAdditionalReportInfo(currentReportInfo, customReportInfos.end(), file);
    writeNextReportAction(currentAction, actions.end(), file);
    u_fputs(L"</tr>\n", file);
    u_fprintf_u(file, L"            <tr><td class=\"label\" rowspan=\"2\">Versions:</td><td>%d <note>(Compiler)</note></td>", getCompilerVersion());
    writeNextAdditionalReportInfo(currentReportInfo, customReportInfos.end(), file);
    writeNextReportAction(currentAction, actions.end(), file);
    u_fputs(L"</tr>", file);
    u_fprintf_u(file, L"            <tr><td>%d <note>(Filetype)</note></td>", getFileTypeVersion());
    writeNextAdditionalReportInfo(currentReportInfo, customReportInfos.end(), file);
    writeNextReportAction(currentAction, actions.end(), file);
    u_fputs(L"</tr>", file);

    while (currentReportInfo != customReportInfos.end() &&
           currentAction != actions.end())
    {
        u_fputs(L"            <tr><td colspan=\"2\"></td>", file);
        writeNextAdditionalReportInfo(currentReportInfo, customReportInfos.end(), file);
        writeNextReportAction(currentAction, actions.end(), file);
        u_fputs(L"</tr>", file);
    }

    u_fputs(L"        </table>", file);
    u_fputs(L"    </body>", file);

    // End
    u_fputs(L"</html>", file);
    u_fclose(file);

    // Write a link to the output log
    uint64_t reportSectionNo = KristallCompilerLog::createNewSection(_fileSectionNo);
    KristallCompilerLog::setSectionText(reportSectionNo, "Report written:");
    KristallCompilerLog::appendSectionLink(reportSectionNo, "./reports/" + report.getFilename(), report.getTitle());
}

UnicodeString File::getClipboardFileName() const
{
    UnicodeString normalFilename(getBinaryFileName());
    normalFilename.insert(4, "clipboards\\");
    return normalFilename;
}

char* File::getBinaryFileName() const
{
    std::string fileTypePrefix(UnicodeStrings::toStdString(getFileTypePrefix()));
    int binaryFileNameLength = 4 + fileTypePrefix.length() + 1 + 8 + 4;
    char* binaryFileName = new char[binaryFileNameLength + 1];
    int createdFileNameLength = sprintf(binaryFileName, "dat\\%s_%08d.bin", fileTypePrefix.c_str(),
                                        getUId());
    if (createdFileNameLength != binaryFileNameLength)
    {
        ExecutionException error("Mismatch in binary file name lengths.");
        error.addData("binaryFileNameLength", toString(binaryFileNameLength));
        error.addData("createdFileNameLength", toString(createdFileNameLength));
        error.addData("uid", toString(getUId()));
        error.addData("fileTypePrefix", toString(fileTypePrefix.c_str()));
        throw error;
    }

    return binaryFileName;
}

FILE* File::openBinaryFile(bool isWrite) const
{
    // Determine the file name
    char* binaryFileName = getBinaryFileName();

    // Open the file
    FILE* binaryFile = fopen(binaryFileName, (isWrite ? "wb" : "rb"));
    if (isWrite && binaryFile == nullptr)
    {
        ExecutionException error("Unable to open the binary file.");
        error.addData("binaryFileName", toString(binaryFileName));
        error.addData("isWrite", toString(isWrite));
        delete[] binaryFileName;
        throw error;
    }

    delete[] binaryFileName;
    return binaryFile;
}

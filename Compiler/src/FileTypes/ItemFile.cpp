#include "KristallCompiler.hpp"
#include "CompilerVersions.h"
#include "General/IndefiniteArticle.h"
#include "Items/BerryUseTrigger.h"

//////////////////////////////
// VERSIONING
//////////////////////////////
uint64_t ItemFile::getFileTypeVersion() const
{
    return COMPILER_VERSION_FILE_ITEM;
}

/////////////////////////////
// CLASS METHODS
/////////////////////////////
ItemFile::ItemFile(UnicodeString handle) : File(handle), _isHm(false)
{

}

uint32_t ItemFile::getUId() const
{
    return Handles::getHandleNo("Items", getHandle());
}

bool ItemFile::canBeHeld() const
{
    if (_itemType == ItemType::KeyItem)
    {
        return false;
    }

    if (_itemType == ItemType::MoveMachine && _isHm)
    {
        return false;
    }

    return true;
}

ItemType ItemFile::getItemType() const
{
    return _itemType;
}

bool ItemFile::getIsHm() const
{
    return _isHm;
}

UnicodeString ItemFile::getContentDirectoryName() const
{
    return "items";
}

UnicodeString ItemFile::getFileTypePrefix() const
{
    return "itm";
}

void ItemFile::performUpdate(CompilerBinaryWriter& writer,
                             const KristallBin& kristallBin)
{
    XmlDocument xml(openSourceXml());

    writer.writeString(xml.firstChild("Name")->getValue().getString());
    writer.writeUInt8(Enums::parse<IndefiniteArticle>(xml.firstChild("Name")->getAttribute("IndefiniteArticle")->getString()));
    writer.writeString(xml.firstChild("PluralName")->getValue().getString());
    writer.writeString(xml.firstChild("Description")->getValue().getString());

    // Item types
    ItemType itemType(static_cast<ItemType>(Enums::parse<ItemType>(xml.firstChild("Type")->getValue().getString())));
    _itemType = itemType;
    writer.writeUInt8(static_cast<uint8_t>(itemType));
    if (itemType == ItemType::PokeBall)
    {
        writer.writeUInt16(xml.firstChild("Header")->firstChild("PokeBallId")->getValue().getUInt16());
        writer.writeLua(xml.firstChild("Header")->firstChild("EffectivenessCode")->getValue().getString());
    }
    else if (itemType == ItemType::MoveMachine)
    {
        _isHm = xml.firstChild("Header")->hasChild("IsHM");
        writer.writeBool(_isHm);
        writer.writeUInt8(xml.firstChild("Header")->firstChild("Number")->getValue().getUInt8());
        writer.writeUInt32(Handles::getHandleNo("Moves",
                                                xml.firstChild("Header")->firstChild("Move")->getValue().getString()));
    }
    else if (itemType == ItemType::Berry)
    {
        writer.writeUInt8(xml.firstChild("Header")->firstChild("Spicy")->getValue().getUInt8());
        writer.writeUInt8(xml.firstChild("Header")->firstChild("Dry")->getValue().getUInt8());
        writer.writeUInt8(xml.firstChild("Header")->firstChild("Sour")->getValue().getUInt8());
        writer.writeUInt8(xml.firstChild("Header")->firstChild("Bitter")->getValue().getUInt8());
        writer.writeUInt8(xml.firstChild("Header")->firstChild("Sweet")->getValue().getUInt8());
        writer.writeUInt8(Enums::parse<BerryUseTrigger>(xml.firstChild("Header")->firstChild("UseTrigger")->getValue().getString()));
        writer.writeUInt8(xml.firstChild("Header")->firstChild("UseData")->getValue().getUInt8());
        writer.writeLua(xml.firstChild("Header")->firstChild("BattleHeldActivateCode")->getValue().getString());
    }
    else if (itemType == ItemType::KeyItem)
    {
        writer.writeBool(xml.firstChild("Header")->firstChild("CanBeRegistered")->getValue().getBool());
    }

    // Common properties again
    bool isBuyable = xml.hasChild("BuyPrice");
    writer.writeBool(isBuyable);
    if (isBuyable)
    {
        writer.writeUInt32(xml.firstChild("BuyPrice")->getValue().getUInt32());
    }
    else
    {
        writer.writeUInt32(0);
    }

    bool isSellable = xml.hasChild("SellPrice");
    writer.writeBool(isSellable);
    if (isSellable)
    {
        writer.writeUInt32(xml.firstChild("SellPrice")->getValue().getUInt32());
    }
    else
    {
        writer.writeUInt32(0);
    }

    bool isUsableInBattle = xml.hasChild("InBattleUse");
    writer.writeBool(isUsableInBattle);
    if (isUsableInBattle)
    {
        writer.writeBool(xml.firstChild("InBattleUse")->getAttribute("TargetsPokemon")->getBool());
        writer.writeLua(xml.firstChild("InBattleUse")->firstChild("UsabilityCode")->getValue().getString());
        writer.writeLua(xml.firstChild("InBattleUse")->firstChild("UseCode")->getValue().getString());
    }
    else
    {
        writer.writeBool(false);
        writer.writeLua("");
        writer.writeLua("");
    }

    bool isUsableOutsideBattle = xml.hasChild("OutsideBattleUse");
    writer.writeBool(isUsableOutsideBattle);
    if (isUsableOutsideBattle)
    {
        writer.writeBool(xml.firstChild("OutsideBattleUse")->getAttribute("TargetsPokemon")->getBool());
        writer.writeLua(xml.firstChild("OutsideBattleUse")->firstChild("UsabilityCode")->getValue().getString());
        writer.writeLua(xml.firstChild("OutsideBattleUse")->firstChild("UseCode")->getValue().getString());
    }
    else
    {
        writer.writeBool(false);
        writer.writeLua("");
        writer.writeLua("");
    }
}

void ItemFile::populateData(CompilerBinaryReader& reader)
{
    // The only information we care about here is the item type and whether it is an HM or not

    reader.readString(); // Name
    reader.readUInt8(); // Indefinite article
    reader.readString(); // PluralName
    reader.readString(); // Description
    _itemType = static_cast<ItemType>(reader.readUInt8());
    if (_itemType == ItemType::MoveMachine)
    {
        _isHm = reader.readBool();
    }
    else
    {
        _isHm = false;
    }
}

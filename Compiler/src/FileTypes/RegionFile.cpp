#include "KristallCompiler.hpp"
#include "CompilerVersions.h"
#include "World/TileNatureType.h"

//////////////////////////////
// VERSIONING
//////////////////////////////
uint64_t RegionFile::getFileTypeVersion() const
{
    return COMPILER_VERSION_FILE_REGION;
}

/////////////////////////////
// CLASS METHODS
/////////////////////////////
RegionFile::RegionFile(UnicodeString handle) : File(handle),
                                            _outdoorMaps(RegionMaps::getOutdoorMaps(getUId()))
{
}

uint32_t RegionFile::getUId() const
{
    return Handles::getHandleNo("Regions", getHandle());
}

int32_t RegionFile::getFlightLocationIndex(uint32_t mapNo) const
{
    auto vectorIterator = std::find(_flightLocations.begin(),
                                    _flightLocations.end(),
                                    mapNo);
    if (vectorIterator == _flightLocations.end())
    {
        return -1;
    }

    return (vectorIterator - _flightLocations.begin());
}

UnicodeString RegionFile::getContentDirectoryName() const
{
    return "regions";
}

UnicodeString RegionFile::getFileTypePrefix() const
{
    return "rgn";
}

void RegionFile::performUpdate(CompilerBinaryWriter& writer,
                                 const KristallBin& kristallBin)
{
    XmlDocument xml(openSourceXml());
    addSourceFilename(kristallBin.askFor<SpriteSheetFile>(static_cast<uint32_t>(SpriteSheet::OverworldExterior))->getCompiledFileName());
    for (RegionMap*& outdoorMap : _outdoorMaps)
    {
        addSourceFilename(kristallBin.askFor<MapFile>(outdoorMap->getMapNo())->getCompiledFileName());
    }


    UnicodeString regionName(xml.firstChild("Name")->getValue().getString());
    writer.writeString(regionName);

    XmlElementPtr maleStartNode(xml.firstChild("MaleStartLocation"));
    assertIsValidOccupationTile(maleStartNode->firstChild("Map")->getValue().getString(),
                                maleStartNode->firstChild("X")->getValue().getUInt16(),
                                maleStartNode->firstChild("Y")->getValue().getUInt16(),
                                false);
    writer.writeUInt32(Handles::getHandleNo("Maps", maleStartNode->firstChild("Map")->getValue().getString()));
    addSourceFilename(kristallBin.askFor<MapFile>(Handles::getHandleNo("Maps",
                                                                       maleStartNode->firstChild("Map")->getValue().getString()))->getCompiledFileName());

    writer.writeUInt16(maleStartNode->firstChild("X")->getValue().getUInt16());
    writer.writeUInt16(maleStartNode->firstChild("Y")->getValue().getUInt16());

    XmlElementPtr femaleStartNode(xml.firstChild("FemaleStartLocation"));
    assertIsValidOccupationTile(femaleStartNode->firstChild("Map")->getValue().getString(),
                                femaleStartNode->firstChild("X")->getValue().getUInt16(),
                                femaleStartNode->firstChild("Y")->getValue().getUInt16(),
                                false);
    writer.writeUInt32(Handles::getHandleNo("Maps", femaleStartNode->firstChild("Map")->getValue().getString()));
    addSourceFilename(kristallBin.askFor<MapFile>(Handles::getHandleNo("Maps",
                                                                   femaleStartNode->firstChild("Map")->getValue().getString()))->getCompiledFileName());

    writer.writeUInt16(femaleStartNode->firstChild("X")->getValue().getUInt16());
    writer.writeUInt16(femaleStartNode->firstChild("Y")->getValue().getUInt16());

    // Flight Locations
    uint64_t nFlightLocationsRaw(xml.firstChild("FlightLocations")->countChildren());
    if (nFlightLocationsRaw > UINT16_MAX)
    {
        ExecutionException error("The number of flight locations must be within the bounds of a UINT16.");
        throw error;
    }
    writer.writeUInt16(static_cast<uint16_t>(nFlightLocationsRaw));
    XmlElementPtr currentFlightNode = xml.firstChild("FlightLocations")->firstChild();
    uint32_t flightLocationMapNo;
    while (currentFlightNode != nullptr)
    {
        assertIsValidOccupationTile(currentFlightNode->getName().getString(),
                                    currentFlightNode->firstChild("X")->getValue().getUInt16(),
                                    currentFlightNode->firstChild("Y")->getValue().getUInt16(),
                                    true);
        flightLocationMapNo = Handles::getHandleNo("Maps", currentFlightNode->getName().getString());
        if (std::find(_flightLocations.begin(), _flightLocations.end(), flightLocationMapNo) !=
            _flightLocations.end())
        {
            ExecutionException error("A map may only have one flight location defined for it.");
            error.addData("map", currentFlightNode->getName().getString());
            throw error;
        }
        _flightLocations.push_back(flightLocationMapNo);
        writer.writeUInt32(flightLocationMapNo);
        writer.writeUInt16(currentFlightNode->firstChild("X")->getValue().getUInt16());
        writer.writeUInt16(currentFlightNode->firstChild("Y")->getValue().getUInt16());
        currentFlightNode = currentFlightNode->nextSibling();
    }

    // Begin: Map Borders
    bool isRowOriginalParityEven(!xml.hasChild("InitialRowParityIsOdd"));
    bool isColumnOriginalParityEven(!xml.hasChild("InitialColumnParityIsOdd"));
    MapBorderGenerator generator(isRowOriginalParityEven, isColumnOriginalParityEven);

    // Get all of the outdoor maps for this region
    /*_outdoorMaps.clear();
    uint32_t currentRequestantMapNo(0);
    MapFile* mapFile(kristallBin.askFor<MapFile>(currentRequestantMapNo));
    ++currentRequestantMapNo;
    while (mapFile != nullptr)
    {
        if (mapFile->getRegionNo() == getUId() && mapFile->getIsOutdoor())
        {

        }
        mapFile = kristallBin.askFor<MapFile>(currentRequestantMapNo);
        ++currentRequestantMapNo;
    }*/

    // Calculate outdoor rectangles
    std::queue<uint32_t> rectangleCalculateQueue;
    _outdoorMaps[0]->isRectangleCalculated = true;
    _outdoorMaps[0]->rectangle = Rect(0, 0, _outdoorMaps[0]->getWidth() * TILE_SIZE,
                                             _outdoorMaps[0]->getHeight() * TILE_SIZE);
    rectangleCalculateQueue.push(_outdoorMaps[0]->getMapNo());
    size_t currentMapIndex;
    size_t destinationMapIndex;
    while (!rectangleCalculateQueue.empty())
    {
        currentMapIndex = this->getOutdoorMapIndex(rectangleCalculateQueue.front());
        rectangleCalculateQueue.pop();
        for (RegionMapBridge& mapBridge : _outdoorMaps[currentMapIndex]->getMapBridges())
        {
            destinationMapIndex = this->getOutdoorMapIndex(mapBridge.destinationMapNo);
            if (_outdoorMaps[destinationMapIndex]->isRectangleCalculated)
            {
                continue;
            }

            _outdoorMaps[destinationMapIndex]->isRectangleCalculated = true;
            _outdoorMaps[destinationMapIndex]->rectangle.width = _outdoorMaps[destinationMapIndex]->getWidth() *
                                                                 TILE_SIZE;
            _outdoorMaps[destinationMapIndex]->rectangle.height = _outdoorMaps[destinationMapIndex]->getHeight() *
                                                                  TILE_SIZE;

            switch (mapBridge.inwardArrow)
            {
                case Direction::North:
                    {
                        _outdoorMaps[destinationMapIndex]->rectangle.x = _outdoorMaps[currentMapIndex]->rectangle.x -
                                                                            (mapBridge.destinationX - mapBridge.tileX) * TILE_SIZE;
                        _outdoorMaps[destinationMapIndex]->rectangle.y = _outdoorMaps[currentMapIndex]->rectangle.y -
                                                                           _outdoorMaps[destinationMapIndex]->rectangle.height;
                        break;
                    }
                case Direction::East:
                    {
                        _outdoorMaps[destinationMapIndex]->rectangle.x = _outdoorMaps[currentMapIndex]->rectangle.x +
                                                                            _outdoorMaps[currentMapIndex]->rectangle.width;
                        _outdoorMaps[destinationMapIndex]->rectangle.y = _outdoorMaps[currentMapIndex]->rectangle.y -
                                                                           (mapBridge.destinationY - mapBridge.tileY) * TILE_SIZE;
                        break;
                    }
                case Direction::South:
                    {
                        _outdoorMaps[destinationMapIndex]->rectangle.x = _outdoorMaps[currentMapIndex]->rectangle.x -
                                                                            (mapBridge.destinationX - mapBridge.tileX) * TILE_SIZE;
                        _outdoorMaps[destinationMapIndex]->rectangle.y = _outdoorMaps[currentMapIndex]->rectangle.y +
                                                                           _outdoorMaps[currentMapIndex]->rectangle.height;
                        break;
                    }
                case Direction::West:
                    {
                        _outdoorMaps[destinationMapIndex]->rectangle.x = _outdoorMaps[currentMapIndex]->rectangle.x -
                                                                            _outdoorMaps[destinationMapIndex]->rectangle.width;
                        _outdoorMaps[destinationMapIndex]->rectangle.y = _outdoorMaps[currentMapIndex]->rectangle.y -
                                                                           (mapBridge.destinationY - mapBridge.tileY) * TILE_SIZE;
                        break;
                    }
            }

            rectangleCalculateQueue.push(mapBridge.destinationMapNo);
        }
    }
    for (RegionMap*& outdoorMap : _outdoorMaps)
    {
        if (!outdoorMap->isRectangleCalculated)
        {
            ExecutionException error("One or more outdoor maps did not calculate an overworld rectangle.");
            error.addData("mapNo", toString(outdoorMap->getMapNo()));
            throw error;
        }
    }

    // Determine the map border rectangles
    generator.process(_outdoorMaps);

    // Adjust the map rectangles so that the origin is at (0, 0)
    generator.fixOrigin(_outdoorMaps);

    // Get the inaccessible areas
    std::vector<RegionInaccessibleArea> inaccessibleAreas(generator.getInaccessibleRegions(regionName,
                                                                    _outdoorMaps,
                                                                    kristallBin.askFor<SpriteSheetFile>(static_cast<uint32_t>(SpriteSheet::OverworldExterior))));

    // Draw the map borders for the outdoor maps now
    generator.drawMapBorders(_outdoorMaps,
                             kristallBin.askFor<SpriteSheetFile>(static_cast<uint32_t>(SpriteSheet::OverworldExterior)));

    // Write the zones now
    uint64_t nZones(inaccessibleAreas.size() + _outdoorMaps.size());
    if (nZones > UINT32_MAX)
    {
        ExecutionException error("The total number of zones a Region may have must fit within the bounds of a UINT32.");
        error.addData("nZones", toString(nZones));
        error.addData("nInaccessibleAreas", toString(inaccessibleAreas.size()));
        error.addData("nOutdoorMaps", toString(_outdoorMaps.size()));
        throw error;
    }
    writer.writeUInt32(static_cast<uint32_t>(nZones));
    for (RegionInaccessibleArea& inaccessibleArea : inaccessibleAreas)
    {
        writer.writeInt64(-1);
        writer.writeBool(true); // isOutdoor
        writer.writeInt32(inaccessibleArea.getX());
        writer.writeInt32(inaccessibleArea.getY());
        writer.writeInt32(inaccessibleArea.getWidth());
        writer.writeInt32(inaccessibleArea.getHeight());
        writer.writeInt32(inaccessibleArea.getX());
        writer.writeInt32(inaccessibleArea.getY());
        writer.writeInt32(inaccessibleArea.getWidth());
        writer.writeInt32(inaccessibleArea.getHeight());

        OverworldRenderableReport inaccessibleORR(inaccessibleArea.writeOverworldRenderable(writer, this));
        writeReport(inaccessibleORR);
    }
    for (RegionMap*& regionMap : _outdoorMaps)
    {
        writer.writeInt64(static_cast<int64_t>(regionMap->getMapNo()));
        writer.writeBool(true); // isOutdoor
        writer.writeRect(regionMap->borderRectangle);
        writer.writeRect(regionMap->rectangle);

        regionMap->blendInMapOverworldRenderable(kristallBin.askFor<MapFile>(regionMap->getMapNo())->getOverworldRenderable());

        OverworldRenderableReport outdoorORR(regionMap->writeOverworldRenderable(writer, this));
        writeReport(outdoorORR);
    }

    // Write out a list of all maps for this Region, in sorted order, to facilitate
    // binary search lookup
    MapFile* currentMapFile = kristallBin.askFor<MapFile>(0);
    uint32_t currentMapNo(0);
    uint32_t cachedRegionNo(getUId());
    std::vector<MapFile*> regionMapFiles;
    while (currentMapFile != nullptr)
    {
        if (currentMapFile->getRegionNo() == cachedRegionNo)
        {
            regionMapFiles.push_back(currentMapFile);
        }

        ++currentMapNo;
        currentMapFile = kristallBin.askFor<MapFile>(currentMapNo);
    }

    uint32_t nMaps(regionMapFiles.size());
    writer.writeUInt32(nMaps);
    bool hasFoundAbsoluteCoordinates;
    for (MapFile*& mapFile : regionMapFiles)
    {
        writer.writeUInt32(mapFile->getUId());
        writer.writeBool(mapFile->getIsOutdoor());

        // Absolute X/Y
        if (mapFile->getIsOutdoor())
        {
            hasFoundAbsoluteCoordinates = false;
            for (RegionMap*& regionMap : _outdoorMaps)
            {
                if (regionMap->getMapNo() == mapFile->getUId())
                {
                    hasFoundAbsoluteCoordinates = true;
                    writer.writeInt32(regionMap->rectangle.x);
                    writer.writeInt32(regionMap->rectangle.y);
                    break;
                }
            }

            if (!hasFoundAbsoluteCoordinates)
            {
                ExecutionException error("An outdoor map could not locate its own absolute coordinates.");
                error.addData("mapNo", toString(mapFile->getUId()));
                throw error;
            }
        }
        else
        {
            writer.writeInt32(0);
            writer.writeInt32(0);
        }
    }

    // Issue the layout report
    RegionLayoutReport layoutReport(getUId(), regionName, _outdoorMaps, inaccessibleAreas);
    writeReport(layoutReport);
}

void RegionFile::populateData(CompilerBinaryReader& reader)
{
    reader.readString(); // name
    reader.readUInt32(); // male start location
    reader.readUInt16();
    reader.readUInt16();
    reader.readUInt32(); // female start location
    reader.readUInt16();
    reader.readUInt16();

    // Flight Locations
    uint16_t nFlightLocations(reader.readUInt16());
    for (size_t index = 0; index < nFlightLocations; ++index)
    {
        _flightLocations.push_back(reader.readUInt32());
        reader.readUInt16();
        reader.readUInt16();
    }
}

void RegionFile::assertIsValidOccupationTile(UnicodeString mapHandle,
                                             uint16_t tileX, uint16_t tileY,
                                             bool requireOutdoor) const
{
    XmlDocument xml(XmlDocument::load("content\\maps\\" + mapHandle + ".xml"));

    if (xml.firstChild("Region")->getValue().getString() != getHandle())
    {
        ExecutionException error("The provided map is not a member of the current region.");
        error.addData("map's region", xml.firstChild("Region")->getValue().getString());
        throw error;
    }

    if (xml.firstChild("Width")->getValue().getUInt16() <= tileX)
    {
        ExecutionException error("The provided x coordinate is outside the width of the map.");
        error.addData("x", toString(tileX));
        error.addData("width", toString(xml.firstChild("Width")->getValue().getUInt16()));
        error.addData("map", mapHandle);
        throw error;
    }

    if (xml.firstChild("Height")->getValue().getUInt16() <= tileY)
    {
        ExecutionException error("The provided y coordinate is outside the height of the map.");
        error.addData("y", toString(tileY));
        error.addData("height", toString(xml.firstChild("Height")->getValue().getUInt16()));
        error.addData("map", mapHandle);
        throw error;
    }

    if (requireOutdoor && xml.hasChild("IsInterior"))
    {
        ExecutionException error("The provided map is not an outdoor map.");
        error.addData("map", mapHandle);
        throw error;
    }

    XmlElementPtr tile = xml.firstChild("Tiles")->firstChild(FunctionPtr<bool, const XmlElement&>(new TileLocator(tileX, tileY)));
    if (tile == nullptr)
    {
        ExecutionException error("Could not locate the specified tile on the map.");
        error.addData("x", toString(tileX));
        error.addData("y", toString(tileY));
        error.addData("map", mapHandle);
        throw error;
    }

    TileNatureType natureType(Enums::parseKeep<TileNatureType>(tile->firstChild("Nature")->getValue().getString()));
    if (natureType == TileNatureType::Solid || natureType == TileNatureType::Ledge)
    {
        ExecutionException error("The specified tile has an unoccupiable nature.");
        error.addData("tileNatureType", toString(static_cast<uint8_t>(natureType)));
        error.addData("x", toString(tileX));
        error.addData("y", toString(tileY));
        error.addData("map", mapHandle);
        throw error;
    }
}

size_t RegionFile::getOutdoorMapIndex(uint32_t mapNo) const
{
    size_t index(0);
    for (RegionMap* const& regionMap : _outdoorMaps)
    {
        if (regionMap->getMapNo() == mapNo)
        {
            return index;
        }

        ++index;
    }

    ExecutionException error("Could not find the requested map in the list of this region's outdoor maps.");
    error.addData("mapNo", toString(mapNo));
    throw error;
}

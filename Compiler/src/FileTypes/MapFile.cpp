#include "KristallCompiler.hpp"
#include "CompilerVersions.h"
#include "World/MapLabel.h"
#include "World/Effects/OverworldEffectType.h"
#include "World/TileNatureType.h"
#include "General/TimeOfDayFlags.h"
#include "World/EncounterMethod.h"
#include "World/TileType.h"
#include "World/Terrain.h"
#include "Items/ItemOverworldObjectForm.h"
#include "World/StaircaseType.h"

//////////////////////////////
// VERSIONING
//////////////////////////////
uint64_t MapFile::getFileTypeVersion() const
{
    return COMPILER_VERSION_FILE_MAP;
}

/////////////////////////////
// CLASS METHODS
/////////////////////////////
MapFile::MapFile(UnicodeString handle) : File(handle), _regionNo(0),
                                         _isOutdoor(false)
{

}

uint32_t MapFile::getUId() const
{
    return Handles::getHandleNo("Maps", getHandle());
}

uint32_t MapFile::getRegionNo() const
{
    return _regionNo;
}

bool MapFile::getIsOutdoor() const
{
    return _isOutdoor;
}

uint16_t MapFile::getWidth() const
{
    return _width;
}

uint16_t MapFile::getHeight() const
{
    return _height;
}

OverworldRenderable& MapFile::getOverworldRenderable() const
{
    return *_overworldRenderable;
}

bool MapFile::isTileVacant(uint16_t tileX, uint16_t tileY) const
{
    if (tileX >= _width || tileY >= _height)
    {
        return false;
    }

    for (const OccupiedTile& tile : _occupiedTiles)
    {
        if (tile.x == tileX && tile.y == tileY)
        {
            return false;
        }
    }

    return true;
}

UnicodeString MapFile::getContentDirectoryName() const
{
    return "maps";
}

UnicodeString MapFile::getFileTypePrefix() const
{
    return "map";
}

void MapFile::performUpdate(CompilerBinaryWriter& writer,
                                 const KristallBin& kristallBin)
{
    XmlDocument xml(openSourceXml());
    if (xml.hasChild("IsInterior"))
    {
        addSourceFilename(kristallBin.askFor<SpriteSheetFile>(static_cast<uint32_t>(SpriteSheet::OverworldInterior))->getCompiledFileName());
    }
    else
    {
        addSourceFilename(kristallBin.askFor<SpriteSheetFile>(static_cast<uint32_t>(SpriteSheet::OverworldExterior))->getCompiledFileName());
    }

    writer.writeString(xml.firstChild("Name")->getValue().getString());

    _width = xml.firstChild("Width")->getValue().getUInt16();
    if (_width == 0)
    {
        ExecutionException error("Maps must have a width of at least 1 tile.");
        throw error;
    }
    writer.writeUInt16(_width);

    _height = xml.firstChild("Height")->getValue().getUInt16();
    if (_height == 0)
    {
        ExecutionException error("Maps must have a height of at least 1 tile.");
        throw error;
    }
    writer.writeUInt16(_height);

    _regionNo = Handles::getHandleNo("Regions", xml.firstChild("Region")->getValue().getString());
    writer.writeUInt32(_regionNo);

    // Interior
    bool isInterior(xml.hasChild("IsInterior"));
    writer.writeBool(isInterior);
    if (isInterior)
    {
        bool hasParentMap(xml.firstChild("IsInterior")->hasAttribute("ParentMap"));
        uint32_t parentMapNo(0);
        if (hasParentMap)
        {
            assertCanBeChildMap(xml.firstChild("IsInterior")->getAttribute("ParentMap")->getString(),
                    xml.firstChild("Region")->getValue().getString());
            parentMapNo = Handles::getHandleNo("Maps", xml.firstChild("IsInterior")->getAttribute("ParentMap")->getString());
        }

        writer.writeBool(hasParentMap);
        writer.writeUInt32(parentMapNo);
    }
    else
    {
        writer.writeBool(false);
        writer.writeUInt32(0);
    }

    // Building
    bool isBuilding(xml.hasChild("IsIndoors"));
    writer.writeBool(isBuilding);
    bool isBikingAllowed(true);
    if (isBuilding)
    {
        isBikingAllowed = (xml.firstChild("IsIndoors")->hasAttribute("BikingAllowed") &&
                           xml.firstChild("IsIndoors")->getAttribute("BikingAllowed")->getBool());
    }
    else
    {
        isBikingAllowed = !xml.hasChild("DisallowBiking");
    }
    writer.writeBool(isBikingAllowed);

    // General attributes
    writer.writeUInt8(Enums::parse<MapLabel>(xml.firstChild("Label")->getValue().getString()));
    writer.writeUInt16(xml.firstChild("BackgroundMusic")->getValue().getUInt16());

    // Zone Tinting
    if (xml.hasChild("ZoneTinting"))
    {
        writer.writeUInt8(xml.firstChild("ZoneTinting")->getAttribute("R")->getUInt8());
        writer.writeUInt8(xml.firstChild("ZoneTinting")->getAttribute("G")->getUInt8());
        writer.writeUInt8(xml.firstChild("ZoneTinting")->getAttribute("B")->getUInt8());
        writer.writeUInt8(xml.firstChild("ZoneTinting")->getAttribute("A")->getUInt8());
    }
    else
    {
        writer.writeUInt8(0);
        writer.writeUInt8(0);
        writer.writeUInt8(0);
        writer.writeUInt8(0);
    }

    // General attributes
    writer.writeUInt8(Enums::parse<OverworldEffectType>(xml.firstChild("MapEffect")->getValue().getString()));

    // Recovery Location
    writer.writeBool(xml.hasChild("RecoveryLocation"));
    if (xml.hasChild("RecoveryLocation"))
    {
        uint16_t tileX(xml.firstChild("RecoveryLocation")->firstChild("X")->getValue().getUInt16());
        uint16_t tileY(xml.firstChild("RecoveryLocation")->firstChild("Y")->getValue().getUInt16());
        if (tileX >= _width)
        {
            ExecutionException error("The specified recovery location lies outside the width of the map.");
            error.addData("tileX", toString(tileX));
            error.addData("width", toString(_width));
            throw error;
        }
        if (tileY >= _height)
        {
            ExecutionException error("The specified recovery location lies outside the height of the map.");
            error.addData("tileY", toString(tileY));
            error.addData("height", toString(_height));
            throw error;
        }

        XmlElementPtr recoveryLocation(xml.firstChild("Tiles")->firstChild(FunctionPtr<bool, const XmlElement&>(new TileLocator(tileX, tileY))));
        if (recoveryLocation == nullptr)
        {
            ExecutionException error("Could not locate the specified recovery location tile.");
            error.addData("tileX", toString(tileX));
            error.addData("tileY", toString(tileY));
            throw error;
        }

        TileNatureType natureType(Enums::parseKeep<TileNatureType>(recoveryLocation->firstChild("Nature")->getValue().getString()));
        if (natureType == TileNatureType::Solid || natureType == TileNatureType::Ledge)
        {
            ExecutionException error("The specified recovery tile does not have an occupiable nature.");
            error.addData("tileX", toString(tileX));
            error.addData("tileY", toString(tileY));
            error.addData("natureType", toString(static_cast<uint8_t>(natureType)));
            throw error;
        }

        writer.writeUInt16(tileX);
        writer.writeUInt16(tileY);
        writer.writeDirection(xml.firstChild("RecoveryLocation")->firstChild("Direction")->getValue().getDirection());
    }

    // Scripts
    uint8_t nScripts(xml.firstChild("Scripts")->countChildren());
    writer.writeUInt8(nScripts);
    XmlElementPtr currentScriptNode(xml.firstChild("Scripts")->firstChild());
    while (currentScriptNode != nullptr)
    {
        writer.writeLua(currentScriptNode->getValue().getString());
        currentScriptNode = currentScriptNode->nextSibling();
    }

    // Encounter Groups
    uint8_t nEncounterGroups(0);
    if (xml.hasChild("EncounterGroups"))
    {
        uint8_t encounterRate(xml.firstChild("EncounterGroups")->getAttribute("EncounterRate")->getUInt8());
        writer.writeUInt8(encounterRate);

        uint64_t nEncounterGroupsRaw(xml.firstChild("EncounterGroups")->countChildren());
        if (nEncounterGroups > UINT8_MAX)
        {
            ExecutionException error("Too many encounter groups were defined. The total number of encounter groups must fit within the bounds of a UINT8.");
            error.addData("count", toString(nEncounterGroupsRaw));
            throw error;
        }
        nEncounterGroups = static_cast<uint8_t>(nEncounterGroupsRaw);
        writer.writeUInt8(nEncounterGroups);

        XmlElementPtr currentEncounterGroupNode = xml.firstChild("EncounterGroups")->firstChild();
        bool isAffectedByStatic;
        bool isAffectedByMagnetPull;
        uint16_t totalGroupEncounterRate;
        uint64_t nGroupEntriesRaw;
        XmlElementPtr currentGroupEntryNode;
        PokemonSpeciesFile* currentEntrySpeciesFile;
        uint8_t minLevel;
        uint8_t maxLevel;
        TimeOfDayFlags encounterTimes;
        while (currentEncounterGroupNode != nullptr)
        {
            isAffectedByMagnetPull = false;
            isAffectedByStatic = false;
            totalGroupEncounterRate = 0;

            nGroupEntriesRaw = currentEncounterGroupNode->countChildren();
            if (nGroupEntriesRaw == 0 || nGroupEntriesRaw > UINT8_MAX)
            {
                ExecutionException error("An encounter group must have a nonzero number of entries that fits within the bounds of a UINT8.");
                error.addData("raw count", toString(nGroupEntriesRaw));
                throw error;
            }
            writer.writeUInt8(static_cast<uint8_t>(nGroupEntriesRaw));

            currentGroupEntryNode = currentEncounterGroupNode->firstChild();
            while (currentGroupEntryNode != nullptr)
            {
                currentEntrySpeciesFile = kristallBin.askFor<PokemonSpeciesFile>(currentGroupEntryNode->getAttribute("Species")->getUInt32());
                if (currentEntrySpeciesFile == nullptr)
                {
                    ExecutionException error("Attempted to add an entry to an EncounterGroup for a PokemonSpecies which doesn't seem to exist.");
                    error.addData("speciesNo", toString(currentGroupEntryNode->getAttribute("Species")->getUInt32()));
                    throw error;
                }
                writer.writeUInt32(currentEntrySpeciesFile->getUId());

                // PokemonAbility::Static
                writer.writeBool((currentEntrySpeciesFile->getType1() == ElementalType::Electric ||
                                  currentEntrySpeciesFile->getType2() == ElementalType::Electric));
                if (!isAffectedByStatic &&
                    (currentEntrySpeciesFile->getType1() == ElementalType::Electric ||
                    currentEntrySpeciesFile->getType2() == ElementalType::Electric))
                {
                    isAffectedByStatic = true;
                }

                // PokemonAbility::MagnetPull
                writer.writeBool((currentEntrySpeciesFile->getType1() == ElementalType::Steel ||
                                  currentEntrySpeciesFile->getType2() == ElementalType::Steel));
                if (!isAffectedByMagnetPull &&
                    (currentEntrySpeciesFile->getType1() == ElementalType::Steel ||
                     currentEntrySpeciesFile->getType2() == ElementalType::Steel))
                {
                    isAffectedByMagnetPull = true;
                }

                // General properties
                if (currentGroupEntryNode->getAttribute("Rate")->getUInt8() == 0)
                {
                    ExecutionException error("The individual encounter rate of a species within an EncounterGroup must be nonzero.");
                    error.addData("species no", toString(currentEntrySpeciesFile->getUId()));
                    throw error;
                }
                writer.writeUInt8(currentGroupEntryNode->getAttribute("Rate")->getUInt8());
                totalGroupEncounterRate += currentGroupEntryNode->getAttribute("Rate")->getUInt8();
                writer.writeUInt8(Enums::parse<EncounterMethod>(currentGroupEntryNode->getAttribute("EncounterMethod")->getString()));

                minLevel = currentGroupEntryNode->getAttribute("MinLevel")->getUInt8Level();
                maxLevel = currentGroupEntryNode->getAttribute("MaxLevel")->getUInt8Level();
                if (minLevel > maxLevel)
                {
                    ExecutionException error("The minimum encounter level of a species in an EncounterGroup must always be less than the maximum encounter level.");
                    error.addData("minLevel", toString(minLevel));
                    error.addData("maxLevel", toString(maxLevel));
                    error.addData("species no", toString(currentEntrySpeciesFile->getUId()));
                    throw error;
                }
                writer.writeUInt8(minLevel);
                writer.writeUInt8(maxLevel);

                // Encounter times
                encounterTimes = TimeOfDayFlags::None;
                if (currentGroupEntryNode->hasChild("Morning"))
                {
                    encounterTimes = encounterTimes | TimeOfDayFlags::Morning;
                }
                if (currentGroupEntryNode->hasChild("Day"))
                {
                    encounterTimes = encounterTimes | TimeOfDayFlags::Day;
                }
                if (currentGroupEntryNode->hasChild("Night"))
                {
                    encounterTimes = encounterTimes | TimeOfDayFlags::Night;
                }

                if (encounterTimes == TimeOfDayFlags::None)
                {
                    ExecutionException error("An encounter node from an EncounterGroup must be encounterable during at least one time of the day.");
                    error.addData("species no", toString(currentEntrySpeciesFile->getUId()));
                    throw error;
                }
                writer.writeUInt8(static_cast<uint8_t>(encounterTimes));

                currentGroupEntryNode = currentGroupEntryNode->nextSibling();
            }

            writer.writeUInt16(totalGroupEncounterRate);
            writer.writeBool(isAffectedByStatic);
            writer.writeBool(isAffectedByMagnetPull);
            currentEncounterGroupNode = currentEncounterGroupNode->nextSibling();
        }
    }
    else
    {
        writer.writeUInt8(0); // encounter rate
        writer.writeUInt8(0); // number encounter groups
    }

    // Occupants
    if (xml.hasChild("Occupants"))
    {
        XmlElementPtr currentOccupantNode(xml.firstChild("Occupants")->firstChild());
        std::vector<OccupantBlockPtr> occupantBlocks;
        OccupiedTile tempTile;
        while (currentOccupantNode != nullptr)
        {
            occupantBlocks = OverworldOccupantManager::getInstance(currentOccupantNode,
                                                                   this, kristallBin);
            for (const OccupantBlockPtr& occupantBlock : occupantBlocks)
            {
                tempTile.x = occupantBlock->getTileX();
                tempTile.y = occupantBlock->getTileY();
                tempTile.occupantPtr = occupantBlock;
                _occupiedTiles.push_back(tempTile);
            }
            currentOccupantNode = currentOccupantNode->nextSibling();
        }
    }

    // Tiles/OverworldRenderable
    _overworldRenderable = std::shared_ptr<OverworldRenderable>(new OverworldRenderable(xml.firstChild("Name")->getValue().getString(),
                                                                                        _width, _height));
    XmlElementPtr currentTileNode(xml.firstChild("Tiles")->firstChild());
    XmlElementPtr currentLayerNode;
    uint8_t previousLayer;
    uint16_t previousX(0);
    uint16_t previousY(0);
    TileType tileType;
    SpriteSheetFile* mapSpriteSheetFile(kristallBin.askFor<SpriteSheetFile>(static_cast<uint32_t>((isInterior ? SpriteSheet::OverworldInterior : SpriteSheet::OverworldExterior))));
    ItemFile* nonVisibleOverworldItemFile;
    OccupiedTile* occupiedTile;
    uint16_t currentX;
    uint16_t currentY;
    bool hasTileAnimationInstance(false);
    uint32_t tileAnimationInstanceNo(0);
    uint32_t tileAnimationInstanceIndex;
    TileAnimInstance tileAnimationInstance;
    try
    {
        while (currentTileNode != nullptr)
        {
            currentX = currentTileNode->getAttribute("X")->getUInt16();
            currentY = currentTileNode->getAttribute("Y")->getUInt16();

            // Check to make sure we're proceeding in order
            if ((previousX == 0 && currentTileNode->getAttribute("X")->getUInt16() > 1) ||
                (previousX == _width - 1 && currentTileNode->getAttribute("X")->getUInt16() != 0) ||
                (previousX != 0 && previousX != _width - 1 && currentTileNode->getAttribute("X")->getUInt16() != previousX + 1))
            {
                ExecutionException error("[X-check] Tiles must be declared within ascending order, by defining one full, consecutive row of X values [0, width).");
                error.addData("previousX", toString(previousX));
                error.addData("previousY", toString(previousY));
                throw error;
            }
            if (previousX < _width - 1 && previousY != currentTileNode->getAttribute("Y")->getUInt16())
            {
                ExecutionException error("[Y-check] Tiles must be declared within ascending order, by defining one full, consecutive row of X values [0, width).");
                error.addData("previousX", toString(previousX));
                error.addData("previousY", toString(previousY));
                throw error;
            }
            previousX = currentTileNode->getAttribute("X")->getUInt16();
            previousY = currentTileNode->getAttribute("Y")->getUInt16();

            // Draw the layers to the OverworldRenderable
            hasTileAnimationInstance = false;
            currentLayerNode = currentTileNode->firstChild("Layers")->firstChild();
            previousLayer = 0;
            while (currentLayerNode != nullptr)
            {
                if ((previousLayer == 0 && currentLayerNode->getAttribute("No")->getUInt8() != 0) &&
                    (previousLayer != 0 && currentLayerNode->getAttribute("No")->getUInt8() != previousLayer + 1))
                {
                    ExecutionException error("Tile layers must be declared in their ascending numerical order.");
                    throw error;
                }

                if (hasTileAnimationInstance)
                {
                    ExecutionException error("A tile may only have (at most) one AnimationInstance defined in "
                                             "any of its Layers, and it must be the top Layer if not defined on "
                                             "the Superlayer.");
                    error.addData("tileX", toString(currentX));
                    error.addData("tileY", toString(currentY));
                    throw error;
                }

                if (currentLayerNode->getAttribute("Type")->getString() == "AnimationInstance")
                {
                    hasTileAnimationInstance = true;
                    tileAnimationInstanceNo = currentLayerNode->getAttribute("ID")->getUInt32();
                }
                else
                {
                    _overworldRenderable->add(currentTileNode->getAttribute("X")->getUInt16(),
                                              currentTileNode->getAttribute("Y")->getUInt16(),
                                              mapSpriteSheetFile,
                                              currentLayerNode->getAttribute("ID")->getUInt32(),
                                              false);
                }

                previousLayer = currentLayerNode->getAttribute("No")->getUInt8();
                currentLayerNode = currentLayerNode->nextSibling();
            }

            // Draw the superlayer to the OverworldRenderable
            if (currentTileNode->hasChild("SuperLayer"))
            {
                if (currentTileNode->firstChild("SuperLayer")->getAttribute("Type")->getString() == "AnimationInstance")
                {
                    if (hasTileAnimationInstance)
                    {
                        ExecutionException error("A Tile may only have (at most) one AnimationInstance defined on it, "
                                                 "between the main layers and the SuperLayer. An AnimationInstance has "
                                                 "already been specified on the main tiles, and therefore cannot be "
                                                 "defined on the Superlayer.");
                        error.addData("tileX", toString(currentX));
                        error.addData("tileY", toString(currentY));
                        throw error;
                    }

                    hasTileAnimationInstance = true;
                    tileAnimationInstanceNo = currentTileNode->firstChild("SuperLayer")->getAttribute("ID")->getUInt32();
                }
                else
                {
                    _overworldRenderable->add(currentTileNode->getAttribute("X")->getUInt16(),
                                              currentTileNode->getAttribute("Y")->getUInt16(),
                                              mapSpriteSheetFile,
                                              currentTileNode->firstChild("SuperLayer")->getAttribute("ID")->getUInt32(),
                                              true);
                }
            }

            if (hasTileAnimationInstance)
            {
                tileAnimationInstance.animationNo = tileAnimationInstanceNo;
                tileAnimationInstance.tileX = currentX;
                tileAnimationInstance.tileY = currentY;
                tileAnimationInstanceIndex = _tileAnimationInstances.size();
                _tileAnimationInstances.push_back(tileAnimationInstance);
            }

            // BEGIN WRITING TO KRISTALL.BIN
            // Tile Type
            tileType = Enums::parseKeep<TileType>(currentTileNode->getName().getString());
            if (tileType == TileType::MapBridgeTile)
            {
                if (isInterior)
                {
                    ExecutionException error("MapBridge tiles cannot currently exist within non-Outdoor maps.");
                    throw error;
                }

                if (!_isOutdoor)
                {
                    _isOutdoor = true;
                }
            }
            writer.writeUInt8(static_cast<uint8_t>(tileType));

            // Tile Nature
            writer.writeUInt8(Enums::parse<TileNatureType>(currentTileNode->firstChild("Nature")->getValue().getString()));
            writer.writeBool(currentTileNode->firstChild("Nature")->hasAttribute("Direction"));
            if (currentTileNode->firstChild("Nature")->hasAttribute("Direction"))
            {
                writer.writeDirection(currentTileNode->firstChild("Nature")->getAttribute("Direction")->getDirection());
            }
            else
            {
                writer.writeDirection(Direction::North);
            }

            // General properties
            writer.writeUInt8(Enums::parse<Terrain>(currentTileNode->firstChild("Terrain")->getValue().getString()));
            if (currentTileNode->hasChild("MapScriptNo"))
            {
                if (currentTileNode->firstChild("MapScriptNo")->getValue().getUInt8() >= nScripts)
                {
                    ExecutionException error("Attempted to bind a map script that does not exist to a Tile.");
                    error.addData("script no", toString(currentTileNode->firstChild("MapScriptNo")->getValue().getUInt8()));
                    error.addData("nScripts", toString(nScripts));
                    throw error;
                }

                writer.writeInt16(static_cast<int16_t>(currentTileNode->firstChild("MapScriptNo")->getValue().getUInt8()));
            }
            else
            {
                writer.writeInt16(-1);
            }
            if (currentTileNode->hasChild("EncounterGroupNo"))
            {
                if (currentTileNode->firstChild("EncounterGroupNo")->getValue().getUInt8() >= nEncounterGroups)
                {
                    ExecutionException error("Attempted to bind an encounter group that does not exist to a Tile.");
                    error.addData("encounterGroupNo", toString(currentTileNode->firstChild("EncounterGroupNo")->getValue().getUInt8()));
                    error.addData("nEncounterGroups", toString(nEncounterGroups));
                    throw error;
                }

                writer.writeInt16(static_cast<int16_t>(currentTileNode->firstChild("EncounterGroupNo")->getValue().getUInt8()));
            }
            else
            {
                writer.writeInt16(-1);
            }
            if (currentTileNode->hasChild("TriggeredOverworldEffect"))
            {
                writer.writeUInt8(Enums::parse<OverworldEffectType>(currentTileNode->firstChild("TriggeredOverworldEffect")->getValue().getString()));
                writer.writeDirection(currentTileNode->firstChild("TriggeredOverworldEffect")->getAttribute("TriggerDirection")->getDirection());
            }
            else
            {
                writer.writeUInt8(static_cast<uint8_t>(OverworldEffectType::None));
                writer.writeDirection(Direction::North);
            }

            writer.writeBool(currentTileNode->hasChild("NonVisibleItem"));
            if (currentTileNode->hasChild("NonVisibleItem"))
            {
                nonVisibleOverworldItemFile = kristallBin.askFor<ItemFile>(Handles::getHandleNo("Items",
                                                                                                currentTileNode->firstChild("NonVisibleItem")->getValue().getString()));
                if (nonVisibleOverworldItemFile == nullptr)
                {
                    ExecutionException error("Attempted to place a non-existant item on a map tile.");
                    throw error;
                }
                writer.writeUInt32(nonVisibleOverworldItemFile->getUId());
                writer.writeUInt32(currentTileNode->firstChild("NonVisibleItem")->getAttribute("RegistrationCode")->getUInt32());
                if (Enums::parseKeep<ItemOverworldObjectForm>(currentTileNode->firstChild("NonVisibleItem")->getAttribute("Form")->getString()) ==
                    ItemOverworldObjectForm::Visible)
                {
                    ExecutionException error("An item which is being added to a tile as a NonVisibleItem may not be outright visible.");
                    throw error;
                }
                writer.writeUInt8(Enums::parse<ItemOverworldObjectForm>(currentTileNode->firstChild("NonVisibleItem")->getAttribute("Form")->getString()));
            }

            writer.writeBool(hasTileAnimationInstance);
            if (hasTileAnimationInstance)
            {
                writer.writeUInt32(tileAnimationInstanceIndex);
            }
            else
            {
                writer.writeUInt32(0);
            }

            bool foundOccupant(false);
            for (const OccupiedTile& tile : _occupiedTiles)
            {
                if (tile.x == currentX && tile.y == currentY)
                {
                    foundOccupant = true;
                    writer.writeBool(true); // hasOccupant
                    tile.occupantPtr->write(writer);
                    break;
                }
            }
            if (!foundOccupant)
            {
                writer.writeBool(false); // hasOccupant
            }

            // TileType header
            if (tileType != TileType::Tile)
            {
                writer.writeDirection(currentTileNode->firstChild("InwardArrow")->getValue().getDirection());

                if (tileType == TileType::DoorTile)
                {
                    writer.writeBool(currentTileNode->hasChild("UseOnExit"));
                    if (currentTileNode->hasChild("UseOnExit") &&
                        currentTileNode->firstChild("UseOnExit")->hasChild("IsStaircase"))
                    {
                            writer.writeUInt8(static_cast<uint8_t>((currentTileNode->firstChild("UseOnExit")->firstChild("IsStaircase")->hasAttribute("GoesUp") ?
                                                                  StaircaseType::UpwardStaircase :
                                                                  StaircaseType::DownwardStaircase)));
                    }
                    else
                    {
                        writer.writeUInt8(static_cast<uint8_t>(StaircaseType::NotAStaircase));
                    }

                    writer.writeUInt32(currentTileNode->firstChild("SoundEffect")->getValue().getUInt32());
                }

                assertOtherMapsTileOccupiable(kristallBin, currentTileNode->firstChild("DestinationMap")->getValue().getString(),
                                              currentTileNode->firstChild("DestinationX")->getValue().getUInt16(),
                                              currentTileNode->firstChild("DestinationY")->getValue().getUInt16());

                writer.writeUInt32(Handles::getHandleNo("Maps", currentTileNode->firstChild("DestinationMap")->getValue().getString()));
                writer.writeUInt16(currentTileNode->firstChild("DestinationX")->getValue().getUInt16());
                writer.writeUInt16(currentTileNode->firstChild("DestinationY")->getValue().getUInt16());
            }

            // FINISHED WITH WRITING THE TILE TO KRISTALL.BIN

            // Next
            currentTileNode = currentTileNode->nextSibling();
        }
    }
    catch (ExecutionException& ex)
    {
        if (currentTileNode != nullptr)
        {
            ex.addData("tileX", toString(currentTileNode->getAttribute("X")->getUInt16()));
            ex.addData("tileY", toString(currentTileNode->getAttribute("Y")->getUInt16()));
        }
        throw;
    }

    // Write TileAnimationInstances
    writer.writeUInt32(_tileAnimationInstances.size());
    for (const TileAnimInstance& instance : _tileAnimationInstances)
    {
        writer.writeUInt32(instance.animationNo);
        writer.writeUInt16(instance.tileX);
        writer.writeUInt16(instance.tileY);
    }

    // Serialize the clipboard
    std::string serializeFileName(UnicodeStrings::toStdString(getClipboardFileName()));
    std::cout << serializeFileName << "\n";
    FILE* file = fopen(serializeFileName.c_str(), "wb");
    CompilerBinaryWriter prerenderSerializeFile(file);
    prerenderSerializeFile.writeUInt32(_regionNo);
    prerenderSerializeFile.writeBool(_isOutdoor);
    prerenderSerializeFile.writeUInt32(_occupiedTiles.size());
    for (const OccupiedTile& location : _occupiedTiles)
    {
        prerenderSerializeFile.writeUInt16(location.x);
        prerenderSerializeFile.writeUInt16(location.y);
    }
    _overworldRenderable->serialize(prerenderSerializeFile);
    prerenderSerializeFile.close();
}

void MapFile::populateData(CompilerBinaryReader& reader)
{
    reader.readString();
    _width = reader.readUInt16();
    _height = reader.readUInt16();

    // Deserialize the clipboard
    std::string serializeFileName(UnicodeStrings::toStdString(getClipboardFileName()));
    FILE* file = fopen(serializeFileName.c_str(), "rb");
    CompilerBinaryReader prerenderSerializeFile(file);
    _regionNo = prerenderSerializeFile.readUInt32();
    _isOutdoor = prerenderSerializeFile.readBool();

    size_t nOccupiedTiles(prerenderSerializeFile.readUInt32());
    _occupiedTiles.reserve(nOccupiedTiles);
    OccupiedTile tempOccupant;
    for (size_t index = 0; index < nOccupiedTiles; ++index)
    {
        tempOccupant.x = prerenderSerializeFile.readUInt16();
        tempOccupant.y = prerenderSerializeFile.readUInt16();
        tempOccupant.occupantPtr = nullptr;
        _occupiedTiles.push_back(tempOccupant);
    }

    _overworldRenderable = OverworldRenderable::deserialize(prerenderSerializeFile);
    prerenderSerializeFile.close();
}

void MapFile::assertCanBeChildMap(UnicodeString parentMapHandle,
                                  UnicodeString region)
{
    try
    {
        if (getHandle() == parentMapHandle)
        {
            ExecutionException error("An interior map cannot have itself on its parent map hierarchy.");
            throw error;
        }

        XmlDocument parentXml(XmlDocument::load("content\\maps\\" + parentMapHandle + ".xml"));
        if (parentXml.firstChild("Region")->getValue().getString() != region)
        {
            ExecutionException error("Interior maps cannot have parent maps who are from another region.");
            error.addData("expected region", region);
            error.addData("conflicting region", parentXml.firstChild("Region")->getValue().getString());
            throw error;
        }

        if (parentXml.hasChild("IsInterior") &&
            parentXml.firstChild("IsInterior")->hasAttribute("ParentMap"))
        {
            assertCanBeChildMap(parentXml.firstChild("IsInterior")->getAttribute("ParentMap")->getString(),
                                region);
        }
    }
    catch (ExecutionException& error)
    {
        error.addData("parentMapHandle", parentMapHandle);
        throw;
    }
}

void MapFile::assertOtherMapsTileOccupiable(const KristallBin& kristallBin,
                                            UnicodeString otherMapHandle,
                                            uint16_t tileX, uint16_t tileY)
{
    MapFile* existantMapFile = nullptr;

    try
    {
        existantMapFile = kristallBin.askFor<MapFile>(Handles::getHandleNo("Maps", otherMapHandle));
    }
    catch (ExecutionException& ex)
    {
        // When we have an exception thrown when asking for a file, it means that the specific
        // file type <MapFile> has no files yet; this means that this is the first MapFile being
        // compiled, and so this isn't an error;
        existantMapFile = nullptr;
    }
    if (existantMapFile != nullptr)
    {
        if (tileX >= existantMapFile->_width || tileY >= existantMapFile->_height)
        {
            ExecutionException error("The specified location is outside of the bounds of the target map.");
            error.addData("requested tileX", toString(tileX));
            error.addData("requested tileY", toString(tileY));
            error.addData("width", toString(existantMapFile->_width));
            error.addData("height", toString(existantMapFile->_height));
            throw error;
        }
    }
    else
    {
        XmlDocument xml(XmlDocument::load("content\\maps\\" + otherMapHandle + ".xml"));

        if (tileX >= xml.firstChild("Width")->getValue().getUInt16() ||
            tileY >= xml.firstChild("Height")->getValue().getUInt16())
        {
            ExecutionException error("The specified location is outside of the bounds of the target map.");
            error.addData("requested tileX", toString(tileX));
            error.addData("requested tileY", toString(tileY));
            error.addData("width", toString(xml.firstChild("Width")->getValue().getUInt16()));
            error.addData("height", toString(xml.firstChild("Height")->getValue().getUInt16()));
            throw error;
        }
    }
}

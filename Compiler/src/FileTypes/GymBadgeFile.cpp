#include "KristallCompiler.hpp"
#include "CompilerVersions.h"

//////////////////////////////
// VERSIONING
//////////////////////////////
uint64_t GymBadgeFile::getFileTypeVersion() const
{
    return COMPILER_VERSION_FILE_GYMBADGE;
}

/////////////////////////////
// CLASS METHODS
/////////////////////////////
GymBadgeFile::GymBadgeFile(UnicodeString handle) : File(handle)
{

}

uint32_t GymBadgeFile::getUId() const
{
    return Handles::getHandleNo("GymBadges", getHandle());
}

UnicodeString GymBadgeFile::getContentDirectoryName() const
{
    return "gym-badges";
}

UnicodeString GymBadgeFile::getFileTypePrefix() const
{
    return "gb";
}

void GymBadgeFile::performUpdate(CompilerBinaryWriter& writer,
                                 const KristallBin& kristallBin)
{
    XmlDocument xml(openSourceXml());

    writer.writeString(xml.firstChild("Name")->getValue().getString());
    bool doesAllowHmUse = xml.hasChild("AllowsHM");
    writer.writeBool(doesAllowHmUse);
    if (doesAllowHmUse)
    {
        writer.writeUInt16(xml.firstChild("AllowsHM")->getValue().getUInt16());
    }
    else
    {
        writer.writeUInt16(0);
    }
    if (xml.hasChild("ObedienceUpTo"))
    {
        writer.writeUInt8(xml.firstChild("ObedienceUpTo")->getValue().getUInt8());
    }
    else
    {
        writer.writeUInt8(0);
    }
}

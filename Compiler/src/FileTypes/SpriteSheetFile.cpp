#include "KristallCompiler.hpp"
#include "CompilerVersions.h"

//////////////////////////////
// VERSIONING
//////////////////////////////
uint64_t SpriteSheetFile::getFileTypeVersion() const
{
    return COMPILER_VERSION_FILE_SPRITESHEET;
}

/////////////////////////////
// CLASS METHODS
/////////////////////////////
SpriteSheetFile::SpriteSheetFile(UnicodeString handle) : File(handle)
{

}

bool SpriteSheetFile::isValid()
{
    XmlDocument xml(openSourceXml());
    if (!xml.hasAttribute("Defines"))
    {
        return false;
    }

    _defines = Enums::parseKeep<SpriteSheet>(xml.getAttribute("Defines")->getString());
    _texture = xml.getAttribute("Texture")->getString();
    return true;
}

uint32_t SpriteSheetFile::getUId() const
{
    return static_cast<uint32_t>(_defines);
}

UnicodeString SpriteSheetFile::getContentDirectoryName() const
{
    return "catalogs";
}

UnicodeString SpriteSheetFile::getFileTypePrefix() const
{
    return "ss";
}

bool SpriteSheetFile::isTextureStoredInCompiler() const
{
    switch (_defines)
    {
        case SpriteSheet::OverworldInterior:
        case SpriteSheet::OverworldExterior:
            {
                return true;
            }
        default:
            {
                return false;
            }
    }

    return false;
}

void SpriteSheetFile::performUpdate(CompilerBinaryWriter& writer,
                                 const KristallBin& kristallBin)
{
    XmlDocument xml(openSourceXml());

    // Texture
    _textureNo = TextureManager::give("content\\" + xml.getAttribute("Texture")->getString() + ".png", this, "Texture");
    writer.writeUInt32(_textureNo);
    //writer.writeTexture("content\\" + xml.getAttribute("Texture")->getString() + ".png");
    addSourceFilename("content\\" + xml.getAttribute("Texture")->getString() + ".png");
    if (isTextureStoredInCompiler())
    {
        _sfTexture = PNG::load("content\\" + xml.getAttribute("Texture")->getString() + ".png");
        if (_sfTexture == nullptr)
        {
            ExecutionException error("Could not load the texture image from file to store within the compiler.");
            throw error;
        }
    }

    // Sprites
    uint64_t nSpritesRaw = (xml.firstChild("Sprites")->countChildren());
    if (nSpritesRaw > UINT32_MAX)
    {
        ExecutionException error("Too many sprites were defined for this spritesheet.");
        error.addData("nSpritesRaw", toString(nSpritesRaw));
        throw error;
    }
    uint32_t nSprites(static_cast<uint32_t>(nSpritesRaw));
    writer.writeUInt32(nSprites);

    std::vector<SpriteSheetSprite> sprites;
    sprites.reserve(nSprites);
    XmlElementPtr currentSpriteNode = xml.firstChild("Sprites")->firstChild();
    uint32_t currentSpriteIndex = 0;
    uint32_t nUniversalAnimations = 0;
    while (currentSpriteNode != nullptr)
    {
        SpriteSheetSprite currentSprite(currentSpriteNode);
        SpriteSheetSpriteInfo spriteInfo;
        if (currentSprite.getId() != currentSpriteIndex)
        {
            ExecutionException error("Sprites defined within a SpriteSheet must be defined in ascending order based on IDs, without any gaps.");
            throw error;
        }
        spriteInfo.id = currentSprite.getId();

        if (currentSprite.isUniversalAnimation())
        {
            ++nUniversalAnimations;
        }
        spriteInfo.isUniversalAnimation = currentSprite.isUniversalAnimation();

        Vector2 corner = currentSprite.getTopLeftCorner(sprites);
        spriteInfo.topLeftCorner = corner;
        writer.writeUInt32(corner.x);
        writer.writeUInt32(corner.y);

        corner = currentSprite.getTopRightCorner(sprites);
        spriteInfo.topRightCorner = corner;
        writer.writeUInt32(corner.x);
        writer.writeUInt32(corner.y);

        corner = currentSprite.getBottomRightCorner(sprites);
        spriteInfo.bottomRightCorner = corner;
        writer.writeUInt32(corner.x);
        writer.writeUInt32(corner.y);

        corner = currentSprite.getBottomLeftCorner(sprites);
        spriteInfo.bottomLeftCorner = corner;
        writer.writeUInt32(corner.x);
        writer.writeUInt32(corner.y);

        writer.writeUInt16(currentSprite.getWidth(sprites));
        spriteInfo.width = currentSprite.getWidth(sprites);
        writer.writeUInt16(currentSprite.getHeight(sprites));
        spriteInfo.height = currentSprite.getHeight(sprites);
        writer.writeInt32(currentSprite.getOffsetX());
        spriteInfo.offsetX = currentSprite.getOffsetX();
        writer.writeInt32(currentSprite.getOffsetY());
        spriteInfo.offsetY = currentSprite.getOffsetY();

        sprites.push_back(currentSprite);
        _sprites.push_back(spriteInfo);
        currentSpriteNode = currentSpriteNode->nextSibling();
        ++currentSpriteIndex;
    }

    // UniversalAnimations
    writer.writeUInt32(nUniversalAnimations);
    for (SpriteSheetSprite& sprite : sprites)
    {
        if (!sprite.isUniversalAnimation())
        {
            continue;
        }

        writer.writeUInt32(sprite.getId());
        XmlElementPtr spriteNode(sprite.getXml());

        uint16_t duration(spriteNode->getAttribute("Duration")->getUInt16());
        if (duration == 0)
        {
            ExecutionException error("A UniversalAnimation may not have a duration of 0.");
            error.addData("spriteId", toString(sprite.getId()));
            throw error;
        }
        writer.writeUInt16(duration);

        std::vector<SpriteSheetAnimationFrame> spriteFrames(parseAnimation(spriteNode->getValue().getString(),
                                                                   duration));
        writer.writeUInt16(static_cast<uint16_t>(spriteFrames.size()));
        for (SpriteSheetAnimationFrame& frame : spriteFrames)
        {
            if (frame.hasOverlay)
            {
                ExecutionException error("Universal Animations may not have overlays defined.");
                error.addData("spriteId", toString(sprite.getId()));
                throw error;
            }

            if (getSprite(frame.spriteId).isUniversalAnimation)
            {
                ExecutionException error("Universal Animations may not have other universal animations for frames.");
                error.addData("spriteId", toString(sprite.getId()));
                error.addData("frameSpriteId", toString(frame.spriteId));
                throw error;
            }

            writer.writeUInt32(frame.spriteId);
            writer.writeUInt16(frame.timeOffset);
        }
    }

    // Animation names
    _animationNames.reserve(xml.firstChild("AnimationNames")->countChildren());
    XmlElementPtr currentAnimationName = xml.firstChild("AnimationNames")->firstChild();
    while (currentAnimationName != nullptr)
    {
        if (currentAnimationName->getValue().getUInt32() != _animationNames.size())
        {
            ExecutionException error("AnimationNames must start at 0 and proceed, defined in order, incrementing by 1 without skipping any values.");
            throw error;
        }

        if (getAnimationNameNo(currentAnimationName->getName().getString()) >= 0)
        {
            ExecutionException error("AnimationNames must be unique.");
            error.addData("animationName", currentAnimationName->getName().getString());
            throw error;
        }

        _animationNames.push_back(currentAnimationName->getName().getString());
        currentAnimationName = currentAnimationName->nextSibling();
    }
    writer.writeUInt32(_animationNames.size());
    for (UnicodeString& animationName : _animationNames)
    {
        writer.writeStandardString(UnicodeStrings::toStdString(animationName));
    }

    // Animation Instances
    uint64_t nAnimationInstancesRaw(xml.firstChild("AnimationInstances")->countChildren());
    if (nAnimationInstancesRaw > UINT32_MAX)
    {
        ExecutionException error("Too many animation instances were defined for one sprite sheet; the number of defines must fit within a UINT32.");
        throw error;
    }
    writer.writeUInt32(static_cast<uint32_t>(nAnimationInstancesRaw));

    XmlElementPtr currentAnimationNode(xml.firstChild("AnimationInstances")->firstChild());
    XmlElementPtr animationNode;
    uint32_t currentAnimationIndex = 0;
    int64_t initialAnimationNo;
    bool isLooping;
    bool isDeadEnd;
    int64_t returnToAnimationNo;
    while (currentAnimationNode != nullptr)
    {
        initialAnimationNo = -1;
        if (currentAnimationNode->getAttribute("ID")->getUInt32() != currentAnimationIndex)
        {
            ExecutionException error("Animation Instances defined within a SpriteSheet must be defined in ascending order based on IDs, without any gaps.");
            throw error;
        }

        for (UnicodeString& animationName : _animationNames)
        {
            animationNode = currentAnimationNode->firstChild(animationName);
            writer.writeBool((animationNode != nullptr)); // isAnimationDefined
            if (animationNode == nullptr)
            {
                continue;
            }

            if (animationNode->nextSibling(animationName) != nullptr)
            {
                ExecutionException error("An AnimationInstance may only define one animation per animation name; all animation names must be uniquely defined.");
                error.addData("animationName", animationName);
                error.addData("animationId", toString(currentAnimationNode->getAttribute("ID")->getUInt32()));
                throw error;
            }

            if (animationNode->hasAttribute("Initial") && animationNode->getAttribute("Initial")->getBool())
            {
                if (initialAnimationNo >= 0)
                {
                    ExecutionException error("Two or more animations may not be defined as being initial animations for an animation instance.");
                    throw error;
                }

                initialAnimationNo = getAnimationNameNo(animationName);
            }

            isLooping = (animationNode->hasAttribute("Looping") && animationNode->getAttribute("Looping")->getBool());
            writer.writeBool(isLooping);
            isDeadEnd = (!animationNode->hasAttribute("ReturnTo"));
            if (isLooping && !isDeadEnd)
            {
                isDeadEnd = true;
            }
            writer.writeBool(isDeadEnd);

            if (!isDeadEnd)
            {
                returnToAnimationNo = getAnimationNameNo(animationNode->getAttribute("ReturnTo")->getString());
                if (returnToAnimationNo < 0)
                {
                    ExecutionException error("A non-dead end animation must return to a valid animation.");
                    throw error;
                }

                if (returnToAnimationNo == getAnimationNameNo(animationName))
                {
                    ExecutionException error("A non-dead end animation may not return to itself; to allow this, specify this animation as 'Looping'.");
                    throw error;
                }
            }
            else
            {
                returnToAnimationNo = getAnimationNameNo(animationName);
            }
            writer.writeUInt32(static_cast<uint32_t>(returnToAnimationNo));

            writer.writeBool(animationNode->hasAttribute("SoundEffect"));
            if (animationNode->hasAttribute("SoundEffect"))
            {
                writer.writeUInt16(animationNode->getAttribute("SoundEffect")->getUInt16());
            }
            else
            {
                writer.writeUInt16(0);
            }

            writer.writeUInt16(animationNode->getAttribute("Duration")->getUInt16());
            std::vector<SpriteSheetAnimationFrame> frames(parseAnimation(animationNode->getValue().getString(),
                                                                         animationNode->getAttribute("Duration")->getUInt16()));
            if (frames.size() > UINT16_MAX)
            {
                ExecutionException error("The number of frames defined for one AnimationInstance animation must fit within the bounds of a UINT16.");
                error.addData("nFrames", toString(frames.size()));
                error.addData("animationNo", toString(currentAnimationIndex));
                throw error;
            }
            writer.writeUInt16(static_cast<uint16_t>(frames.size()));

            for (SpriteSheetAnimationFrame& frame : frames)
            {
                if (getSprite(frame.spriteId).isUniversalAnimation)
                {
                    ExecutionException error("Frames of AnimationInstances are not allowed to be UniversalAnimations.");
                    throw error;
                }

                if (frame.hasOverlay && getSprite(frame.overlaySpriteId).isUniversalAnimation)
                {
                    ExecutionException error("Overlay frames of AnimationInstances are not allowed to be UniversalAnimations.");
                    throw error;
                }

                writer.writeUInt32(frame.spriteId);
                if (frame.hasOverlay)
                {
                    writer.writeUInt32(frame.overlaySpriteId);
                }
                else
                {
                    writer.writeUInt32(0);
                }
                writer.writeUInt16(frame.timeOffset);
            }
        }

        if (initialAnimationNo < 0)
        {
            ExecutionException error("One animation must be defined as the initial animation for an AnimationInstance.");
            error.addData("id", toString(currentAnimationNode->getAttribute("ID")->getUInt32()));
            throw error;
        }

        writer.writeUInt32(initialAnimationNo);

        currentAnimationNode = currentAnimationNode->nextSibling();
        ++currentAnimationIndex;
    }
}

void SpriteSheetFile::populateData(CompilerBinaryReader& reader)
{
    _textureNo = reader.readUInt32();
    if (isTextureStoredInCompiler())
    {
        _sfTexture = TextureManager::loadTexture(this, "Texture");
    }

    // Sprites
    uint32_t nSprites(reader.readUInt32());
    _sprites.reserve(nSprites);
    for (uint32_t index = 0; index < nSprites; ++index)
    {
        SpriteSheetSpriteInfo sprite;
        sprite.id = index;
        sprite.isUniversalAnimation = false;
        sprite.topLeftCorner.x = reader.readUInt32();
        sprite.topLeftCorner.y = reader.readUInt32();
        sprite.topRightCorner.x = reader.readUInt32();
        sprite.topRightCorner.y = reader.readUInt32();
        sprite.bottomRightCorner.x = reader.readUInt32();
        sprite.bottomRightCorner.y = reader.readUInt32();
        sprite.bottomLeftCorner.x = reader.readUInt32();
        sprite.bottomLeftCorner.y = reader.readUInt32();
        sprite.width = reader.readUInt16();
        sprite.height = reader.readUInt16();
        sprite.offsetX = reader.readInt32();
        sprite.offsetY = reader.readInt32();
        _sprites.push_back(sprite);
    }

    // UniversalAnimations
    uint32_t nUniversalAnimations(reader.readUInt32());
    for (uint32_t index = 0; index < nUniversalAnimations; ++index)
    {
        _sprites[reader.readUInt32()].isUniversalAnimation = true;

        reader.readUInt16(); //duration
        uint16_t nFrames(reader.readUInt16());
        for (uint16_t throwaway = 0; throwaway < nFrames; ++throwaway)
        {
            reader.readUInt32();
            reader.readUInt16();
        }
    }

    // Animation names
    uint32_t nAnimationNames(reader.readUInt32());
    _animationNames.reserve(nAnimationNames);
    for (uint32_t index = 0; index < nAnimationNames; ++index)
    {
        std::string animationName(reader.readStandardString());
        _animationNames.push_back(UnicodeString(animationName.c_str()));
    }
}

SpriteSheetSpriteInfo SpriteSheetFile::getSprite(uint32_t spriteId) const
{
    if (spriteId >= _sprites.size())
    {
        ExecutionException error("The specified spriteId is outside the number of defined sprites.");
        error.addData("spriteId", toString(spriteId));
        error.addData("nSprites", toString(_sprites.size()));
        throw error;
    }

    if (_sprites[spriteId].id == spriteId)
    {
        return _sprites[spriteId];
    }

    for (size_t index = 0; index < _sprites.size(); ++index)
    {
        if (_sprites[index].id == spriteId)
        {
            return _sprites[index];
        }
    }

    ExecutionException error("The requested spriteId is not defined.");
    error.addData("spriteId", toString(spriteId));
    throw error;
}

SDL_Surface* SpriteSheetFile::getTexture()
{
    if (!isTextureStoredInCompiler())
    {
        ExecutionException error("Attempted to get the sf::Texture of a SpriteSheet that doesn't store its texture in the compiler.");
        error.addData("SpriteSheet", toString(_defines));
        throw error;
    }

    return _sfTexture;
}

int64_t SpriteSheetFile::getAnimationNameNo(UnicodeString animationName) const
{
    auto vectorIterator = std::find(_animationNames.begin(), _animationNames.end(),
                                    animationName);
    if (vectorIterator == _animationNames.end())
    {
        return -1;
    }

    return (vectorIterator - _animationNames.begin());
}

std::vector<SpriteSheetAnimationFrame> SpriteSheetFile::parseAnimation(UnicodeString animationString,
                                                                       uint16_t duration) const
{
    std::vector<UnicodeString> mainAndOverlay(UnicodeStrings::split(animationString, '|'));

    // Main frames
    std::vector<SpriteSheetAnimationFrame> mainFrames;
    std::vector<UnicodeString> mainFramePieces(UnicodeStrings::split(mainAndOverlay[0], ','));
    int64_t currentTime(0);
    for (UnicodeString& framePiece : mainFramePieces)
    {
        std::vector<UnicodeString> frameProperties(UnicodeStrings::split(framePiece, ':'));
        SpriteSheetAnimationFrame frame;
        int64_t rawSpriteId(UnicodeStrings::toInt64(frameProperties[0]));
        if (rawSpriteId < 0 || rawSpriteId >= UINT32_MAX)
        {
            ExecutionException error("An animation string contained a frame that does not fit within the bounds of a UINT32.");
            throw error;
        }
        frame.spriteId = static_cast<uint32_t>(rawSpriteId);
        frame.timeOffset = static_cast<uint16_t>(currentTime);
        frame.hasOverlay = false;
        mainFrames.push_back(frame);

        if (UnicodeStrings::toInt64(frameProperties[1]) < 0)
        {
            ExecutionException error("An animation string may not contain any frames whose duration are less than 0.");
            throw error;
        }
        currentTime += UnicodeStrings::toInt64(frameProperties[1]);
        if (currentTime > UINT16_MAX)
        {
            ExecutionException error("An animation string cannot last for a combined duration of greater than the bounds of a UINT16.");
            throw error;
        }
        if (static_cast<uint16_t>(currentTime) > duration)
        {
            ExecutionException error("The provided animation string was defined to last for a longer time than the specified duration.");
            error.addData("specifiedDuration", toString(duration));
            error.addData("currentTime (at exceed)", toString(currentTime));
            error.addData("frameNo", toString(mainFrames.size() - 1));
            throw error;
        }
    }

    if (static_cast<uint16_t>(currentTime) != duration)
    {
        ExecutionException error("The provided animation string is not defined to last for the same duration as was specified explicitly.");
        error.addData("specifiedDuration", toString(duration));
        error.addData("definedDuration", toString(currentTime));
        error.addData("animationString", animationString);
        throw error;
    }

    // Overlay frames
    if (mainAndOverlay.size() == 2)
    {
        std::vector<UnicodeString> overlayFramePieces(UnicodeStrings::split(mainAndOverlay[1], ','));
        currentTime = 0;
        int64_t remainingOverlayTime = 0;
        size_t mainFrameIndex = 0;
        for (UnicodeString& framePiece : overlayFramePieces)
        {
            std::vector<UnicodeString> frameProperties(UnicodeStrings::split(framePiece, ':'));
            int64_t rawSpriteId(UnicodeStrings::toInt64(frameProperties[0]));
            if (rawSpriteId < 0 || rawSpriteId >= UINT32_MAX)
            {
                ExecutionException error("An animation string may not have an overlay that does not fit within the bounds of a UINT32.");
                throw error;
            }
            remainingOverlayTime = UnicodeStrings::toInt64(frameProperties[1]);
            if (remainingOverlayTime < 0 || remainingOverlayTime > UINT16_MAX)
            {
                ExecutionException error("An animation string may not have an overlay whose duration does not fit within the bounds of a UINT16.");
                throw error;
            }

            currentTime += remainingOverlayTime;
            if (currentTime >= UINT16_MAX)
            {
                ExecutionException error("The animation string defined overlays whose total duration exceeded the bounds of a UINT16.");
                throw error;
            }
            if (static_cast<uint16_t>(currentTime) > duration)
            {
                ExecutionException error("The animation string defined overlays whose total duration exceeded that of the explicitly specified duration.");
                error.addData("specifiedDuration", toString(duration));
                error.addData("definedDuration (at exceed)", toString(currentTime));
                error.addData("animationString", animationString);
                throw error;
            }

            while (remainingOverlayTime > 0)
            {
                if (mainFrameIndex > mainFrames.size())
                {
                    ExecutionException error("The combined duration of an animation string's overlays may not extend that of the primary frames.");
                    throw error;
                }
                else if (mainFrameIndex == mainFrames.size())
                {
                    SpriteSheetAnimationFrame appendFrame;
                    appendFrame.spriteId = mainFrames.back().spriteId;
                    appendFrame.timeOffset = static_cast<uint16_t>(currentTime);
                    mainFrames.push_back(appendFrame);
                }

                mainFrames[mainFrameIndex].hasOverlay = true;
                mainFrames[mainFrameIndex].overlaySpriteId = static_cast<uint32_t>(rawSpriteId);

                if (mainFrameIndex < mainFrames.size() - 1)
                {
                    remainingOverlayTime -= ((int64_t)mainFrames[mainFrameIndex + 1].timeOffset - (int64_t)mainFrames[mainFrameIndex].timeOffset);
                    if (remainingOverlayTime < 0)
                    {
                        // The current overlay does not last long enough to fully span the current frame
                        // We need to break up the current frame into two separate frames
                        SpriteSheetAnimationFrame postCurrentFrame;
                        if (remainingOverlayTime < -UINT16_MAX)
                        {
                            ExecutionException error("The specified overlay lasts for too long. This error shouldn't occur in reality, but is here to ensure all the same.");
                            throw error;
                        }
                        postCurrentFrame.spriteId = mainFrames[mainFrameIndex].spriteId;
                        postCurrentFrame.timeOffset = static_cast<uint16_t>(-remainingOverlayTime);
                        mainFrames.insert(mainFrames.begin() + mainFrameIndex, postCurrentFrame);
                    }
                }
                else
                {
                    remainingOverlayTime = 0;
                }
                ++mainFrameIndex;
            }
        }

        if (mainFrameIndex != mainFrames.size())
        {
            ExecutionException error("An animation string which has overlays must provide overlays for the same duration as that occupied by main frames.");
            throw error;
        }

        if (static_cast<uint16_t>(currentTime) != duration)
        {
            ExecutionException error("The animation string did not define overlays whose duration matched that of the explicitly specified duration.");
            error.addData("specifiedDuration", toString(duration));
            error.addData("definedDuration", toString(currentTime));
            error.addData("animationString", animationString);
            throw error;
        }
    }

    return mainFrames;
}

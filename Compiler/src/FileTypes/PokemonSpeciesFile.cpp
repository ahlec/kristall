#include "KristallCompiler.hpp"
#include "CompilerVersions.h"
#include "Pokemon/PokemonAbility.h"
#include "Pokemon/PokemonExperienceGroup.h"
#include "Pokemon/PokemonGenderRatio.h"
#include "Pokemon/PokemonMoveAcquisitionMode.h"
#include "Pokemon/PokemonSpeciesColor.h"
#include "Pokemon/PokemonEggGroup.h"
#include "Pokemon/PokemonEvolutionStimulus.h"
#include "General/TimeOfDayFlags.h"

//////////////////////////////
// VERSIONING
//////////////////////////////
uint64_t PokemonSpeciesFile::getFileTypeVersion() const
{
    return COMPILER_VERSION_FILE_POKEMONSPECIES;
}

/////////////////////////////
// CLASS METHODS
/////////////////////////////
PokemonSpeciesFile::PokemonSpeciesFile(UnicodeString handle) : File(handle)
{
    int64_t rawNumber = UnicodeStrings::toInt64(handle);
    if (rawNumber < 0 || rawNumber > UINT32_MAX)
    {
        ExecutionException error("An invalid value was provided for the pokemon species number.");
        throw error;
    }

    _speciesNo = static_cast<uint32_t>(rawNumber);
}

uint32_t PokemonSpeciesFile::getUId() const
{
    return _speciesNo;
}

ElementalType PokemonSpeciesFile::getType1() const
{
    return _type1;
}

ElementalType PokemonSpeciesFile::getType2() const
{
    return _type2;
}

UnicodeString PokemonSpeciesFile::getContentDirectoryName() const
{
    return "pokemon";
}

UnicodeString PokemonSpeciesFile::getFileTypePrefix() const
{
    return "pkmn";
}

void PokemonSpeciesFile::performUpdate(CompilerBinaryWriter& writer,
                                       const KristallBin& kristallBin)
{
    XmlDocument xml(openSourceXml());

    writer.writeString(xml.firstChild("Name")->getValue().getString());
    writer.writeString(xml.firstChild("Species")->getValue().getString());
    writer.writeString(xml.firstChild("Pokedex")->getValue().getString());

    // Types
    ElementalType type1(xml.firstChild("Type1")->getValue().getElementalType());
    if (type1 == ElementalType::None)
    {
        ExecutionException error("A pokemon must have a defined first type (any type other than 'None').");
        throw error;
    }
    writer.writeElementalType(type1);
    _type1 = type1;
    ElementalType type2(ElementalType::None);
    if (xml.hasChild("Type2"))
    {
        type2 = xml.firstChild("Type2")->getValue().getElementalType();
    }
    if (type1 == type2)
    {
        ExecutionException error("A pokemon may not have for their second type the same as their first type.");
        throw error;
    }
    writer.writeElementalType(type2);
    _type2 = type2;

    // Abilities
    std::vector<UnicodeString> abilities(UnicodeStrings::split(xml.firstChild("Abilities")->getValue().getString(),
                                                               ','));
    PokemonAbility ability1(static_cast<PokemonAbility>(Enums::parse<PokemonAbility>(abilities[0])));
    PokemonAbility ability2(PokemonAbility::None);
    if (abilities.size() == 2)
    {
        ability2 = static_cast<PokemonAbility>(Enums::parse<PokemonAbility>(abilities[1]));
    }

    if (ability1 == PokemonAbility::None)
    {
        ExecutionException error("A pokemon must have a defined first ability (any ability other than 'None').");
        throw error;
    }
    if (ability1 == ability2)
    {
        ExecutionException error("A pokemon may not have their second ability the same as their first ability.");
        throw error;
    }
    writer.writeUInt16(static_cast<uint16_t>(ability1));
    writer.writeUInt16(static_cast<uint16_t>(ability2));

    // Base stats
    std::vector<UnicodeString> baseStats(UnicodeStrings::split(xml.firstChild("BaseStats")->getValue().getString(),
                                                               ','));
    if (baseStats.size() != 6)
    {
        ExecutionException error("Base stats must be a comma delineated list of six values.");
        throw error;
    }
    int64_t rawStatValue = 0;
    for (size_t index = 0; index < 6; ++index)
    {
        rawStatValue = UnicodeStrings::toInt64(baseStats[index]);
        if (rawStatValue < 0 || rawStatValue > UINT8_MAX)
        {
            ExecutionException error("Base stats must be within the range of a uint8_t.");
            throw error;
        }
        writer.writeUInt8(static_cast<uint8_t>(rawStatValue));
    }

    // Common properties
    if (xml.hasChild("BaseFriendship"))
    {
        writer.writeUInt8(xml.firstChild("BaseFriendship")->getValue().getUInt8());
    }
    else
    {
        writer.writeUInt8(70);
    }

    writer.writeUInt16(xml.firstChild("CatchRate")->getValue().getUInt16());
    writer.writeUInt8(Enums::parse<PokemonExperienceGroup>(xml.firstChild("ExperienceGroup")->getValue().getString()));
    writer.writeUInt16(xml.firstChild("BaseEXPYield")->getValue().getUInt16());

    // EV yields
    std::vector<UnicodeString> evYields(UnicodeStrings::split(xml.firstChild("EffortValueYield")->getValue().getString(),
                                                               ','));
    if (evYields.size() != 6)
    {
        ExecutionException error("EffortValueYield must be a comma delineated list of six values.");
        throw error;
    }
    for (size_t index = 0; index < 6; ++index)
    {
        rawStatValue = UnicodeStrings::toInt64(evYields[index]);
        if (rawStatValue < 0 || rawStatValue > UINT8_MAX)
        {
            ExecutionException error("EV yield must be within the range of a uint8_t.");
            throw error;
        }
        writer.writeUInt8(static_cast<uint8_t>(rawStatValue));
    }

    // Common properties
    writer.writeUInt8(Enums::parse<PokemonGenderRatio>(xml.firstChild("GenderRatio")->getValue().getString()));

    // Learnset
    XmlElementPtr learnset(xml.firstChild("Learnset"));
    uint64_t nLearnsetEntries = learnset->countChildren();
    if (nLearnsetEntries > UINT16_MAX)
    {
        ExecutionException error("There are too many moves defined for this learnset.");
        throw error;
    }
    writer.writeUInt16(static_cast<uint16_t>(nLearnsetEntries));
    XmlElementPtr entry = learnset->firstChild();
    for (size_t index = 0; index < nLearnsetEntries; ++index, entry = entry->nextSibling())
    {
        writer.writeUInt16(Handles::getHandleNo("Moves",
                                                entry->getValue().getString()));

        PokemonMoveAcquisitionMode entryType(static_cast<PokemonMoveAcquisitionMode>(Enums::parse<PokemonMoveAcquisitionMode>(entry->getName().getString())));
        writer.writeUInt8(static_cast<uint8_t>(entryType));

        if (entryType == PokemonMoveAcquisitionMode::Leveling)
        {
            int32_t level(entry->getAttribute("Level")->getInt32());
            if (level < 1 || level > 100)
            {
                ExecutionException error("The acquisition level of a learnset entry must be a number between 1-100, inclusive.");
                throw error;
            }
            writer.writeInt32(level);
        }
        else if (entryType == PokemonMoveAcquisitionMode::MoveMachineHM ||
                 entryType == PokemonMoveAcquisitionMode::MoveMachineTM)
        {
            uint32_t moveMachineItemNo = Handles::getHandleNo("Items",
                                                            entry->getAttribute("Item")->getString());
            ItemFile* moveMachine(kristallBin.askFor<ItemFile>(moveMachineItemNo));
            if (moveMachine == nullptr)
            {
                ExecutionException error("Unable to locate the specified move machine.");
                throw error;
            }
            if (moveMachine->getItemType() != ItemType::MoveMachine)
            {
                ExecutionException error("The referenced item is not actually a move machine.");
                throw error;
            }
            if (moveMachine->getIsHm() != (entryType == PokemonMoveAcquisitionMode::MoveMachineHM))
            {
                ExecutionException error("The referenced move machine is not the appropriate type of move machine.");
                throw error;
            }

            writer.writeInt32(moveMachineItemNo);
        }
        else if (entryType == PokemonMoveAcquisitionMode::Egg)
        {
            if (entry->hasAttribute("IfFatherHolds"))
            {
                uint32_t heldItemNo(Handles::getHandleNo("Items",
                                                       entry->getAttribute("IfFatherHolds")->getString()));
                ItemFile* heldItem = kristallBin.askFor<ItemFile>(heldItemNo);
                if (heldItem == nullptr)
                {
                    ExecutionException error("The requested held item was unable to be located.");
                    throw error;
                }

                if (!heldItem->canBeHeld())
                {
                    ExecutionException error("The requested held item cannot be held due to item hold constraints.");
                    throw error;
                }

                writer.writeUInt32(heldItemNo);
            }
            else
            {
                writer.writeInt32(-1);
            }
        }
        else
        {
            writer.writeInt32(0);
        }
    }

    // Common properties
    writer.writeBool(xml.hasChild("IsLarge"));
    writer.writeUInt8(Enums::parse<PokemonSpeciesColor>(xml.firstChild("Color")->getValue().getString()));
    writer.writeFloat(xml.firstChild("Weight")->getValue().getFloat());
    writer.writeFloat(xml.firstChild("Height")->getValue().getFloat());

    // Egg Groups
    std::vector<UnicodeString> eggGroups(UnicodeStrings::split(xml.firstChild("EggGroup")->getValue().getString(),
                                                               ','));
    PokemonEggGroup eggGroup1(static_cast<PokemonEggGroup>(Enums::parse<PokemonEggGroup>(eggGroups[0])));
    PokemonEggGroup eggGroup2(PokemonEggGroup::None);
    if (eggGroups.size() == 2)
    {
        eggGroup2 = static_cast<PokemonEggGroup>(Enums::parse<PokemonEggGroup>(eggGroups[1]));
    }

    if (eggGroup1 == PokemonEggGroup::None)
    {
        ExecutionException error("A pokemon must have a defined first egg group (any group other than 'None').");
        throw error;
    }
    if (eggGroup1 == eggGroup2)
    {
        ExecutionException error("A pokemon may not have their second egg group the same as their first egg group.");
        throw error;
    }
    writer.writeUInt8(static_cast<uint8_t>(eggGroup1));
    writer.writeUInt8(static_cast<uint8_t>(eggGroup2));
    writer.writeUInt8(xml.firstChild("EggCycles")->getValue().getUInt8());

    // Evolutions
    uint8_t nEvolutions = xml.firstChild("Evolutions")->countChildren();
    writer.writeUInt8(nEvolutions);
    writer.writeUInt16(xml.firstChild("Evolutions")->getAttribute("BaseSpecies")->getUInt16());
    writer.writeBool(xml.firstChild("Evolutions")->hasAttribute("Incense"));
    if (xml.firstChild("Evolutions")->hasAttribute("Incense"))
    {
        uint32_t itemNo = Handles::getHandleNo("Items", xml.firstChild("Evolutions")->getAttribute("Incense")->getString());
        ItemFile* incense = kristallBin.askFor<ItemFile>(itemNo);
        if (incense == nullptr)
        {
            ExecutionException error("Could not locate the specified breeding incense item.");
            throw error;
        }
        if (!incense->canBeHeld())
        {
            ExecutionException error("The specified breeding incense cannot be held.");
            throw error;
        }
        writer.writeUInt32(itemNo);
        writer.writeUInt16(xml.firstChild("Evolutions")->getAttribute("IncenseBaby")->getUInt16());
    }
    else
    {
        writer.writeUInt32(0);
        writer.writeUInt16(0);
    }
    XmlElementPtr evolutionNode = xml.firstChild("Evolutions")->firstChild();
    while (evolutionNode != nullptr)
    {
        writer.writeUInt16(evolutionNode->getAttribute("Species")->getUInt16());
        PokemonEvolutionStimulus stimulus(Enums::parseKeep<PokemonEvolutionStimulus>(evolutionNode->getName().getString()));
        writer.writeUInt8(static_cast<uint8_t>(stimulus));

        if (stimulus == PokemonEvolutionStimulus::Leveling)
        {
            writer.writeUInt8(evolutionNode->getValue().getUInt8Level());
        }
        else if (stimulus == PokemonEvolutionStimulus::ItemUse)
        {
            writer.writeUInt32(Handles::getHandleNo("Items", evolutionNode->getValue().getString()));
        }
        else if (stimulus == PokemonEvolutionStimulus::Friendship)
        {
            uint8_t timesOfDayTriggered(0);
            if (!evolutionNode->getValue().getString().isEmpty())
            {
                timesOfDayTriggered = Enums::parseFlags<TimeOfDayFlags>(evolutionNode->getValue().getString());
            }

            if (timesOfDayTriggered == 0)
            {
                timesOfDayTriggered = static_cast<uint8_t>(TimeOfDayFlags::Morning |
                                                         TimeOfDayFlags::Day |
                                                         TimeOfDayFlags::Night);
            }

            writer.writeUInt8(timesOfDayTriggered);
        }
        else if (stimulus == PokemonEvolutionStimulus::LuaFunction)
        {
            if (evolutionNode->getValue().getString().isEmpty())
            {
                ExecutionException error("An evolution triggered by a custom LuaFunction must have a Lua function.");
                throw error;
            }
            writer.writeLua(evolutionNode->getValue().getString());
        }

        evolutionNode = evolutionNode->nextSibling();
    }

    // WildHeldItems
    int64_t oneRarityWildHeldItem = getWildHeldItem(kristallBin, xml.firstChild("WildHeldItems"), 1);
    int64_t fiveRarityWildHeldItem = getWildHeldItem(kristallBin, xml.firstChild("WildHeldItems"), 5);
    int64_t fiftyRarityWildHeldItem = getWildHeldItem(kristallBin, xml.firstChild("WildHeldItems"), 50);
    int64_t hundredRarityWildHeldItem = getWildHeldItem(kristallBin, xml.firstChild("WildHeldItems"), 100);
    if (hundredRarityWildHeldItem >= 0 && (oneRarityWildHeldItem >= 0 ||
                                          fiveRarityWildHeldItem >= 0 ||
                                          fiftyRarityWildHeldItem >= 0))
    {
        ExecutionException error("A Pokemon with a 100% chance wild held item cannot have any other possible hold items.");
        throw error;
    }
    writer.writeBool(oneRarityWildHeldItem >= 0);
    if (oneRarityWildHeldItem >= 0)
    {
        writer.writeUInt32(static_cast<uint32_t>(oneRarityWildHeldItem));
    }
    else
    {
        writer.writeUInt32(0);
    }
    writer.writeBool(fiveRarityWildHeldItem >= 0);
    if (fiveRarityWildHeldItem >= 0)
    {
        writer.writeUInt32(static_cast<uint32_t>(fiveRarityWildHeldItem));
    }
    else
    {
        writer.writeUInt32(0);
    }
    writer.writeBool(fiftyRarityWildHeldItem >= 0);
    if (fiftyRarityWildHeldItem >= 0)
    {
        writer.writeUInt32(static_cast<uint32_t>(fiftyRarityWildHeldItem));
    }
    else
    {
        writer.writeUInt32(0);
    }
    writer.writeBool(hundredRarityWildHeldItem >= 0);
    if (hundredRarityWildHeldItem >= 0)
    {
        writer.writeUInt32(static_cast<uint32_t>(hundredRarityWildHeldItem));
    }
    else
    {
        writer.writeUInt32(0);
    }

    // Formes
    uint64_t nFormesRaw = (xml.hasChild("Formes") ? xml.firstChild("Formes")->countChildren() : 0);
    if (nFormesRaw > 255)
    {
        ExecutionException error("A pokemon may have, at max, 255 unique formes.");
        throw error;
    }
    uint8_t nFormes = static_cast<uint8_t>(nFormesRaw);
    writer.writeUInt8(nFormes);
    if (nFormes > 0)
    {
        XmlElementPtr currentForme = xml.firstChild("Formes")->firstChild();
        bool hasDefault = false;
        while (currentForme != nullptr)
        {
            writer.writeString(currentForme->firstChild("Name")->getValue().getString());
            bool isFormeDefault(currentForme->hasChild("Default"));
            if (isFormeDefault && hasDefault)
            {
                ExecutionException error("A pokemon may only have one default forme specified.");
                throw error;
            }
            if (isFormeDefault)
            {
                hasDefault = true;
            }
            writer.writeBool(isFormeDefault);

            bool isBattleOnly(currentForme->hasChild("BattleOnly"));
            if (isFormeDefault && isBattleOnly)
            {
                ExecutionException error("A pokemon's default forme may not be specified as a battle-only forme.");
                throw error;
            }
            writer.writeBool(isBattleOnly);

            if (currentForme->hasChild("Type1"))
            {
                ElementalType formeType1(currentForme->firstChild("Type1")->getValue().getElementalType());
                if (formeType1 == ElementalType::None)
                {
                    ExecutionException error("A pokemon's forme must have a defined first type (any type other than 'None').");
                    throw error;
                }
                writer.writeElementalType(formeType1);

                ElementalType formeType2(ElementalType::None);
                if (currentForme->hasChild("Type2"))
                {
                    formeType2 = currentForme->firstChild("Type2")->getValue().getElementalType();
                }
                if (formeType1 == formeType2)
                {
                    ExecutionException error("A pokemon's forme may not have for their second type the same as their first type.");
                    throw error;
                }
                writer.writeElementalType(formeType2);
            }
            else
            {
                writer.writeElementalType(type1);
                writer.writeElementalType(type2);
            }

            if (currentForme->hasChild("Abilities"))
            {
                std::vector<UnicodeString> formeAbilities(UnicodeStrings::split(currentForme->firstChild("Abilities")->getValue().getString(),
                                                                           ','));
                PokemonAbility formeAbility1(static_cast<PokemonAbility>(Enums::parse<PokemonAbility>(formeAbilities[0])));
                PokemonAbility formeAbility2(PokemonAbility::None);
                if (formeAbilities.size() == 2)
                {
                    formeAbility2 = static_cast<PokemonAbility>(Enums::parse<PokemonAbility>(formeAbilities[1]));
                }

                if (formeAbility1 == PokemonAbility::None)
                {
                    ExecutionException error("A pokemon's forme must have a defined first ability (any ability other than 'None').");
                    throw error;
                }
                if (formeAbility1 == formeAbility2)
                {
                    ExecutionException error("A pokemon's forme may not have their second ability the same as their first ability.");
                    throw error;
                }
                writer.writeUInt16(static_cast<uint16_t>(formeAbility1));
                writer.writeUInt16(static_cast<uint16_t>(formeAbility2));
            }
            else
            {
                writer.writeUInt16(static_cast<uint16_t>(ability1));
                writer.writeUInt16(static_cast<uint16_t>(ability2));
            }

            if (currentForme->hasChild("BaseStats"))
            {
                std::vector<UnicodeString> formeBaseStats(UnicodeStrings::split(currentForme->firstChild("BaseStats")->getValue().getString(),
                                                               ','));
                if (formeBaseStats.size() != 6)
                {
                    ExecutionException error("A pokemon's forme's base stats must be a comma delineated list of six values.");
                    throw error;
                }

                for (size_t index = 0; index < 6; ++index)
                {
                    rawStatValue = UnicodeStrings::toInt64(formeBaseStats[index]);
                    if (rawStatValue < 0 || rawStatValue > UINT8_MAX)
                    {
                        ExecutionException error("A pokemon's forme's base stats must be within the range of a uint8_t.");
                        throw error;
                    }
                    writer.writeUInt8(static_cast<uint8_t>(rawStatValue));
                }
            }
            else
            {
                for (size_t index = 0; index < 6; ++index)
                {
                    rawStatValue = UnicodeStrings::toInt64(baseStats[index]);
                    if (rawStatValue < 0 || rawStatValue > UINT8_MAX)
                    {
                        ExecutionException error("A pokemon's base stats must be within the range of a uint8_t.");
                        throw error;
                    }
                    writer.writeUInt8(static_cast<uint8_t>(rawStatValue));
                }
            }

            currentForme = currentForme->nextSibling();
        }

        if (!hasDefault)
        {
            ExecutionException error("A pokemon who has multiple formes must have a default forme specified.");
            throw error;
        }
    }
}

void PokemonSpeciesFile::populateData(CompilerBinaryReader& reader)
{
    reader.readString();    // Name
    reader.readString();    // Species
    reader.readString();    // Pokedex

    _type1 = reader.readElementalType();
    _type2 = reader.readElementalType();
}

int64_t PokemonSpeciesFile::getWildHeldItem(const KristallBin& kristallBin,
                                          XmlElementPtr node,
                                          uint8_t rarity) const
{
    XmlElementPtr currentNode = node->firstChild();
    while (currentNode != nullptr)
    {
        if (currentNode->getValue().getUInt8() == rarity)
        {
            uint32_t itemNo = Handles::getHandleNo("Items", currentNode->getName().getString());
            ItemFile* itemFile = kristallBin.askFor<ItemFile>(itemNo);
            if (itemFile == nullptr)
            {
                ExecutionException error("Could not locate the specified wild held item.");
                throw error;
            }

            if (!itemFile->canBeHeld())
            {
                ExecutionException error("The specified wild held item cannot be held.");
                throw error;
            }

            return (int64_t)itemNo;
        }

        currentNode = currentNode->nextSibling();
    }

    return -1;
}

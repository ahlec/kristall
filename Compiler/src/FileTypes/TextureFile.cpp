#include "KristallCompiler.hpp"
#include "CompilerVersions.h"

//////////////////////////////
// VERSIONING
//////////////////////////////
uint64_t TextureFile::getFileTypeVersion() const
{
    return COMPILER_VERSION_FILE_TEXTURE;
}

/////////////////////////////
// CLASS METHODS
/////////////////////////////
TextureFile::TextureFile(uint32_t textureNo) : File(UnicodeStrings::fromInt64(textureNo)), _textureNo(textureNo)
{

}

uint32_t TextureFile::getUId() const
{
    return _textureNo;
}

UnicodeString TextureFile::getContentDirectoryName() const
{
    return "DOESNTEXIST";
}

UnicodeString TextureFile::getFileTypePrefix() const
{
    return "tex";
}

void TextureFile::performUpdate(CompilerBinaryWriter& writer, const KristallBin& kristallBin)
{
    addSourceFilename(TextureManager::getTextureFilename(_textureNo));
    SDL_Surface* surface(TextureManager::loadTexture(_textureNo));
    writer.writeTexture(surface);
    SDL_FreeSurface(surface);
}

bool TextureFile::hasSourceXml() const
{
    return false;
}

#include "KristallCompiler.hpp"
#include "CompilerVersions.h"

//////////////////////////////
// VERSIONING
//////////////////////////////
uint64_t FrameFile::getFileTypeVersion() const
{
    return COMPILER_VERSION_FILE_FRAME;
}

/////////////////////////////
// CLASS METHODS
/////////////////////////////
FrameFile::FrameFile(UnicodeString handle) : File(handle)
{

}

uint32_t FrameFile::getUId() const
{
    return Handles::getHandleNo("Frames", getHandle());
}

UnicodeString FrameFile::getContentDirectoryName() const
{
    return "gui/frames";
}

UnicodeString FrameFile::getFileTypePrefix() const
{
    return "fr";
}

void FrameFile::performUpdate(CompilerBinaryWriter& writer, const KristallBin& kristallBin)
{
    XmlDocument xml(openSourceXml());

    writer.writeString(xml.firstChild("Handle")->getValue().getString());
    writer.writeUInt32(xml.firstChild("TopLeft")->getValue().getUInt32());
    writer.writeUInt32(xml.firstChild("Top")->getValue().getUInt32());
    writer.writeUInt32(xml.firstChild("TopRight")->getValue().getUInt32());
    writer.writeUInt32(xml.firstChild("Left")->getValue().getUInt32());
    writer.writeUInt32(xml.firstChild("Center")->getValue().getUInt32());
    writer.writeUInt32(xml.firstChild("Right")->getValue().getUInt32());
    writer.writeUInt32(xml.firstChild("BottomLeft")->getValue().getUInt32());
    writer.writeUInt32(xml.firstChild("Bottom")->getValue().getUInt32());
    writer.writeUInt32(xml.firstChild("BottomRight")->getValue().getUInt32());
}

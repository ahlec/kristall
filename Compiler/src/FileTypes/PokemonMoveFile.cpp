#include "KristallCompiler.hpp"
#include "CompilerVersions.h"
#include "Pokemon/PokemonMoveDamageCategory.h"
#include "Pokemon/PokemonMoveDetails.h"
#include "Pokemon/PokemonMoveTarget.h"

//////////////////////////////
// VERSIONING
//////////////////////////////
uint64_t PokemonMoveFile::getFileTypeVersion() const
{
    return COMPILER_VERSION_FILE_POKEMONMOVE;
}

/////////////////////////////
// CLASS METHODS
/////////////////////////////
PokemonMoveFile::PokemonMoveFile(UnicodeString handle) : File(handle)
{

}

uint32_t PokemonMoveFile::getUId() const
{
    return Handles::getHandleNo("Moves", getHandle());
}

UnicodeString PokemonMoveFile::getContentDirectoryName() const
{
    return "moves";
}

UnicodeString PokemonMoveFile::getFileTypePrefix() const
{
    return "mv";
}

void PokemonMoveFile::performUpdate(CompilerBinaryWriter& writer,
                                    const KristallBin& kristallBin)
{
    XmlDocument xml(openSourceXml());

    writer.writeString(xml.firstChild("Name")->getValue().getString());
    writer.writeString(xml.firstChild("Description")->getValue().getString());
    writer.writeElementalType(xml.firstChild("Type")->getValue().getElementalType());
    if (xml.firstChild("Type")->hasAttribute("IgnoreType"))
    {
        writer.writeBool(xml.firstChild("Type")->getAttribute("IgnoreType")->getBool());
    }
    else
    {
        writer.writeBool(false);
    }

    writer.writeUInt8(Enums::parse<PokemonMoveDamageCategory>(xml.firstChild("DamageCategory")->getValue().getString()));

    writer.writeUInt8(xml.firstChild("BasePP")->getValue().getUInt8());
    if (xml.hasChild("MoveMachine"))
    {
        bool isHM = (xml.firstChild("MoveMachine")->getAttribute("Type")->getString() == "HM");
        bool isTM = (xml.firstChild("MoveMachine")->getAttribute("Type")->getString() == "TM");
        if (isHM == isTM)
        {
            ExecutionException error("An invalid specification of move machine types was given for the pokemon move.");
            throw error;
        }
        writer.writeBool(isHM);
        writer.writeBool(isTM);
        writer.writeUInt16(xml.firstChild("MoveMachine")->getValue().getUInt16());
    }
    else
    {
        writer.writeBool(false);
        writer.writeBool(false);
        writer.writeUInt16(0);
    }

    bool hasBasePower = xml.hasChild("BasePower");
    writer.writeBool(hasBasePower);
    if (hasBasePower)
    {
        writer.writeUInt8(xml.firstChild("BasePower")->getValue().getUInt8());
    }
    else
    {
        writer.writeUInt8(0);
    }

    bool hasAccuracy = xml.hasChild("Accuracy");
    writer.writeBool(hasAccuracy);
    if (hasAccuracy)
    {
        uint8_t accuracy(xml.firstChild("Accuracy")->getValue().getUInt8());
        if (accuracy > 100)
        {
            ExecutionException error("Accuracy for a pokemon move may not be a value greater than 100.");
            throw error;
        }
        writer.writeUInt8(accuracy);
    }
    else
    {
        writer.writeUInt8(0);
    }

    if (xml.hasChild("Priority"))
    {
        int8_t priority(xml.firstChild("Priority")->getValue().getInt8());
        if (priority < -8 || priority > 8)
        {
            ExecutionException error("Priority for a pokemon move may not be outside the inclusive range of -8 to +8.");
            error.addData("priority", toString(priority));
            throw error;
        }
        writer.writeInt8(priority);
    }
    else
    {
        writer.writeInt8(0);
    }

    uint32_t moveDetails = 0;
    XmlElementPtr detailNode = xml.firstChild("Details")->firstChild();
    while (detailNode != nullptr)
    {
        moveDetails |= Enums::parse<PokemonMoveDetails>(detailNode->getName().getString());
        detailNode = detailNode->nextSibling();
    }
    writer.writeUInt32(moveDetails);

    writer.writeUInt8(Enums::parse<PokemonMoveTarget>(xml.firstChild("Targets")->getValue().getString()));

    writer.writeLua(xml.firstChild("SetupCode")->getValue().getString());
    writer.writeLua(xml.firstChild("ExecutionCode")->getValue().getString());

    bool isUsableOutsideBattle = xml.hasChild("OutsideBattleUse");
    writer.writeBool(isUsableOutsideBattle);
    if (isUsableOutsideBattle)
    {
        if (xml.firstChild("OutsideBattleUse")->hasAttribute("HasTarget"))
        {
            writer.writeBool(xml.firstChild("OutsideBattleUse")->getAttribute("HasTarget")->getBool());
        }
        else
        {
            writer.writeBool(false);
        }

        writer.writeLua(xml.firstChild("OutsideBattleUse")->firstChild("CanUseCode")->getValue().getString());
        writer.writeLua(xml.firstChild("OutsideBattleUse")->firstChild("UseCode")->getValue().getString());
    }
    else
    {
        writer.writeBool(false);
        writer.writeLua("");
        writer.writeLua("");
    }
}

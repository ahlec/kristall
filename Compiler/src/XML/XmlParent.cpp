#include "KristallCompiler.hpp"
using namespace rapidxml;

XmlParent::XmlParent(XmlDocument* document) : _doc(document)
{
}

XmlParent::~XmlParent()
{
}

void XmlParent::setNode(xml_node<UChar>* node)
{
    _node = node;
}

uint64_t XmlParent::countChildren(Predicate<XmlElement> predicate) const
{
    uint64_t total = 0;

    xml_node<UChar>* child = _node->first_node();
    while (child != nullptr)
    {
        if (child->type() == node_element)
        {
            if (predicate != nullptr)
            {
                XmlElement childElement(child, _doc);
                if (predicate(childElement))
                {
                    ++total;
                }
            }
            else
            {
                ++total;
            }
        }
        child = child->next_sibling();
    }

    return total;
}

uint64_t XmlParent::countChildren(UnicodeString name) const
{
    uint64_t total = 0;

    xml_node<UChar>* child = _node->first_node(name.getTerminatedBuffer(), name.length());
    while (child != nullptr)
    {
        ++total;
        child = child->next_sibling(name.getTerminatedBuffer(), name.length());
    }

    return total;
}

bool XmlParent::hasChild(Predicate<XmlElement> predicate) const
{
    xml_node<UChar>* child = _node->first_node();
    while (child != nullptr)
    {
        if (child->type() == node_element)
        {
            if (predicate != nullptr)
            {
                XmlElement childElement(child, _doc);
                if (predicate(childElement))
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        child = child->next_sibling();
    }

    return false;
}

bool XmlParent::hasChild(UnicodeString name) const
{
    return (_node->first_node(name.getTerminatedBuffer(), name.length()) != nullptr);
}

XmlElementPtr XmlParent::firstChild(Predicate<XmlElement> predicate) const
{
    xml_node<UChar>* child = _node->first_node();
    while (child != nullptr)
    {
        if (child->type() == node_element)
        {
            if (predicate != nullptr)
            {
                XmlElement childElement(child, _doc);
                if (predicate(childElement))
                {
                    return XmlElementPtr(new XmlElement(child, _doc));
                }
            }
            else
            {
                return XmlElementPtr(new XmlElement(child, _doc));
            }
        }
        child = child->next_sibling();
    }

    return nullptr;
}

XmlElementPtr XmlParent::firstChild(UnicodeString name) const
{
    xml_node<UChar>* child = _node->first_node(name.getTerminatedBuffer(), name.length());
    if (child == nullptr)
    {
        return nullptr;
    }

    return XmlElementPtr(new XmlElement(child, _doc));
}

XmlElementPtr XmlParent::appendChild(UnicodeString name)
{
    xml_node<UChar>* child = _node->document()->allocate_node(node_element, getString(name));
    _node->append_node(child);
    return XmlElementPtr(new XmlElement(child, _doc));
}

void XmlParent::empty()
{
    _node->remove_all_nodes();
}

bool XmlParent::hasAttributes() const
{
    return (_node->first_attribute() != nullptr);
}

bool XmlParent::hasAttribute(UnicodeString name) const
{
    return (_node->first_attribute(name.getTerminatedBuffer(), name.length()) != nullptr);
}

XmlDataPtr XmlParent::getAttribute(UnicodeString name) const
{
    xml_attribute<UChar>* attribute = _node->first_attribute(name.getTerminatedBuffer(),
                                                             name.length());
    if (attribute == nullptr)
    {
        return nullptr;
    }

    return XmlDataPtr(new XmlData(attribute, false));
}

void XmlParent::setAttribute(UnicodeString name, XmlValue value)
{
    xml_attribute<UChar>* attribute = _node->first_attribute(name.getTerminatedBuffer(), name.length());
    bool isAttributeNew(attribute == nullptr);
    if (attribute == nullptr)
    {
        attribute = _node->document()->allocate_attribute(getString(name));
    }

    attribute->value(getString(value.getStringValue()));
    if (isAttributeNew)
    {
        _node->append_attribute(attribute);
    }
}

XmlElementPtr XmlParent::appendChildWithValue(UnicodeString name, XmlValue value)
{
    XmlElementPtr childNode = appendChild(name);
    childNode->setValue(value);
    return childNode;
}

UChar* XmlParent::getString(UnicodeString str)
{
    return _doc->getOrAllocateString(str);
}

#include "KristallCompiler.hpp"
using namespace rapidxml;

XmlData::XmlData(xml_base<UChar>* xmlBase, bool isName) : _xmlBase(xmlBase), _isName(isName)
{
}

UnicodeString XmlData::getString() const
{
    return UnicodeString(_isName ? _xmlBase->name() : _xmlBase->value());
}

uint8_t XmlData::getUInt8() const
{
    int64_t fullValue = getInt64();
    if (fullValue < 0 || fullValue > UINT8_MAX)
    {
        ExecutionException error("Data type overflow occurred when reading from XML data.");
        error.addData("fullValue", toString(fullValue));
        error.addData("destination", "uint8_t");
        throw error;
    }

    return static_cast<uint8_t>(fullValue);
}

uint8_t XmlData::getUInt8Level() const
{
    int64_t fullValue = getInt64();
    if (fullValue < 1 || fullValue > 100)
    {
        ExecutionException error("Data type overflow occurred when reading from XML data.");
        error.addData("fullValue", toString(fullValue));
        error.addData("destination", "uint8_tLevel");
        throw error;
    }

    return static_cast<uint8_t>(fullValue);
}

uint16_t XmlData::getUInt16() const
{
    int64_t fullValue = getInt64();
    if (fullValue < 0 || fullValue > UINT16_MAX)
    {
        ExecutionException error("Data type overflow occurred when reading from XML data.");
        error.addData("fullValue", toString(fullValue));
        error.addData("destination", "uint16_t");
        throw error;
    }

    return static_cast<uint16_t>(fullValue);
}

uint32_t XmlData::getUInt32() const
{
    int64_t fullValue = getInt64();
    if (fullValue < 0 || fullValue > UINT32_MAX)
    {
        ExecutionException error("Data type overflow occurred when reading from XML data.");
        error.addData("fullValue", toString(fullValue));
        error.addData("destination", "uint32_t");
        throw error;
    }

    return static_cast<uint32_t>(fullValue);
}

int8_t XmlData::getInt8() const
{
    int64_t fullValue = getInt64();
    if (fullValue < INT8_MIN || fullValue > INT8_MAX)
    {
        ExecutionException error("Data type overflow occurred when reading from XML data.");
        error.addData("fullValue", toString(fullValue));
        error.addData("destination", "int8_t");
        throw error;
    }

    return static_cast<int8_t>(fullValue);
}

int16_t XmlData::getInt16() const
{
    int64_t fullValue = getInt64();
    if (fullValue < INT16_MIN || fullValue > INT16_MAX)
    {
        ExecutionException error("Data type overflow occurred when reading from XML data.");
        error.addData("fullValue", toString(fullValue));
        error.addData("destination", "int16_t");
        throw error;
    }

    return static_cast<int16_t>(fullValue);
}

int32_t XmlData::getInt32() const
{
    int64_t fullValue = getInt64();
    if (fullValue < INT32_MIN || fullValue > INT32_MAX)
    {
        ExecutionException error("Data type overflow occurred when reading from XML data.");
        error.addData("fullValue", toString(fullValue));
        error.addData("destination", "int32_t");
        throw error;
    }

    return static_cast<int32_t>(fullValue);
}

int64_t XmlData::getInt64() const
{
    static NumberFormat* numberFormatter = nullptr;
    if (numberFormatter == nullptr)
    {
        UErrorCode errorCode = U_ZERO_ERROR;
        numberFormatter = NumberFormat::createInstance(errorCode);
    }

    Formattable formattable;
    ParsePosition position;
    UnicodeString str(getString());
    numberFormatter->parse(str, formattable, position);
    return formattable.getInt64();
}

float XmlData::getFloat() const
{
    static NumberFormat* numberFormatter = nullptr;
    if (numberFormatter == nullptr)
    {
        UErrorCode errorCode = U_ZERO_ERROR;
        numberFormatter = NumberFormat::createInstance(errorCode);
    }

    Formattable formattable;
    ParsePosition position;
    UnicodeString str(getString());
    numberFormatter->parse(str, formattable, position);
    return (float)formattable.getDouble();
}

bool XmlData::getBool() const
{
    UnicodeString str(getString().toLower());

    return (str == "true");
}

Direction XmlData::getDirection() const
{
    UnicodeString str(getString().toLower());

    if (str == "north")
    {
        return Direction::North;
    }
    else if (str == "east")
    {
        return Direction::East;
    }
    else if (str == "south")
    {
        return Direction::South;
    }
    else
    {
        return Direction::West;
    }
}

ElementalType XmlData::getElementalType() const
{
    return static_cast<ElementalType>(Enums::parse<ElementalType>(getString()));
}

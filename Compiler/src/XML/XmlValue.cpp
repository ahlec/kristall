#include "KristallCompiler.hpp"

XmlValue::XmlValue(UnicodeString str) : _strValue(str)
{
}

XmlValue::XmlValue(std::string str) : XmlValue(UnicodeString(str.c_str()))
{
}

XmlValue::XmlValue(const char* cStr) : _strValue(cStr)
{
}

XmlValue::XmlValue(uint64_t val) : _strValue(UnicodeStrings::fromInt64(val))
{
}

XmlValue::XmlValue(uint32_t val) : _strValue(UnicodeStrings::fromInt64(val))
{
}

XmlValue::XmlValue(int32_t val) : _strValue(UnicodeStrings::fromInt64(val))
{
}

/*XmlValue::XmlValue(bool value) : _strValue(value ? "True" : "False")
{
}*/

UnicodeString XmlValue::getStringValue() const
{
    return _strValue;
}

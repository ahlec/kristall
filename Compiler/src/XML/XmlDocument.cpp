#include "KristallCompiler.hpp"
#include <rapidxml_print.hpp>
#include <cstring>
using namespace rapidxml;

XmlDocument::XmlDocument(UnicodeString rootElement) : XmlParent(this)
{
    rapidxml::xml_node<UChar>* rootNode = _document.allocate_node(node_element, getOrAllocateString(rootElement));
    _document.append_node(rootNode);
    setNode(rootNode);
}

XmlDocument::XmlDocument(UnicodeString fileName, bool isLoadedExternally) : XmlParent(this), _buffer(nullptr), _bufferLength(0)
{
    std::string fileNameStd(UnicodeStrings::toStdString(fileName));
    UFILE* file = u_fopen(fileNameStd.c_str(), "r", NULL, "UTF-8");
    if (file == nullptr)
    {
        ExecutionException error("Could not load the XML file.");
        error.addData("fileName", toString(fileNameStd.c_str()));
        throw error;
    }

    fseek(u_fgetfile(file), 0, SEEK_END);
    _bufferLength = ftell(u_fgetfile(file));
    u_frewind(file);

    _buffer = new UChar[_bufferLength + 1];
    size_t charactersRead = u_file_read(_buffer, _bufferLength, file);
    _buffer[charactersRead] = 0;

    u_fclose(file);

    if (_buffer[0] == 0xfeff)
    {
        _document.parse<0>(&_buffer[1]);
    }
    else
    {
        _document.parse<0>(_buffer);
    }
    setNode(_document.first_node());
}

XmlDocument::XmlDocument(const XmlDocument& other) : XmlParent(this), _buffer(nullptr),
                                                     _bufferLength(other._bufferLength)
{
    _buffer = new UChar[_bufferLength + 1];
    memcpy(_buffer, other._buffer, sizeof(UChar) * (_bufferLength + 1));

    if (_buffer[0] == 0xfeff)
    {
        _document.parse<0>(&_buffer[1]);
    }
    else
    {
        _document.parse<0>(_buffer);
    }
    setNode(_document.first_node());
}

XmlDocument::~XmlDocument()
{
    if (_buffer != nullptr)
    {
        delete[] _buffer;
        _buffer = nullptr;
    }
}

XmlDocument XmlDocument::load(UnicodeString fileName)
{
    return XmlDocument(fileName, true);
}

void XmlDocument::save(UnicodeString fileName)
{
    std::wstring outputStringWChar;
    rapidxml::print(std::back_inserter(outputStringWChar), _document, 0);
    UnicodeString outputString(outputStringWChar.data());

    std::string stdFileName(UnicodeStrings::toStdString(fileName));
    UFILE* file = u_fopen(stdFileName.c_str(), "w", NULL, "UTF-8");
    UChar bom(0xFEFF);
    u_file_write(&bom, 1, file);
    u_file_write(outputString.getTerminatedBuffer(), outputString.length(), file);
    u_fclose(file);
}

UChar* XmlDocument::getOrAllocateString(UnicodeString str)
{
    return _document.allocate_string(str.getTerminatedBuffer());//, str.length());
    /*if (_allocatedStrings.count(str) == 0)
    {
        _allocatedStrings.insert(std::pair<UnicodeString, UChar*>(str, _document.allocate_string(str.getTerminatedBuffer(), str.length())));
    }*/
    return _allocatedStrings[str];
}

#include "KristallCompiler.hpp"
using namespace rapidxml;

XmlElement::XmlElement(xml_node<UChar>* node, XmlDocument* document) : XmlParent(document), _node(node)
{
    setNode(node);
}

XmlData XmlElement::getName() const
{
    return XmlData(_node, true);
}

XmlData XmlElement::getValue() const
{
    return XmlData(_node, false);
}

size_t XmlElement::getIndex() const
{
    xml_node<UChar>* parent = _node->parent();
    if (parent == nullptr)
    {
        return 0;
    }

    int64_t index = 0;
    xml_node<UChar>* child = parent->first_node();
    while (child != nullptr)
    {
        if (child == _node)
        {
            return index;
        }

        child = child->next_sibling();
        ++index;
    }

    return -1;  // seems to be a pointer error, or mismatch somewhere?
}

bool XmlElement::isEmpty() const
{
    return !hasChild();
}

XmlElementPtr XmlElement::nextSibling(Predicate<XmlElement> predicate) const
{
    xml_node<UChar>* parent = _node->parent();
    if (parent == nullptr)
    {
        return nullptr;
    }

    xml_node<UChar>* sibling = _node->next_sibling();
    while (sibling != nullptr)
    {
        if (predicate != nullptr)
        {
            XmlElement siblingElement(sibling, _doc);
            if (predicate(siblingElement))
            {
                return XmlElementPtr(new XmlElement(sibling, _doc));
            }
        }
        else
        {
            return XmlElementPtr(new XmlElement(sibling, _doc));
        }

        sibling = sibling->next_sibling();
    }

    return nullptr;
}

XmlElementPtr XmlElement::nextSibling(UnicodeString name) const
{
    xml_node<UChar>* parent = _node->parent();
    if (parent == nullptr)
    {
        return nullptr;
    }

    xml_node<UChar>* sibling = _node->next_sibling();
    while (sibling != nullptr)
    {
        if (XmlData(sibling, true).getString() == name)
        {
            return XmlElementPtr(new XmlElement(sibling, _doc));
        }

        sibling = sibling->next_sibling();
    }

    return nullptr;
}

void XmlElement::_setValue(XmlValue value)
{
    _node->value(getString(value.getStringValue()));
}

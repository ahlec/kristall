#include "KristallCompiler.hpp"

namespace Handles
{
    uint32_t getHandleNo(UnicodeString type, UnicodeString handle)
    {
        static XmlDocument handlesXml = XmlDocument::load("handleTranslations.xml");
        return handlesXml.firstChild(type)->firstChild(handle)->getValue().getUInt32();
    }
};

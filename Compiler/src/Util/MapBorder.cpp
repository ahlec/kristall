#include "KristallCompiler.hpp"


static const uint32_t treeSprites[] = { 491, 492, 59, 60, 54, 52, 494, 495, 54, 52, 59, 60, 54, 52, 53, 51 };
static const uint32_t mountainSprites[] = { 150, 150, 150, 150, 153, 153, 152, 152, 154, 151, 154, 151, 158, 155, 157, 156 };
static const uint32_t grassSprites[] = { 5, 6, 7, 8 };

// Index manipulation
inline uint8_t switchRowParity(uint8_t index)
{
    switch (index)
    {
        case 0:
            {
                return 2;
            }
        case 1:
            {
                return 3;
            }
        case 2:
            {
                return 0;
            }
        case 3:
            {
                return 1;
            }
        case 8:
            {
                return 10;
            }
        case 9:
            {
                return 11;
            }
        case 10:
            {
                return 8;
            }
        case 11:
            {
                return 9;
            }
        default:
            {
                return index;
            }
    }
}

inline bool isIndexTopBorder(uint8_t index)
{
    return (index == 4 || index == 5 || index == 12 || index == 13);
}

inline bool isIndexLeftBorder(uint8_t index)
{
    return (index == 8 || index == 10 || index == 12 || index == 14);
}

inline bool isIndexRightBorder(uint8_t index)
{
    return (index == 9 || index == 11 || index == 13 || index == 15);
}


inline uint32_t determineMapBorderBackground(MapBorder tileFill, MapBorder north,
                                           MapBorder east, MapBorder west,
                                           uint8_t index)
{
    if (isIndexTopBorder(index))
    {
        switch (tileFill)
        {
            case MapBorder::Trees:
            case MapBorder::OddTrees:
            case MapBorder::Mountain:
            case MapBorder::DoubleTierMountain:
                {
                    if (north == MapBorder::Ocean)
                    {
                        return 540;
                    }

                    return grassSprites[index % 4];
                }
            default:
                {
                    break;
                }
        }
    }
    else if (isIndexLeftBorder(index) || isIndexRightBorder(index))
    {
        switch (tileFill)
        {
            case MapBorder::Trees:
            case MapBorder::OddTrees:
            case MapBorder::Mountain:
            case MapBorder::DoubleTierMountain:
                {
                    switch ((isIndexLeftBorder(index) ? west : east))
                    {
                        case MapBorder::Trees:
                        case MapBorder::OddTrees:
                        case MapBorder::Grass:
                            {
                                return 5;
                            }
                        case MapBorder::Ocean:
                            {
                                return 540;
                            }
                    }
                }
        }
    }

    switch (tileFill)
    {
        case MapBorder::Trees:
        case MapBorder::WhiteFence:
        case MapBorder::OddTrees:
            {
                return grassSprites[index % 4];
            }
        default:
            {
                return 0;
            }
    }
}

inline uint32_t determineMapBorderMain(MapBorder tileFill, uint8_t index)
{
    switch (tileFill)
    {
        case MapBorder::None:
        case MapBorder::Black:
            {
                return 0;
            }
        case MapBorder::Trees:
            {
                return treeSprites[index];
            }
        case MapBorder::Ocean:
            {
                return 540;
            }
        case MapBorder::Mountain:
        case MapBorder::DoubleTierMountain:
            {
                return mountainSprites[index];
            }
        case MapBorder::Grass:
            {
                return grassSprites[index % 4];
            }
        case MapBorder::WhiteFence:
            {
                return 493;
            }
        case MapBorder::OddTrees:
            {
                return treeSprites[switchRowParity(index)];
            }
    }
}



namespace MapBorders
{
    std::vector<uint32_t> getSpriteNos(MapBorderTile tile)
    {
        std::vector<uint32_t> sprites;

        // Determine the index
        uint8_t index(tile.getIndex());

        // Background sprite (if any)
        uint32_t backgroundSpriteNo(determineMapBorderBackground(tile.fill, tile.north,
                                                               tile.east, tile.west,
                                                               index));
        if (backgroundSpriteNo != 0)
        {
            sprites.push_back(backgroundSpriteNo);
        }

        // Main sprite
        sprites.push_back(determineMapBorderMain(tile.fill, index));

        // Finish
        return sprites;
    }
}

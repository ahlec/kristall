#include "KristallCompiler.hpp"
#include "unicode/smpdtfmt.h"

// translates from UTF-16
void fromUTF16(UChar uchar, std::vector<char>& charBuffer)
{
    uint16_t intVal = static_cast<uint16_t>(uchar);
    if (intVal <= 127)
    {
        charBuffer.push_back(static_cast<char>(intVal));
    }
    else if (intVal <= 2047)
    {
        uint16_t char1 = ((intVal >> 6) & 31) | 192;
        charBuffer.push_back(static_cast<char>(char1));
        uint16_t char2 = (intVal & 63) | 128;
        charBuffer.push_back(static_cast<char>(char2));
    }
    else
    {
        ExecutionException error("Encountered a character that is currently unhandled.");
        error.addData("uchar", toString(uchar));
        throw error;
    }
}

namespace UnicodeStrings
{
    std::string toStdString(UnicodeString unicodeString)
    {
        try
        {
            std::vector<char> charBuffer;
            for (uint64_t index = 0; index < unicodeString.length(); ++index)
            {
                fromUTF16(unicodeString.charAt(index), charBuffer);
            }

            std::stringstream stream;
            for (int32_t index = 0; index < charBuffer.size(); ++index)
            {
                stream << charBuffer.at(index);
            }

            return stream.str();
        }
        catch (ExecutionException& ex)
        {
            char* extractBuffer = new char[unicodeString.length()];
            unicodeString.extract(0, unicodeString.length(), extractBuffer, unicodeString.length());
            ex.addData("unicodeString", toString(extractBuffer));// toString(unicodeString));
            throw ex;
        }
    }

    std::vector<UnicodeString> split(UnicodeString unicodeString, UChar delimiter)
    {
        UnicodeString delimiterStr(delimiter);

        std::vector<UnicodeString> segments;
        int32_t currentPosition = 0;
        int32_t index = unicodeString.indexOf(delimiterStr, currentPosition);
        while (index > -1)
        {
            segments.push_back(UnicodeString(unicodeString, currentPosition,
                                             index - currentPosition));
            currentPosition = index + 1;
            index = unicodeString.indexOf(delimiterStr, currentPosition);
        }

        if (currentPosition < unicodeString.length())
        {
            if (currentPosition == 0)
            {
                segments.push_back(unicodeString);
            }
            else
            {
                segments.push_back(UnicodeString(unicodeString, currentPosition,
                                                 unicodeString.length() - currentPosition));
            }
        }

        return segments;
    }

    int64_t toInt64(UnicodeString unicodeString)
    {
        static NumberFormat* numberFormatter = nullptr;
        if (numberFormatter == nullptr)
        {
            UErrorCode errorCode = U_ZERO_ERROR;
            numberFormatter = NumberFormat::createInstance(errorCode);
        }

        Formattable formattable;
        ParsePosition position;
        numberFormatter->parse(unicodeString, formattable, position);
        return formattable.getInt64();
    }

    UnicodeString fromInt64(int64_t value)
    {
        UErrorCode errorCode=  U_ZERO_ERROR;
        NumberFormat* numberFormatter = NumberFormat::createInstance(errorCode);
        numberFormatter->setGroupingUsed(false);
        UnicodeString str;
        str = numberFormatter->format(value, str);
        delete numberFormatter;
        return str;
    }

    UnicodeString fromDouble(double value)
    {
        UErrorCode errorCode=  U_ZERO_ERROR;
        NumberFormat* numberFormatter = NumberFormat::createInstance(errorCode);

        UnicodeString str;
        str = numberFormatter->format(value, str);
        delete numberFormatter;
        return str;
    }

    UnicodeString getCurrentDateTime()
    {
        UErrorCode errorCode(U_ZERO_ERROR);
        SimpleDateFormat formatter(L"EEEE dd MMMM yyyy  HH:mm:ss", errorCode);
        UDate now(Calendar::getNow());
        UnicodeString str;
        FieldPosition fieldPosition(0);
        formatter.format(now, str, fieldPosition);
        return str;
    }

    UnicodeString getCompileDateTime()
    {
        static bool s_isUDateParsed(false);
        static UDate s_compileUDate;
        if (!s_isUDateParsed)
        {
            UErrorCode parseErrorCode(U_ZERO_ERROR);
            SimpleDateFormat parser(L"MMM dd yyyy HH:mm:ss", Locale::getUS(), parseErrorCode);
            UnicodeString compileDateTimeStr(__DATE__ " " __TIME__);
            s_compileUDate = parser.parse(compileDateTimeStr, parseErrorCode);
            s_isUDateParsed = true;
        }

        UErrorCode errorCode(U_ZERO_ERROR);
        SimpleDateFormat formatter(L"EEEE dd MMMM yyyy  HH:mm:ss", errorCode);
        UnicodeString formattedStr;
        FieldPosition fieldPosition(0);
        formatter.format(s_compileUDate, formattedStr, fieldPosition);
        return formattedStr;
    }
}

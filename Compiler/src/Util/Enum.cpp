#include "KristallCompiler.hpp"

static std::map<UnicodeString, std::map<UnicodeString, int64_t>> s_register;

namespace Enums
{
    void _declareEnumValue(UnicodeString enumName, UnicodeString valueName, int64_t baseValue)
    {
        if (s_register.count(enumName) == 0)
        {
            std::string trueEnumName(UnicodeStrings::toStdString(enumName));
            if (trueEnumName.length() < 12)
            {
                // One digit truncation
                trueEnumName = trueEnumName.substr(1);
            }
            else if (trueEnumName.length() < 103)
            {
                // Two digit truncation
                trueEnumName = trueEnumName.substr(2);
            }
            printf("\t'%s'\n", trueEnumName.c_str());
            s_register[enumName] = std::map<UnicodeString, int64_t>();
        }

        s_register[enumName][valueName] = baseValue;
    }

    int64_t _getEnumValue(UnicodeString enumName, UnicodeString valueName)
    {
        if (s_register.count(enumName) == 0)
        {
            ExecutionException error("Attempted to get the value of an unregistered enum.");
            error.addData("enumName", enumName);
            throw error;
        }

        std::map<UnicodeString, int64_t>& registry(s_register[enumName]);
        if (registry.count(valueName) == 0)
        {
            ExecutionException error("The specified enumeration value does not exist.");
            throw error;
        }

        return registry[valueName];
    }

    class FindByValueFunctor
    {
    public:
        FindByValueFunctor(int64_t value) : _value(value)
        {
        }

        bool operator()(std::pair<UnicodeString, int64_t> entry)
        {
            return (entry.second == _value);
        }

    private:
        int64_t _value;
    };

    UnicodeString _getValueName(UnicodeString enumName, int64_t baseValue)
    {
        if (s_register.count(enumName) == 0)
        {
            ExecutionException error("Attempted to get the string representation of an unregistered enum.");
            error.addData("enumName", enumName);
            throw error;
        }

        std::map<UnicodeString, int64_t>& registry(s_register[enumName]);

        auto mapIterator = std::find_if(registry.begin(), registry.end(),
                                        FindByValueFunctor(baseValue));
        if (mapIterator == registry.end())
        {
            ExecutionException error("The specified enumeration value was not defined.");
            error.addData("enumName", enumName);
            error.addData("baseValue", ::toString(baseValue));
            throw error;
        }

        return mapIterator->first;
    }

    bool _isRegisteredEnum(UnicodeString enumName)
    {
        return (s_register.count(enumName) == 1);
    }
};

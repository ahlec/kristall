#include "KristallCompiler.hpp"
#include <sys/types.h>
#include <sys/stat.h>

namespace Files
{
    uint64_t getLastModifiedTimestamp(UnicodeString filename)
    {
        int32_t length(filename.length());
        std::string filenameStd;
        filename.toUTF8String(filenameStd);
        struct stat buf;
        if (stat(filenameStd.c_str(), &buf) != 0)
        {
            ExecutionException error("An error occurred while getting the statistics for a file.");
            error.addData("filename", toString(filenameStd.c_str()));
            error.addData("errno", toString(errno));
            throw error;
        }

        return buf.st_mtime;
    }
};

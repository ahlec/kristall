#include "Util/Directory.h"
#include <sys/stat.h>
#include <unistd.h>

namespace Directory
{
    bool exists(UnicodeString directoryPath)
    {
        std::string stdstrDirectoryPath;
        directoryPath.toUTF8String(stdstrDirectoryPath);
        if (access(stdstrDirectoryPath.c_str(), 0) == 0)
        {
            struct stat status;
            stat(stdstrDirectoryPath.c_str(), &status);

            return ((status.st_mode & S_IFDIR) != 0);
        }

        return false;
    }

    void create(UnicodeString directoryPath)
    {
        std::string stdstrDirectoryPath;
        directoryPath.toUTF8String(stdstrDirectoryPath);
        CreateDirectory(stdstrDirectoryPath.c_str(), NULL);
    }
};

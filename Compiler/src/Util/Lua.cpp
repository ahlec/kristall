#include "KristallCompiler.hpp"
#include "lua.hpp"

int luaDumpVectorWriter(lua_State* lua, const void* chunkData, size_t chunkSize, void* dataParam)
{
    std::vector<char>* charVector = (std::vector<char>*)dataParam;
    const char* chunkDataArray = (const char*)chunkData;
    for (int index = 0; index < chunkSize; ++index)
    {
        charVector->push_back(chunkDataArray[index]);
    }
    return 0;
}

namespace Lua
{
    std::vector<char> compile(UnicodeString luaCode)
    {
        // Prepare and filter based on the parameter
        std::vector<char> compiledCode;
        if (luaCode.length() == 0)
        {
            return compiledCode;
        }
        std::string luaCodeStd(UnicodeStrings::toStdString(luaCode));

        // Initialize Lua
        lua_State* lua = luaL_newstate();

        // Load the code
        int32_t loadReturnValue = luaL_loadstring(lua, luaCodeStd.c_str());
        if (loadReturnValue != 0)
        {
            ExecutionException error("An error occurred when loading the Lua string during compilation.");
            throw error;
        }

        // Read the compiled code
        int32_t dumpReturnValue = lua_dump(lua, luaDumpVectorWriter, &compiledCode);
        if (dumpReturnValue != 0)
        {
            ExecutionException error("An error occurred when dumping the compiled Lua code.");
            throw error;
        }

        // Wrap up
        lua_close(lua);
        return compiledCode;
    }
}

#include "KristallCompiler.hpp"

static std::map<uint32_t, std::vector<RegionMap*>> s_maps;
static bool s_isInitialized(false);

RegionMap* parseRegionMap(UnicodeString fileName)
{
    fileName.remove(fileName.length() - 4, 4);
    RegionMap* regionMap(new RegionMap(fileName));
    if (regionMap->isExteriorOverworld())
    {
        return regionMap;
    }

    delete regionMap;
    return nullptr;
};

namespace RegionMaps
{
    std::vector<RegionMap*> getOutdoorMaps(uint32_t regionNo)
    {
        if (!s_isInitialized)
        {
            std::vector<RegionMap*> maps(Directory::getAllFiles("content\\maps",
                                                                FunctionPtr<RegionMap*, UnicodeString>(parseRegionMap)));
            for (RegionMap* regionMap : maps)
            {
                if (regionMap == nullptr)
                {
                    continue;
                }

                if (s_maps.count(regionMap->getRegionNo()) == 0)
                {
                    s_maps[regionMap->getRegionNo()] = std::vector<RegionMap*>();
                }

                s_maps[regionMap->getRegionNo()].push_back(regionMap);
            }
            s_isInitialized = true;
        }

        if (s_maps.count(regionNo) == 1)
        {
            return s_maps[regionNo];
        }
        else
        {
            return std::vector<RegionMap*>();
        }
    }
};

#include "KristallCompiler.hpp"

CompilerBinaryWriter::CompilerBinaryWriter(FILE* file) : BinaryWriter(file)
{
}

void CompilerBinaryWriter::writeLua(UnicodeString unicodeString)
{
    std::vector<char> compiledCode(Lua::compile(unicodeString));
    uint32_t compiledSize = static_cast<uint32_t>(compiledCode.size());
    writeCharArray(compiledCode.data(), compiledSize);
}

class PngCrushCBWFunctor : public Functor<void, FILE*>
{
public:
    PngCrushCBWFunctor(CompilerBinaryWriter* writer) : _writer(writer)
    {
    }

    virtual void operator()(FILE* crushedFile) override
    {
        fseek(crushedFile, 0, SEEK_END);
        uint64_t fileLength(ftell(crushedFile));
        fseek(crushedFile, 0, SEEK_SET);

        _writer->writeUInt64(fileLength);

        const uint64_t BUFFER_SIZE = 8192;
        char buffer[BUFFER_SIZE];
        uint64_t bytesRead = 0;
        size_t bufferRead = fread(buffer, 1, std::min(BUFFER_SIZE, fileLength), crushedFile);
        while (bytesRead < fileLength)
        {
            if (bufferRead > 0)
            {
                _writer->writeRawBytes(buffer, 1, bufferRead);
                bytesRead += bufferRead;
            }

            if (bytesRead == fileLength)
            {
                break;
            }
            else if (bytesRead > fileLength)
            {
                ExecutionException error("Read too many bytes!");
                throw error;
            }

            bufferRead = fread(buffer, 1, std::min(BUFFER_SIZE, fileLength), crushedFile);
        }
    }

private:
    CompilerBinaryWriter* _writer;
};

void CompilerBinaryWriter::writeTexture(UnicodeString fileName)
{
    PNG::crush(fileName, FunctionPtr<void, FILE*>(new PngCrushCBWFunctor(this)));
}

void CompilerBinaryWriter::writeTexture(SDL_Surface* surface)
{
    // Copy to an image and save
    PNG::save("tmpCBWSFTXT.png", surface);

    // Crush and write
    writeTexture("tmpCBWSFTXT.png");

    // Delete the temp existing file
    remove("tmpCBWSFTXT.png");
}

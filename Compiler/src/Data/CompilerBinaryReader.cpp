#include "KristallCompiler.hpp"

uint32_t getFileSize(FILE* file)
{
    size_t currentPosition = ftell(file);
    fseek(file, 0, SEEK_END);
    uint32_t fileSize = ftell(file);
    fseek(file, currentPosition, SEEK_SET);

    return fileSize;
}

class FileCloseFunctor : public Functor<void>
{
public:
    FileCloseFunctor(FILE* filePointer) : _pointer(filePointer)
    {
    }

    virtual void operator()() override
    {
        if (_pointer != nullptr)
        {
            fclose(_pointer);
            _pointer = nullptr;
        }
    }

private:
    FILE* _pointer;
};

FunctionPtr<void> getFileCloseCallback(FILE* file)
{
    return FunctionPtr<void>(new FileCloseFunctor(file));
    //std::shared_ptr<FileCloseFunctor> functor(new FileCloseFunctor(file));
    //return FunctionPtr<void>(std::shared_ptr<Function<void>>(new FunctorFunction<FileCloseFunctor, void>(functor)));
}

CompilerBinaryReader::CompilerBinaryReader(FILE* file) : BinaryReader(file, FileType::PlayerGame, 1,
                                                                      getFileSize(file),
                                                                      getFileCloseCallback(file))
{
}

CompilerBinaryReader::CompilerBinaryReader(FILE* file, size_t fileSize) : BinaryReader(file, FileType::PlayerGame, 1,
                                                                                       fileSize, getFileCloseCallback(file))
{

}

SDL_Surface* CompilerBinaryReader::readSurface()
{
    return readSdlSurface();
}

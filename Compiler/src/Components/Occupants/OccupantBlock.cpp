#include "KristallCompiler.hpp"

OccupantBlock::OccupantBlock(OverworldOccupantType occupantType, uint16_t tileX, uint16_t tileY) :
    _occupantType(occupantType), _tileX(tileX), _tileY(tileY)
{
}

uint16_t OccupantBlock::getTileX() const
{
    return _tileX;
}

uint16_t OccupantBlock::getTileY() const
{
    return _tileY;
}

void OccupantBlock::write(CompilerBinaryWriter& writer)
{
    writer.writeUInt8(static_cast<uint8_t>(_occupantType));
    writeData(writer);
}

#include "KristallCompiler.hpp"
#include "General/Numbers.h"

NamedOverworldObject::NamedOverworldObject(UnicodeString handle) :
                                OverworldOccupantBase(SpriteSheet::OverworldObjects),
                                _handle(handle)
{
    XmlDocument xml(XmlDocument::load("content/overworld-objects/" + handle + ".xml"));
    _width = xml.firstChild("Width")->getValue().getUInt16();
    _height = xml.firstChild("Height")->getValue().getUInt16();
    if (_width == 0 || _height == 0)
    {
        ExecutionException error("A NamedOverworldObject must be, at minimum, 1x1 tiles in dimension.");
        error.addData("handle", handle);
        error.addData("width", toString(_width));
        error.addData("height", toString(_height));
        throw error;
    }

    _isMovable = xml.hasChild("IsMovable");

    // Blocks
    XmlElementPtr currentBlock = xml.firstChild("Block");
    size_t nBlocksAdded(0);
    Block tempBlock;
    UnicodeString spriteStr;
    while (currentBlock != nullptr)
    {
        tempBlock.tileX = currentBlock->getAttribute("X")->getUInt16();
        tempBlock.tileY = currentBlock->getAttribute("Y")->getUInt16();

        if (tempBlock.tileX >= _width || tempBlock.tileY >= _height)
        {
            ExecutionException error("Blocks may only be defined for the area within the NamedOverworldObject.");
            error.addData("handle", handle);
            error.addData("block x", toString(tempBlock.tileX));
            error.addData("block y", toString(tempBlock.tileY));
            error.addData("object width", toString(_width));
            error.addData("object height", toString(_height));
            throw error;
        }

        spriteStr = currentBlock->getAttribute("Sprite")->getString();
        tempBlock.isAnimationInstance = spriteStr.startsWith('{');
        if (tempBlock.isAnimationInstance)
        {
            tempBlock.spriteNo = Numbers::as<uint32_t>(UnicodeStrings::toInt64(UnicodeString(spriteStr, 1, spriteStr.length() - 2)));
        }
        else
        {
            tempBlock.spriteNo = Numbers::as<uint32_t>(UnicodeStrings::toInt64(spriteStr));
        }

        tempBlock.isSign = false;
        tempBlock.isCounter = false;
        if (currentBlock->hasAttribute("Type"))
        {
            if (currentBlock->getAttribute("Type")->getString() == "Sign")
            {
                tempBlock.isSign = true;
            }
            else if (currentBlock->getAttribute("Type")->getString() == "Counter")
            {
                tempBlock.isCounter = true;
            }
            else
            {
                ExecutionException error("An invalid Block Type was encountered when parsing a NamedOverworldObject.");
                error.addData("handle", handle);
                error.addData("block type", currentBlock->getAttribute("Type")->getString());
                error.addData("block x", currentBlock->getAttribute("X")->getString());
                error.addData("block y", currentBlock->getAttribute("Y")->getString());
                throw error;
            }
        }

        if (currentBlock->hasAttribute("OffsetX"))
        {
            tempBlock.offsetX = currentBlock->getAttribute("OffsetX")->getInt16();
        }
        else
        {
            tempBlock.offsetX = 0;
        }

        if (currentBlock->hasAttribute("OffsetY"))
        {
            tempBlock.offsetY = currentBlock->getAttribute("OffsetY")->getInt16();
        }
        else
        {
            tempBlock.offsetY = 0;
        }

        _blockBases.push_back(tempBlock);
        ++nBlocksAdded;

        currentBlock = currentBlock->nextSibling("Block");
    }

    if (nBlocksAdded == 0)
    {
        ExecutionException error("At least one Block must be defined for a NamedOverworldObject.");
        error.addData("handle", handle);
        throw error;
    }
}

std::vector<OccupantBlockPtr> NamedOverworldObject::instantiate(uint16_t tileX, uint16_t tileY,
                                                                XmlElementPtr occupantXml, MapFile* map,
                                                                SpriteSheetFile* spriteSheetFile)
{
    if (tileX + _width > map->getWidth() || tileY + _height > map->getHeight())
    {
        ExecutionException error("This NamedOverworldObject will not fit on the map in the location specified.");
        error.addData("tileX", toString(tileX));
        error.addData("tileY", toString(tileY));
        error.addData("object handle", _handle);
        error.addData("object width", toString(_width));
        error.addData("object height", toString(_height));
        error.addData("map width", toString(map->getWidth()));
        error.addData("map height", toString(map->getHeight()));
        throw error;
    }

    std::vector<OccupantBlockPtr> blocks;
    blocks.reserve(_blockBases.size());
    for (const Block& blockBase : _blockBases)
    {
        if (!map->isTileVacant(blockBase.tileX + tileX, blockBase.tileY + tileY))
        {
            ExecutionException error("Attempted to instantiate an OccupantBlock on a tile which already has an occupant.");
            error.addData("object handle", _handle);
            error.addData("dest tileX", toString(blockBase.tileX + tileX));
            error.addData("dest tileY", toString(blockBase.tileY + tileY));
            throw error;
        }

        blocks.push_back(OccupantBlockPtr(new NamedObjectOccupantBlock(blockBase.tileX + tileX,
                                                                     blockBase.tileY + tileY,
                                                                     blockBase.spriteNo,
                                                                     blockBase.isAnimationInstance,
                                                                     _isMovable, blockBase.isSign,
                                                                     blockBase.isCounter,
                                                                     blockBase.offsetX, blockBase.offsetY)));
    }

    return blocks;
}

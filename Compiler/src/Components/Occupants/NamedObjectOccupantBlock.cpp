#include "KristallCompiler.hpp"

NamedObjectOccupantBlock::NamedObjectOccupantBlock(uint16_t tileX,
                                                   uint16_t tileY, uint32_t spriteNo,
                                                   bool isAnimationInstance,
                                                   bool isMovable, bool isSign,
                                                   bool isCounter, int16_t offsetX,
                                                   int16_t offsetY) : OccupantBlock(OverworldOccupantType::OverworldObject, tileX, tileY),
                                                   _spriteNo(spriteNo),
                                                   _isAnimationInstance(isAnimationInstance),
                                                   _isMovable(isMovable), _isSign(isSign),
                                                   _isCounter(isCounter), _offsetX(offsetX),
                                                   _offsetY(offsetY)
{
}

void NamedObjectOccupantBlock::writeData(CompilerBinaryWriter& writer)
{
    writer.writeBool(_isAnimationInstance);
    writer.writeUInt32(_spriteNo);
    writer.writeBool(_isMovable);
    writer.writeBool(_isSign);
    writer.writeBool(_isCounter);
    writer.writeInt16(_offsetX);
    writer.writeInt16(_offsetY);
}

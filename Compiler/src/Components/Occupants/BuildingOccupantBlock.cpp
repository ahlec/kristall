#include "KristallCompiler.hpp"

BuildingOccupantBlock::BuildingOccupantBlock(uint16_t tileX, uint16_t tileY, uint32_t spriteNo,
                                             bool isOverhead, bool isSign, int16_t offsetX,
                                             int16_t offsetY) : OccupantBlock(OverworldOccupantType::BuildingBlock, tileX, tileY),
                                             _spriteNo(spriteNo), _isOverhead(isOverhead), _isSign(isSign),
                                             _offsetX(offsetX), _offsetY(offsetY)
{
}

void BuildingOccupantBlock::writeData(CompilerBinaryWriter& writer)
{
    writer.writeUInt32(_spriteNo);
    writer.writeBool(_isOverhead);
    writer.writeBool(_isSign);
    writer.writeInt16(_offsetX);
    writer.writeInt16(_offsetY);
}

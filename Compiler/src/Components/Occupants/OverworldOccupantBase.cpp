#include "KristallCompiler.hpp"

OverworldOccupantBase::OverworldOccupantBase(SpriteSheet spriteSheet) : _spriteSheet(spriteSheet)
{
}

std::vector<OccupantBlockPtr> OverworldOccupantBase::create(uint16_t tileX, uint16_t tileY,
                                                            MapFile* map, const KristallBin& kristallBin, XmlElementPtr occupantXml)
{
    SpriteSheetFile* spriteSheetFile(kristallBin.askFor<SpriteSheetFile>(static_cast<uint32_t>(_spriteSheet)));
    return instantiate(tileX, tileY, occupantXml, map, spriteSheetFile);
}

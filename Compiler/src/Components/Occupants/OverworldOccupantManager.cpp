#include "KristallCompiler.hpp"

std::vector<OccupantBlockPtr> OverworldOccupantManager::getInstance(XmlElementPtr occupantXml,
                                                                    MapFile* map, const KristallBin& kristallBin)
{
    // Singleton
    static std::shared_ptr<OverworldOccupantManager> s_singleton(nullptr);
    if (s_singleton == nullptr)
    {
        s_singleton = std::shared_ptr<OverworldOccupantManager>(new OverworldOccupantManager());
    }

    // Get OverworldOccupantBase
    OverworldOccupantBase* occupantBase(nullptr);
    UnicodeString occupantType(occupantXml->getName().getString());
    if (occupantType == "NamedOverworldObject")
    {
        UnicodeString handle(occupantXml->firstChild("Handle")->getValue().getString());
        if (s_singleton->_namedObjects.count(handle) == 0)
        {
            s_singleton->_namedObjects.insert(std::pair<UnicodeString, NamedOverworldObject>(handle,
                                                                                             NamedOverworldObject(handle)));
        }
        occupantBase = &s_singleton->_namedObjects.at(handle);
    }
    else if (occupantType == "BuildingFacade")
    {
        UnicodeString handle(occupantXml->firstChild("Handle")->getValue().getString());
        if (s_singleton->_buildings.count(handle) == 0)
        {
            s_singleton->_buildings.insert(std::pair<UnicodeString, BuildingOverworldObject>(handle,
                                                                                             BuildingOverworldObject(handle)));
        }
        occupantBase = &s_singleton->_buildings.at(handle);
    }


    // Return an empty vector if we didn't find anything
    // Note, this should only be temporary during development; production should throw an
    // exception if it is passed an invalid type.
    if (occupantBase == nullptr)
    {
        return std::vector<OccupantBlockPtr>();
    }

    // Instantiate
    uint16_t tileX(occupantXml->getAttribute("X")->getUInt16());
    uint16_t tileY(occupantXml->getAttribute("Y")->getUInt16());
    return occupantBase->create(tileX, tileY, map, kristallBin, occupantXml);

}

OverworldOccupantManager::OverworldOccupantManager()
{
}

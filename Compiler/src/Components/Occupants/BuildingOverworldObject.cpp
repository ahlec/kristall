#include "KristallCompiler.hpp"

BuildingOverworldObject::BuildingOverworldObject(UnicodeString handle) :
                                OverworldOccupantBase(SpriteSheet::Buildings),
                                _handle(handle)
{
    XmlDocument xml(XmlDocument::load("content/building-facades/" + handle + ".xml"));
    _hasMultipleFormes = xml.hasChild("Forme");

    if (_hasMultipleFormes)
    {
        XmlElementPtr formeNode(xml.firstChild("Forme"));
        while (formeNode != nullptr)
        {
            if (formeNode->hasChild("Explicit"))
            {
                _formes.push_back(parseExplicit(*formeNode));
            }
            else
            {
                _formes.push_back(parseSimple(*formeNode));
            }
            formeNode = formeNode->nextSibling("Forme");
        }
    }
    else
    {
        if (xml.hasChild("Explicit"))
        {
            _formes.push_back(parseExplicit(xml));
        }
        else
        {
            _formes.push_back(parseSimple(xml));
        }
    }

    if (_formes.size() == 0)
    {
        ExecutionException error("A building facade must have at least one facade defined.");
        error.addData("building handle", _handle);
        throw error;
    }
}

std::vector<OccupantBlockPtr> BuildingOverworldObject::instantiate(uint16_t tileX, uint16_t tileY,
                                                                XmlElementPtr occupantXml, MapFile* map,
                                                                SpriteSheetFile* spriteSheetFile)
{
    // Is the forme valid?
    uint8_t formeNo(occupantXml->firstChild("Forme")->getValue().getUInt8());
    if (formeNo == 0 && _hasMultipleFormes)
    {
        ExecutionException error("Attempted to the get sole facade of a multiforme building.");
        error.addData("building handle", _handle);
        throw error;
    }
    else if (formeNo != 0 && !_hasMultipleFormes)
    {
        ExecutionException error("Attempted to get one of many facades of a singleforme building.");
        error.addData("building handle", _handle);
        throw error;
    }

    if (_hasMultipleFormes && formeNo > _formes.size())
    {
        ExecutionException error("Attempted to reference an invalid forme of the specified building.");
        error.addData("referenced formeNo", toString(formeNo));
        error.addData("defiend formes", toString(_formes.size()));
        error.addData("building handle", _handle);
        throw error;
    }

    // Create the instantiated blocks using the base blocks from the desired forme
    size_t formeIndex(formeNo);
    if (_hasMultipleFormes)
    {
        --formeIndex;
    }

    std::vector<OccupantBlockPtr> blocks;
    blocks.reserve(_formes[formeIndex].size());
    for (const FacadeBlock& blockBase : _formes[formeIndex])
    {
        if (!map->isTileVacant(blockBase.x + tileX, blockBase.y + tileY))
        {
            ExecutionException error("Attempted to instantiate an OccupantBlock on a tile which already has an occupant.");
            error.addData("object handle", _handle);
            error.addData("dest tileX", toString(blockBase.x + tileX));
            error.addData("dest tileY", toString(blockBase.y + tileY));
            throw error;
        }

        blocks.push_back(OccupantBlockPtr(new BuildingOccupantBlock(blockBase.x + tileX,
                                                                     blockBase.y + tileY,
                                                                     blockBase.spriteNo,
                                                                     blockBase.isOverhead,
                                                                     blockBase.isSign, blockBase.offsetX, blockBase.offsetY)));
    }

    return blocks;
}

std::vector<BuildingOverworldObject::FacadeBlock> BuildingOverworldObject::parseExplicit(XmlParent& xml)
{
    std::vector<FacadeBlock> blocks;
    blocks.reserve(xml.countChildren("Explicit"));

    XmlElementPtr explicitNode(xml.firstChild("Explicit"));
    FacadeBlock tempBlock;
    while (explicitNode != nullptr)
    {
        tempBlock.x = explicitNode->getAttribute("X")->getUInt16();
        tempBlock.y = explicitNode->getAttribute("Y")->getUInt16();
        tempBlock.spriteNo = explicitNode->getAttribute("Sprite")->getUInt32();

        tempBlock.isOverhead = false;
        tempBlock.isSign = false;
        if (explicitNode->hasAttribute("Type"))
        {
            if (explicitNode->getAttribute("Type")->getString() == "Overhead")
            {
                tempBlock.isOverhead = true;
            }
            else if (explicitNode->getAttribute("Type")->getString() == "Sign")
            {
                tempBlock.isSign = true;
            }
            else
            {
                ExecutionException error("Invalid building block type detected.");
                error.addData("type", explicitNode->getAttribute("Type")->getString());
                error.addData("x", toString(tempBlock.x));
                error.addData("y", toString(tempBlock.y));
                error.addData("building handle", _handle);
                if (xml.hasAttribute("Number"))
                {
                    error.addData("formeNo", toString(xml.getAttribute("Number")->getUInt32()));
                }
                throw error;
            }
        }

        tempBlock.offsetX = 0;
        if (explicitNode->hasAttribute("OffsetX"))
        {
            tempBlock.offsetX = explicitNode->getAttribute("OffsetX")->getInt16();
        }

        tempBlock.offsetY = 0;
        if (explicitNode->hasAttribute("OffsetY"))
        {
            tempBlock.offsetY = explicitNode->getAttribute("OffsetY")->getInt16();
        }

        blocks.push_back(tempBlock);
        explicitNode = explicitNode->nextSibling("Explicit");
    }

    return blocks;
}

BuildingOverworldObject::FacadeBlockLocator::FacadeBlockLocator(uint16_t x, uint16_t y) : _x(x), _y(y)
{
}

bool BuildingOverworldObject::FacadeBlockLocator::operator()(BuildingOverworldObject::FacadeBlock& input)
{
    return (input.x == _x && input.y == _y);
}

std::vector<BuildingOverworldObject::FacadeBlock> BuildingOverworldObject::parseSimple(XmlParent& xml)
{
    std::vector<FacadeBlock> blocks;

    uint16_t width(xml.firstChild("Width")->getValue().getUInt16());
    uint16_t height(xml.firstChild("Height")->getValue().getUInt16());
    blocks.resize(width * height);
    uint16_t x;
    for (uint16_t y = 0; y < height; ++y)
    {
        for (x = 0; x < width; ++x)
        {
            FacadeBlock& facadeBlock(blocks[y * width + x]);
            facadeBlock.x = x;
            facadeBlock.y = y;
            facadeBlock.isOverhead = false;
            facadeBlock.isSign = false;
            facadeBlock.offsetX = 0;
            facadeBlock.offsetY = 0;
        }
    }

    // Process "Interact" nodes
    XmlElementPtr interactNode(xml.firstChild("Interact"));
    while (interactNode != nullptr)
    {
        if (interactNode->getAttribute("X")->getUInt16() >= width || interactNode->getAttribute("Y")->getUInt16() >= height)
        {
            ExecutionException error("The provided Interact block is outside of the bounds of the simple facade.");
            error.addData("building handle", _handle);
            if (xml.hasAttribute("Number"))
            {
                error.addData("formeNo", xml.getAttribute("Number")->getString());
            }
            error.addData("x", interactNode->getAttribute("X")->getString());
            error.addData("y", interactNode->getAttribute("Y")->getString());
            error.addData("facade width", toString(width));
            error.addData("facade height", toString(height));
            throw error;
        }

        if (interactNode->hasAttribute("Type"))
        {
            if (interactNode->getAttribute("Type")->getString() == "Sign")
            {
                blocks[interactNode->getAttribute("Y")->getUInt16() * width + interactNode->getAttribute("X")->getUInt16()].isSign = true;
            }
            else
            {
                ExecutionException error("Unhandled Interact Type provided for a simple building facade.");
                error.addData("type", interactNode->getAttribute("Type")->getString());
                error.addData("x", interactNode->getAttribute("X")->getString());
                error.addData("y", interactNode->getAttribute("Y")->getString());
                error.addData("building handle", _handle);
                if (xml.hasAttribute("Number"))
                {
                    error.addData("formeNo", xml.getAttribute("Number")->getString());
                }
                throw error;
            }
        }

        interactNode = interactNode->nextSibling("Interact");
    }

    // Process "Overhead" nodes
    XmlElementPtr overheadNode(xml.firstChild("Overhead"));
    while (overheadNode != nullptr)
    {
        if (overheadNode->getAttribute("X")->getUInt16() >= width || overheadNode->getAttribute("Y")->getUInt16() >= height)
        {
            ExecutionException error("The provided Overhead block is outside of the bounds of the simple facade.");
            error.addData("building handle", _handle);
            if (xml.hasAttribute("Number"))
            {
                error.addData("formeNo", xml.getAttribute("Number")->getString());
            }
            error.addData("x", overheadNode->getAttribute("X")->getString());
            error.addData("y", overheadNode->getAttribute("Y")->getString());
            error.addData("facade width", toString(width));
            error.addData("facade height", toString(height));
            throw error;
        }

        FacadeBlock& facadeBlock(blocks[overheadNode->getAttribute("Y")->getUInt16() * width + overheadNode->getAttribute("X")->getUInt16()]);
        if (facadeBlock.isSign)
        {
            ExecutionException error("In order to match functionality with the Explicit facades, a simple facade block may not be both an overhead and a sign.");
            error.addData("x", toString(facadeBlock.x));
            error.addData("y", toString(facadeBlock.y));
            error.addData("building handle", _handle);
            if (xml.hasAttribute("Number"))
            {
                error.addData("formeNo", xml.getAttribute("Number")->getString());
            }
            throw error;
        }

        facadeBlock.isOverhead = true;
        facadeBlock.spriteNo = overheadNode->getAttribute("Sprite")->getUInt32();
        if (overheadNode->hasAttribute("OffsetX"))
        {
            facadeBlock.offsetX = overheadNode->getAttribute("OffsetX")->getInt16();
        }
        else
        {
            facadeBlock.offsetX = 0;
        }
        if (overheadNode->hasAttribute("OffsetY"))
        {
            facadeBlock.offsetY = overheadNode->getAttribute("OffsetY")->getInt16();
        }
        else
        {
            facadeBlock.offsetY = 0;
        }

        overheadNode = overheadNode->nextSibling("Overhead");
    }

    // Remove blocks that are marked as vacate
    XmlElementPtr vacateNode(xml.firstChild("Vacate"));
    while (vacateNode != nullptr)
    {
        blocks.erase(std::remove_if(blocks.begin(), blocks.end(), FacadeBlockLocator(vacateNode->getAttribute("X")->getUInt16(),
                                                                                     vacateNode->getAttribute("Y")->getUInt16())));
        vacateNode = vacateNode->nextSibling("Vacate");
    }

    // Set the primary render block
    for (size_t index = blocks.size() - 1; index >= 0; --index)
    {
        if (blocks[index].isOverhead)
        {
            continue;
        }

        blocks[index].spriteNo = xml.firstChild("Sprite")->getValue().getUInt32();
        blocks[index].offsetX = (width - 1 - blocks[index].x) * TILE_SIZE;
        blocks[index].offsetY = (height - 1 - blocks[index].y) * TILE_SIZE;
        if (xml.hasChild("OffsetX"))
        {
            blocks[index].offsetX += xml.firstChild("OffsetX")->getValue().getInt16();
        }
        if (xml.hasChild("OffsetY"))
        {
            blocks[index].offsetY += xml.firstChild("OffsetY")->getValue().getInt16();
        }

        return blocks;
    }

    ExecutionException error("No blocks cound be found to server as the primary render block for this simple building facade.");
    error.addData("building handle", toString(_handle));
    if (xml.hasAttribute("Number"))
    {
        error.addData("formeNo", toString(xml.getAttribute("Number")->getString()));
    }
    throw error;
}

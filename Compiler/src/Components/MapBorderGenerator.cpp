#include "KristallCompiler.hpp"
#include "General/Strings.h"

MapBorderGenerator::MapBorderGenerator(bool isRowOriginalParityEven,
                                       bool isColumnOriginalParityEven) :
                                           _isRowOriginalParityEven(isRowOriginalParityEven),
                                           _isColumnOriginalParityEven(isColumnOriginalParityEven)
{
}

void MapBorderGenerator::process(std::vector<RegionMap*>& regionMaps)
{
    // Input
    input(regionMaps, false);
    initializeBorderRectangles();
    resolveConflicts();

    // Now we begin retracting in certain directions. We do this to allow for larger, uninterrupted
    // areas of map borders (for consistency). We do this in three different directions (each of
    // which will be documented). Retractions are done by focusing on INDECES, because comparisons
    // include both a map's natural rectangle, and the current border rectangle.
    retractI();
    retractII();
    retractIII();

    // Expand
    expandSouthwest();

    // Output
    for (RegionMap*& regionMap : _regionMaps)
    {
        regionMap->markFullyCalculated();
    }

    output(regionMaps);
}

void MapBorderGenerator::fixOrigin(std::vector<RegionMap*>& regionMaps)
{
    // Input
    input(regionMaps);

    // Get deltas
    ValueResults deltaX = findValue(RectField::Left, SearchCriteria::Min);
    ValueResults deltaY = findValue(RectField::Top, SearchCriteria::Min);
    for (RegionMap*& regionMap : _regionMaps)
    {
        // in all likelihood, deltaX and deltaY will
        // be negative values; recall that we just
        // choose the first "outdor" map to begin when
        // we were stitching together the outdoor maps.
        // It is VERY likely that there will be maps that
        // extend to the north and west of this arbitrary
        // map. Therefore, these maps that extend would have
        // origins in the negatives. To adjust all of the maps
        // appropriately, we subtract.

        if (deltaX.hasValue)
        {
            regionMap->rectangle.x -= deltaX.value;
            regionMap->borderRectangle.x -= deltaX.value;
        }

        if (deltaY.hasValue)
        {
            regionMap->rectangle.y -= deltaY.value;
            regionMap->borderRectangle.y -= deltaY.value;
        }
    }

    // Output
    output(regionMaps);
}

std::vector<RegionInaccessibleArea> MapBorderGenerator::getInaccessibleRegions(UnicodeString regionName,
                                                                               std::vector<RegionMap*>& regionMaps,
                                                                               SpriteSheetFile* overworldExteriorSpriteSheet)
{
    // Input
    input(regionMaps);

    // Collect the possible tiles
    std::vector<Vector2> tiles;
    int32_t current;
    MapBorderResults result;
    for (RegionMap*& regionMap : _regionMaps)
    {
        // Check horizontally
        for (current = regionMap->borderRectangle.x; current <= regionMap->borderRectangle.x +
                                                                   regionMap->borderRectangle.width; current += TILE_SIZE)
        {
            // North
            result = getMapBorder(current + (TILE_SIZE / 2), regionMap->borderRectangle.y - (TILE_SIZE / 2),
                                  Direction::South);
            if (!result.hasValue)
            {
                tiles.push_back(Vector2(current, regionMap->borderRectangle.y - TILE_SIZE));
            }

            // South
            result = getMapBorder(current + (TILE_SIZE / 2), regionMap->borderRectangle.y +
                                                             regionMap->borderRectangle.height +
                                                             (TILE_SIZE / 2), Direction::North);
            if (!result.hasValue)
            {
                tiles.push_back(Vector2(current, regionMap->borderRectangle.y +
                                                      regionMap->borderRectangle.height));
            }
        }

        // Check vertically
        for (current = regionMap->borderRectangle.y; current <= regionMap->borderRectangle.y +
                                                                  regionMap->borderRectangle.height; current += TILE_SIZE)
        {
            // West
            result = getMapBorder(regionMap->borderRectangle.x - (TILE_SIZE / 2), current + (TILE_SIZE / 2),
                                  Direction::East);
            if (!result.hasValue)
            {
                tiles.push_back(Vector2(regionMap->borderRectangle.x - TILE_SIZE, current));
            }

            // East
            result = getMapBorder(regionMap->borderRectangle.x + regionMap->borderRectangle.width + (TILE_SIZE / 2),
                                  current + (TILE_SIZE / 2), Direction::West);
            if (!result.hasValue)
            {
                tiles.push_back(Vector2(regionMap->borderRectangle.x +
                                             regionMap->borderRectangle.width, current));
            }
        }
    }

    // Collect the regions
    _inaccessibleAreas.clear();
    InaccessibleAreaCollector collector(regionName, tiles, findValue(RectField::Left, SearchCriteria::Min).value,
                                        findValue(RectField::Top, SearchCriteria::Min).value,
                                        findValue(RectField::Right, SearchCriteria::Max).value,
                                        findValue(RectField::Bottom, SearchCriteria::Max).value,
                                        FunctionPtr<bool, int32_t, int32_t>::fromMember<MapBorderGenerator>(this, &MapBorderGenerator::isLocationEmpty),
                                        FunctionPtr<bool, Rect>::fromMember<MapBorderGenerator>(this, &MapBorderGenerator::doesRectIntersectAnywhere));
    while (collector.hasMore())
    {
        if (collector.next())
        {
            _inaccessibleAreas.push_back(collector.get());
        }
    }

    // Populate the OverworldRenderables
    int32_t currentX;
    int32_t currentY;
    int32_t riaRight;
    int32_t riaBottom;
    MapBorderTile mapBorderTile(Direction::North, false, false);
    for (RegionInaccessibleArea& ria : _inaccessibleAreas)
    {
        riaRight = ria.getX() + ria.getWidth();
        riaBottom = ria.getY() + ria.getHeight();
        for (currentY = ria.getY() + (TILE_SIZE / 2); currentY < riaBottom; currentY += TILE_SIZE)
        {
            for (currentX = ria.getX() + (TILE_SIZE / 2); currentX < riaRight; currentX += TILE_SIZE)
            {
                mapBorderTile = MapBorderTile(Direction::North, isCurrentRowOdd(currentY),
                                              isCurrentColumnOdd(currentX), false);

                // North
                result = getMapBorder(currentX, currentY - TILE_SIZE, Direction::South);
                if (result.hasValue)
                {
                    mapBorderTile.north = result.value;
                    mapBorderTile.fill = result.value;
                }
                else
                {
                    mapBorderTile.north = MapBorder::Black;
                }

                // East
                result = getMapBorder(currentX + TILE_SIZE, currentY, Direction::West);
                if (result.hasValue)
                {
                    mapBorderTile.east = result.value;
                    if (mapBorderTile.fill == MapBorder::None)
                    {
                        mapBorderTile.fill = result.value;
                    }
                }
                else
                {
                    mapBorderTile.east = MapBorder::Black;
                }

                // South
                result = getMapBorder(currentX, currentY + TILE_SIZE, Direction::North);
                if (result.hasValue)
                {
                    mapBorderTile.south = result.value;
                    if (mapBorderTile.fill == MapBorder::None)
                    {
                        mapBorderTile.fill = result.value;
                    }
                }
                else
                {
                    mapBorderTile.south = MapBorder::Black;
                }

                // West
                result = getMapBorder(currentX - TILE_SIZE , currentY, Direction::East);
                if (result.hasValue)
                {
                    mapBorderTile.west = result.value;
                    if (mapBorderTile.fill == MapBorder::None)
                    {
                        mapBorderTile.fill = result.value;
                    }
                }
                else
                {
                    mapBorderTile.west = MapBorder::Black;
                }

                // Set
                if (mapBorderTile.fill == MapBorder::None)
                {
                    mapBorderTile.fill = MapBorder::Black;
                }
                ria.setTile(currentX - (TILE_SIZE / 2), currentY - (TILE_SIZE / 2), mapBorderTile,
                            overworldExteriorSpriteSheet);
            }
        }
    }

    // Output
    output(regionMaps);
    return _inaccessibleAreas;
}

void MapBorderGenerator::drawMapBorders(std::vector<RegionMap*>& regionMaps,
                                        SpriteSheetFile* overworldExteriorSpriteSheet)
{
    // Input
    input(regionMaps);

    // Draw the map borders
    int32_t currentX;
    int32_t currentY;
    int32_t borderRight;
    int32_t borderBottom;
    int16_t tileX;
    int16_t tileY;
    MapBorderTile mapBorderTile(Direction::North, false, false);
    MapBorderResults result;
    for (RegionMap*& regionMap : _regionMaps)
    {
        std::string regionMapName(UnicodeStrings::toStdString(regionMap->getMapName()));

        borderRight = regionMap->borderRectangle.x + regionMap->borderRectangle.width;
        borderBottom = regionMap->borderRectangle.y + regionMap->borderRectangle.height;
        tileX = 0;
        tileY = 0;
        for (currentY = regionMap->borderRectangle.y + (TILE_SIZE / 2); currentY < borderBottom; currentY += TILE_SIZE)
        {
            for (currentX = regionMap->borderRectangle.x + (TILE_SIZE / 2); currentX < borderRight; currentX += TILE_SIZE)
            {
                if (regionMap->rectangle.contains(currentX, currentY))
                {
                    continue;
                }

                mapBorderTile = startOutdoorMapBorderTile(currentX, currentY,
                                                          regionMap->rectangle, regionMap->borderRectangle);

                // North
                result = getMapBorder(currentX, currentY - TILE_SIZE, Direction::South);
                if (result.hasValue)
                {
                    mapBorderTile.north = result.value;
                }
                else
                {
                    mapBorderTile.north = MapBorder::Black;
                }

                // East
                result = getMapBorder(currentX + TILE_SIZE, currentY, Direction::West);
                if (result.hasValue)
                {
                    mapBorderTile.east = result.value;
                }
                else
                {
                    mapBorderTile.east = MapBorder::Black;
                }

                // South
                result = getMapBorder(currentX, currentY + TILE_SIZE, Direction::North);
                if (result.hasValue)
                {
                    mapBorderTile.south = result.value;
                }
                else
                {
                    mapBorderTile.south = MapBorder::Black;
                }

                // West
                result = getMapBorder(currentX - TILE_SIZE , currentY, Direction::East);
                if (result.hasValue)
                {
                    mapBorderTile.west = result.value;
                }
                else
                {
                    mapBorderTile.west = MapBorder::Black;
                }

                // Fill
                result = getMapBorder(currentX, currentY, Directions::complement(mapBorderTile.getTraverseDirection()));
                if (result.hasValue)
                {
                    mapBorderTile.fill = result.value;
                }
                else
                {
                    mapBorderTile.fill = MapBorder::Black;
                }

                // Set
                regionMap->drawMapBorder((currentX - (TILE_SIZE / 2) - regionMap->borderRectangle.x) / TILE_SIZE,
                                         (currentY - (TILE_SIZE / 2) - regionMap->borderRectangle.y) / TILE_SIZE,
                                         mapBorderTile, overworldExteriorSpriteSheet);
            }
        }
    }

    // Output
    output(regionMaps);
}

void MapBorderGenerator::input(std::vector<RegionMap*>& regionMaps,
                               bool inputBorderRectangles)
{
    /*_mapNos.clear();
    _rectangles.clear();
    _borderRectangles.clear();

    for (RegionMap*& regionMap : regionMaps)
    {
        _mapNos.push_back(regionMap->getMapNo());
        _rectangles.push_back(regionMap->rectangle);
        if (inputBorderRectangles)
        {
            _borderRectangles.push_back(regionMap->borderRectangle);
        }
    }*/

    _regionMaps.clear();
    _regionMaps.swap(regionMaps);
}

void MapBorderGenerator::initializeBorderRectangles()
{
    // Extend each map border as far north and as far west as it can go
    // Fully encompass the map rectangle, as well as extending in these two
    // directions.
    ValueResults searchResults;
    for (RegionMap*& regionMap : _regionMaps)//Rect& rect : _rectangles)
    {
        regionMap->borderRectangle = regionMap->rectangle;
        //Rect borderRect(rect);

        // Extend as far north
        searchResults = searchRects(RectField::Bottom, SearchCriteria::Max,
                                     regionMap->borderRectangle, SearchDomains::Both,
                                     SearchFilter::OnlyRectsAbove);
        if (!searchResults.hasValue)
        {
            searchResults = findValue(RectField::Top, SearchCriteria::Min);
        }

        regionMap->borderRectangle.y = searchResults.value;
        regionMap->borderRectangle.height += (regionMap->rectangle.y - regionMap->borderRectangle.y);

        // Extend as far west
        searchResults = searchRects(RectField::Right, SearchCriteria::Max,
                                     regionMap->borderRectangle, SearchDomains::Both,
                                     SearchFilter::OnlyRectsLeft);
        if (!searchResults.hasValue)
        {
            searchResults = findValue(RectField::Left, SearchCriteria::Min);
        }
        regionMap->borderRectangle.x = searchResults.value;
        regionMap->borderRectangle.width += (regionMap->rectangle.x - regionMap->borderRectangle.x);

        // Update width and height
        regionMap->isBorderRectangleCalculated = true;
        //_borderRectangles.push_back(borderRect);
    }

    validateAll();
}

void MapBorderGenerator::resolveConflicts()
{
    // Since when we were extending above, we did so much without specifying a width and height
    // until the end, we need to now check each pair of map border rectangles to see if rectA's
    // origin (top, left) is contained anywhere within rectB (as long as rectA != rectB). Every
    // time there is a collision like this, retract rectA so that its new origin is at the
    // (bottom, right) of rectB (so there is no more collision).
    size_t nRectangles(_regionMaps.size());
    size_t indexB;
    int32_t deltaX;
    int32_t deltaY;
    int32_t iteration;
    for (size_t indexA = 0; indexA < nRectangles; ++indexA)
    {
        iteration = 0;
        Rect& borderA(_regionMaps[indexA]->borderRectangle);

        for (indexB = 0; indexB < nRectangles; ++indexB)
        {
            if (indexB == indexA)
            {
                continue;
            }

            Rect& borderB(_regionMaps[indexB]->borderRectangle);

            if (borderB.contains(borderA.x, borderA.y))
            {
                deltaX = (borderB.x + borderB.width - borderA.x);
                deltaY = (borderB.y + borderB.height - borderA.y);
                borderA.width -= deltaX;
                borderA.height -= deltaY;
                borderA.x = borderB.x + borderB.width;
                borderA.y = borderB.y + borderB.height;

                try
                {
                    validate(indexA);
                }
                catch (ExecutionException& error)
                {
                    error.addData("indexA", toString(indexA));
                    error.addData("indexB", toString(indexB));
                    error.addData("deltaX", toString(deltaX));
                    error.addData("deltaY", toString(deltaY));
                    error.addData("number previous modifications", toString(iteration));
                    throw;
                }

                ++iteration;
            }
        }
    }

    validateAll();
}

void MapBorderGenerator::retractI()
{
    // Retraction I: Retraction of Y coordinate (allowing a northeastern border to expand south)
    // For every pair of indeces indexA and indexB, if indexB has a map border whose bottom is greater
    // than the top of border[indexA] but is LESS than the top of natural[indexA], then we consider
    // it (if border[indexB]'s right value is less than border[indexA]'s left value). Once we have all
    // our considerations, we choose the map border whose right value is the largest, and we retract
    // border[indexA]'s top value to touch the bottom of border[indexB]. HOWEVER, as a caviat, we
    // will only look at indexA's for who there exist no other borders that contain the point
    // (border[indexA].left - 2, border[indexA].top + 1); this is because this would mean that
    // the two borders would be touching, and reducing indexA would not allow indexB to expand south.
    size_t nRectangles(_regionMaps.size());
    size_t indexB;
    std::vector<size_t> indecesToConsider;
    indecesToConsider.reserve(nRectangles);
    bool isBBottomGtATop(false);
    bool isBBottomLeATopNatural(false);
    bool isBFullyRightOfA(false);
    ValueResults results;
    int32_t deltaY;
    for (size_t indexA = 0; indexA < nRectangles; ++indexA)
    {
        Rect& borderA(_regionMaps[indexA]->borderRectangle);

        for (indexB = 0; indexB < nRectangles; ++indexB)
        {
            if (indexA == indexB)
            {
                continue;
            }

            Rect& borderB(_regionMaps[indexB]->borderRectangle);

            isBBottomGtATop = (borderB.y + borderB.height > borderA.y);
            isBBottomLeATopNatural = (borderB.y + borderB.height <=
                                      _regionMaps[indexA]->rectangle.y);
            isBFullyRightOfA = (borderB.x + borderB.width <=
                                borderA.x);
            if (isBBottomGtATop && isBBottomLeATopNatural && isBFullyRightOfA)
            {
                indecesToConsider.push_back(indexB);
            }
        }

        if (indecesToConsider.size() == 0)
        {
            continue;
        }

        results = findValue(RectField::Right, SearchCriteria::Max,
                            SearchDomains::Both, ReturnValue::Index,
                            indecesToConsider);
        if (!results.hasValue)
        {
            ExecutionException error("Retraction I subset searching did not return any value.");
            error.addData("indexA", toString(indexA));
            error.addData("size_indecesToConsider", toString(indecesToConsider.size()));
            throw error;
        }

        deltaY = (_regionMaps[results.value]->borderRectangle.y +
                  _regionMaps[results.value]->borderRectangle.height -
                  borderA.y);
        borderA.height -= deltaY;
        borderA.y = _regionMaps[results.value]->borderRectangle.y +
                      _regionMaps[results.value]->borderRectangle.height;
        indecesToConsider.clear();
        validate(indexA);
    }

    validateAll();
}

void MapBorderGenerator::retractII()
{
    // Retraction II: Retraction of Y coordinate (allowing a northWESTERN border to expand south)
    // This retraction phase looks almost identical to Retraction I, except that instead of excluding
    // indexAs for whom some other map border contains (border[indexA].left - 2, border[indexA].top + 1),
    // it excludes those whom some other map border contains (border[indexA].right + 2,
    // border[indexA].top + 1). It runs a similar selection criteria as well to Retraction I, except
    // that instead of border[indexB]'s right value being less than border[indexA]'s left value, it
    // is border[indexB]'s right value being less than border[indexA]'s left value.
    size_t nRectangles(_regionMaps.size());
    size_t indexB;
    std::vector<size_t> indecesToConsider;
    indecesToConsider.reserve(nRectangles);
    bool isBBottomGtATop(false);
    bool isBBottomLeATopNatural(false);
    bool isAFullyRightOfB(false);
    ValueResults results;
    int32_t deltaY;
    for (size_t indexA = 0; indexA < nRectangles; ++indexA)
    {
        Rect& borderA(_regionMaps[indexA]->borderRectangle);

        for (indexB = 0; indexB < nRectangles; ++indexB)
        {
            if (indexA == indexB)
            {
                continue;
            }

            Rect& borderB(_regionMaps[indexB]->borderRectangle);

            isBBottomGtATop = (borderB.y + borderB.height > borderA.y);
            isBBottomLeATopNatural = (borderB.y + borderB.height <=
                                      _regionMaps[indexA]->rectangle.y);
            isAFullyRightOfB = (borderA.x + borderA.width <= borderB.x);
            if (isBBottomGtATop && isBBottomLeATopNatural && isAFullyRightOfB)
            {
                indecesToConsider.push_back(indexB);
            }
        }

        if (indecesToConsider.size() == 0)
        {
            continue;
        }

        results = findValue(RectField::Right, SearchCriteria::Max,
                            SearchDomains::Both, ReturnValue::Index,
                            indecesToConsider);
        if (!results.hasValue)
        {
            ExecutionException error("Retraction II subset searching did not return any value.");
            error.addData("indexA", toString(indexA));
            error.addData("size_indecesToConsider", toString(indecesToConsider.size()));
            throw error;
        }

        deltaY = (_regionMaps[results.value]->borderRectangle.y +
                  _regionMaps[results.value]->borderRectangle.height -
                  borderA.y);
        borderA.height -= deltaY;
        borderA.y = _regionMaps[results.value]->borderRectangle.y +
                                        _regionMaps[results.value]->borderRectangle.height;
        indecesToConsider.clear();
        validate(indexA);
    }

    validateAll();
}

void MapBorderGenerator::retractIII()
{
    // Retraction III: Retraction of X coordinate (allowing a northwestern border to expand east)
    // The third retraction phase is in the same vein as the previous two, but now focuses on a different
    // coordinate: the X coordinate. It excludes all indexAs for which some other map border contains
    // (border[indexA].left + 1, border[indexA].top - 2). It considers any indexB (indexB != indexA)
    // where border[indexB].left > border[indexA].left but border[indexB].left <= natural[indexA].left,
    // and border[indexB].bottom <= border[indexA].top. It then chooses the final indexB from the
    // candidates by which border[indexB].bottom value is the largest, and retracts indexA's border
    // east.
    size_t nRectangles(_regionMaps.size());
    size_t indexB;
    std::vector<size_t> indecesToConsider;
    indecesToConsider.reserve(nRectangles);
    bool isBLeftGtALeft(false);
    bool isBLeftLeALeftNatural(false);
    bool isBFullyAboveA(false);
    ValueResults results;
    int32_t deltaX;
    for (size_t indexA = 0; indexA < nRectangles; ++indexA)
    {
        Rect& borderA(_regionMaps[indexA]->borderRectangle);

        for (indexB = 0; indexB < nRectangles; ++indexB)
        {
            if (indexA == indexB)
            {
                continue;
            }

            Rect& borderB(_regionMaps[indexB]->borderRectangle);

            isBLeftGtALeft = (borderB.x > borderA.x);
            isBLeftLeALeftNatural = (borderB.x <= _regionMaps[indexA]->rectangle.x);
            isBFullyAboveA = (borderB.y + borderB.height <= borderA.y);
            if (isBLeftGtALeft && isBLeftLeALeftNatural && isBFullyAboveA)
            {
                indecesToConsider.push_back(indexB);
            }
        }

        if (indecesToConsider.size() == 0)
        {
            continue;
        }

        results = findValue(RectField::Bottom, SearchCriteria::Max,
                            SearchDomains::Both, ReturnValue::Index,
                            indecesToConsider);
        if (!results.hasValue)
        {
            ExecutionException error("Retraction III subset searching did not return any value.");
            error.addData("indexA", toString(indexA));
            error.addData("size_indecesToConsider", toString(indecesToConsider.size()));
            throw error;
        }

        deltaX = (_regionMaps[results.value]->borderRectangle.x - borderA.x);
        borderA.width -= deltaX;
        borderA.x = _regionMaps[results.value]->borderRectangle.x; // <---
        #warning "                      Is this an error? ------^"
        indecesToConsider.clear();
        validate(indexA);
    }

    validateAll();
}

void MapBorderGenerator::expandSouthwest()
{
    ValueResults results;
    for (RegionMap*& regionMap : _regionMaps)
    {
        // Height
        results = searchRects(RectField::Top, SearchCriteria::Min,
                              regionMap->borderRectangle, SearchDomains::Both,
                              SearchFilter::OnlyRectsBelow);
        if (!results.hasValue)
        {
            results = findValue(RectField::Bottom, SearchCriteria::Max);
        }
        regionMap->borderRectangle.height = results.value - regionMap->borderRectangle.y;

        // Width
        results = searchRects(RectField::Left, SearchCriteria::Min,
                              regionMap->borderRectangle, SearchDomains::Both,
                              SearchFilter::OnlyRectsRight);
        if (!results.hasValue)
        {
            results = findValue(RectField::Right, SearchCriteria::Max);
        }
        regionMap->borderRectangle.width = results.value - regionMap->borderRectangle.x;
    }

    validateAll();
}

void MapBorderGenerator::output(std::vector<RegionMap*>& regionMaps)
{
    /*for (size_t index = 0; index < _mapNos.size(); ++index)
    {
        if (regionMap[index]->getMapNo() != _mapNos[index])
        {
            ExecutionException error("Data mismatch when attempting output from MapBorderGenerator.");
            error.addData("currentMapNo", toString(regionMap[index]->getMapNo()));
            error.addData("expectedMapNo", toString(_mapNos[index]));
            throw error;
        }

        regionMap[index]->isBorderRectangleCalculated = true;
        regionMap[index]->borderRectangle = _borderRectangles[index];
    }*/
    _regionMaps.swap(regionMaps);
}

void MapBorderGenerator::validate(size_t index) const
{
    Rect& rectangle(_regionMaps[index]->rectangle);
    Rect& borderRectangle(_regionMaps[index]->borderRectangle);

    if (rectangle.width < 0 || rectangle.height < 0)
    {
        ExecutionException error("A map rectangle may not have negative dimensions.");
        error.addData("width", toString(rectangle.width));
        error.addData("height", toString(rectangle.height));
        error.addData("mapNo", toString(_regionMaps[index]->getMapNo()));
        throw error;
    }

    if (borderRectangle.width < 0 || borderRectangle.height < 0)
    {
        ExecutionException error("A map border rectangle may not have negative dimensions.");
        error.addData("width", toString(borderRectangle.width));
        error.addData("height", toString(borderRectangle.height));
        error.addData("mapNo", toString(_regionMaps[index]->getMapNo()));
        throw error;
    }

    // Temporarily increase the width/height of the border rectangle, so that the
    // coordinates along the right and bottom borders can be included when calculating
    // for contains()
    ++borderRectangle.width;
    ++borderRectangle.height;

    if (!borderRectangle.contains(rectangle.x, rectangle.y))
    {
        ExecutionException error("Detected a map border rectangle which doesn't contain the top-left coordinates of the corresponding map rectangle.");
        error.addData("mapNo", toString(_regionMaps[index]->getMapNo()));
        error.addData("mapBorderRectangle", toString(borderRectangle));
        error.addData("mapRectangle", toString(rectangle));
        throw error;
    }

    if (!borderRectangle.contains(rectangle.x + rectangle.width, rectangle.y))
    {
        ExecutionException error("Detected a map border rectangle which doesn't contain the top-right coordinates of the corresponding map rectangle.");
        error.addData("mapNo", toString(_regionMaps[index]->getMapNo()));
        error.addData("mapBorderRectangle", toString(borderRectangle));
        error.addData("mapRectangle", toString(rectangle));
        throw error;
    }

    if (!borderRectangle.contains(rectangle.x + rectangle.width,
                                  rectangle.y + rectangle.height))
    {
        ExecutionException error("Detected a map border rectangle which doesn't contain the bottom-right coordinates of the corresponding map rectangle.");
        error.addData("mapNo", toString(_regionMaps[index]->getMapNo()));
        error.addData("mapBorderRectangle", toString(borderRectangle));
        error.addData("mapRectangle", toString(rectangle));
        throw error;
    }

    if (!borderRectangle.contains(rectangle.x, rectangle.y + rectangle.height))
    {
        ExecutionException error("Detected a map border rectangle which doesn't contain the bottom-left coordinates of the corresponding map rectangle.");
        error.addData("mapNo", toString(_regionMaps[index]->getMapNo()));
        error.addData("mapBorderRectangle", toString(borderRectangle));
        error.addData("mapRectangle", toString(rectangle));
        throw error;
    }

    // Return the dimensions of the border rectangle
    --borderRectangle.width;
    --borderRectangle.height;
}

void MapBorderGenerator::validateAll() const
{
    for (size_t index = 0; index < _regionMaps.size(); ++index)
    {
        validate(index);
    }
}

bool MapBorderGenerator::doesPassFilter(SearchFilter filter, const Rect& testValue,
                                        const Rect& controlGroup) const
{
    // Basic, constant checks
    if (filter == SearchFilter::AllRects)
    {
        return true;
    }

    if (testValue == controlGroup)
    {
        return false;
    }

    // Does the test value fit the broad filter?
    switch (filter)
    {
        case SearchFilter::OnlyRectsAbove:
            {
                if (testValue.y + testValue.height > controlGroup.y)
                {
                    return false;
                }
                break;
            }
        case SearchFilter::OnlyRectsBelow:
            {
                if (testValue.y < controlGroup.y + controlGroup.height)
                {
                    return false;
                }
                break;
            }
        case SearchFilter::OnlyRectsLeft:
            {
                if (testValue.x + testValue.width > controlGroup.x)
                {
                    return false;
                }
                break;
            }
        case SearchFilter::OnlyRectsRight:
            {
                if (testValue.x < controlGroup.x + controlGroup.width)
                {
                    return false;
                }
                break;
            }
        default:
            {
                return false;
            }
    }

    // Determine the points for overlapping
    bool doesTestSpanControl(false);
    bool doesTestEndWithinControlValues(false);
    bool doesTestStartWithinControlValues(false);

    switch (filter)
    {
        case SearchFilter::OnlyRectsAbove:
        case SearchFilter::OnlyRectsBelow:
            {
                doesTestSpanControl = (testValue.x < controlGroup.x &&
                                       testValue.x + testValue.width >=
                                       controlGroup.x + controlGroup.width);
                doesTestEndWithinControlValues = (testValue.x + testValue.width >= controlGroup.x &&
                                                  testValue.x + testValue.width <
                                                  controlGroup.x + controlGroup.width);
                doesTestStartWithinControlValues = (testValue.x >= controlGroup.x &&
                                                    testValue.x < controlGroup.x +
                                                    controlGroup.width);
                break;
            }
        case SearchFilter::OnlyRectsLeft:
        case SearchFilter::OnlyRectsRight:
            {
                doesTestSpanControl = (testValue.y < controlGroup.y &&
                                       testValue.y + testValue.height >= controlGroup.y +
                                       controlGroup.height);
                doesTestEndWithinControlValues = (testValue.y + testValue.height > controlGroup.y &&
                                                  testValue.y + testValue.height <=
                                                  controlGroup.y + controlGroup.height);
                doesTestStartWithinControlValues = (testValue.y >= controlGroup.y &&
                                                    testValue.y < controlGroup.y + controlGroup.height);
                break;
            }
        default:
            {
                break;
            }
    }

    // Finish
    return (doesTestSpanControl || doesTestEndWithinControlValues ||
            doesTestStartWithinControlValues);
}

MapBorderGenerator::ValueResults MapBorderGenerator::findValue(RectField field, SearchCriteria criteria,
                                           SearchDomains domain, ReturnValue returnValue,
                                           std::vector<size_t> subset)
{
    ValueResults results;
    results.hasValue = false;
    int32_t valueToReturn;
    int32_t localValue;

    size_t innerIndex;
    bool foundRectangle;
    for (size_t index = 0; index < _regionMaps.size(); ++index)
    {
        // If we have a subset, make sure the current rectangle is in it.
        if (subset.size() > 0)
        {
            foundRectangle = false;
            for (innerIndex = 0; innerIndex < subset.size(); ++innerIndex)
            {
                if (subset[innerIndex] == index)
                {
                    foundRectangle = true;
                    break;
                }
            }
            if (!foundRectangle)
            {
                continue;
            }
        }

        // Search rectangles
        if (domain != SearchDomains::BorderRectanglesOnly)
        {
            // Get the appropriate value
            switch (field)
            {
                case RectField::Top:
                    {
                        localValue = _regionMaps[index]->rectangle.y;
                        break;
                    }
                case RectField::Left:
                    {
                        localValue = _regionMaps[index]->rectangle.x;
                        break;
                    }
                case RectField::Bottom:
                    {
                        localValue = _regionMaps[index]->rectangle.y + _regionMaps[index]->rectangle.height;
                        break;
                    }
                case RectField::Right:
                    {
                        localValue = _regionMaps[index]->rectangle.x + _regionMaps[index]->rectangle.width;
                        break;
                    }
            }

            // Update results as necessary
            if (!results.hasValue ||
                (criteria == SearchCriteria::Min && localValue < results.value) ||
                (criteria == SearchCriteria::Max && localValue > results.value))
            {
                results.value = localValue;
                results.hasValue = true;

                switch (returnValue)
                {
                    case ReturnValue::Field:
                        {
                            valueToReturn = localValue;
                            break;
                        }
                    case ReturnValue::Index:
                        {
                            valueToReturn = index;
                            break;
                        }
                }
            }
        }

        // Search border rectangles
        if (domain != SearchDomains::RectanglesOnly &&
            _regionMaps[index]->isBorderRectangleCalculated)
        {
            // Get the appropriate value
            switch (field)
            {
                case RectField::Top:
                    {
                        localValue = _regionMaps[index]->borderRectangle.y;
                        break;
                    }
                case RectField::Left:
                    {
                        localValue = _regionMaps[index]->borderRectangle.x;
                        break;
                    }
                case RectField::Bottom:
                    {
                        localValue = _regionMaps[index]->borderRectangle.y + _regionMaps[index]->borderRectangle.height;
                        break;
                    }
                case RectField::Right:
                    {
                        localValue = _regionMaps[index]->borderRectangle.x + _regionMaps[index]->borderRectangle.width;
                        break;
                    }
            }

            // Update results as necessary
            if (!results.hasValue ||
                (criteria == SearchCriteria::Min && localValue < results.value) ||
                (criteria == SearchCriteria::Max && localValue > results.value))
            {
                results.value = localValue;
                results.hasValue = true;

                switch (returnValue)
                {
                    case ReturnValue::Field:
                        {
                            valueToReturn = localValue;
                            break;
                        }
                    case ReturnValue::Index:
                        {
                            valueToReturn = index;
                            break;
                        }
                }
            }
        }
    }

    results.value = valueToReturn;
    return results;
}

MapBorderGenerator::ValueResults MapBorderGenerator::searchRects(RectField field, SearchCriteria criteria,
                                             Rect relativeValue, SearchDomains domain,
                                             SearchFilter filter)
{
    ValueResults results;
    results.hasValue = false;
    int32_t localValue;

    for (RegionMap*& regionMap : _regionMaps)
    {
        // Search rectangles
        if (domain != SearchDomains::BorderRectanglesOnly &&
            (filter == SearchFilter::AllRects || doesPassFilter(filter,
                                                                regionMap->rectangle,
                                                                relativeValue)))
        {
            // Get the appropriate value
            switch (field)
            {
                case RectField::Top:
                    {
                        localValue = regionMap->rectangle.y;
                        break;
                    }
                case RectField::Left:
                    {
                        localValue = regionMap->rectangle.x;
                        break;
                    }
                case RectField::Bottom:
                    {
                        localValue = regionMap->rectangle.y + regionMap->rectangle.height;
                        break;
                    }
                case RectField::Right:
                    {
                        localValue = regionMap->rectangle.x + regionMap->rectangle.width;
                        break;
                    }
            }

            // Update results as necessary
            if (!results.hasValue ||
                (criteria == SearchCriteria::Min && localValue < results.value) ||
                (criteria == SearchCriteria::Max && localValue > results.value))
            {
                results.value = localValue;
                results.hasValue = true;
            }
        }

        // Search border rectangles
        if (domain != SearchDomains::RectanglesOnly &&
            regionMap->isBorderRectangleCalculated &&
            (filter == SearchFilter::AllRects || doesPassFilter(filter,
                                                                regionMap->borderRectangle,
                                                                relativeValue)))
        {
            // Get the appropriate value
            switch (field)
            {
                case RectField::Top:
                    {
                        localValue = regionMap->borderRectangle.y;
                        break;
                    }
                case RectField::Left:
                    {
                        localValue = regionMap->borderRectangle.x;
                        break;
                    }
                case RectField::Bottom:
                    {
                        localValue = regionMap->borderRectangle.y + regionMap->borderRectangle.height;
                        break;
                    }
                case RectField::Right:
                    {
                        localValue = regionMap->borderRectangle.x + regionMap->borderRectangle.width;
                        break;
                    }
            }

            // Update results as necessary
            if (!results.hasValue ||
                (criteria == SearchCriteria::Min && localValue < results.value) ||
                (criteria == SearchCriteria::Max && localValue > results.value))
            {
                results.value = localValue;
                results.hasValue = true;
            }
        }
    }

    return results;
}

bool MapBorderGenerator::isLocationEmpty(int32_t x, int32_t y)
{
    for (RegionMap*& regionMap : _regionMaps)
    {
        if (regionMap->borderRectangle.contains(x, y))
        {
            return false;
        }
    }

    for (RegionInaccessibleArea& inaccessibleArea : _inaccessibleAreas)
    {
        if (inaccessibleArea.getX() <= x && inaccessibleArea.getY() <= y &&
            inaccessibleArea.getX() + inaccessibleArea.getWidth() >= x &&
            inaccessibleArea.getY() + inaccessibleArea.getHeight() >= y)
        {
            return false;
        }
    }

    return true;
}

bool MapBorderGenerator::doesRectIntersectAnywhere(Rect rect)
{
    for (RegionMap*& regionMap : _regionMaps)
    {
        if (regionMap->borderRectangle.intersects(rect))
        {
            return true;
        }
    }

    for (RegionInaccessibleArea& inaccessibleArea : _inaccessibleAreas)
    {
        if (Rect(inaccessibleArea.getX(), inaccessibleArea.getY(),
                        inaccessibleArea.getWidth(), inaccessibleArea.getHeight()).intersects(rect))
        {
            return true;
        }
    }

    return false;
}

MapBorderGenerator::MapBorderResults MapBorderGenerator::getMapBorder(int32_t x, int32_t y, Direction askingDirection)
{
    // Inaccessible Regions
    for (RegionInaccessibleArea& inaccessibleArea : _inaccessibleAreas)
    {
        if (x < inaccessibleArea.getX() || y < inaccessibleArea.getY() ||
            x >= inaccessibleArea.getX() + inaccessibleArea.getWidth() ||
            y >= inaccessibleArea.getY() + inaccessibleArea.getHeight())
        {
            // Doesn't contain (x, y)
            continue;
        }

        // If it is already set, then return it
        MapBorder currentMapBorder(inaccessibleArea.getTile(x, y));
        if (currentMapBorder != MapBorder::None)
        {
            MapBorderResults results;
            results.hasValue = true;
            results.value = currentMapBorder;
            return results;
        }

        // Move about the inside of the inaccessible region in the complement of the asking direction
        // to locate either an already defined tile, or the bounds of the RIA.
        MapBorder currentValue;
        switch (askingDirection)
        {
            case Direction::North:
                {
                    int32_t bottom(inaccessibleArea.getY() + inaccessibleArea.getHeight());
                    int32_t currentY;
                    for (currentY = y + TILE_SIZE; currentY < bottom; currentY += TILE_SIZE)
                    {
                        currentValue = inaccessibleArea.getTile(x, currentY);
                        if (currentValue != MapBorder::None)
                        {
                            MapBorderResults result;
                            result.hasValue = true;
                            result.value = currentValue;
                            return result;
                        }
                    }

                    return getMapBorder(x, currentY, askingDirection);
                }
            case Direction::East:
                {
                    int32_t currentX;
                    for (currentX = x - TILE_SIZE; currentX >= inaccessibleArea.getX(); currentX -= TILE_SIZE)
                    {
                        currentValue = inaccessibleArea.getTile(currentX, y);
                        if (currentValue != MapBorder::None)
                        {
                            MapBorderResults result;
                            result.hasValue = true;
                            result.value = currentValue;
                            return result;
                        }
                    }

                    return getMapBorder(currentX, y, askingDirection);
                }
            case Direction::South:
                {
                    int32_t currentY;
                    for (currentY = y - TILE_SIZE; currentY >= inaccessibleArea.getY(); currentY -= TILE_SIZE)
                    {
                        currentValue = inaccessibleArea.getTile(x, currentY);
                        if (currentValue != MapBorder::None)
                        {
                            MapBorderResults result;
                            result.hasValue = true;
                            result.value = currentValue;
                            return result;
                        }
                    }

                    return getMapBorder(x, currentY, askingDirection);
                }
            case Direction::West:
                {
                    int32_t right(inaccessibleArea.getX() + inaccessibleArea.getWidth());
                    int32_t currentX;
                    for (currentX = x + TILE_SIZE; currentX < right; currentX += TILE_SIZE)
                    {
                        currentValue = inaccessibleArea.getTile(currentX, y);
                        if (currentValue != MapBorder::None)
                        {
                            MapBorderResults result;
                            result.hasValue = true;
                            result.value = currentValue;
                            return result;
                        }
                    }

                    return getMapBorder(currentX, y, askingDirection);
                }
        }
    }

    // Region Maps
    for (RegionMap*& regionMap : _regionMaps)
    {
        if (!regionMap->borderRectangle.contains(x, y))
        {
            continue;
        }

        int16_t tileX(std::floor((x - regionMap->rectangle.x) / TILE_SIZE));
        int16_t tileY(std::floor((y - regionMap->rectangle.y) / TILE_SIZE));
        MapBorderResults results;
        results.hasValue = true;

        if (x < regionMap->rectangle.x && x >= regionMap->borderRectangle.x)
        {
            // West, Northwest, Southwest
            if (tileY < 0)
            {
                // Northwest
                results.value = regionMap->getWestMapBorder(0);
            }
            else if (tileY >= regionMap->getHeight())
            {
                // Southwest
                results.value = regionMap->getWestMapBorder(regionMap->getHeight() - 1);
            }
            else
            {
                results.value = regionMap->getWestMapBorder(tileY);
            }
        }
        else if (x >= regionMap->rectangle.x + regionMap->rectangle.width &&
                 x < regionMap->borderRectangle.x + regionMap->borderRectangle.width)
        {
            // East, Northeast, Southeast
            if (tileY < 0)
            {
                // Northeast
                results.value = regionMap->getEastMapBorder(0);
            }
            else if (tileY >= regionMap->getHeight())
            {
                // Southeast
                results.value = regionMap->getEastMapBorder(regionMap->getHeight() - 1);
            }
            else
            {
                results.value = regionMap->getEastMapBorder(tileY);
            }
        }
        else if (y < regionMap->rectangle.y && y >= regionMap->borderRectangle.y)
        {
            // North (only due north)
            results.value = regionMap->getNorthMapBorder(tileX);
        }
        else if (y >= regionMap->rectangle.y + regionMap->rectangle.height &&
                 y < regionMap->borderRectangle.y + regionMap->borderRectangle.height)
        {
            // South (only due south)
            results.value = regionMap->getSouthMapBorder(tileX);
        }
        else if (regionMap->rectangle.contains(x, y))
        {
            // inside the map itself
            if (tileX > 0 && tileX < regionMap->getWidth() - 1 && tileY > 0 && tileY < regionMap->getHeight() - 1)
            {
                ExecutionException error("Cannot get a MapBorder for a location inside a map.");
                error.addData("tileX", toString(tileX));
                error.addData("tileY", toString(tileY));
                error.addData("x", toString(x));
                error.addData("y", toString(y));
                error.addData("targetMap", toString(regionMap->getMapNo()));
                error.addData("map width", toString(regionMap->getWidth()));
                error.addData("map height", toString(regionMap->getHeight()));
                throw error;
            }

            if (tileX == 0 && (askingDirection == Direction::East ||
                               (tileY > 0 && tileY < regionMap->getHeight() - 1)))
            {
                results.value = regionMap->getWestMapBorder(tileY);
            }
            else if (tileX == regionMap->getWidth() - 1 && (askingDirection == Direction::West ||
                                                            (tileY > 0 && tileY < regionMap->getHeight() - 1)))
            {
                results.value = regionMap->getEastMapBorder(tileY);
            }
            else if (tileY == 0 && (askingDirection == Direction::North ||
                                    (tileX > 0 && tileX < regionMap->getWidth() - 1)))
            {
                #warning "                ^--- error? South asking direction instead?"
                results.value = regionMap->getNorthMapBorder(tileX);
            }
            else if (tileY == regionMap->getHeight() - 1 && (askingDirection == Direction::South ||
                                                             (tileX > 0 && tileX < regionMap->getWidth() - 1)))
            {
                results.value = regionMap->getSouthMapBorder(tileX);
            }
            else
            {
                results.hasValue = false;
            }
        }
        else
        {
            results.hasValue = false;
        }

        if (results.hasValue)
        {
            return results;
        }
    }

    MapBorderResults noResults;
    noResults.hasValue = false;
    return noResults;
}

MapBorderTile MapBorderGenerator::startOutdoorMapBorderTile(int32_t currentX, int32_t currentY,
                                                            Rect& rectangle, Rect& borderRectangle)
{
    bool isNorthMap(currentY < rectangle.y);
    bool isEastMap(currentX >= rectangle.x + rectangle.width);
    bool isSouthMap(currentY >= rectangle.y + rectangle.height);
    bool isWestMap(currentX < rectangle.x);

    // Determine direction
    Direction direction;
    if (isNorthMap)
    {
        direction = Direction::North;
    }
    else if (isSouthMap)
    {
        direction = Direction::South;
    }
    else if (isWestMap)
    {
        direction = Direction::West;
    }
    else
    {
        direction = Direction::East;
    }

    // Finish
    return MapBorderTile(direction, isCurrentRowOdd(currentY), isCurrentColumnOdd(currentX));
}

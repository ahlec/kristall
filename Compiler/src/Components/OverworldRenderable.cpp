#include "KristallCompiler.hpp"

/* OverworldRenderable::Overflow */
OverworldRenderable::Overflow::Overflow(uint32_t sprteId) : spriteId(sprteId)
{
}

bool OverworldRenderable::Overflow::hasIntersection(Vector2& location)
{
    for (size_t index(0); index < instances.size(); ++index)
    {
        if (instances[index] == location)
        {
            return true;
        }
    }
    return false;
}

SDL_Surface* createSurface(int width, int height)
{
    #if SDL_BYTEORDER == SDL_BIG_ENDIAN
        uint32_t rmask = 0xff000000;
        uint32_t gmask = 0x00ff0000;
        uint32_t bmask = 0x0000ff00;
        uint32_t amask = 0x000000ff;
    #else
        uint32_t rmask = 0x000000ff;
        uint32_t gmask = 0x0000ff00;
        uint32_t bmask = 0x00ff0000;
        uint32_t amask = 0xff000000;
    #endif

    return SDL_CreateRGBSurface(0, width, height, 32, rmask, gmask, bmask, amask);
};

/* OverworldRenderable */
OverworldRenderable::OverworldRenderable(UnicodeString name,
                                         uint16_t width,
                                         uint16_t height) : _name(name),
                                                          _width(width),
                                                          _height(height),
                                                          _hasSuperlayer(false),
                                                          _mainTexture(nullptr),
                                                          _superlayerTexture(nullptr)
{
    // Populate capped data stores
    size_t nTiles(_width * _height);
    _isTileCapped.reserve(nTiles);
    _isSuperlayerCapped.reserve(nTiles);
    for (size_t index = 0; index < nTiles; ++index)
    {
        _isTileCapped.push_back(false);
        _isSuperlayerCapped.push_back(false);
    }

    // Create the main texture
    _mainTexture = createSurface(_width * TILE_SIZE, _height * TILE_SIZE);
    if (_mainTexture == nullptr)
    {
        ExecutionException error("Could not create the RenderTexture to represent the main background of this OverworldRenderable.");
        error.addData("width", toString(_width));
        error.addData("height", toString(_height));
        throw error;
    }
}

std::shared_ptr<OverworldRenderable> OverworldRenderable::deserialize(CompilerBinaryReader& reader)
{
    UnicodeString name(reader.readString());
    uint16_t width(reader.readUInt16());
    uint16_t height(reader.readUInt16());

    std::shared_ptr<OverworldRenderable> renderable(new OverworldRenderable(name, width, height));

    // Main texture
    SDL_Surface* tempSurface(reader.readSurface());
    SDL_BlitSurface(tempSurface, NULL, renderable->_mainTexture, NULL);
    SDL_FreeSurface(tempSurface);

    size_t nOverflows(reader.readUInt32());
    uint32_t nEntries;
    uint32_t subIndex;
    Overflow tempOverflow(0);
    Vector2 tempInstance;
    for (size_t index = 0; index < nOverflows; ++index)
    {
        tempOverflow = Overflow(reader.readUInt32());
        nEntries = reader.readUInt32();
        tempOverflow.instances.reserve(nEntries);

        for (subIndex = 0; subIndex < nEntries; ++subIndex)
        {
            tempInstance.x = reader.readInt32();
            tempInstance.y = reader.readInt32();
            tempOverflow.instances.push_back(tempInstance);
        }

        renderable->_overflows.push_back(tempOverflow);
    }

    // Superlayer
    renderable->_hasSuperlayer = reader.readBool();
    if (renderable->_hasSuperlayer)
    {
        renderable->_superlayerTexture = createSurface(width * TILE_SIZE, height * TILE_SIZE);
        if (renderable->_superlayerTexture == nullptr)
        {
            ExecutionException error("Could not create a RenderTexture to represent this OverworldRenderable's superlayer.");
            error.addData("width", toString(width));
            error.addData("height", toString(height));
            throw error;
        }

        SDL_Surface* tempSuperlayerSurface(reader.readSurface());
        SDL_BlitSurface(tempSuperlayerSurface, NULL, renderable->_superlayerTexture, NULL);
        SDL_FreeSurface(tempSuperlayerSurface);

        size_t nSuperlayerOverflows(reader.readUInt32());
        for (size_t index = 0; index < nSuperlayerOverflows; ++index)
        {
            tempOverflow = Overflow(reader.readUInt32());
            nEntries = reader.readUInt32();
            tempOverflow.instances.reserve(nEntries);

            for (subIndex = 0; subIndex < nEntries; ++subIndex)
            {
                tempInstance.x = reader.readInt32();
                tempInstance.y = reader.readInt32();
                tempOverflow.instances.push_back(tempInstance);
            }

            renderable->_superlayerOverflows.push_back(tempOverflow);
        }
    }

    // Caps
    uint16_t tileX;
    uint16_t tileY;
    for (tileY = 0; tileY < height; ++tileY)
    {
        for (tileX = 0; tileX < width; ++tileX)
        {
            renderable->_isTileCapped[(tileY * width) + tileX] = reader.readBool();
            renderable->_isSuperlayerCapped[(tileY * width) + tileX] = reader.readBool();
        }
    }

    // Finish
    return renderable;
}

UnicodeString OverworldRenderable::getName() const
{
    return _name;
}

uint16_t OverworldRenderable::getWidth() const
{
    return _width;
}

uint16_t OverworldRenderable::getHeight() const
{
    return _height;
}

void OverworldRenderable::add(uint16_t tileX, uint16_t tileY,
                              SpriteSheetFile* spriteSheet, uint32_t entryNo,
                              bool addToSuperlayer)
{
    // Check to make sure specified position is valid
    if (tileX >= _width || tileY >= _height)
    {
        ExecutionException error("Attempted to add a sprite to a location out of the bounds of the OverworldRenderable.");
        error.addData("tileX", toString(tileX));
        error.addData("width", toString(_width));
        error.addData("tileY", toString(tileY));
        error.addData("height", toString(_height));
        throw error;
    }

    // Check to make sure the sprite sheet exists.
    if (spriteSheet == nullptr)
    {
        ExecutionException error("A valid SpriteSheetFile must be provided when adding a sprite to an OverworldRenderable.");
        throw error;
    }

    // Do nothing if the entry number is the empty sprite.
    if (entryNo == 0)
    {
        return;
    }

    // Determine the absolute location of this addition (also serves as a point of checking
    // to make sure the provided sprite/animation exists)
    int32_t absoluteX(tileX * TILE_SIZE);
    int32_t absoluteY(tileY * TILE_SIZE);
    bool isAnimated(false);
    SpriteSheetSpriteInfo spriteInfo;
    try
    {
        spriteInfo = spriteSheet->getSprite(entryNo);
        absoluteX += spriteInfo.offsetX;
        absoluteY += spriteInfo.offsetY;
        isAnimated = spriteInfo.isUniversalAnimation;
    }
    catch (ExecutionException& error)
    {
        throw;
    }

    // If we're adding to the superlayer, ensure that we have registered that we have
    // a superlayer
    if (addToSuperlayer && !_hasSuperlayer)
    {
        _hasSuperlayer = true;
        _superlayerTexture = createSurface(_width * TILE_SIZE, _height * TILE_SIZE);
        if (_superlayerTexture == nullptr)
        {
            ExecutionException error("Could not create a RenderTexture to represent this OverworldRenderable's superlayer.");
            error.addData("width", toString(_width));
            error.addData("height", toString(_height));
            throw error;
        }
    }

    // If the tile is already capped, overflow the add and finish (nothing more to do)
    if (isCapped(tileX, tileY, addToSuperlayer))
    {
        addOverflow(entryNo, addToSuperlayer, absoluteX, absoluteY);
        return;
    }

    // Cap and overflow, if the specified sprite is animated
    if (isAnimated)
    {
        addOverflow(entryNo, addToSuperlayer, absoluteX, absoluteY);

        if (addToSuperlayer)
        {
            _isSuperlayerCapped[(tileY * _width) + tileX] = true;
        }
        else
        {
            _isTileCapped[(tileY * _width) + tileX] = true;
        }

        return;
    }

    // Draw to the appropriate texture
    SDL_Rect srcRect;
    srcRect.x = spriteInfo.topLeftCorner.x;
    srcRect.y = spriteInfo.topLeftCorner.y;
    srcRect.w = spriteInfo.bottomRightCorner.x - spriteInfo.topLeftCorner.x;
    srcRect.h = spriteInfo.bottomRightCorner.y - spriteInfo.topLeftCorner.y;
    SDL_Rect destRect;
    destRect.x = absoluteX;
    destRect.y = absoluteY;
    destRect.w = TILE_SIZE;
    destRect.h = TILE_SIZE;
    SDL_BlitSurface(spriteSheet->getTexture(), &srcRect,
        (addToSuperlayer ? _superlayerTexture : _mainTexture), &destRect);
}

void OverworldRenderable::blendIn(OverworldRenderable& other, uint16_t tileX, uint16_t tileY)
{
    // Ensure that the location and size are appropriate
    if (tileX >= _width || tileY >= _height)
    {
        ExecutionException error("The provided blend location is out of the bounds of the destination OverworldRenderable.");
        error.addData("tileX", toString(tileX));
        error.addData("tileY", toString(tileY));
        error.addData("width", toString(_width));
        error.addData("height", toString(_height));
        throw error;
    }

    if (tileX + other.getWidth() > _width || tileY + other.getHeight() > _height)
    {
        ExecutionException error("The provided OverworldRenderable to blend into the destination OverworldRenderable is too large to fit.");
        error.addData("tileX", toString(tileX));
        error.addData("tileY", toString(tileY));
        error.addData("other width", toString(other.getWidth()));
        error.addData("other height", toString(other.getHeight()));
        error.addData("destination width", toString(_width));
        error.addData("destination height", toString(_height));
        throw error;
    }

    // Draw the main texture
    SDL_Rect blendDestRect;
    blendDestRect.x = tileX * TILE_SIZE;
    blendDestRect.y = tileY * TILE_SIZE;
    blendDestRect.w = other._width * TILE_SIZE;
    blendDestRect.h = other._height * TILE_SIZE;
    SDL_BlitSurface(other._mainTexture, NULL, _mainTexture, &blendDestRect);

    // Copy over main overflows
    Vector2 overflowOffsets(tileX * TILE_SIZE, tileY * TILE_SIZE);
    for (Overflow& overflow : other._overflows)
    {
        for (Vector2& instance : overflow.instances)
        {
            addOverflow(overflow.spriteId, false, instance.x + overflowOffsets.x, instance.y + overflowOffsets.y);
        }
    }

    // Copy over the superlayer, if the blend-in has one
    if (other._hasSuperlayer)
    {
        // Create the superlayer here if this destination OverworldRenderable doesn't have a superlayer
        _hasSuperlayer = true;
        _superlayerTexture = createSurface(_width * TILE_SIZE, _height * TILE_SIZE);
        if (_superlayerTexture == nullptr)
        {
            ExecutionException error("Could not create a RenderTexture to represent this OverworldRenderable's superlayer.");
            error.addData("width", toString(_width));
            error.addData("height", toString(_height));
            throw error;
        }

        // Draw the superlayer texture
        SDL_BlitSurface(other._superlayerTexture, NULL, _superlayerTexture, &blendDestRect);

        // Copy over superlayer overflows
        for (Overflow& overflow : other._superlayerOverflows)
        {
            for (Vector2& instance : overflow.instances)
            {
                addOverflow(overflow.spriteId, true, instance.x + overflowOffsets.x, instance.y + overflowOffsets.y);
            }
        }
    }

    // Copy over the caps
    uint16_t copyX;
    uint16_t copyY;
    for (copyY = 0; copyY < other._height; ++copyY)
    {
        for (copyX = 0; copyX < other._width; ++copyX)
        {
            _isTileCapped[((copyY + tileY) * _width) + (tileX + copyX)] = other.isCapped(copyX, copyY, false);
            _isSuperlayerCapped[((copyY + tileY) * _width) + (tileX + copyX)] = other.isCapped(copyX, copyY, true);
        }
    }
}

OverworldRenderableReport OverworldRenderable::write(CompilerBinaryWriter& writer, File* issuer)
{
    writer.writeUInt16(_width);
    writer.writeUInt16(_height);

    UnicodeString xmlSafeName(_name);
    xmlSafeName.findAndReplace(" ", "_");

    // Main texture
    uint32_t mainTextureNo(TextureManager::give(_mainTexture, issuer, xmlSafeName + "-Main"));
    writer.writeUInt32(mainTextureNo);
    size_t nMainOverflows(_overflows.size());
    writer.writeUInt32(nMainOverflows);
    for (Overflow& overflow : _overflows)
    {
        writer.writeUInt32(overflow.spriteId);
        writer.writeUInt32(overflow.instances.size());
        for (Vector2& instance : overflow.instances)
        {
            writer.writeUInt32(instance.x);
            writer.writeUInt32(instance.y);
        }
    }

    // Superlayer
    writer.writeBool(_hasSuperlayer);
    uint32_t superlayerTextureNo(0);
    if (_hasSuperlayer)
    {
        superlayerTextureNo = TextureManager::give(_superlayerTexture, issuer, xmlSafeName + "-Superlayer");
        writer.writeUInt32(superlayerTextureNo);

        size_t nSuperlayerOverflows(_superlayerOverflows.size());
        writer.writeUInt32(nSuperlayerOverflows);
        for (Overflow& overflow : _superlayerOverflows)
        {
            writer.writeUInt32(overflow.spriteId);
            writer.writeUInt32(overflow.instances.size());
            for (Vector2& instance : overflow.instances)
            {
                writer.writeUInt32(instance.x);
                writer.writeUInt32(instance.y);
            }
        }
    }

    // Finish up
    OverworldRenderableReport report(*this, mainTextureNo, superlayerTextureNo);
    report.saveReferences();    // Save here, because the textures will be destroyed
                                // when the OverworldRenderable is destroyed. This function
                                // is protected against multiple calls.
    return report;
}

void OverworldRenderable::serialize(CompilerBinaryWriter& writer)
{
    writer.writeString(_name);
    writer.writeUInt16(_width);
    writer.writeUInt16(_height);

    // Main texture
    writer.writeTexture(_mainTexture);
    size_t nOverflows(_overflows.size());
    writer.writeUInt32(nOverflows);
    for (Overflow& overflowEntry : _overflows)
    {
        writer.writeUInt32(overflowEntry.spriteId);
        writer.writeUInt32(overflowEntry.instances.size());
        for (Vector2& instance : overflowEntry.instances)
        {
            writer.writeInt32(instance.x);
            writer.writeInt32(instance.y);
        }
    }

    // Superlayer
    writer.writeBool(_hasSuperlayer);
    if (_hasSuperlayer)
    {
        writer.writeTexture(_superlayerTexture);

        size_t nSuperlayerOverflows(_superlayerOverflows.size());
        writer.writeUInt32(nSuperlayerOverflows);
        for (Overflow& overflowEntry : _superlayerOverflows)
        {
            writer.writeUInt32(overflowEntry.spriteId);
            writer.writeUInt32(overflowEntry.instances.size());
            for (Vector2& overflowInstance : overflowEntry.instances)
            {
                writer.writeInt32(overflowInstance.x);
                writer.writeInt32(overflowInstance.y);
            }
        }
    }

    // Caps
    uint16_t tileX;
    uint16_t tileY;
    for (tileY = 0; tileY < _height; ++tileY)
    {
        for (tileX = 0; tileX < _width; ++tileX)
        {
            writer.writeBool(isCapped(tileX, tileY, false));
            writer.writeBool(isCapped(tileX, tileY, true));
        }
    }
}

void OverworldRenderable::addOverflow(uint32_t entryNo, bool addToSuperlayer, int32_t absoluteX, int32_t absoluteY)
{
    std::vector<Overflow>& appropriateOverflow(addToSuperlayer ? _superlayerOverflows : _overflows);
    Vector2 newLocation(absoluteX, absoluteY);

    Overflow* currentCandidate(nullptr);
    for (Overflow& overflow : appropriateOverflow)
    {
        if (overflow.spriteId == entryNo && currentCandidate == nullptr)
        {
            currentCandidate = &overflow;
        }
        else if (overflow.spriteId != entryNo && currentCandidate != nullptr)
        {
            if (overflow.hasIntersection(newLocation))
            {
                currentCandidate = nullptr;
            }
        }
    }

    if (currentCandidate != nullptr)
    {
        currentCandidate->instances.push_back(newLocation);
    }
    else
    {
        Overflow newOverflow(entryNo);
        newOverflow.instances.push_back(newLocation);
        appropriateOverflow.push_back(newOverflow);
    }
}

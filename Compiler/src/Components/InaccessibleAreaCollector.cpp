#include "KristallCompiler.hpp"
#include <algorithm>

InaccessibleAreaCollector::InaccessibleAreaCollector(UnicodeString regionName,
                                                     std::vector<Vector2> validTiles,
                                                     int32_t minX, int32_t minY,
                                                     int32_t maxX, int32_t maxY,
                                                     FunctionPtr<bool, int32_t, int32_t> isLocationEmpty,
                                                     FunctionPtr<bool, Rect> rectIntersects) :
                                                         _regionName(regionName), _hasNext(false),
                                                        _minX(minX), _minY(minY), _maxX(maxX),
                                                        _maxY(maxY), _isLocationEmpty(isLocationEmpty),
                                                        _rectIntersects(rectIntersects), _current("NULLREG", 0, 0, 0, 0),
                                                        _currentIteration(0)
{
    _tiles.swap(validTiles);
}

bool InaccessibleAreaCollector::hasMore()
{
    return !_tiles.empty();
}

class TileRemover
{
public:
    TileRemover(Rect newRegion) : _newRegion(newRegion)
    {
    }

    bool operator()(Vector2 tile)
    {
        return _newRegion.contains(tile);
    }

private:
    Rect _newRegion;
};

bool InaccessibleAreaCollector::next()
{
    // Find our start location
    int32_t startX;
    int32_t startY;
    do
    {
        if (_tiles.empty())
        {
            _hasNext = false;
            return false;
        }

        startX = _tiles.front().x;
        startY = _tiles.front().y;
        _tiles.erase(_tiles.begin());
    } while (startX < _minX || startY < _minY ||
             startX > _maxX || startY > _maxY);

    // Move the start position as far to the left as it can contiguously go without
    // intersecting with anything that exists
    int32_t containX(startX - (TILE_SIZE / 2));
    int32_t containY(startY + (TILE_SIZE / 2));
    while (startX > _minX && _isLocationEmpty(containX, containY))
    {
        if (startX < _minX + TILE_SIZE - 1)
        {
            startX = startX + 1;
            break;
        }

        containX -= TILE_SIZE;
        startX -= TILE_SIZE;
    }
    if (startX < _minX)
    {
        startX = _minX;
    }

    // Move the start position as far north now, in the same manner
    containX = startX + (TILE_SIZE / 2);
    containY = startY - (TILE_SIZE / 2);
    while (startY - TILE_SIZE >= _minY && _isLocationEmpty(containX, containY))
    {
        if (startY - TILE_SIZE < _minY)
        {
            break;
        }

        containY -= TILE_SIZE;
        startY -= TILE_SIZE;
    }
    if (startY < _minY)
    {
        startY = _minY;
    }

    // Check to make sure that the start position is a valid multiple of the tile size
    if (startY % TILE_SIZE != 0 || startX % TILE_SIZE != 0)
    {
        ExecutionException error("Start locations for inaccessible areas must be multiples of the tile size.");
        error.addData("tileSize", toString(TILE_SIZE));
        error.addData("startX", toString(startX));
        error.addData("startY", toString(startY));
        error.addData("x mod size", toString(startX % TILE_SIZE));
        error.addData("y mod size", toString(startY % TILE_SIZE));
        throw error;
    }

    // Find the end position by going right as far as possible without intersecting
    // anything
    int32_t endX(startX);
    containX = endX + (TILE_SIZE / 2);
    containY = startY + (TILE_SIZE / 2);
    while (endX < _maxX && _isLocationEmpty(containX, containY))
    {
        endX += TILE_SIZE;
        containX += TILE_SIZE;
    }

    // Build a rectangle using the startX, startY, and endX that we have generated so far. We will set its
    // height initially to TILE_SIZE**, to reflect that it occupies a full tile. We will then continue to increase
    // the height by TILE_SIZE until we have a conflict, where 1) the bottom of the rectangle is greater than
    // maxYLocation or 2) the rectangle intersects with either a map border or an already-existing
    // inaccessible region. When this happens, we will subtract 16 from the height, because the conflicting
    // row does not count for the region. **BECAUSE WE FINISH UP BY SUBTRACTING TILE_SIZE FROM THE HEIGHT, we initially
    // set the height of our rectangle to TILE_SIZE * 2, not to TILE_SIZE (though logically, we mean it to be
    // only TILE_SIZE, and thus why my comment above is so -- for clarity/explanation)
    Rect encompassRect(startX, startY, endX - startX, TILE_SIZE * 2);
    while (encompassRect.y + encompassRect.height < _maxY && !_rectIntersects(encompassRect))
    {
        encompassRect.height += TILE_SIZE;
    }
    int32_t endY(startY + encompassRect.height - TILE_SIZE);

    // See if we have a rectangle with an area now
    if (endX <= startX || endY <= startY)
    {
        _hasNext = false;
        return false;
    }

    // We have a valid region, so we need to remove all of the possible tiles that this current rectangle
    // encompasses.
    TileRemover remover(encompassRect);
    _tiles.erase(std::remove_if(_tiles.begin(), _tiles.end(), remover), _tiles.end());

    // Populate the data and return
    _hasNext = true;
    _current = RegionInaccessibleArea(_regionName + "_RIA" + UnicodeStrings::fromInt64(_currentIteration),
                                      startX, startY, endX - startX, endY - startY);
    ++_currentIteration;
    return true;
}

RegionInaccessibleArea InaccessibleAreaCollector::get()
{
    return _current;
}

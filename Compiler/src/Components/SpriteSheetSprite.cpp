#include "KristallCompiler.hpp"

SpriteSheetSprite::SpriteSheetSprite(XmlElementPtr node) : _node(node)
{
}

SpriteSheetSprite::SpriteSheetSprite() : _node(nullptr)
{
}

uint32_t SpriteSheetSprite::getId() const
{
    return _node->getAttribute("ID")->getUInt32();
}

bool SpriteSheetSprite::isUniversalAnimation() const
{
    return _node->hasAttribute("Duration");
}

SpriteTransformation SpriteSheetSprite::getTransformation() const
{
    if (_node->hasAttribute("Rotation"))
    {
        uint16_t rotation(_node->getAttribute("Rotation")->getUInt16());
        if (rotation == 90)
        {
            return SpriteTransformation::Rotate90;
        }
        else if (rotation == 180)
        {
            return SpriteTransformation::Rotate180;
        }
        else if (rotation == 270)
        {
            return SpriteTransformation::Rotate270;
        }
        else if (rotation == 45)
        {
            return SpriteTransformation::Rotate45;
        }
        else if (rotation == 225)
        {
            return SpriteTransformation::Rotate225;
        }

        ExecutionException error("An invalid rotation amount was provided for the sprite.");
        error.addData("rotation", toString(rotation));
        throw error;
    }

    return SpriteTransformation::None;
}

Vector2 SpriteSheetSprite::getTopLeftCorner(const std::vector<SpriteSheetSprite>& definedSprites) const
{
    SpriteTransformation transformation(getTransformation());
    if (transformation == SpriteTransformation::None)
    {
        Rect rect(getDefinedRect());
        return Vector2(rect.x, rect.y);
    }

    uint32_t baseSpriteNo(getBaseSpriteId());
    if (baseSpriteNo >= definedSprites.size())
    {
        ExecutionException error("The base sprite for a transformation sprite must be defined before the transformed sprite.");
        throw error;
    }

    uint32_t id = getId();

    if (definedSprites[baseSpriteNo].getTransformation() != SpriteTransformation::None)
    {
        ExecutionException error("A transformed sprite may not be a transformation of a transformed sprite.");
        throw error;
    }

    Rect baseRect(definedSprites[baseSpriteNo].getDefinedRect());

    switch (transformation)
    {
        case SpriteTransformation::Rotate90:
            {
                return Vector2(baseRect.x, baseRect.y + baseRect.height);
            }
        case SpriteTransformation::Rotate180:
            {
                return Vector2(baseRect.x + baseRect.width, baseRect.y + baseRect.height);
            }
        case SpriteTransformation::Rotate270:
            {
                return Vector2(baseRect.x + baseRect.width, baseRect.y);
            }
        default:
            {
                ExecutionException error("The provided sprite transformation is currently unhandled.");
                throw error;
            }
    }
}

Vector2 SpriteSheetSprite::getTopRightCorner(const std::vector<SpriteSheetSprite>& definedSprites) const
{
    SpriteTransformation transformation(getTransformation());
    if (transformation == SpriteTransformation::None)
    {
        Rect rect(getDefinedRect());
        return Vector2(rect.x + rect.width, rect.y);
    }

    uint32_t baseSpriteNo(getBaseSpriteId());
    if (baseSpriteNo >= definedSprites.size())
    {
        ExecutionException error("The base sprite for a transformation sprite must be defined before the transformed sprite.");
        throw error;
    }

    if (definedSprites[baseSpriteNo].getTransformation() != SpriteTransformation::None)
    {
        ExecutionException error("A transformed sprite may not be a transformation of a transformed sprite.");
        throw error;
    }

    Rect baseRect(definedSprites[baseSpriteNo].getDefinedRect());

    switch (transformation)
    {
        case SpriteTransformation::Rotate90:
            {
                return Vector2(baseRect.x, baseRect.y);
            }
        case SpriteTransformation::Rotate180:
            {
                return Vector2(baseRect.x, baseRect.y + baseRect.height);
            }
        case SpriteTransformation::Rotate270:
            {
                return Vector2(baseRect.x + baseRect.width, baseRect.y + baseRect.height);
            }
        default:
            {
                ExecutionException error("The provided sprite transformation is currently unhandled.");
                throw error;
            }
    }
}

Vector2 SpriteSheetSprite::getBottomRightCorner(const std::vector<SpriteSheetSprite>& definedSprites) const
{
    SpriteTransformation transformation(getTransformation());
    if (transformation == SpriteTransformation::None)
    {
        Rect rect(getDefinedRect());
        return Vector2(rect.x + rect.width, rect.y + rect.height);
    }

    uint32_t baseSpriteNo(getBaseSpriteId());
    if (baseSpriteNo >= definedSprites.size())
    {
        ExecutionException error("The base sprite for a transformation sprite must be defined before the transformed sprite.");
        throw error;
    }

    if (definedSprites[baseSpriteNo].getTransformation() != SpriteTransformation::None)
    {
        ExecutionException error("A transformed sprite may not be a transformation of a transformed sprite.");
        throw error;
    }

    Rect baseRect(definedSprites[baseSpriteNo].getDefinedRect());

    switch (transformation)
    {
        case SpriteTransformation::Rotate90:
            {
                return Vector2(baseRect.x + baseRect.width, baseRect.y);
            }
        case SpriteTransformation::Rotate180:
            {
                return Vector2(baseRect.x, baseRect.y);
            }
        case SpriteTransformation::Rotate270:
            {
                return Vector2(baseRect.x, baseRect.y + baseRect.height);
            }
        default:
            {
                ExecutionException error("The provided sprite transformation is currently unhandled.");
                throw error;
            }
    }
}

Vector2 SpriteSheetSprite::getBottomLeftCorner(const std::vector<SpriteSheetSprite>& definedSprites) const
{
    SpriteTransformation transformation(getTransformation());
    if (transformation == SpriteTransformation::None)
    {
        Rect rect(getDefinedRect());
        return Vector2(rect.x, rect.y + rect.height);
    }

    uint32_t baseSpriteNo(getBaseSpriteId());
    if (baseSpriteNo >= definedSprites.size())
    {
        ExecutionException error("The base sprite for a transformation sprite must be defined before the transformed sprite.");
        throw error;
    }

    if (definedSprites[baseSpriteNo].getTransformation() != SpriteTransformation::None)
    {
        ExecutionException error("A transformed sprite may not be a transformation of a transformed sprite.");
        throw error;
    }

    Rect baseRect(definedSprites[baseSpriteNo].getDefinedRect());

    switch (transformation)
    {
        case SpriteTransformation::Rotate90:
            {
                return Vector2(baseRect.x + baseRect.width, baseRect.y + baseRect.height);
            }
        case SpriteTransformation::Rotate180:
            {
                return Vector2(baseRect.x + baseRect.width, baseRect.y);
            }
        case SpriteTransformation::Rotate270:
            {
                return Vector2(baseRect.x, baseRect.y);
            }
        default:
            {
                ExecutionException error("The provided sprite transformation is currently unhandled.");
                throw error;
            }
    }
}

uint16_t SpriteSheetSprite::getWidth(const std::vector<SpriteSheetSprite>& definedSprites) const
{
    SpriteTransformation transformation(getTransformation());
    Rect rect;
    if (transformation == SpriteTransformation::None)
    {
        rect = getDefinedRect();
    }
    else
    {
        uint32_t baseSpriteNo(getBaseSpriteId());
        if (baseSpriteNo >= definedSprites.size())
        {
            ExecutionException error("The base sprite for a transformation sprite must be defined before the transformed sprite.");
            throw error;
        }

        if (definedSprites[baseSpriteNo].getTransformation() != SpriteTransformation::None)
        {
            ExecutionException error("A transformed sprite may not be a transformation of a transformed sprite.");
            throw error;
        }

        rect = definedSprites[baseSpriteNo].getDefinedRect();
    }

    if (rect.width > UINT16_MAX)
    {
        ExecutionException error("A sprite's width may not exceed the bounds of a uint16_t.");
        throw error;
    }

    return static_cast<uint16_t>(rect.width);
}

uint16_t SpriteSheetSprite::getHeight(const std::vector<SpriteSheetSprite>& definedSprites) const
{
    SpriteTransformation transformation(getTransformation());
    Rect rect;
    if (transformation == SpriteTransformation::None)
    {
        rect = getDefinedRect();
    }
    else
    {
        uint32_t baseSpriteNo(getBaseSpriteId());
        if (baseSpriteNo >= definedSprites.size())
        {
            ExecutionException error("The base sprite for a transformation sprite must be defined before the transformed sprite.");
            throw error;
        }

        if (definedSprites[baseSpriteNo].getTransformation() != SpriteTransformation::None)
        {
            ExecutionException error("A transformed sprite may not be a transformation of a transformed sprite.");
            throw error;
        }

        rect = definedSprites[baseSpriteNo].getDefinedRect();
    }

    if (rect.height > UINT16_MAX)
    {
        ExecutionException error("A sprite's height may not exceed the bounds of a uint16_t.");
        throw error;
    }

    return static_cast<uint16_t>(rect.height);
}

int32_t SpriteSheetSprite::getOffsetX() const
{
    if (_node->hasAttribute("OffsetX"))
    {
        return _node->getAttribute("OffsetX")->getInt32();
    }

    return 0;
}

int32_t SpriteSheetSprite::getOffsetY() const
{
    if (_node->hasAttribute("OffsetY"))
    {
        return _node->getAttribute("OffsetY")->getInt32();
    }

    return 0;
}

XmlElementPtr SpriteSheetSprite::getXml() const
{
    return _node;
}

uint32_t SpriteSheetSprite::getBaseSpriteId() const
{
    if (isUniversalAnimation())
    {
        std::vector<UnicodeString> uncleanBreak(UnicodeStrings::split(_node->getValue().getString(), ':'));

        int64_t rawFirstSprite(UnicodeStrings::toInt64(uncleanBreak[0]));
        if (rawFirstSprite < 0 || rawFirstSprite > UINT32_MAX)
        {
            ExecutionException error("Could not get the first sprite of the first frame of the universal animation.");
            error.addData("rawFirstSprite", toString(rawFirstSprite));
            error.addData("id", toString(getId()));
            throw error;
        }

        return static_cast<uint32_t>(rawFirstSprite);
    }

    if (getTransformation() != SpriteTransformation::None)
    {
        return _node->getValue().getUInt32();
    }

    return -1;
}

Rect SpriteSheetSprite::getDefinedRect() const
{
    std::vector<UnicodeString> rectPieces(UnicodeStrings::split(_node->getValue().getString(), ','));

    Rect rect;
    rect.x = UnicodeStrings::toInt64(rectPieces[0]);
    rect.y = UnicodeStrings::toInt64(rectPieces[1]);
    rect.width = UnicodeStrings::toInt64(rectPieces[2]);
    rect.height = UnicodeStrings::toInt64(rectPieces[3]);
    return rect;
}

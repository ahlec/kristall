#include "KristallCompiler.hpp"

RegionMap::RegionMap(UnicodeString mapHandle) : _handle(mapHandle), _isOutdoor(false),
                                                isRectangleCalculated(false),
                                                isBorderRectangleCalculated(false)
{
    XmlDocument xml(XmlDocument::load(getSourceXmlFilename()));

    // Determine if the map is an outdoor map
    if (xml.hasChild("IsInterior"))
    {
        return;
    }
    _isOutdoor = true;

    // Map bridges
    XmlElementPtr currentMapBridgeNode(xml.firstChild("Tiles")->firstChild("MapBridgeTile"));
    while (currentMapBridgeNode != nullptr)
    {
        RegionMapBridge mapBridge;
        mapBridge.tileX = currentMapBridgeNode->getAttribute("X")->getUInt16();
        mapBridge.tileY = currentMapBridgeNode->getAttribute("Y")->getUInt16();
        mapBridge.destinationMapNo = Handles::getHandleNo("Maps",
                                                          currentMapBridgeNode->firstChild("DestinationMap")->getValue().getString());
        mapBridge.destinationX = currentMapBridgeNode->firstChild("DestinationX")->getValue().getUInt16();
        mapBridge.destinationY = currentMapBridgeNode->firstChild("DestinationY")->getValue().getUInt16();
        mapBridge.inwardArrow = currentMapBridgeNode->firstChild("InwardArrow")->getValue().getDirection();
        _mapBridges.push_back(mapBridge);

        currentMapBridgeNode = currentMapBridgeNode->nextSibling("MapBridgeTile");
    }
    if (_mapBridges.size() == 0)
    {
        return;
    }

    // Parse data
    _regionNo = Handles::getHandleNo("Regions", xml.firstChild("Region")->getValue().getString());
    _mapNo = Handles::getHandleNo("Maps", mapHandle);
    _mapName = xml.firstChild("Name")->getValue().getString();
    _width = xml.firstChild("Width")->getValue().getUInt16();
    _height = xml.firstChild("Height")->getValue().getUInt16();

    // Parse map borders
    _mapBorders.reserve(_width * 2 + _height * 2);
    XmlElementPtr currentNorthBorder(xml.firstChild("MapBorders")->firstChild("North")->firstChild());
    size_t x(0);
    while (x < _width)
    {
        if (x > currentNorthBorder->getAttribute("End")->getUInt16())
        {
            currentNorthBorder = currentNorthBorder->nextSibling();
            if (currentNorthBorder == nullptr || currentNorthBorder->getAttribute("Start")->getUInt16() != x)
            {
                ExecutionException error("MapBorder nodes must be defined, in ascending order, to cover every space without skipping any map values.");
                error.addData("side", "north");
                error.addData("x", toString(x));
                error.addData("map", mapHandle);
                throw error;
            }
        }
        _mapBorders.push_back(Enums::parseKeep<MapBorder>(currentNorthBorder->getValue().getString()));
        ++x;
    }

    XmlElementPtr currentEastBorder(xml.firstChild("MapBorders")->firstChild("East")->firstChild());
    size_t y(0);
    while (y < _height)
    {
        if (y > currentEastBorder->getAttribute("End")->getUInt16())
        {
            currentEastBorder = currentEastBorder->nextSibling();
            if (currentEastBorder == nullptr || currentEastBorder->getAttribute("Start")->getUInt16() != y)
            {
                ExecutionException error("MapBorder nodes must be defined, in ascending order, to cover every space without skipping any map values.");
                error.addData("side", "east");
                error.addData("y", toString(y));
                error.addData("map", mapHandle);
                throw error;
            }
        }
        _mapBorders.push_back(Enums::parseKeep<MapBorder>(currentEastBorder->getValue().getString()));
        ++y;
    }

    XmlElementPtr currentSouthBorder(xml.firstChild("MapBorders")->firstChild("South")->firstChild());
    x = 0;
    while (x < _width)
    {
        if (x > currentSouthBorder->getAttribute("End")->getUInt16())
        {
            currentSouthBorder = currentSouthBorder->nextSibling();
            if (currentSouthBorder == nullptr || currentSouthBorder->getAttribute("Start")->getUInt16() != x)
            {
                ExecutionException error("MapBorder nodes must be defined, in ascending order, to cover every space without skipping any map values.");
                error.addData("side", "south");
                error.addData("x", toString(x));
                error.addData("map", mapHandle);
                throw error;
            }
        }
        _mapBorders.push_back(Enums::parseKeep<MapBorder>(currentSouthBorder->getValue().getString()));
        ++x;
    }

    XmlElementPtr currentWestBorder(xml.firstChild("MapBorders")->firstChild("West")->firstChild());
    y = 0;
    while (y < _height)
    {
        if (y > currentWestBorder->getAttribute("End")->getUInt16())
        {
            currentWestBorder = currentWestBorder->nextSibling();
            if (currentWestBorder == nullptr || currentWestBorder->getAttribute("Start")->getUInt16() != y)
            {
                ExecutionException error("MapBorder nodes must be defined, in ascending order, to cover every space without skipping any map values.");
                error.addData("side", "west");
                error.addData("y", toString(y));
                error.addData("map", mapHandle);
                throw error;
            }
        }
        _mapBorders.push_back(Enums::parseKeep<MapBorder>(currentWestBorder->getValue().getString()));
        ++y;
    }
}

UnicodeString RegionMap::getSourceXmlFilename() const
{
    return "content\\maps\\" + _handle + ".xml";
}

bool RegionMap::isExteriorOverworld() const
{
    return (_isOutdoor && _mapBridges.size() > 0);
}

uint32_t RegionMap::getRegionNo() const
{
    return _regionNo;
}

uint32_t RegionMap::getMapNo() const
{
    return _mapNo;
}

UnicodeString RegionMap::getMapName() const
{
    return _mapName;
}

std::vector<RegionMapBridge> RegionMap::getMapBridges() const
{
    return _mapBridges;
}

uint16_t RegionMap::getWidth() const
{
    return _width;
}

uint16_t RegionMap::getHeight() const
{
    return _height;
}

UnicodeString RegionMap::getMainTextureReferenceFilename() const
{
    // Because we haven't saved, we need to do our best guess here;
    // Not the best, but the cyclical nature of Regions and Maps becomes
    // very difficult to find an angelic solution for.
    return "reference/" + _mapName + " Main.png";
}

UnicodeString RegionMap::getRenderableReportFilename() const
{
    return _renderableReportFilename;
}

MapBorder RegionMap::getNorthMapBorder(uint16_t x) const
{
    if (x >= _width)
    {
        ExecutionException error("Attempted to retrieve the MapBorder for a location that doesn't exist.");
        error.addData("x", toString(x));
        error.addData("width", toString(_width));
        error.addData("side", "north");
        error.addData("mapNo", toString(_mapNo));
        throw error;
    }

    return _mapBorders[x];
}

MapBorder RegionMap::getEastMapBorder(uint16_t y) const
{
    if (y >= _height)
    {
        ExecutionException error("Attempted to retrieve the MapBorder for a location that doesn't exist.");
        error.addData("y", toString(y));
        error.addData("height", toString(_height));
        error.addData("side", "east");
        error.addData("mapNo", toString(_mapNo));
        throw error;
    }

    return _mapBorders[_width + y];
}

MapBorder RegionMap::getSouthMapBorder(uint16_t x) const
{
    if (x >= _width)
    {
        ExecutionException error("Attempted to retrieve the MapBorder for a location that doesn't exist.");
        error.addData("x", toString(x));
        error.addData("width", toString(_width));
        error.addData("side", "south");
        error.addData("mapNo", toString(_mapNo));
        throw error;
    }

    return _mapBorders[_width + _height + x];
}

MapBorder RegionMap::getWestMapBorder(uint16_t y) const
{
    if (y >= _height)
    {
        ExecutionException error("Attempted to retrieve the MapBorder for a location that doesn't exist.");
        error.addData("y", toString(y));
        error.addData("height", toString(_height));
        error.addData("side", "west");
        error.addData("mapNo", toString(_mapNo));
        throw error;
    }

    return _mapBorders[_width * 2 + _height + y];
}

void RegionMap::markFullyCalculated()
{
    if (_overworldRenderable != nullptr)
    {
        ExecutionException error("A RegionMap that has already been marked fully completed may not be remarked.");
        error.addData("mapNo", toString(_mapNo));
        error.addData("mapName", _mapName);
        throw error;
    }

    _overworldRenderable = std::shared_ptr<OverworldRenderable>(new OverworldRenderable(_mapName,
                                                                                        borderRectangle.width / TILE_SIZE,
                                                                                        borderRectangle.height / TILE_SIZE));
}

void RegionMap::addSprite(uint16_t tileX, uint16_t tileY, uint32_t spriteNo,
                          bool isAnimationInstance, SpriteSheetFile* spriteSheet,
                          bool isSuperlayer)
{
    if (_overworldRenderable == nullptr)
    {
        ExecutionException error("Attempted to write the OverworldRenderable for an invalid RegionMap.");
        throw error;
    }


}

void RegionMap::drawMapBorder(uint16_t fullTileX, uint16_t fullTileY, MapBorderTile mapBorderTile,
                              SpriteSheetFile* spriteSheet)
{
    // Check input parameters to make sure they are valid
    if (mapBorderTile.fill == MapBorder::None)
    {
        ExecutionException error("Attempted to set the fill MapBorder of a tile within a RegionMap to None.");
        error.addData("fullTileX", toString(fullTileX));
        error.addData("fullTileY", toString(fullTileY));
        error.addData("mapName", _mapName);
        error.addData("mapNo", toString(_mapNo));
        throw error;
    }

    if (_overworldRenderable == nullptr)
    {
        ExecutionException error("Attempted to add to the OverworldRenderable of an invalid RegionMap.");
        throw error;
    }

    // Check input parameters for position to ensure they are valid, and translate from relative
    // tile positions to absolute coordinates
    int32_t absoluteX(borderRectangle.x + fullTileX * TILE_SIZE);
    int32_t absoluteY(borderRectangle.y + fullTileY * TILE_SIZE);
    if (rectangle.contains(absoluteX, absoluteY))
    {
        ExecutionException error("Attempted to draw a MapBorder within the bounds of the defined map.");
        error.addData("fullTileX", toString(fullTileX));
        error.addData("fullTileY", toString(fullTileY));
        error.addData("absoluteX", toString(absoluteX));
        error.addData("absoluteY", toString(absoluteY));
        error.addData("rectangle", toString(rectangle));
        error.addData("borderRectangle", toString(borderRectangle));
        error.addData("mapNo", toString(_mapNo));
        throw error;
    }
    if (!borderRectangle.contains(absoluteX, absoluteY))
    {
        ExecutionException error("Attempted to draw a MapBorder outside the bounds of the border rectangle.");
        error.addData("fullTileX", toString(fullTileX));
        error.addData("fullTileY", toString(fullTileY));
        error.addData("absoluteX", toString(absoluteX));
        error.addData("absoluteY", toString(absoluteY));
        error.addData("rectangle", toString(rectangle));
        error.addData("borderRectangle", toString(borderRectangle));
        error.addData("mapNo", toString(_mapNo));
        throw error;
    }

    // Now that we have all of our necessary values, we can draw the MapBorderTile
    std::vector<uint32_t> spriteNos(MapBorders::getSpriteNos(mapBorderTile));
    for (uint32_t& spriteNo : spriteNos)
    {
        _overworldRenderable->add(fullTileX, fullTileY, spriteSheet, spriteNo, false);
    }
}

void RegionMap::blendInMapOverworldRenderable(OverworldRenderable& mapOverworldRenderable)
{
    if (_overworldRenderable == nullptr)
    {
        ExecutionException error("Attempted to blend the MapFile's OverworldRenderable into a RegionMap's OverworldRenderable, which had not been created.");
        throw error;
    }

    // Determine tile start locations on large overworld
    uint16_t tileX((rectangle.x - borderRectangle.x) / TILE_SIZE);
    uint16_t tileY((rectangle.y - borderRectangle.y) / TILE_SIZE);

    _overworldRenderable->blendIn(mapOverworldRenderable, tileX, tileY);
}

OverworldRenderableReport RegionMap::writeOverworldRenderable(CompilerBinaryWriter& writer, File* issuer)
{
    if (_overworldRenderable == nullptr)
    {
        ExecutionException error("Attempted to write the OverworldRenderable for an invalid RegionMap.");
        throw error;
    }

    OverworldRenderableReport report(_overworldRenderable->write(writer, issuer));
    _renderableReportFilename = report.getFilename();
    return report;
}

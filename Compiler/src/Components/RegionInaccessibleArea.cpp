#include "KristallCompiler.hpp"

RegionInaccessibleArea::RegionInaccessibleArea(UnicodeString name, int32_t x,
                                               int32_t y, int32_t width,
                                               int32_t height) : _name(name), _x(x), _y(y),
                                               _width(width), _height(height)
{
    // Check input
    if (x % TILE_SIZE != 0 || y % TILE_SIZE != 0)
    {
        ExecutionException error("The location of an RIA must be a multiple of the tile size.");
        error.addData("x", toString(x));
        error.addData("y", toString(y));
        error.addData("TILE_SIZE", toString(TILE_SIZE));
        error.addData("x mod TILE_SIZE", toString(x % TILE_SIZE));
        error.addData("y mod TILE_SIZE", toString(y % TILE_SIZE));
        throw error;
    }

    if (width == 0 && height == 0)
    {
        return;
    }

    if (width % TILE_SIZE != 0 || height % TILE_SIZE != 0)
    {
        ExecutionException error("The dimensions of an RIA must be a multiple of the tile size.");
        error.addData("width", toString(width));
        error.addData("height", toString(height));
        error.addData("TILE_SIZE", toString(TILE_SIZE));
        error.addData("width mod TILE_SIZE", toString(width % TILE_SIZE));
        error.addData("height mod TILE_SIZE", toString(height % TILE_SIZE));
        throw error;
    }

    if (width < 0 || height < 0)
    {
        ExecutionException error("The dimensions of an RIA must be positive.");
        error.addData("width", toString(width));
        error.addData("height", toString(height));
        throw error;
    }

    // Prepopulate the tiles
    size_t nTiles((width / TILE_SIZE) * (height / TILE_SIZE));
    _tiles.reserve(nTiles);
    for (size_t index = 0; index < nTiles; ++index)
    {
        _tiles.push_back(MapBorder::None);
    }

    // Create the OverworldRenderable
    _overworldRenderable = std::shared_ptr<OverworldRenderable>(new OverworldRenderable(name,
                                                                                        width / TILE_SIZE,
                                                                                        height / TILE_SIZE));
}

UnicodeString RegionInaccessibleArea::getName() const
{
    return _name;
}

int32_t RegionInaccessibleArea::getX() const
{
    return _x;
}

int32_t RegionInaccessibleArea::getY() const
{
    return _y;
}

int32_t RegionInaccessibleArea::getWidth() const
{
    return _width;
}

int32_t RegionInaccessibleArea::getHeight() const
{
    return _height;
}

UnicodeString RegionInaccessibleArea::getRenderableReportFilename() const
{
    return _renderableReportFilename;
}

UnicodeString RegionInaccessibleArea::getMainTextureReferenceFilename() const
{
    return _mainTextureReferenceFilename;
}

MapBorder RegionInaccessibleArea::getTile(int32_t x, int32_t y) const
{
    if (x < _x || y < _y || x >= _x + _width || y >= _y + _height)
    {
        ExecutionException error("Attempted to get the MapBorder at a location not contained within the specified RIA.");
        error.addData("location X", toString(x));
        error.addData("location Y", toString(y));
        error.addData("RIA X", toString(_x));
        error.addData("RIA Y", toString(_y));
        error.addData("RIA Width", toString(_width));
        error.addData("RIA Height", toString(_height));
        throw error;
    }

    return _tiles[std::floor((y - _y) / TILE_SIZE) * (_width / TILE_SIZE) + std::floor((x - _x) / TILE_SIZE)];
}

void RegionInaccessibleArea::setTile(int32_t x, int32_t y, MapBorderTile tile,
                                     SpriteSheetFile* overworldExteriorSpriteSheet)
{
    // Check parameters
    if (x < _x || y < _y || x >= _x + _width || y >= _y + _height)
    {
        ExecutionException error("Attempted to set the MapBorder at a location not contained within the specified RIA.");
        error.addData("location X", toString(x));
        error.addData("location Y", toString(y));
        error.addData("RIA X", toString(_x));
        error.addData("RIA Y", toString(_y));
        error.addData("RIA Width", toString(_width));
        error.addData("RIA Height", toString(_height));
        throw error;
    }

    if (tile.fill == MapBorder::None)
    {
        ExecutionException error("Attempted to set the fill MapBorder of a tile within an RIA to None.");
        error.addData("location X", toString(x));
        error.addData("location Y", toString(y));
        throw error;
    }

    if (_overworldRenderable == nullptr)
    {
        ExecutionException error("Attempted to add to the OverworldRenderable of an RIA with an invalid dimension; OverworldRenderable was never created.");
        error.addData("width", toString(_width));
        error.addData("height", toString(_height));
        throw error;
    }

    uint16_t tileX(std::floor((x - _x) / TILE_SIZE));
    uint16_t tileY(std::floor((y - _y) / TILE_SIZE));

    // Set the fill tile in the vector
    _tiles[tileY * (_width / TILE_SIZE) + tileX] = tile.fill;

    // Draw appropriately on the main texture
    std::vector<uint32_t> spriteNos(MapBorders::getSpriteNos(tile));
    for (uint32_t& spriteNo : spriteNos)
    {
        _overworldRenderable->add(tileX, tileY, overworldExteriorSpriteSheet, spriteNo, false);
    }
}

OverworldRenderableReport RegionInaccessibleArea::writeOverworldRenderable(CompilerBinaryWriter& writer, File* issuer)
{
    if (_overworldRenderable == nullptr)
    {
        ExecutionException error("Attempted to write the OverworldRenderable of an RIA with an invalid dimension; OverworldRenderable was never created.");
        error.addData("width", toString(_width));
        error.addData("height", toString(_height));
        throw error;
    }

    OverworldRenderableReport report(_overworldRenderable->write(writer, issuer));
    _mainTextureReferenceFilename = report.getMainTextureFilename();
    _renderableReportFilename = report.getFilename();
    return report;
}


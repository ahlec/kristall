#include "KristallCompiler.hpp"

MapBorderTile::MapBorderTile(Direction traverseDirection,
                             bool isRowOdd, bool isColumnOdd,
                             bool isRelative) : _traverseDirection(traverseDirection),
                             _isRowOdd(isRowOdd), _isColumnOdd(isColumnOdd),
                             _isRelative(isRelative), fill(MapBorder::None),
                             north(MapBorder::None), east(MapBorder::None),
                             south(MapBorder::None), west(MapBorder::None)
{
}

Direction MapBorderTile::getTraverseDirection() const
{
    return _traverseDirection;
}

MapBorder MapBorderTile::getForwardBorder() const
{
    if (_isRelative)
    {
        switch (_traverseDirection)
        {
            case Direction::North:
                {
                    return north;
                }
            case Direction::East:
                {
                    return east;
                }
            case Direction::South:
                {
                    return south;
                }
            case Direction::West:
                {
                    return west;
                }
        }
    }
    else
    {
        return north;
    }
}

MapBorder MapBorderTile::getBackwardBorder() const
{
    if (_isRelative)
    {
        switch (_traverseDirection)
        {
            case Direction::North:
                {
                    return south;
                }
            case Direction::East:
                {
                    return west;
                }
            case Direction::South:
                {
                    return north;
                }
            case Direction::West:
                {
                    return east;
                }
        }
    }
    else
    {
        return south;
    }
}

MapBorder MapBorderTile::getRightBorder() const
{
    if (_isRelative)
    {
        switch (_traverseDirection)
        {
            case Direction::North:
                {
                    return east;
                }
            case Direction::East:
                {
                    return south;
                }
            case Direction::South:
                {
                    return west;
                }
            case Direction::West:
                {
                    return north;
                }
        }
    }
    else
    {
        return east;
    }
}

MapBorder MapBorderTile::getLeftBorder() const
{
    if (_isRelative)
    {
        switch (_traverseDirection)
        {
            case Direction::North:
                {
                    return west;
                }
            case Direction::East:
                {
                    return north;
                }
            case Direction::South:
                {
                    return east;
                }
            case Direction::West:
                {
                    return south;
                }
        }
    }
    else
    {
        return west;
    }
}

uint8_t MapBorderTile::getIndex() const
{
    std::string travDirStd;
    switch (_traverseDirection)
    {
        case Direction::North:
        {
            travDirStd = "N";
            break;
        }
        case Direction::East:
        {
            travDirStd = "E";
            break;
        }
        case Direction::South:
        {
            travDirStd = "S";
            break;
        }
        case Direction::West:
        {
            travDirStd = "W";
            break;
        }
    }
    /*kc_printf("fill: %d; dir: %s; n: %d; e: %d; s: %d; w: %d;; f: %d; b: %d; r: %d; l: %d;;",
              static_cast<uint32_t>(fill), travDirStd.c_str(),
              static_cast<uint32_t>(north), static_cast<uint32_t>(east),
              static_cast<uint32_t>(south), static_cast<uint32_t>(west),
              static_cast<uint32_t>(getForwardBorder()),
              static_cast<uint32_t>(getBackwardBorder()), static_cast<uint32_t>(getRightBorder()),
              static_cast<uint32_t>(getLeftBorder()));*/
    if ((fill == MapBorder::Trees && north == MapBorder::OddTrees &&
        south == MapBorder::Trees) || (fill == MapBorder::OddTrees &&
        north == MapBorder::Trees && south == MapBorder::OddTrees))
    {
        return (_isColumnOdd ? 7 : 6);
    }

    // Standard
    bool isContinuedForward(fill == getForwardBorder());
    bool isContinuedRight(fill == getRightBorder());
    bool isContinuedLeft(fill == getLeftBorder());
    bool isContinuedBackward(fill == getBackwardBorder());

    if (isContinuedForward && isContinuedRight && isContinuedLeft)
    {
        return (_isColumnOdd ? 1 : 0) + (_isRowOdd ? 2 : 0);
    }

    // Top-right
    if (!isContinuedForward && !isContinuedRight)
    {
        switch (_traverseDirection)
        {
            case Direction::North:
                {
                    return 13;
                }
            case Direction::East:
                {
                    return 15;
                }
            case Direction::South:
                {
                    return 14;
                }
            case Direction::West:
                {
                    return 12;
                }
        }
    }

    if (!isContinuedForward && !isContinuedLeft)
    {
        switch (_traverseDirection)
        {
            case Direction::North:
                {
                    return 12;
                }
            case Direction::East:
                {
                    return 13;
                }
            case Direction::South:
                {
                    return 15;
                }
            case Direction::West:
                {
                    return 14;
                }
        }
    }

    if (!isContinuedBackward & !isContinuedLeft)
    {
        switch (_traverseDirection)
        {
            case Direction::North:
                {
                    return 14;
                }
            case Direction::East:
                {
                    return 12;
                }
            case Direction::South:
                {
                    return 13;
                }
            case Direction::West:
                {
                    return 15;
                }
        }
    }

    if (!isContinuedBackward && !isContinuedRight)
    {
        switch (_traverseDirection)
        {
            case Direction::North:
                {
                    return 15;
                }
            case Direction::East:
                {
                    return 14;
                }
            case Direction::South:
                {
                    return 12;
                }
            case Direction::West:
                {
                    return 13;
                }
        }
    }

    if (!isContinuedForward)
    {
        switch (_traverseDirection)
        {
            case Direction::North:
                {
                    return (_isColumnOdd ? 5: 4);
                }
            case Direction::East:
                {
                    return (_isRowOdd ? 11 : 9);
                }
            case Direction::South:
                {
                    return (_isColumnOdd ? 7 : 6);
                }
            case Direction::West:
                {
                    return (_isRowOdd ? 10 : 8);
                }
        }
    }

    if (!isContinuedRight)
    {
        switch (_traverseDirection)
        {
            case Direction::North:
                {
                    return (_isRowOdd ? 11 : 9);
                }
            case Direction::East:
                {
                    return (_isColumnOdd ? 7 : 6);
                }
            case Direction::South:
                {
                    return (_isRowOdd ? 10 : 8);
                }
            case Direction::West:
                {
                    return (_isColumnOdd ? 5 : 4);
                }
        }
    }

    // If we're still here, the only unfulfilled conditional is (!isContinuedLeft)
    switch (_traverseDirection)
    {
        case Direction::North:
            {
                return (_isRowOdd ? 10 : 8);
            }
        case Direction::East:
            {
                return (_isColumnOdd ? 5 : 4);
            }
        case Direction::South:
            {
                return (_isRowOdd ? 11 : 9);
            }
        case Direction::West:
            {
                return (_isColumnOdd ? 7 : 6);
            }
    }
}

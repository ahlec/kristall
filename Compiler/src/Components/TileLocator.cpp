#include "KristallCompiler.hpp"

TileLocator::TileLocator(uint16_t x, uint16_t y) : _x(x), _y(y)
{
}

bool TileLocator::operator()(const XmlElement& node)
{
    return (node.getAttribute("X")->getUInt16() == _x &&
            node.getAttribute("Y")->getUInt16() == _y);
}

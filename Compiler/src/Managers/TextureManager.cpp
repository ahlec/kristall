#include "KristallCompiler.hpp"

static bool s_hasBegunCompile(false);
static const int32_t DAT_FILENAME_FIXED_LENGTH = 25;
static std::shared_ptr<TextureManager> s_singleton(nullptr);

uint32_t TextureManager::give(SDL_Surface* surface, File* issuer, UnicodeString name)
{
    if (s_singleton == nullptr)
    {
        s_singleton = std::shared_ptr<TextureManager>(new TextureManager());
    }

    if (issuer == nullptr)
    {
        ExecutionException error("All textures issued through the TextureManager must have an associated issuing file.");
        error.addData("name", name);
        throw error;
    }

    XmlElementPtr entryNode(s_singleton->retrieveNode(issuer, name));
    UnicodeString unicodeDatFilename(entryNode->getValue().getString());
    PNG::save(unicodeDatFilename, surface);

    return entryNode->getAttribute("Id")->getUInt32();
}

uint32_t TextureManager::give(UnicodeString textureFilename, File* issuer, UnicodeString name)
{
    if (s_singleton == nullptr)
    {
        s_singleton = std::shared_ptr<TextureManager>(new TextureManager());
    }

    if (issuer == nullptr)
    {
        ExecutionException error("All textures issued through the TextureManager must have an associated issuing file.");
        error.addData("name", name);
        throw error;
    }

    XmlElementPtr entryNode(s_singleton->retrieveNode(issuer, name));
    UnicodeString unicodeDatFilename(entryNode->getValue().getString(), 0, DAT_FILENAME_FIXED_LENGTH);

    SDL_Surface* tempSurface = PNG::load(textureFilename);
    if (tempSurface == nullptr)
    {
        ExecutionException error("Unable to load the specified texture from the provided filename.");
        error.addData("textureFilename", textureFilename);
        error.addData("issuer", issuer->getCompiledFileName());
        error.addData("name", name);
        throw error;
    }
    PNG::save(unicodeDatFilename, tempSurface);

    return entryNode->getAttribute("Id")->getUInt32();
}

uint32_t TextureManager::getTextureNo(File* issuer, UnicodeString name)
{
    if (s_singleton == nullptr)
    {
        s_singleton = std::shared_ptr<TextureManager>(new TextureManager());
    }

    if (issuer == nullptr)
    {
        ExecutionException error("All textures issued through the TextureManager must have an associated issuing file.");
        error.addData("name", name);
        throw error;
    }

    XmlElementPtr nodeEntry(s_singleton->_xml.firstChild(s_singleton->getNodeName(issuer, name)));
    if (nodeEntry == nullptr)
    {
        ExecutionException error("The specified file and name combination does not have any registered texture within the TextureManager.");
        error.addData("issuer", issuer->getCompiledFileName());
        error.addData("name", name);
        throw error;
    }

    return nodeEntry->getAttribute("Id")->getUInt32();
}

SDL_Surface* TextureManager::loadTexture(File* issuer, UnicodeString name)
{
    if (s_singleton == nullptr)
    {
        s_singleton = std::shared_ptr<TextureManager>(new TextureManager());
    }

    if (issuer == nullptr)
    {
        ExecutionException error("All textures issued through the TextureManager must have an associated issuing file.");
        error.addData("name", name);
        throw error;
    }

    XmlElementPtr nodeEntry(s_singleton->_xml.firstChild(s_singleton->getNodeName(issuer, name)));
    if (nodeEntry == nullptr)
    {
        ExecutionException error("The specified file and name combination does not have any registered texture within the TextureManager.");
        error.addData("issuer", issuer->getCompiledFileName());
        error.addData("name", name);
        throw error;
    }

    UnicodeString textureDatFilename(nodeEntry->getValue().getString());
    SDL_Surface* texture(PNG::load(textureDatFilename));
    if (texture == nullptr)
    {
        ExecutionException error("Unable to load the texture from the stored file.");
        error.addData("issuer", issuer->getCompiledFileName());
        error.addData("name", name);
        error.addData("textureNo", toString(nodeEntry->getAttribute("Id")->getUInt32()));
        throw error;
    }
    return texture;
}

class TextureIDLocator : public Functor<bool, const XmlElement&>
{
public:
    TextureIDLocator(uint32_t textureNo) : _textureNo(textureNo)
    {
    };

    bool operator()(const XmlElement& xmlNode)
    {
        return (xmlNode.getAttribute("Id")->getUInt32() == _textureNo);
    }

private:
    uint32_t _textureNo;
};

SDL_Surface* TextureManager::loadTexture(uint32_t textureNo)
{
    if (s_singleton == nullptr)
    {
        s_singleton = std::shared_ptr<TextureManager>(new TextureManager());
    }

    std::shared_ptr<TextureIDLocator> locator(std::make_shared<TextureIDLocator>(textureNo));
    XmlElementPtr nodeEntry(s_singleton->_xml.firstChild(Predicate<XmlElement>(new TextureIDLocator(textureNo))));
    if (nodeEntry == nullptr)
    {
        ExecutionException error("The specified textureNo does not have any registered texture within the TextureManager.");
        error.addData("textureNo", toString(textureNo));
        throw error;
    }

    UnicodeString textureDatFilename(nodeEntry->getValue().getString());
    SDL_Surface* surface(PNG::load(textureDatFilename));
    if (surface == nullptr)
    {
        ExecutionException error("Unable to load the texture from the stored file.");
        error.addData("textureNo", toString(textureNo));
        throw error;
    }
    return surface;
}

UnicodeString TextureManager::getTextureFilename(uint32_t textureNo)
{
    if (s_singleton == nullptr)
    {
        s_singleton = std::shared_ptr<TextureManager>(new TextureManager());
    }

    std::shared_ptr<TextureIDLocator> locator(std::make_shared<TextureIDLocator>(textureNo));
    XmlElementPtr nodeEntry(s_singleton->_xml.firstChild(Predicate<XmlElement>(new TextureIDLocator(textureNo))));
    if (nodeEntry == nullptr)
    {
        ExecutionException error("The specified textureNo does not have any registered texture within the TextureManager.");
        error.addData("textureNo", toString(textureNo));
        throw error;
    }

    return nodeEntry->getValue().getString();
}

// INSTANCE
TextureManager::TextureManager() : _xml(XmlDocument::load("textureManager.xml"))
{
    _nextId = _xml.getAttribute("NextId")->getUInt32();

    FunctionPtr<void> closeFunc(FunctionPtr<void>::fromMember<TextureManager>(this, &TextureManager::close));
    Finalizer::chain(closeFunc);
}

UnicodeString TextureManager::getNodeName(File* issuer, UnicodeString name) const
{
    UnicodeString compiledFileName(issuer->getCompiledFileName());
    compiledFileName.findAndReplace(" ", "_");
    return UnicodeString(compiledFileName, 4, compiledFileName.length() - 8) + "-" + name;
}

XmlElementPtr TextureManager::retrieveNode(File* issuer, UnicodeString name)
{
    UnicodeString nodeName(getNodeName(issuer, name));

    XmlElementPtr existingNode(_xml.firstChild(nodeName));
    if (existingNode != nullptr)
    {
        return existingNode;
    }

    uint32_t newNodeId(_nextId++);
    char datFilename[DAT_FILENAME_FIXED_LENGTH + 1];
    snprintf(datFilename, DAT_FILENAME_FIXED_LENGTH + 1, "dat\\textures\\%08d.png", newNodeId);
    datFilename[DAT_FILENAME_FIXED_LENGTH] = 0;
    UnicodeString unicodeDatFilename(datFilename);
    XmlElementPtr newNode(_xml.appendChild(nodeName, unicodeDatFilename));
    newNode->setAttribute("Id", newNodeId);
    return newNode;
}

void TextureManager::compile(KristallBin& kristallBin)
{
    if (s_singleton == nullptr)
    {
        s_singleton = std::shared_ptr<TextureManager>(new TextureManager());
    }

    s_hasBegunCompile = true;

    uint64_t contentSectionNo(KristallCompilerLog::createNewSection(0));
    KristallCompilerLog::setSectionText(contentSectionNo, "[TEXTURES]");
    printf("[TEXTURES]\n");

    XmlElementPtr currentNode(s_singleton->_xml.firstChild());
    while (currentNode != nullptr)
    {
        TextureFile* file = new TextureFile(currentNode->getAttribute("Id")->getUInt32());
        file->update(kristallBin, contentSectionNo);
        kristallBin.addFile(file);
        currentNode = currentNode->nextSibling();
    }

    printf("\n");
}

void TextureManager::close()
{
    _xml.setAttribute("NextId", _nextId);
    _xml.save("textureManager.xml");
}

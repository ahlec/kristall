#include "KristallCompiler.hpp"
#include "General/RandomGenerator.h"

static std::vector<UnicodeString> s_actionUIds;
static RandomGenerator s_randomGenerator(0, 25);

inline UnicodeString generateActionUId()
{
    UnicodeString uid;

    for (size_t index = 0; index < 6; ++index)
    {
        uid += (UChar)(s_randomGenerator.next() + 0x61);
    }

    return uid;
}

UnicodeString registerNewActionUId()
{
    UnicodeString uid(generateActionUId());
    while (std::find(s_actionUIds.begin(), s_actionUIds.end(), uid) != s_actionUIds.end())
    {
        uid = generateActionUId();
    }

    s_actionUIds.push_back(uid);

    return uid;
}

ReportAction::ReportAction(UnicodeString label) : _label(label),
                                    _uid(registerNewActionUId())
{
}

UnicodeString ReportAction::outputLabel() const
{
    return "<label for='" + getUId() + "'>" + _label + "</label>";
}

UnicodeString ReportAction::getUId() const
{
    return _uid;
}

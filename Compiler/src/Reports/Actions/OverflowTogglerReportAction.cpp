#include "KristallCompiler.hpp"

OverflowTogglerReportAction::OverflowTogglerReportAction() : ReportAction("Display Overflow Tiles")
{
}

UnicodeString OverflowTogglerReportAction::outputHead() const
{
    UnicodeString script =
        L"<script type=\"text/javascript\">\n"
        L"  $(document).ready(function () {\n"
        L"    var overflowSelect = $('#" + getUId() + L"').change(function () {\n"
        L"      $('div.overflowTile').remove();\n"
        L"      if ($(this).val() != '') {\n"
        L"          var location;\n"
        L"          $('li[data-overflow=' + $(this).val() + '] li').each(function () {\n"
        L"              location = $(this).text().split(',');\n"
        L"              $(document.createElement('div')).addClass('overflowTile')\n"
        L"                                              .css('left', location[0] + 'px')\n"
        L"                                              .css('top', location[1] + 'px')\n"
        L"                                              .appendTo('div.renderContainer');\n"
        L"          });\n"
        L"      }\n"
        L"    });\n"
        L"    $('li[data-overflow]').each(function (index) {\n"
        L"        var newOption = $(document.createElement('option'));\n"
        L"        newOption.val($(this).data('overflow'));\n"
        L"        newOption.text($(this).data('overflow'));\n"
        L"        overflowSelect.append(newOption);\n"
        L"    });\n"
        L"    overflowSelect.prop('disabled', false);\n"
        L"  });\n"
        L"</script>\n";
    return script;
}

UnicodeString OverflowTogglerReportAction::outputDom() const
{
    return "<select id=\"" + getUId() +
           "\" disabled=\"disabled\"><option value=\"\"></option></select>\n";
}

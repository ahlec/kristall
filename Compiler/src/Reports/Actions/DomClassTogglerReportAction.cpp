#include "KristallCompiler.hpp"

DomClassTogglerReportAction::DomClassTogglerReportAction(UnicodeString label,
                                                         UnicodeString subjectSelectors,
                                                         UnicodeString classA,
                                                         UnicodeString classB) : ReportAction(label),
                                                            _subjectSelectors(subjectSelectors),
                                                            _hasClassA(true),
                                                            _classA(classA), _classB(classB)
{
}


DomClassTogglerReportAction::DomClassTogglerReportAction(UnicodeString label,
                                                         UnicodeString subjectSelectors,
                                                         UnicodeString classB) : ReportAction(label),
                                                            _subjectSelectors(subjectSelectors),
                                                            _hasClassA(false),
                                                            _classB(classB)
{
}

UnicodeString DomClassTogglerReportAction::outputHead() const
{
    UnicodeString script =
        L"<script type=\"text/javascript\">\n"
        L"  $(document).ready(function () {\n"
        L"    $('#" + getUId() + L"').change(function () {\n"
        L"      var subjects = $('" + _subjectSelectors + L"');\n";

    if (_hasClassA)
    {
        script += "      if (subjects.hasClass('" + _classA + "')) {\n";
        script += "        subjects.removeClass('" + _classA + "').addClass('" + _classB + "');\n";
        script += "      }\n";
        script += "      else {\n";
        script += "        subjects.removeClass('" + _classB + "').addClass('" + _classA + "');\n";
        script += "      }\n";
    }
    else
    {
        script += "      subjects.toggleClass('" + _classB + "');\n";
    }

    script +=
        "    }).prop('checked', !$('" + _subjectSelectors + "').hasClass('" + _classB + "')).prop('disabled', false);\n" +
        "  });\n" +
        "</script>\n";
    return script;
}

UnicodeString DomClassTogglerReportAction::outputDom() const
{
    return "<input type=\"checkbox\" id=\"" + getUId() +
           "\" disabled=\"disabled\" />\n";
}

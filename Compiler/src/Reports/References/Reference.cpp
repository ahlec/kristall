#include "KristallCompiler.hpp"

Reference::Reference(UnicodeString filename) : _filename(filename)
{
}

UnicodeString Reference::getFilename() const
{
    return _filename;
}

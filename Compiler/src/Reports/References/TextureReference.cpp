#include "KristallCompiler.hpp"

TextureReference::TextureReference(UnicodeString name, SDL_Surface* texture) :
                                    Reference(name + L".png"), _texture(texture)
{
}

void TextureReference::write(UnicodeString fullFilename)
{
    if (_texture == nullptr)
    {
        ExecutionException error("Attempted to write a TextureReference with a nullptr Texture.");
        throw error;
    }

    PNG::save(fullFilename, _texture);
}

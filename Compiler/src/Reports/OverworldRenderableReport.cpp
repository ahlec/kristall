#include "KristallCompiler.hpp"

OverworldRenderableReport::OverworldRenderableReport(const OverworldRenderable& renderable, uint32_t mainTextureNo,
                                                     uint32_t superlayerTextureNo) :
                                                Report(renderable._name + " Overworld Renderable"),
                                                _name(renderable._name), _width(renderable._width),
                                                _height(renderable._height), _mainTextureNo(mainTextureNo),
                                                _superlayerTextureNo(superlayerTextureNo), _mainTexture(renderable._mainTexture),
                                                _superlayerTexture((renderable._hasSuperlayer ?
                                                                    renderable._superlayerTexture :
                                                                        nullptr))
{
    RptOverflow temp;

    // Main Overflows
    for (const OverworldRenderable::Overflow& overflow : renderable._overflows)
    {
        temp.spriteNo = overflow.spriteId;

        for (auto instance : overflow.instances)
        {
            temp.instances.push_back(instance);
        }

        _mainOverflow.push_back(temp);
        temp.instances.clear();
    }
}

std::vector<ReportInfo> OverworldRenderableReport::getAdditionalInfo()
{
    std::vector<ReportInfo> info;
    info.reserve(3);

    // Width
    ReportInfo widthInfo;
    widthInfo.label = "Width";
    widthInfo.value = UnicodeStrings::fromInt64(_width) + " <note>(Tiles)</note>";
    info.push_back(widthInfo);

    // Height
    ReportInfo heightInfo;
    heightInfo.label = "Height";
    heightInfo.value = UnicodeStrings::fromInt64(_height) + " <note>(Tiles)</note>";
    info.push_back(heightInfo);

    // Has Superlayer
    ReportInfo hasSuperlayerInfo;
    hasSuperlayerInfo.label = "Has Superlayer";
    hasSuperlayerInfo.value = (_superlayerTexture != nullptr ? "Yes" : "No");
    info.push_back(hasSuperlayerInfo);

    // Main Texture No
    ReportInfo mainTextureNoInfo;
    mainTextureNoInfo.label = "Main TextureNo";
    mainTextureNoInfo.value = UnicodeStrings::fromInt64(_mainTextureNo);
    info.push_back(mainTextureNoInfo);

    // Superlayer Texture No
    if (_superlayerTexture != nullptr)
    {
        ReportInfo superlayerTextureNoInfo;
        superlayerTextureNoInfo.label = "Superlayer TextureNo";
        superlayerTextureNoInfo.value = UnicodeStrings::fromInt64(_superlayerTextureNo);
        info.push_back(superlayerTextureNoInfo);
    }

    return info;
}

UnicodeString OverworldRenderableReport::outputHeader()
{
    UnicodeString header =
        "<style>\n"
        "  div.renderContainer {\n"
        "    border:1px solid black;\n"
        "    position:relative;\n"
        "    background-color:yellow;\n"
        "    display:block;\n"
        "    width:";
    header += UnicodeStrings::fromInt64(_width * TILE_SIZE);
    header += "px;\n"
        "    height:";
    header += UnicodeStrings::fromInt64(_height * TILE_SIZE);

    header += "px;\n"
        "  }\n"
        "  div.renderContainer > div.render {\n"
        "    display:block;\n"
        "    position:absolute;\n"
        "    top:0px;\n"
        "    left:0px;\n"
        "    right:0px;\n"
        "    bottom:0px;\n"
        "    background-repeat:no-repeat;\n"
        "  }\n"
        "  div.renderContainer > div.render.hide {\n"
        "    display:none;\n"
        "  }\n"
        "  div.gridLine {\n"
        "    position:absolute;\n"
        "  }\n"
        "  div.gridLine.horizontal {\n"
        "    border-top:1px solid black;\n"
        "    left:0px;\n"
        "    right:0px;\n"
        "  }\n"
        "  div.gridLine.vertical {\n"
        "    border-left:1px solid black;\n"
        "    top:0px;\n"
        "    bottom:0px;\n"
        "  }\n"
        "  div.renderContainer > div.overflowTile {\n"
        "    display:block;\n"
        "    position:absolute;\n"
        "    width:";

    header += UnicodeStrings::fromInt64(TILE_SIZE);
    header += "px;\n"
        "    height:";
    header += UnicodeStrings::fromInt64(TILE_SIZE);
    header += "px;\n"
        "    background-color:rgba(0,0,255,0.6);\n"
        "  }\n"
        "</style>\n";

    return header;
}

UnicodeString OverworldRenderableReport::outputBody()
{
    // Main
    UnicodeString body = L"<div class=\"renderContainer\">\n";
    body += L"    <div class=\"render\" id=\"main\" style=\"background-image:url('" +
                       _mainTextureFilename.findAndReplace("\\", "/") + "');\"></div>\n";
    if (_superlayerTexture != nullptr)
    {
        body += L"    <div class=\"render\" id=\"superlayer\" style=\"background-image:url('" +
                    _superlayerTextureFilename.findAndReplace("\\", "/") + "');\"></div>\n";
    }

    for (uint16_t x = 1; x < _width; ++x)
    {
        body += L"    <div class=\"gridLine vertical\" style=\"left:";
        body += UnicodeStrings::fromInt64(x * TILE_SIZE);
        body += "px;\"></div>\n";
    }
    for (uint16_t y = 1 ; y < _height; ++y)
    {
        body += L"    <div class=\"gridLine horizontal\" style=\"top:";
        body += UnicodeStrings::fromInt64(y * TILE_SIZE);
        body += "px;\"></div>\n";
    }

    body += L"</div>\n<hr />\n<h2>Overflows (Main)</h2>\n<ul>\n";

    // Main Overflows
    for (RptOverflow overflow : _mainOverflow)
    {
        body += L"    <li data-overflow=\"" + UnicodeStrings::fromInt64(overflow.spriteNo) +
            "\"><b>" + UnicodeStrings::fromInt64(overflow.spriteNo) + "</b>\n        <ul>\n";
        for (Vector2& instance : overflow.instances)
        {
            body += L"            <li>" + UnicodeStrings::fromInt64(instance.x) + ", " +
                    UnicodeStrings::fromInt64(instance.y) + "</li>\n";
        }
        body += L"        </ul>\n    </li>\n";
    }
    body += "</ul>\n";

    return body;
}

std::vector<ReportActionPtr> OverworldRenderableReport::getActions()
{
    std::vector<ReportActionPtr> actions;

    if (_superlayerTexture != nullptr)
    {
        actions.push_back(ReportActionPtr(new DomClassTogglerReportAction("Show Main Render", "div#main", "hide")));
        actions.push_back(ReportActionPtr(new DomClassTogglerReportAction("Show Superlayer Render", "div#superlayer", "hide")));
    }

    actions.push_back(ReportActionPtr(new OverflowTogglerReportAction()));

    return actions;
}

UnicodeString OverworldRenderableReport::getMainTextureFilename() const
{
    return _mainTextureFilename;
}

void OverworldRenderableReport::outputReferences()
{
    // Main reference
    TextureReference mainTextRef(_name + L" Main", _mainTexture);
    _mainTextureFilename = submitReference(mainTextRef);

    // Superlayer reference
    if (_superlayerTexture != nullptr)
    {
        TextureReference superlayerTextRef(_name + L" Superlayer", _superlayerTexture);
        _superlayerTextureFilename = submitReference(superlayerTextRef);
    }
}

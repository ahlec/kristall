#include "KristallCompiler.hpp"

RegionLayoutReport::RegionLayoutReport(uint32_t regionNo, UnicodeString regionName,
                                       std::vector<RegionMap*> outdoorMaps,
                                       std::vector<RegionInaccessibleArea> inaccessibleAreas) :
                                           Report(regionName + " (" +
                                                  UnicodeStrings::fromInt64(regionNo) +
                                                  ") Region Layout"),
                                           _outdoorMaps(outdoorMaps),
                                           _inaccessibleAreas(inaccessibleAreas)

{
}

UnicodeString RegionLayoutReport::outputHeader()
{
    RegionSize regionSize(getRegionSize());

    UnicodeString header =
        "        <style>\n"
        "          div.region {\n"
        "            position:relative;\n"
        "            background-color:yellow;\n"
        "          }\n"
        "          div.map, div.inaccessibleArea {\n"
        "            border:1px solid black;\n"
        "            position:absolute;\n"
        "            background-color:white;\n"
        "            text-align:center;\n"
        "            background-size:100% 100%;\n"
        "            background-repeat:no-repeat;\n"
        "          }\n"
        "          div.zone > div.cover {\n"
        "            position:absolute;\n"
        "            top:0px;\n"
        "            bottom:0px;\n"
        "            left:0px;\n"
        "            right:0px;\n"
        "            opacity:0.6;\n"
        "          }\n"
        "          div.zone.map > div.cover:hover {\n"
        "            background-color:#BCED91;\n"
        "          }\n"
        "          div.zone.inaccessibleArea > div.cover:hover {\n"
        "            background-color:#C67171;\n"
        "          }\n"
        "          div.noOutline {\n"
        "            border:none;\n"
        "          }\n"
        "          div.noLabelText {\n"
        "            font-size:0px;\n"
        "          }\n"
        "        </style>\n";
    header += "        <script type=\"text/javascript\">\n";
    header += "            var REGION_WIDTH = ";
    header += UnicodeStrings::fromInt64(regionSize.width);
    header += ";\n            var REGION_HEIGHT = ";
    header += UnicodeStrings::fromInt64(regionSize.height);
    header +=
        ";\n"
        "            $(document).ready(function () {\n"
        "                $(window).resize(function () {\n"
        "                    $(\"#region\").height($(window).width() * (REGION_HEIGHT / REGION_WIDTH));\n"
        "                }).resize();\n"
        "                borderRect();\n"
        "                inaccessibleAreas();\n"
        "                $(\"div.map, div.inaccessibleArea\").click(function (){\n"
        "                    window.open($(this).data(\"renderpage\"), \"_blank\");\n"
        "                });\n"
        "            });\n"
        "            function rect() {\n"
        "                $(\"div.map\").each(function (index, elem) {\n"
        "                    var rectangle = $(elem).data(\"rectangle\");\n"
        "                    $(elem).css(\"top\", ((rectangle[1] / REGION_HEIGHT) * 100) + \"%\").\n"
        "                           css(\"left\", ((rectangle[0] / REGION_WIDTH) * 100) + \"%\").\n"
        "                           css(\"width\", ((rectangle[2] / REGION_WIDTH) * 100) + \"%\").\n"
        "                           css(\"height\", ((rectangle[3] / REGION_HEIGHT) * 100) + \"%\").\n"
        "                           css(\"backgroundImage\", \"\");\n"
        "                });\n"
        "            };\n"
        "            function inaccessibleAreas() {\n"
        "                $(\"div.inaccessibleArea\").each(function (index, elem) {\n"
        "                    var rectangle = $(elem).data(\"rectangle\");\n"
        "                    $(elem).css(\"top\", ((rectangle[1] / REGION_HEIGHT) * 100) + \"%\").\n"
        "                           css(\"left\", ((rectangle[0] / REGION_WIDTH) * 100) + \"%\").\n"
        "                           css(\"width\", ((rectangle[2] / REGION_WIDTH) * 100) + \"%\").\n"
        "                           css(\"height\", ((rectangle[3] / REGION_HEIGHT) * 100) + \"%\").\n"
        "                           css(\"backgroundImage\", 'url(\"' + $(elem).data(\"render\") + '\")');\n"
        "                });\n"
        "            };\n"
        "            function borderRect() {\n"
        "                $(\"div.map\").each(function (index, elem) {\n"
        "                    var rectangle = $(elem).data(\"borderrectangle\");\n"
        "                    $(elem).css(\"top\", ((rectangle[1] / REGION_HEIGHT) * 100) + \"%\").\n"
        "                           css(\"left\", ((rectangle[0] / REGION_WIDTH) * 100) + \"%\").\n"
        "                           css(\"width\", ((rectangle[2] / REGION_WIDTH) * 100) + \"%\").\n"
        "                           css(\"height\", ((rectangle[3] / REGION_HEIGHT) * 100) + \"%\").\n"
        "                           css(\"backgroundImage\", 'url(\"' + $(elem).data(\"render\") + '\")');\n"
        "                });\n"
        "            };\n"
        "        </script>\n";

    return header;
}

UnicodeString RegionLayoutReport::outputBody()
{
    RegionSize regionSize(getRegionSize());

    UnicodeString body = "            <div class=\"region\" id=\"region\">\n";

    for (RegionMap*& regionMap : _outdoorMaps)
    {
        body += "                <div class=\"map zone\" data-rectangle=\"[";
        body += UnicodeStrings::fromInt64(regionMap->rectangle.x);
        body += ",";
        body += UnicodeStrings::fromInt64(regionMap->rectangle.y);
        body += ",";
        body += UnicodeStrings::fromInt64(regionMap->rectangle.width);
        body += ",";
        body += UnicodeStrings::fromInt64(regionMap->rectangle.height);
        body += "]\" data-borderrectangle=\"[";
        body += UnicodeStrings::fromInt64(regionMap->borderRectangle.x);
        body += ",";
        body += UnicodeStrings::fromInt64(regionMap->borderRectangle.y);
        body += ",";
        body += UnicodeStrings::fromInt64(regionMap->borderRectangle.width);
        body += ",";
        body += UnicodeStrings::fromInt64(regionMap->borderRectangle.height);
        body += "]\" data-mapname=\"";
        body += regionMap->getMapName();
        body += "\" data-mapno=\"";
        body += UnicodeStrings::fromInt64(regionMap->getMapNo());
        body += "\" title=\"";
        body += regionMap->getMapName();
        body += "\" data-render=\"";
        body += regionMap->getMainTextureReferenceFilename();
        body += "\" data-renderpage=\"";
        body += regionMap->getRenderableReportFilename();
        body += "\">";
        body += UnicodeStrings::fromInt64(regionMap->getMapNo());
        body += "<div class=\"cover\"></div></div>\n";
    }

    for (RegionInaccessibleArea& inaccessibleArea : _inaccessibleAreas)
    {
        body += "                <div class=\"inaccessibleArea zone\" data-rectangle=\"[";
        body += UnicodeStrings::fromInt64(inaccessibleArea.getX());
        body += ",";
        body += UnicodeStrings::fromInt64(inaccessibleArea.getY());
        body += ",";
        body += UnicodeStrings::fromInt64(inaccessibleArea.getWidth());
        body += ",";
        body += UnicodeStrings::fromInt64(inaccessibleArea.getHeight());
        body += "]\" data-render=\"";
        body += inaccessibleArea.getMainTextureReferenceFilename();
        body += "\" title=\"";
        body += inaccessibleArea.getName();
        body += "\" data-renderpage=\"";
        body += inaccessibleArea.getRenderableReportFilename();
        body += "\"><div class=\"cover\"></div></div>\n";
    }

    return body + "            </div>\n";
}

std::vector<ReportActionPtr> RegionLayoutReport::getActions()
{
    std::vector<ReportActionPtr> actions;

    actions.push_back(ReportActionPtr(new DomClassTogglerReportAction("Show Zone Outlines", "div.zone", "noOutline")));
    actions.push_back(ReportActionPtr(new DomClassTogglerReportAction("Show Zone Labels", "div.zone", "noLabelText")));

    return actions;
}

RegionLayoutReport::RegionSize RegionLayoutReport::getRegionSize()
{
    bool hasWidth(false);
    bool hasHeight(false);
    int32_t currentWidth(0);
    int32_t currentHeight(0);

    for (RegionMap*& regionMap : _outdoorMaps)
    {
        if (!hasWidth || regionMap->borderRectangle.x + regionMap->borderRectangle.width > currentWidth)
        {
            hasWidth = true;
            currentWidth = regionMap->borderRectangle.x + regionMap->borderRectangle.width;
        }

        if (!hasHeight || regionMap->borderRectangle.y + regionMap->borderRectangle.height > currentHeight)
        {
            hasHeight = true;
            currentHeight = regionMap->borderRectangle.y + regionMap->borderRectangle.height;
        }
    }

    for (RegionInaccessibleArea& inaccessibleArea : _inaccessibleAreas)
    {
        if (!hasWidth || inaccessibleArea.getX() + inaccessibleArea.getWidth() > currentWidth)
        {
            hasWidth = true;
            currentWidth = inaccessibleArea.getX() + inaccessibleArea.getWidth();
        }

        if (!hasHeight || inaccessibleArea.getY() + inaccessibleArea.getHeight() > currentHeight)
        {
            hasHeight = true;
            currentHeight = inaccessibleArea.getY() + inaccessibleArea.getHeight();
        }
    }

    RegionSize regionSize;
    regionSize.width = currentWidth;
    regionSize.height = currentHeight;
    return regionSize;
}

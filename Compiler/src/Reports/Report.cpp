#include "KristallCompiler.hpp"

Report::Report(UnicodeString title) : _title(title), _areReferencesSaved(false)
{
}

UnicodeString Report::getTitle() const
{
    return _title;
}

UnicodeString Report::getFilename() const
{
    return _title + ".htm";
}

void Report::saveReferences()
{
    if (_areReferencesSaved)
    {
        return;
    }

    _areReferencesSaved = true;
    outputReferences();
}

std::vector<ReportInfo> Report::getAdditionalInfo()
{
    return std::vector<ReportInfo>();
}

std::vector<ReportActionPtr> Report::getActions()
{
    return std::vector<ReportActionPtr>();
}

UnicodeString Report::outputHeader()
{
    return "";
}

void Report::outputReferences()
{
}

UnicodeString Report::submitReference(Reference& reportReference)
{
    // Open the file
    UnicodeString referenceFilename("reference//" + reportReference.getFilename());

    // Write to the file
    try
    {
        reportReference.write("reports//" + referenceFilename);
    }
    catch (ExecutionException& error)
    {
        error.addData("report title", _title);
        error.addData("reference filename", referenceFilename);
        throw;
    }

    // Finish up
    return referenceFilename;
}

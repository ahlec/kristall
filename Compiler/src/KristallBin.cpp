#include "KristallCompiler.hpp"
#include "CompilerVersions.h"

KristallBin::KristallBin() : _sizeOfHeader(0)
{
    _file = fopen("Kristall.bin", "wb");
    if (_file == nullptr)
    {
        ExecutionException error("An error occurred when attempting to open Kristall.bin for writing.");
        throw error;
    }

    _writer = new CompilerBinaryWriter(_file);
}

KristallBin::~KristallBin()
{
}

void KristallBin::addFile(File* file)
{
    UnicodeString contentType(typeid(*file).name());
    if (_contentFiles.count(contentType) == 0)
    {
        _contentTypes.push_back(contentType);
        _contentFiles[contentType] = std::vector<File*>();
        _sizeOfHeader += sizeof(uint16_t);
    }

    _contentFiles[contentType].push_back(file);

    // Write the file header
    _sizeOfHeader += sizeof(uint64_t);    // fileOffset
    _sizeOfHeader += sizeof(uint32_t);    // fileSize
    _sizeOfHeader += sizeof(uint8_t) + sizeof(uint8_t) + sizeof(uint8_t) + sizeof(uint8_t); // integrity bytes
}

void KristallBin::compile()
{
    CompilerBinaryWriter& writer(*_writer);

    uint64_t fileOffset = 0;
    uint32_t fileSize = 0;
    uint64_t accumulatedFileSize(0);
    for (UnicodeString& contentType : _contentTypes)
    {
        std::vector<File*>& contentFiles(_contentFiles[contentType]);
        size_t nFiles(contentFiles.size());
        if (nFiles > UINT16_MAX)
        {
            ExecutionException error("There are too many defined files of the provided type to fit into a UINT16.");
            error.addData("contentType", toString(UnicodeStrings::toStdString(contentType).c_str()));
            throw error;
        }
        writer.writeUInt16(static_cast<uint16_t>(nFiles));
        for (File* file : contentFiles)
        {
            fileOffset = _sizeOfHeader + accumulatedFileSize;
            fileSize = file->getBinarySize();
            accumulatedFileSize += fileSize;
            writer.writeUInt64(fileOffset);
            writer.writeUInt32(fileSize);
            writer.writeUInt8(0);
            writer.writeUInt8(17);
            writer.writeUInt8(fileOffset % UINT8_MAX);
            writer.writeUInt8(fileSize % UINT8_MAX);
        }
    }

    for (UnicodeString& contentType : _contentTypes)
    {
        std::vector<File*>& contentFiles(_contentFiles[contentType]);
        for (File* file : contentFiles)
        {
            file->write(writer);
        }
    }

    // Write Compiler Version information
    writer.writeUInt64(COMPILER_VERSION_MAIN);
    writer.writeUInt64(COMPILER_VERSION_FILE_SPRITESHEET);
    writer.writeUInt64(COMPILER_VERSION_FILE_GYMBADGE);
    writer.writeUInt64(COMPILER_VERSION_FILE_ITEM);
    writer.writeUInt64(COMPILER_VERSION_FILE_MAP);
    writer.writeUInt64(COMPILER_VERSION_FILE_POKEMONMOVE);
    writer.writeUInt64(COMPILER_VERSION_FILE_POKEMONSPECIES);
    writer.writeUInt64(COMPILER_VERSION_FILE_REGION);
    writer.writeUInt64(COMPILER_VERSION_FILE_TEXTURE);
    writer.writeUInt64(COMPILER_VERSION_FILE_FRAME);
}

void KristallBin::close()
{
    _writer->close();
    _writer = nullptr;

    for (UnicodeString& contentType : _contentTypes)
    {
        std::vector<File*>& contentFiles(_contentFiles[contentType]);
        for (File* file : contentFiles)
        {
            delete file;
        }
    }
}

File* KristallBin::retrieveFile(UnicodeString classType, uint32_t uid) const
{
    if (_contentFiles.count(classType) == 0)
    {
        ExecutionException error("Attempted to retrieve a File for a type that hasn't been compiled yet.");
        throw error;
    }

    const std::vector<File*>& contentFiles(_contentFiles.at(classType));

    // Try the obvious, as it should be sorted already
    if (contentFiles.size() > uid && contentFiles[uid]->getUId() == uid)
    {
        return contentFiles[uid];
    }

    // Check one higher
    if (contentFiles.size() > uid + 1 && contentFiles[uid + 1]->getUId() == uid)
    {
        return contentFiles[uid + 1];
    }

    // Check one lower
    if (uid >= 1 && contentFiles.size() > uid - 1 && contentFiles[uid - 1]->getUId() == uid)
    {
        return contentFiles[uid - 1];
    }

    // Just search for it
    for (File* file : contentFiles)
    {
        if (file->getUId() == uid)
        {
            return file;
        }
    }

    // It doesn't exist
    return nullptr;
}

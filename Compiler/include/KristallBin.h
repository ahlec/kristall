#ifndef COMPILER_INCLUDE_KRISTALLBIN_H
#define COMPILER_INCLUDE_KRISTALLBIN_H

class KristallBin
{
public:
    KristallBin();
    ~KristallBin();

    void addFile(File* file);

    void compile();
    void close();

    template<class T>
    T* askFor(uint32_t uid) const
    {
        UnicodeString classType(typeid(T).name());
        return &static_cast<T&>(*retrieveFile(classType, uid));
    };

private:
    FILE* _file;
    CompilerBinaryWriter* _writer;
    std::vector<UnicodeString> _contentTypes;
    std::map<UnicodeString, std::vector<File*>> _contentFiles;
    uint64_t _sizeOfHeader;

    File* retrieveFile(UnicodeString classType, uint32_t uid) const;
};

#endif

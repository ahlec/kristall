#ifndef COMPILER_INCLUDE_KRISTALLCOMPILERLOGCOLOR_H
#define COMPILER_INCLUDE_KRISTALLCOMPILERLOGCOLOR_H

enum class KristallCompilerLogColor
{
    Black,
    Green,
    Red,
    Yellow,
    Orange
};

#endif

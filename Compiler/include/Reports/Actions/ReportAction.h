#ifndef COMPILER_INCLUDE_REPORTS_ACTIONS_REPORTACTION_H
#define COMPILER_INCLUDE_REPORTS_ACTIONS_REPORTACTION_H

class ReportAction
{
public:
    ReportAction(UnicodeString label);

    UnicodeString outputLabel() const;
    virtual UnicodeString outputHead() const=0;
    virtual UnicodeString outputDom() const=0;

protected:
    UnicodeString getUId() const;

private:
    UnicodeString _label;
    UnicodeString _uid;
};

typedef std::shared_ptr<ReportAction> ReportActionPtr;

#endif

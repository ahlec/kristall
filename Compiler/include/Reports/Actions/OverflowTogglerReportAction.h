#ifndef COMPILER_INCLUDE_REPORTS_ACTIONS_OVERFLOWTOGGLERREPORTACTION_H
#define COMPILER_INCLUDE_REPORTS_ACTIONS_OVERFLOWTOGGLERREPORTACTION_H

class OverflowTogglerReportAction : public ReportAction
{
public:
    OverflowTogglerReportAction();

    virtual UnicodeString outputHead() const final;
    virtual UnicodeString outputDom() const final;
};

#endif

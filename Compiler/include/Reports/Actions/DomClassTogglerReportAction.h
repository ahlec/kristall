#ifndef COMPILER_INCLUDE_REPORTS_ACTIONS_DOMCLASSTOGGLERREPORTACTION_H
#define COMPILER_INCLUDE_REPORTS_ACTIONS_DOMCLASSTOGGLERREPORTACTION_H

class DomClassTogglerReportAction : public ReportAction
{
public:
    DomClassTogglerReportAction(UnicodeString label, UnicodeString subjectSelectors,
                                UnicodeString classA,
                                UnicodeString classB);
    DomClassTogglerReportAction(UnicodeString label, UnicodeString subjectSelectors,
                                UnicodeString classB);

    virtual UnicodeString outputHead() const final;
    virtual UnicodeString outputDom() const final;

private:
    UnicodeString _subjectSelectors;
    bool _hasClassA;
    UnicodeString _classA;
    UnicodeString _classB;
};

#endif

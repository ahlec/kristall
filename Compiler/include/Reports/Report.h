#ifndef COMPILER_INCLUDE_REPORTS_REPORT_H
#define COMPILER_INCLUDE_REPORTS_REPORT_H

class Report
{
public:
    Report(UnicodeString title);

    UnicodeString getTitle() const;
    UnicodeString getFilename() const;
    void saveReferences();
    virtual std::vector<ReportInfo> getAdditionalInfo();
    virtual std::vector<ReportActionPtr> getActions();
    virtual UnicodeString outputHeader();
    virtual UnicodeString outputBody()=0;

protected:
    virtual void outputReferences();
    UnicodeString submitReference(Reference& reportReference);

private:
    UnicodeString _title;
    bool _areReferencesSaved;
};

#endif

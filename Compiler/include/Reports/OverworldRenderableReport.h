#ifndef COMPILER_INCLUDE_REPORTS_OVERWORLDRENDERABLEREPORT_H
#define COMPILER_INCLUDE_REPORTS_OVERWORLDRENDERABLEREPORT_H

#include "DataTypes.h"
#include "General/Vector2.h"
#include <SDL.h>

class OverworldRenderableReport : public Report
{
public:
    OverworldRenderableReport(const OverworldRenderable& renderable,
                              uint32_t mainTextureNo, uint32_t superlayerTextureNo);

    virtual std::vector<ReportInfo> getAdditionalInfo() final;
    virtual UnicodeString outputHeader() final;
    virtual UnicodeString outputBody() final;
    virtual std::vector<ReportActionPtr> getActions() final;

    UnicodeString getMainTextureFilename() const;

protected:
    virtual void outputReferences() final;

private:
    struct RptOverflow
    {
        uint32_t spriteNo;
        std::vector<Vector2> instances;
    };

    UnicodeString _name;
    uint16_t _width;
    uint16_t _height;
    uint32_t _mainTextureNo;
    uint32_t _superlayerTextureNo;
    SDL_Surface* _mainTexture;
    SDL_Surface* _superlayerTexture;
    UnicodeString _mainTextureFilename;
    UnicodeString _superlayerTextureFilename;
    std::vector<RptOverflow> _mainOverflow;
};

#endif

#ifndef COMPILER_INCLUDE_REPORTS_REPORTINFO_H
#define COMPILER_INCLUDE_REPORTS_REPORTINFO_H

struct ReportInfo
{
    UnicodeString label;
    UnicodeString value;
};

#endif

#ifndef COMPILER_INCLUDE_REPORTS_REFERENCES_REFERENCE_H
#define COMPILER_INCLUDE_REPORTS_REFERENCES_REFERENCE_H

class Report;

class Reference
{
    friend class Report;
public:
    Reference(UnicodeString filename);

    UnicodeString getFilename() const;

protected:
    virtual void write(UnicodeString fullFilename)=0;

private:
    UnicodeString _filename;
};

#endif

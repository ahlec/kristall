#ifndef COMPILER_INCLUDE_REPORTS_REFERENCES_TEXTUREREFERENCE_H
#define COMPILER_INCLUDE_REPORTS_REFERENCES_TEXTUREREFERENCE_H

#include <SDL.h>

class TextureReference : public Reference
{
public:
    TextureReference(UnicodeString name, SDL_Surface* texture);

protected:
    virtual void write(UnicodeString fullFilename) final;

private:
    SDL_Surface* _texture;
};

#endif

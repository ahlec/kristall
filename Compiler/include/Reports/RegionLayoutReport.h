#ifndef COMPILER_INCLUDE_REPORTS_REGIONLAYOUTREPORT_H
#define COMPILER_INCLUDE_REPORTS_REGIONLAYOUTREPORT_H

class RegionLayoutReport : public Report
{
public:
    RegionLayoutReport(uint32_t regionNo, UnicodeString regionName,
                       std::vector<RegionMap*> outdoorMaps,
                       std::vector<RegionInaccessibleArea> inaccessibleAreas);

    virtual UnicodeString outputHeader() final;
    virtual UnicodeString outputBody() final;
    virtual std::vector<ReportActionPtr> getActions() override;

private:
    std::vector<RegionMap*> _outdoorMaps;
    std::vector<RegionInaccessibleArea> _inaccessibleAreas;

    struct RegionSize
    {
        int32_t width;
        int32_t height;
    };

    RegionSize getRegionSize();
};

#endif

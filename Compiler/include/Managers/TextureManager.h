#ifndef COMPILER_INCLUDE_MANAGERS_MANAGER_H
#define COMPILER_INCLUDE_MANAGERS_MANAGER_H

class TextureManager
{
    friend int main(int argc, char **argv);
public:
    static uint32_t give(UnicodeString textureFilename, File* issuer, UnicodeString name);
    static uint32_t give(SDL_Surface* entry, File* issuer, UnicodeString name);
    static uint32_t getTextureNo(File* issuer, UnicodeString name);
    static SDL_Surface* loadTexture(File* issuer, UnicodeString name);
    static SDL_Surface* loadTexture(uint32_t textureNo);
    static UnicodeString getTextureFilename(uint32_t textureNo);

private:
    XmlDocument _xml;
    uint32_t _nextId;

    TextureManager();
    UnicodeString getNodeName(File* issuer, UnicodeString name) const;
    XmlElementPtr retrieveNode(File* issuer, UnicodeString name);
    static void compile(KristallBin& kristallBin);
    void close();
};

#endif

#ifndef COMPILER_INCLUDE_XML_XMLDATA_H
#define COMPILER_INCLUDE_XML_XMLDATA_H

#include <rapidxml.hpp>

class XmlData
{
public:
    XmlData(rapidxml::xml_base<UChar>* xmlBase, bool isName);

    UnicodeString getString() const;

    uint8_t getUInt8() const;
    uint8_t getUInt8Level() const;
    uint16_t getUInt16() const;
    uint32_t getUInt32() const;

    int8_t getInt8() const;
    int16_t getInt16() const;
    int32_t getInt32() const;
    int64_t getInt64() const;

    float getFloat() const;

    bool getBool() const;
    Direction getDirection() const;
    ElementalType getElementalType() const;

private:
    rapidxml::xml_base<UChar>* _xmlBase;
    bool _isName;

};

typedef std::shared_ptr<XmlData> XmlDataPtr;

#endif

#ifndef COMPILER_INCLUDE_XML_XMLELEMENT_H
#define COMPILER_INCLUDE_XML_XMLELEMENT_H

#include <rapidxml.hpp>

class XmlElement : public XmlParent
{
public:
    XmlElement(rapidxml::xml_node<UChar>* node, XmlDocument* document);

    // Data
    XmlData getName() const;
    XmlData getValue() const;
    size_t getIndex() const;
    template<typename T>
    void setValue(T value)
    {
        _setValue(XmlValue(value));
    }

    // Child nodes
    bool isEmpty() const;

    // Self
    XmlElementPtr nextSibling(Predicate<XmlElement> predicate = nullptr) const;
    XmlElementPtr nextSibling(UnicodeString name) const;

private:
    rapidxml::xml_node<UChar>* _node;
    void _setValue(XmlValue value);
};

#endif

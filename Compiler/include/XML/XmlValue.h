#ifndef COMPILER_INCLUDE_XML_XMLVALUE_H
#define COMPILER_INCLUDE_XML_XMLVALUE_H

class XmlValue
{
public:
    XmlValue(UnicodeString str);
    XmlValue(std::string str);
    XmlValue(char* cStr);
    XmlValue(const char* str);
    XmlValue(uint64_t value);
    XmlValue(int64_t value);
    XmlValue(uint32_t value);
    XmlValue(int32_t value);
    XmlValue(uint16_t value);
    XmlValue(int16_t value);
    XmlValue(uint8_t value);
    XmlValue(int8_t value);
    XmlValue(double value);
    XmlValue(float value);
    XmlValue(bool value);

    template <typename T, class = typename std::enable_if<std::is_enum<T>::value, T>::type>
    XmlValue(T enumValue)
    {
        if (Enums::isRegisteredEnum<T>())
        {
            _strValue = Enums::toString<T>(enumValue);
        }
        else
        {
            ExecutionException error("Could not create an XmlValue using the templated constructor, as the type was not valid.");
            error.addData("T", toString(UnicodeString(typeid(T).name())));
            throw error;
        }
    }

    UnicodeString getStringValue() const;

private:
    UnicodeString _strValue;
};

#endif

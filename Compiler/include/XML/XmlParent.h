#ifndef COMPILER_INCLUDE_XML_XMLPARENT_H
#define COMPILER_INCLUDE_XML_XMLPARENT_H

#include <rapidxml.hpp>
#include "General/Predicate.h"

class XmlParent
{
    friend class XmlDocument;
public:
    XmlParent(XmlDocument* document);
    virtual ~XmlParent();

    uint64_t countChildren(Predicate<XmlElement> predicate = nullptr) const;
    uint64_t countChildren(UnicodeString name) const;
    bool hasChild(Predicate<XmlElement> predicate = nullptr) const;
    bool hasChild(UnicodeString name) const;
    XmlElementPtr firstChild(Predicate<XmlElement> predicate = nullptr) const;
    XmlElementPtr firstChild(UnicodeString name) const;
    XmlElementPtr appendChild(UnicodeString name);
    template<typename T>
    XmlElementPtr appendChild(UnicodeString name, T value)
    {
        return appendChildWithValue(name, convertToXmlValue(value));
    };
    void empty();

    // Attributes
    bool hasAttributes() const;
    bool hasAttribute(UnicodeString name) const;
    XmlDataPtr getAttribute(UnicodeString name) const;
    template<typename T>
    void setAttribute(UnicodeString name, T value)
    {
        setAttribute(name, convertToXmlValue(value));
    };
    void removeAttribute(UnicodeString name);

protected:
    void setAttribute(UnicodeString name, XmlValue value);
    void setNode(rapidxml::xml_node<UChar>* node);
    XmlElementPtr appendChildWithValue(UnicodeString name, XmlValue value);
    UChar* getString(UnicodeString str);
    XmlDocument* _doc;

    template<typename T>
    XmlValue convertToXmlValue(T value)
    {
        static_assert((!std::is_pointer<T>::value || std::is_same<T, const char*>::value), "<T> may only be a pointer if it is a char*");
        return XmlValue(value);
    };

private:
    rapidxml::xml_node<UChar>* _node;
};

#endif

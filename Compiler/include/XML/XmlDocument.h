#ifndef COMPILER_INCLUDE_XML_XMLDOCUMENT_H
#define COMPILER_INCLUDE_XML_XMLDOCUMENT_H

#include <rapidxml.hpp>

class XmlDocument : public XmlParent
{
    friend UChar* XmlParent::getString(UnicodeString);
public:
    XmlDocument(UnicodeString rootElement);
    XmlDocument(const XmlDocument& other);
    virtual ~XmlDocument();

    static XmlDocument load(UnicodeString fileName);
    void save(UnicodeString fileName);

private:
    XmlDocument(UnicodeString fileName, bool isLoadedExternally);
    rapidxml::xml_document<UChar> _document;
    UChar* _buffer;
    size_t _bufferLength;

    UChar* getOrAllocateString(UnicodeString);
    std::map<UnicodeString, UChar*> _allocatedStrings;
};

#endif

#ifndef COMPILER_INCLUDE_XML_PREDEFINE_H
#define COMPILER_INCLUDE_XML_PREDEFINE_H

class XmlParent;
class XmlElement;
class XmlDocument;

typedef std::shared_ptr<XmlElement> XmlElementPtr;

#endif

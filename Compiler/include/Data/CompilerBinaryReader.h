#ifndef COMPILER_INCLUDE_DATA_COMPILERBINARYREADER_H
#define COMPILER_INCLUDE_DATA_COMPILERBINARYREADER_H

#include "Data/BinaryReader.h"

class CompilerBinaryReader : public BinaryReader
{
public:
    CompilerBinaryReader(FILE* file);
    CompilerBinaryReader(FILE* file, size_t fileSize);

    SDL_Surface* readSurface();
};

#endif

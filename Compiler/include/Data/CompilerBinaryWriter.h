#ifndef COMPILER_INCLUDE_DATA_COMPILERBINARYWRITER_H
#define COMPILER_INCLUDE_DATA_COMPILERBINARYWRITER_H

#include "Data/BinaryWriter.h"
#include <SDL.h>

class CompilerBinaryWriter : public BinaryWriter
{
public:
    CompilerBinaryWriter(FILE* file);

    void writeLua(UnicodeString);

    void writeTexture(UnicodeString fileName);
    void writeTexture(SDL_Surface* surface);
};

#endif

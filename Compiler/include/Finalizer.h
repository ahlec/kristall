#ifndef COMPILER_INCLUDE_FINALIZER_H
#define COMPILER_INCLUDE_FINALIZER_H

#include "General/Function.h"

class Finalizer
{
    friend void callFinalizer();
public:
    static void chain(FunctionPtr<void> func);

private:
    static void call();
};

#endif

#ifndef COMPILER_INCLUDE_COMPONENTS_INACCESSIBLEAREACOLLECTOR_H
#define COMPILER_INCLUDE_COMPONENTS_INACCESSIBLEAREACOLLECTOR_H

#include "DataTypes.h"
#include "General/Vector2.h"
#include "General/Rect.h"

class InaccessibleAreaCollector
{
public:
    InaccessibleAreaCollector(UnicodeString regionName,
                              std::vector<Vector2> validTiles,
                              int32_t minX, int32_t minY, int32_t maxX,
                              int32_t maxY,
                              FunctionPtr<bool, int32_t, int32_t> isLocationEmpty,
                              FunctionPtr<bool, Rect> rectIntersects);

    bool hasMore();
    bool next();
    RegionInaccessibleArea get();

private:
    UnicodeString _regionName;
    std::vector<Vector2> _tiles;
    bool _hasNext;
    int32_t _minX;
    int32_t _minY;
    int32_t _maxX;
    int32_t _maxY;
    FunctionPtr<bool, int32_t, int32_t> _isLocationEmpty;
    FunctionPtr<bool, Rect> _rectIntersects;
    RegionInaccessibleArea _current;
    int64_t _currentIteration;

};

#endif

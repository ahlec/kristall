#ifndef COMPILER_INCLUDE_COMPONENTS_OCCUPANTS_OCCUPANTBLOCK_H
#define COMPILER_INCLUDE_COMPONENTS_OCCUPANTS_OCCUPANTBLOCK_H

#include "DataTypes.h"
#include "World/OverworldOccupantType.h"

class OccupantBlock
{
public:
    OccupantBlock(OverworldOccupantType occupantType, uint16_t tileX, uint16_t tileY);

    uint16_t getTileX() const;
    uint16_t getTileY() const;
    void write(CompilerBinaryWriter& writer);

protected:
    virtual void writeData(CompilerBinaryWriter& writer)=0;

private:
    OverworldOccupantType _occupantType;
    uint16_t _tileX;
    uint16_t _tileY;
};

typedef std::shared_ptr<OccupantBlock> OccupantBlockPtr;

#endif

#ifndef COMPILER_INCLUDE_COMPONENTS_OCCUPANTS_NAMEDOBJECTOCCUPANTBLOCK_H
#define COMPILER_INCLUDE_COMPONENTS_OCCUPANTS_NAMEDOBJECTOCCUPANTBLOCK_H

class NamedObjectOccupantBlock : public OccupantBlock
{
public:
    NamedObjectOccupantBlock(uint16_t tileX, uint16_t tileY, uint32_t spriteNo,
                             bool isAnimationInstance, bool isMovable,
                             bool isSign, bool isCounter, int16_t offsetX,
                             int16_t offsetY);

protected:
    virtual void writeData(CompilerBinaryWriter& writer) final;

private:
    uint32_t _spriteNo;
    bool _isAnimationInstance;
    bool _isMovable;
    bool _isSign;
    bool _isCounter;
    int16_t _offsetX;
    int16_t _offsetY;

};

#endif

#ifndef COMPILER_INCLUDE_COMPONENTS_OVERWORLDOCCUPANTBASE_H
#define COMPILER_INCLUDE_COMPONENTS_OVERWORLDOCCUPANTBASE_H

class OverworldOccupantBase
{
public:
    OverworldOccupantBase(SpriteSheet spriteSheet);
    std::vector<OccupantBlockPtr> create(uint16_t tileX, uint16_t tileY, MapFile* map,
                                         const KristallBin& kristallBin, XmlElementPtr occupantXml);

protected:
    virtual std::vector<OccupantBlockPtr> instantiate(uint16_t tileX, uint16_t tileY,
                                                      XmlElementPtr occupantXml, MapFile* map,
                                                      SpriteSheetFile* spriteSheet) = 0;

private:
    SpriteSheet _spriteSheet;
};

#endif

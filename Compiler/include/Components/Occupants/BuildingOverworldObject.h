#ifndef COMPILER_INCLUDE_COMPONENTS_BUILDINGOVERWORLDOBJECT_H
#define COMPILER_INCLUDE_COMPONENTS_BUILDINGOVERWORLDOBJECT_H

class BuildingOverworldObject : public OverworldOccupantBase
{
public:
    BuildingOverworldObject(UnicodeString handle);

protected:
    virtual std::vector<OccupantBlockPtr> instantiate(uint16_t tileX, uint16_t tileY,
                                                      XmlElementPtr occupantXml, MapFile* map,
                                                      SpriteSheetFile* spriteSheet) final;

private:
    struct FacadeBlock
    {
        uint16_t x;
        uint16_t y;
        uint32_t spriteNo;
        bool isOverhead;
        bool isSign;
        int16_t offsetX;
        int16_t offsetY;
    };

    class FacadeBlockLocator
    {
    public:
        FacadeBlockLocator(uint16_t x, uint16_t y);
        bool operator()(FacadeBlock& input);

    private:
        uint16_t _x;
        uint16_t _y;
    };

    UnicodeString _handle;
    bool _hasMultipleFormes;
    uint64_t _nFormes;

    std::vector<std::vector<FacadeBlock>> _formes;

    std::vector<FacadeBlock> parseExplicit(XmlParent& xml);
    std::vector<FacadeBlock> parseSimple(XmlParent& xml);
};

#endif

#ifndef COMPILER_INCLUDE_COMPONENTS_NAMEDOVERWORLDOBJECT_H
#define COMPILER_INCLUDE_COMPONENTS_NAMEDOVERWORLDOBJECT_H

class NamedOverworldObject : public OverworldOccupantBase
{
public:
    NamedOverworldObject(UnicodeString handle);

protected:
    virtual std::vector<OccupantBlockPtr> instantiate(uint16_t tileX, uint16_t tileY,
                                                      XmlElementPtr occupantXml, MapFile* map,
                                                      SpriteSheetFile* spriteSheet) final;

private:
    struct Block
    {
        uint16_t tileX;
        uint16_t tileY;
        uint32_t spriteNo;
        bool isAnimationInstance;
        bool isSign;
        bool isCounter;
        int16_t offsetX;
        int16_t offsetY;
    };

    UnicodeString _handle;
    uint16_t _width;
    uint16_t _height;
    bool _isMovable;
    std::vector<Block> _blockBases;

};

#endif

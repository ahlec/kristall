#ifndef COMPILER_INCLUDE_COMPONENTS_OVERWORLDOCCUPANTMANAGER_H
#define COMPILER_INCLUDE_COMPONENTS_OVERWORLDOCCUPANTMANAGER_H

class OverworldOccupantManager
{
public:
    static std::vector<OccupantBlockPtr> getInstance(XmlElementPtr occupantXml,
                                                     MapFile* map, const KristallBin& kristallBin);

private:
    OverworldOccupantManager();

    std::map<UnicodeString, NamedOverworldObject> _namedObjects;
    std::map<UnicodeString, BuildingOverworldObject> _buildings;
};

#endif

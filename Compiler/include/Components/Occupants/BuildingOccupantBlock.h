#ifndef COMPILER_INCLUDE_COMPONENTS_OCCUPANTS_BUILDINGOCCUPANTBLOCK_H
#define COMPILER_INCLUDE_COMPONENTS_OCCUPANTS_BUILDINGOCCUPANTBLOCK_H

class BuildingOccupantBlock : public OccupantBlock
{
public:
    BuildingOccupantBlock(uint16_t tileX, uint16_t tileY, uint32_t spriteNo,
                          bool isOverhead, bool isSign, int16_t offsetX, int16_t offsetY);

protected:
    virtual void writeData(CompilerBinaryWriter& writer) final;

private:
    uint32_t _spriteNo;
    bool _isAnimationInstance;
    bool _isOverhead;
    bool _isSign;
    int16_t _offsetX;
    int16_t _offsetY;

};

#endif

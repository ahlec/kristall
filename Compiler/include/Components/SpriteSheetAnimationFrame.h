#ifndef COMPILER_INCLUDE_COMPONENTS_SPRITESHEETANIMATION_H
#define COMPILER_INCLUDE_COMPONENTS_SPRITESHEETANIMATION_H

struct SpriteSheetAnimationFrame
{
    uint32_t spriteId;
    uint16_t timeOffset;
    bool hasOverlay;
    uint32_t overlaySpriteId;
};

#endif

#ifndef COMPILER_INCLUDE_COMPONENTS_MAPBORDERBLOCK_H
#define COMPILER_INCLUDE_COMPONENTS_MAPBORDERBLOCK_H

struct MapBorderBlock
{
    uint16_t start;
    uint16_t end;
    MapBorder fill;
    MapBorder north;
    MapBorder east;
    MapBorder south;
    MapBorder west;
    bool isTouchingAMapDirectionOne; // relative to interpretation
    bool isTouchingAMapDirectionTwo; // relative to interpretation
};

#endif

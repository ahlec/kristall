#ifndef COMPILER_INCLUDE_COMPONENTS_REGIONINACCESSIBLEAREA_H
#define COMPILER_INCLUDE_COMPONENTS_REGIONINACCESSIBLEAREA_H

class SpriteSheetFile;

class RegionInaccessibleArea
{
public:
    RegionInaccessibleArea(UnicodeString name, int32_t x, int32_t y,
                           int32_t width, int32_t height);

    UnicodeString getName() const;
    int32_t getX() const;
    int32_t getY() const;
    int32_t getWidth() const;
    int32_t getHeight() const;
    UnicodeString getRenderableReportFilename() const;
    UnicodeString getMainTextureReferenceFilename() const;

    // X and Y are dealt with as absolute locations, in regards to parameters
    MapBorder getTile(int32_t x, int32_t y) const;
    void setTile(int32_t x, int32_t y, MapBorderTile tile,
                 SpriteSheetFile* overworldExteriorSpriteSheet);

    OverworldRenderableReport writeOverworldRenderable(CompilerBinaryWriter& writer, File* issuer);

private:
    UnicodeString _name;
    int32_t _x;
    int32_t _y;
    int32_t _width;
    int32_t _height;
    std::shared_ptr<OverworldRenderable> _overworldRenderable;
    std::vector<MapBorder> _tiles;
    UnicodeString _renderableReportFilename;
    UnicodeString _mainTextureReferenceFilename;
};

#endif

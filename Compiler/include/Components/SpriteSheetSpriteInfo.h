#ifndef COMPILER_INCLUDE_COMPONENTS_SPRITESHEETSPRITEINFO_H
#define COMPILER_INCLUDE_COMPONENTS_SPRITESHEETSPRITEINFO_H

#include "DataTypes.h"
#include "General/Vector2.h"

struct SpriteSheetSpriteInfo
{
    uint32_t id;
    bool isUniversalAnimation;
    Vector2 topLeftCorner;
    Vector2 topRightCorner;
    Vector2 bottomRightCorner;
    Vector2 bottomLeftCorner;
    uint16_t width;
    uint16_t height;
    int32_t offsetX;
    int32_t offsetY;
};

#endif

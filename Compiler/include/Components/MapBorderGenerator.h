#ifndef COMPILER_INCLUDE_COMPONENTS_MAPBORDERGENERATOR_H
#define COMPILER_INCLUDE_COMPONENTS_MAPBORDERGENERATOR_H

#include "DataTypes.h"
#include "Cornerstones.h"
#include "General/Rect.h"
#include "General/Math.h"
class SpriteSheetFile;

class MapBorderGenerator
{
public:
    MapBorderGenerator(bool isRowOriginalParityEven = true,
                       bool isColumnOriginalParityEven = true);

    void process(std::vector<RegionMap*>& regionMaps);
    void fixOrigin(std::vector<RegionMap*>& regionMaps);
    std::vector<RegionInaccessibleArea> getInaccessibleRegions(UnicodeString regionName,
                                                               std::vector<RegionMap*>& regionMaps,
                                                               SpriteSheetFile* overworldExteriorSpriteSheet);
    void drawMapBorders(std::vector<RegionMap*>& regionMaps,
                        SpriteSheetFile* overworldExteriorSpriteSheet);

private:
    struct ValueResults
    {
        bool hasValue;
        int32_t value;
    };

    struct MapBorderResults
    {
        bool hasValue;
        MapBorder value;
    };

    enum class RectField
    {
        Top,
        Left,
        Bottom,
        Right
    };
    enum class SearchCriteria
    {
        Min,
        Max
    };
    enum class SearchDomains
    {
        Both,
        RectanglesOnly,
        BorderRectanglesOnly
    };
    enum class SearchFilter
    {
        AllRects,
        OnlyRectsAbove,
        OnlyRectsBelow,
        OnlyRectsLeft,
        OnlyRectsRight
    };
    enum class ReturnValue
    {
        Field,
        Index
    };

    bool _isRowOriginalParityEven;
    bool _isColumnOriginalParityEven;
    std::vector<RegionMap*> _regionMaps;
    std::vector<RegionInaccessibleArea> _inaccessibleAreas;

    void input(std::vector<RegionMap*>& regionMaps,
               bool inputBorderRectangles = true);
    void initializeBorderRectangles();
    void resolveConflicts();
    void retractI();
    void retractII();
    void retractIII();
    void expandSouthwest();
    void adjustOrigin();
    void output(std::vector<RegionMap*>& regionMaps);

    void validate(size_t index) const;
    void validateAll() const;
    bool doesPassFilter(SearchFilter filter, const Rect& testValue,
                        const Rect& controlGroup) const;
    ValueResults findValue(RectField field, SearchCriteria criteria,
                           SearchDomains domain = SearchDomains::Both,
                           ReturnValue returnValue = ReturnValue::Field,
                           std::vector<size_t> subset = std::vector<size_t>());
    ValueResults searchRects(RectField field, SearchCriteria criteria,
                             Rect relativeValue,
                             SearchDomains domain = SearchDomains::Both,
                             SearchFilter filter = SearchFilter::AllRects);
    bool isLocationEmpty(int32_t x, int32_t y);
    bool doesRectIntersectAnywhere(Rect rect);
    MapBorderResults getMapBorder(int32_t x, int32_t y, Direction askingDirection);
    MapBorderTile startOutdoorMapBorderTile(int32_t currentX, int32_t currentY,
                                            Rect& rectangle, Rect& borderRectangle);

    inline bool isCurrentColumnOdd(uint32_t absoluteX)
    {
        return ((int)std::floor(absoluteX / TILE_SIZE) % 2 == (_isColumnOriginalParityEven ? 1 : 0));
    }
    inline bool isCurrentRowOdd(uint32_t absoluteY)
    {
        return ((int)std::floor(absoluteY / TILE_SIZE) % 2 == (_isRowOriginalParityEven ? 1 : 0));
    }
};

#endif

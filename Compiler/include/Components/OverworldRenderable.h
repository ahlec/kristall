#ifndef COMPILER_INCLUDE_COMPONENTS_OVERWORLDRENDERABLE_H
#define COMPILER_INCLUDE_COMPONENTS_OVERWORLDRENDERABLE_H

#include "DataTypes.h"
#include "General/Vector2.h"

class File;
class SpriteSheetFile;
class OverworldRenderableReport;

class OverworldRenderable
{
    friend class OverworldRenderableReport;
public:
    OverworldRenderable(UnicodeString name, uint16_t width,
                        uint16_t height);

    static std::shared_ptr<OverworldRenderable> deserialize(CompilerBinaryReader& reader);

    UnicodeString getName() const;
    uint16_t getWidth() const;
    uint16_t getHeight() const;

    void add(uint16_t tileX, uint16_t tileY,
             SpriteSheetFile* spriteSheet, uint32_t entryNo,
             bool addToSuperlayer = false);

    void blendIn(OverworldRenderable& other, uint16_t tileX, uint16_t tileY);

    OverworldRenderableReport write(CompilerBinaryWriter& writer, File* issuer);
    void serialize(CompilerBinaryWriter& writer);

private:
    class Overflow
    {
    public:
        Overflow(uint32_t sprteId);
        uint32_t spriteId;
        std::vector<Vector2> instances;

        bool hasIntersection(Vector2& location);
    };

    UnicodeString _name;
    uint16_t _width;
    uint16_t _height;
    bool _hasSuperlayer;
    std::vector<bool> _isTileCapped;
    std::vector<bool> _isSuperlayerCapped;
    std::vector<Overflow> _overflows;
    std::vector<Overflow> _superlayerOverflows;
    //std::map<uint32_t, std::vector<Vector2>> _overflows;
    //std::map<uint32_t, std::vector<Vector2>> _superlayerOverflows;
    SDL_Surface* _mainTexture;
    SDL_Surface* _superlayerTexture;

    inline bool isCapped(uint16_t tileX, uint16_t tileY, bool isSuperlayer) const
    {
        if (isSuperlayer)
        {
            return _isSuperlayerCapped[(tileY * _width) + tileX];
        }

        return _isTileCapped[(tileY * _width) + tileX];
    };

    void addOverflow(uint32_t entryNo, bool addToSuperlayer, int32_t absoluteX, int32_t absoluteY);
};

#endif

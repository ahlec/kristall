#ifndef COMPILER_INCLUDE_COMPONENTS_REGIONMAP_H
#define COMPILER_INCLUDE_COMPONENTS_REGIONMAP_H

#include "DataTypes.h"
#include "General/Rect.h"

class RegionMap
{
public:
    RegionMap(UnicodeString mapHandle);

    UnicodeString getSourceXmlFilename() const;
    bool isExteriorOverworld() const;

    uint32_t getRegionNo() const;
    uint32_t getMapNo() const;
    UnicodeString getMapName() const;
    uint16_t getWidth() const;
    uint16_t getHeight() const;
    std::vector<RegionMapBridge> getMapBridges() const;
    UnicodeString getMainTextureReferenceFilename() const;
    UnicodeString getRenderableReportFilename() const;

    MapBorder getNorthMapBorder(uint16_t x) const;
    MapBorder getEastMapBorder(uint16_t y) const;
    MapBorder getSouthMapBorder(uint16_t x) const;
    MapBorder getWestMapBorder(uint16_t y) const;

    bool isRectangleCalculated;
    Rect rectangle;
    bool isBorderRectangleCalculated;
    Rect borderRectangle;

    void markFullyCalculated();

    void addSprite(uint16_t tileX, uint16_t tileY, uint32_t spriteNo,
                   bool isAnimationInstance, SpriteSheetFile* spriteSheet,
                   bool isSuperlayer = false);
    void drawMapBorder(uint16_t fullTileX, uint16_t fullTileY, MapBorderTile mapBorderTile,
                       SpriteSheetFile* spriteSheet);
    void blendInMapOverworldRenderable(OverworldRenderable& mapOverworldRenderable);
    OverworldRenderableReport writeOverworldRenderable(CompilerBinaryWriter& writer, File* issuer);

private:
    UnicodeString _handle;
    bool _isOutdoor;
    std::vector<RegionMapBridge> _mapBridges;
    uint32_t _regionNo;
    uint32_t _mapNo;
    UnicodeString _mapName;
    uint16_t _width;
    uint16_t _height;
    std::vector<MapBorder> _mapBorders;

    std::shared_ptr<OverworldRenderable> _overworldRenderable;
    UnicodeString _renderableReportFilename;

};

#endif

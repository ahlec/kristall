#ifndef COMPILER_INCLUDE_COMPONENTS_SPRITESHEETSPRITE_H
#define COMPILER_INCLUDE_COMPONENTS_SPRITESHEETSPRITE_H

#include "DataTypes.h"
#include "XML/Predefine.h"
#include "Graphics/SpriteTransformation.h"
#include "General/Vector2.h"
#include "General/Rect.h"

class SpriteSheetSprite
{
public:
    SpriteSheetSprite(XmlElementPtr node);
    SpriteSheetSprite();

    uint32_t getId() const;
    bool isUniversalAnimation() const;
    SpriteTransformation getTransformation() const;
    Vector2 getTopLeftCorner(const std::vector<SpriteSheetSprite>& definedSprites) const;
    Vector2 getTopRightCorner(const std::vector<SpriteSheetSprite>& definedSprites) const;
    Vector2 getBottomRightCorner(const std::vector<SpriteSheetSprite>& definedSprites) const;
    Vector2 getBottomLeftCorner(const std::vector<SpriteSheetSprite>& definedSprites) const;
    uint16_t getWidth(const std::vector<SpriteSheetSprite>& definedSprites) const;
    uint16_t getHeight(const std::vector<SpriteSheetSprite>& definedSprites) const;
    int32_t getOffsetX() const;
    int32_t getOffsetY() const;

    XmlElementPtr getXml() const;

private:
    XmlElementPtr _node;
    uint32_t getBaseSpriteId() const;
    Rect getDefinedRect() const;

};

#endif

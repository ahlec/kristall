#ifndef COMPILER_INCLUDE_COMPONENT_MAPBORDER_H
#define COMPILER_INCLUDE_COMPONENT_MAPBORDER_H

#include "DataTypes.h"

enum class MapBorder : uint8_t
{
    None,
    Black,
    Trees,
    Ocean,
    Mountain,
    Grass,
    WhiteFence,
    DoubleTierMountain,
    OddTrees
};

#endif

#ifndef COMPILER_INCLUDE_COMPONENTS_TILELOCATOR_H
#define COMPILER_INCLUDE_COMPONENTS_TILELOCATOR_H

class TileLocator : public Functor<bool, const XmlElement&>
{
public:
    TileLocator(uint16_t x, uint16_t y);

    virtual bool operator()(const XmlElement& node) final;

private:
    uint16_t _x;
    uint16_t _y;
};

#endif

#ifndef COMPILER_INCLUDE_COMPONENTS_MAPBORDERTILE_H
#define COMPILER_INCLUDE_COMPONENTS_MAPBORDERTILE_H

#include "General/Direction.h"
#include "Components/MapBorder.h"

class MapBorderTile
{
public:
    MapBorderTile(Direction traverseDirection, bool isRowOdd,
                  bool isColumnOdd, bool isRelative = true);


    MapBorder fill;
    MapBorder north;
    MapBorder east;
    MapBorder south;
    MapBorder west;

    Direction getTraverseDirection() const;
    MapBorder getForwardBorder() const;
    MapBorder getBackwardBorder() const;
    MapBorder getRightBorder() const;
    MapBorder getLeftBorder() const;

    uint8_t getIndex() const;

private:
    Direction _traverseDirection;
    bool _isRowOdd;
    bool _isColumnOdd;
    bool _isRelative;
};

#endif

#ifndef COMPILER_INCLUDE_COMPONENTS_REGIONMAPBRIDGE_H
#define COMPILER_INCLUDE_COMPONENTS_REGIONMAPBRIDGE_H

struct RegionMapBridge
{
    uint16_t tileX;
    uint16_t tileY;
    uint32_t destinationMapNo;
    uint16_t destinationX;
    uint16_t destinationY;
    Direction inwardArrow;
};

#endif

#ifndef COMPILER_INCLUDE_KRISTALLCOMPILERLOG_H
#define COMPILER_INCLUDE_KRISTALLCOMPILERLOG_H

class KristallCompilerLog
{
    friend int main(int argc, char **argv);
    friend void handleSignals(int signalParam);
public:
    static uint64_t createNewSection(uint64_t parentSection = 0);
    static void setSectionText(uint64_t sectionNo, UnicodeString text,
                               KristallCompilerLogColor color = KristallCompilerLogColor::Black);
    static void appendSectionText(uint64_t sectionNo, UnicodeString text,
                                  KristallCompilerLogColor color = KristallCompilerLogColor::Black);
    static void appendSectionLink(uint64_t sectionNo, UnicodeString url, UnicodeString text);

private:
    static void start();
    static void done();
};

#endif

#ifndef COMPILER_INCLUDE_UTIL_HANDLE_H
#define COMPILER_INCLUDE_UTIL_HANDLE_H

namespace Handles
{
    uint32_t getHandleNo(UnicodeString type, UnicodeString handle);
};

#endif

#ifndef COMPILER_INCLUDE_UTIL_PNG_H
#define COMPILER_INCLUDE_UTIL_PNG_H

#include <SDL.h>

namespace PNG
{
    void crush(UnicodeString fileName, FunctionPtr<void, FILE*> callback);
    SDL_Surface* load(UnicodeString filename);
    void save(UnicodeString filename, SDL_Surface* surface);
};

#endif

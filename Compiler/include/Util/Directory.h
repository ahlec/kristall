#ifndef COMPILER_INCLUDE_UTIL_DIRECTORY_H
#define COMPILER_INCLUDE_UTIL_DIRECTORY_H

#include <sstream>
#include <vector>
#include <windows.h>
#include "unicode/unistr.h"
using namespace icu;
#include "General/Function.h"
#include "Util/UnicodeString.h"

namespace Directory
{
    #ifdef UNICODE
    typedef std::wostringstream tstringstream;
    #else
    typedef std::ostringstream tstringstream;
    #endif

    bool exists(UnicodeString directoryPath);
    void create(UnicodeString directoryPath);

    template<class T>
    std::vector<T*> getAllFiles(UnicodeString directoryPath, FunctionPtr<T*, UnicodeString> transformation)
    {
        // Start the procedure
        WIN32_FIND_DATA findFileData;
        HANDLE findFileHandle;
        tstringstream searchPath;
        searchPath << UnicodeStrings::toStdString(directoryPath) << "\\*";
        std::vector<T*> returnValues;

        // Perform the first search
        findFileHandle = FindFirstFile(searchPath.str().c_str(), &findFileData);
        if (findFileHandle == INVALID_HANDLE_VALUE)
        {
            ExecutionException error("An error occurred when attempting to search the directory.");
            throw error;
        }

        // Search the files
        std::stringstream handleStream;
        do
        {
            if (!(findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
            {
                T* transformedEntry = transformation(UnicodeString(findFileData.cFileName));
                if (transformedEntry != nullptr)
                {
                    returnValues.push_back(transformedEntry);
                }
            }
        } while (FindNextFile(findFileHandle, &findFileData));

        // Wrap up
        FindClose(findFileHandle);
        return returnValues;
    }
};

#endif

#ifndef COMPILER_INCLUDE_UTIL_TEMPLATE_H
#define COMPILER_INCLUDE_UTIL_TEMPLATE_H

namespace Templates
{
    UnicodeString getTypeName(UnicodeString prettyFunction, UnicodeString typeParameter);
};

#endif

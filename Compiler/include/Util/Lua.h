#ifndef COMPILER_INCLUDE_UTIL_LUA_H
#define COMPILER_INCLUDE_UTIL_LUA_H

namespace Lua
{
    std::vector<char> compile(UnicodeString luaCode);
};

#endif

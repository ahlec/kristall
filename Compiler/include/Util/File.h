#ifndef COMPILER_INCLUDE_UTIL_FILE_H
#define COMPILER_INCLUDE_UTIL_FILE_H

namespace Files
{
    uint64_t getLastModifiedTimestamp(UnicodeString fileName);
};

#endif

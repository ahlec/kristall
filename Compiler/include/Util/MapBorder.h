#ifndef COMPILER_INCLUDE_UTIL_MAPBORDER_H
#define COMPILER_INCLUDE_UTIL_MAPBORDER_H

namespace MapBorders
{
    std::vector<uint32_t> getSpriteNos(MapBorderTile tileInfo);
};

#endif

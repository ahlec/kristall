#ifndef COMPILER_INCLUDE_UTIL_UNICODESTRING_H
#define COMPILER_INCLUDE_UTIL_UNICODESTRING_H

namespace UnicodeStrings
{
    std::string toStdString(UnicodeString);

    std::vector<UnicodeString> split(UnicodeString, UChar);

    int64_t toInt64(UnicodeString);
    UnicodeString fromInt64(int64_t);
    UnicodeString fromDouble(double);

    UnicodeString getCurrentDateTime();
    UnicodeString getCompileDateTime();
};

String toString(UnicodeString);

#endif

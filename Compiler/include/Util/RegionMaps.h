#ifndef COMPILER_INCLUDE_UTIL_REGIONMAPS_H
#define COMPILER_INCLUDE_UTIL_REGIONMAPS_H

namespace RegionMaps
{
    std::vector<RegionMap*> getOutdoorMaps(uint32_t regionNo);
};

#endif

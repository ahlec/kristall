#ifndef COMPILER_INCLUDE_UTIL_ENUM_H
#define COMPILER_INCLUDE_UTIL_ENUM_H

namespace Enums
{
    void _declareEnumValue(UnicodeString enumName, UnicodeString valueName, int64_t baseValue);
    int64_t _getEnumValue(UnicodeString enumName, UnicodeString valueName);
    UnicodeString _getValueName(UnicodeString enumName, int64_t baseValue);
    bool _isRegisteredEnum(UnicodeString enumName);

    template<typename T>
    using UnderlyingType = typename std::underlying_type<T>::type;

    template<typename T>
    using ValueMap = std::map<UnicodeString, T>;

    template<typename T>
    void declare(ValueMap<T> values)
    {
        UnicodeString enumName(typeid(T).name());
        //UnicodeString enumName(Templates::getTypeName(UnicodeString(__PRETTY_FUNCTION__), "T"));
        for (auto& keyValuePair : values)
        {
            _declareEnumValue(enumName, keyValuePair.first, static_cast<int64_t>(keyValuePair.second));
        }
    };

    template<typename T>
    UnderlyingType<T> parse(UnicodeString value)
    {
        UnicodeString enumName(typeid(T).name());
        //UnicodeString enumName(Templates::getTypeName(UnicodeString(__PRETTY_FUNCTION__), "T"));
        return static_cast<UnderlyingType<T>>(_getEnumValue(enumName, value));
    };

    template<typename T>
    T parseKeep(UnicodeString value)
    {
        UnicodeString enumName(typeid(T).name());
        return static_cast<T>(_getEnumValue(enumName, value));
    };

    template<typename T>
    UnderlyingType<T> parseFlags(UnicodeString values, UChar delimiter = ',')
    {
        UnicodeString enumName(typeid(T).name());
        //UnicodeString enumName(Templates::getTypeName(UnicodeString(__PRETTY_FUNCTION__), "T"));
        int64_t compiled = 0;
        std::vector<UnicodeString> valueVector(UnicodeStrings::split(values, delimiter));
        for (UnicodeString& value : valueVector)
        {
            compiled |= _getEnumValue(enumName, value);
        }

        return static_cast<UnderlyingType<T>>(compiled);
    };

    template<typename T>
    UnicodeString toString(T value)
    {
        return _getValueName(UnicodeString(typeid(T).name()), static_cast<int64_t>(value));
    };

    template<typename T>
    bool isRegisteredEnum()
    {
        return _isRegisteredEnum(UnicodeString(typeid(T).name()));
    };
};

#endif

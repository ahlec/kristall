#ifndef COMPILER_INCLUDE_FILETYPES_ITEMFILE_H
#define COMPILER_INCLUDE_FILETYPES_ITEMFILE_H

#include "FileTypes/File.h"
#include "Items/ItemType.h"

class ItemFile : public File
{
public:
    ItemFile(UnicodeString handle);

    virtual uint32_t getUId() const override;

    bool canBeHeld() const;
    ItemType getItemType() const;
    bool getIsHm() const;

protected:
    virtual uint64_t getFileTypeVersion() const override;
    virtual UnicodeString getContentDirectoryName() const override;
    virtual UnicodeString getFileTypePrefix() const override;
    virtual void performUpdate(CompilerBinaryWriter& writer,
                               const KristallBin& kristallBin) override;
    virtual void populateData(CompilerBinaryReader& reader) override;

private:
    ItemType _itemType;
    bool _isHm;
};

#endif

#ifndef COMPILER_INCLUDE_FILETYPES_FRAMEFILE_H
#define COMPILER_INCLUDE_FILETYPES_FRAMEFILE_H

class FrameFile : public File
{
public:
    FrameFile(UnicodeString handle);
    virtual uint32_t getUId() const override;

protected:
    virtual uint64_t getFileTypeVersion() const override;
    virtual UnicodeString getContentDirectoryName() const override;
    virtual UnicodeString getFileTypePrefix() const override;
    virtual void performUpdate(CompilerBinaryWriter& writer,
                               const KristallBin& kristallBin) override;
};

#endif

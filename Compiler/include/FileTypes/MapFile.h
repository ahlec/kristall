#ifndef COMPILER_INCLUDE_FILETYPES_MAPFILE_H
#define COMPILER_INCLUDE_FILETYPES_MAPFILE_H

class MapFile : public File
{
public:
    MapFile(UnicodeString handle);
    virtual uint32_t getUId() const override;

    uint32_t getRegionNo() const;
    bool getIsOutdoor() const;
    uint16_t getWidth() const;
    uint16_t getHeight() const;
    OverworldRenderable& getOverworldRenderable() const;
    bool isTileVacant(uint16_t tileX, uint16_t tileY) const;

protected:
    virtual uint64_t getFileTypeVersion() const override;
    virtual UnicodeString getContentDirectoryName() const override;
    virtual UnicodeString getFileTypePrefix() const override;
    virtual void performUpdate(CompilerBinaryWriter& writer,
                               const KristallBin& kristallBin) override;
    virtual void populateData(CompilerBinaryReader& reader) override;

private:
    struct OccupiedTile
    {
        uint16_t x;
        uint16_t y;
        OccupantBlockPtr occupantPtr;
    };
    struct TileAnimInstance
    {
        uint32_t animationNo;
        uint16_t tileX;
        uint16_t tileY;
    };

    uint32_t _regionNo;
    bool _isOutdoor;
    uint16_t _width;
    uint16_t _height;

    std::shared_ptr<OverworldRenderable> _overworldRenderable;
    std::vector<OccupiedTile> _occupiedTiles;
    std::vector<TileAnimInstance> _tileAnimationInstances;

    void assertCanBeChildMap(UnicodeString parentMapHandle,
                             UnicodeString region);
    void assertOtherMapsTileOccupiable(const KristallBin& kristallBin,
                                   UnicodeString otherMapHandle,
                                   uint16_t tileX, uint16_t tileY);
};

#endif

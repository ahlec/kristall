#ifndef COMPILER_INCLUDE_FILETYPES_REGIONFILE_H
#define COMPILER_INCLUDE_FILETYPES_REGIONFILE_H

class RegionFile : public File
{
public:
    RegionFile(UnicodeString handle);
    virtual uint32_t getUId() const override;

    int32_t getFlightLocationIndex(uint32_t mapNo) const;

protected:
    virtual uint64_t getFileTypeVersion() const override;
    virtual UnicodeString getContentDirectoryName() const override;
    virtual UnicodeString getFileTypePrefix() const override;
    virtual void performUpdate(CompilerBinaryWriter& writer,
                               const KristallBin& kristallBin) override;
    virtual void populateData(CompilerBinaryReader& reader) override;

private:
    std::vector<uint32_t> _flightLocations;
    std::vector<RegionMap*> _outdoorMaps;

    void assertIsValidOccupationTile(UnicodeString mapHandle,
                                     uint16_t tileX, uint16_t tileY,
                                     bool requireOutdoor) const;
    size_t getOutdoorMapIndex(uint32_t mapNo) const;
};

#endif

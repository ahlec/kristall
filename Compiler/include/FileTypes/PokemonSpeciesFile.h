#ifndef COMPILER_INCLUDE_FILETYPES_POKEMONSPECIESFILE_H
#define COMPILER_INCLUDE_FILETYPES_POKEMONSPECIESFILE_H

class PokemonSpeciesFile : public File
{
public:
    PokemonSpeciesFile(UnicodeString handle);
    virtual uint32_t getUId() const override;

    ElementalType getType1() const;
    ElementalType getType2() const;

protected:
    virtual uint64_t getFileTypeVersion() const override;
    virtual UnicodeString getContentDirectoryName() const override;
    virtual UnicodeString getFileTypePrefix() const override;
    virtual void performUpdate(CompilerBinaryWriter& writer,
                               const KristallBin& kristallBin) override;
    virtual void populateData(CompilerBinaryReader& reader) override;

private:
    uint32_t _speciesNo;
    ElementalType _type1;
    ElementalType _type2;

    int64_t getWildHeldItem(const KristallBin& kristallBin,
                           XmlElementPtr node,
                           uint8_t rarity) const;
};

#endif

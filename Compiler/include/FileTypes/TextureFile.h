#ifndef COMPILER_INCLUDE_FILETYPES_TEXTUREFILE_H
#define COMPILER_INCLUDE_FILETYPES_TEXTUREFILE_H

class TextureFile : public File
{
public:
    TextureFile(uint32_t textureNo);
    virtual uint32_t getUId() const override;

protected:
    virtual uint64_t getFileTypeVersion() const override;
    virtual UnicodeString getContentDirectoryName() const override;
    virtual UnicodeString getFileTypePrefix() const override;
    virtual void performUpdate(CompilerBinaryWriter& writer,
                               const KristallBin& kristallBin) override;
    virtual bool hasSourceXml() const override;

private:
    uint32_t _textureNo;
};

#endif

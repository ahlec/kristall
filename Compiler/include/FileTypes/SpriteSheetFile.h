#ifndef COMPILER_INCLUDE_FILETYPES_SPRITESHEETFILE_H
#define COMPILER_INCLUDE_FILETYPES_SPRITESHEETFILE_H

#include "FileTypes/File.h"
#include "Graphics/SpriteSheet.h"

class SpriteSheetFile : public File
{
public:
    SpriteSheetFile(UnicodeString handle);
    virtual bool isValid() override;
    virtual uint32_t getUId() const override;

    uint32_t getTextureNo() const;
    SpriteSheetSpriteInfo getSprite(uint32_t spriteId) const;
    SDL_Surface* getTexture();

protected:
    virtual uint64_t getFileTypeVersion() const override;
    virtual UnicodeString getContentDirectoryName() const override;
    virtual UnicodeString getFileTypePrefix() const override;
    virtual void performUpdate(CompilerBinaryWriter& writer,
                               const KristallBin& kristallBin) override;
    virtual void populateData(CompilerBinaryReader& reader) override;

private:
    SpriteSheet _defines;
    UnicodeString _texture;
    SDL_Surface* _sfTexture;
    uint32_t _textureNo;
    std::vector<SpriteSheetSpriteInfo> _sprites;
    std::vector<UnicodeString> _animationNames;

    int64_t getAnimationNameNo(UnicodeString animationName) const;
    std::vector<SpriteSheetAnimationFrame> parseAnimation(UnicodeString animationString,
                                                          uint16_t duration) const;

    bool isTextureStoredInCompiler() const;
};

#endif

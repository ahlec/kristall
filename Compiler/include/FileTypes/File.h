#ifndef COMPILER_INCLUDE_FILE_H
#define COMPILER_INCLUDE_FILE_H

class File
{
public:
    File(UnicodeString handle);
    virtual ~File();

    virtual bool isValid();
    bool isCurrent() const;
    void update(const KristallBin& kristallBin, uint64_t contentSectionNo);
    uint64_t getBinarySize() const;
    void write(CompilerBinaryWriter& writer) const;
    virtual uint32_t getUId() const=0;
    UnicodeString getCompiledFileName() const;

protected:
    virtual uint64_t getFileTypeVersion() const=0;
    virtual UnicodeString getContentDirectoryName() const=0;
    virtual UnicodeString getFileTypePrefix() const=0;
    virtual void performUpdate(CompilerBinaryWriter& writer, const KristallBin& kristallBin)=0;
    void addSourceFilename(UnicodeString filename);
    virtual void populateData(CompilerBinaryReader& reader);
    XmlDocument openSourceXml() const;
    UnicodeString getHandle() const;
    virtual bool hasSourceXml() const;

    void writeReport(Report& report) const;

    UnicodeString getClipboardFileName() const;

private:
    UnicodeString _handle;
    std::vector<UnicodeString> _sourceFilenames;
    uint64_t _fileSectionNo;

    uint64_t getCompilerVersion() const;
    char* getBinaryFileName() const;
    FILE* openBinaryFile(bool isWrite) const;
};

#endif

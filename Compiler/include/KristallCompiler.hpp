#ifndef KRISTALLCOMPILER_H
#define KRISTALLCOMPILER_H

#include <iostream>
#include <windows.h>
#include <Shellapi.h>
#include <csignal>
#include <algorithm>

#include "Blacklist.h"

#include "unicode/unistr.h"
#include "unicode/ustdio.h"
#include "unicode/numfmt.h"
#include "unicode/datefmt.h"
using namespace icu;

#include "Finalizer.h"

#include "Components/MapBorder.h"
#include "Components/MapBorderTile.h"

#include "Util/UnicodeString.h"
#include "Util/Template.h"
#include "Util/Enum.h"
#include "Util/Directory.h"
#include "Util/File.h"
#include "Util/Handle.h"
#include "Util/Lua.h"
#include "Util/PNG.h"
#include "Util/MapBorder.h"

#include "Data/CompilerBinaryWriter.h"
#include "Data/CompilerBinaryReader.h"

#include "XML/Predefine.h"
#include "XML/XmlValue.h"
#include "XML/XmlData.h"
#include "XML/XmlParent.h"
#include "XML/XmlElement.h"
#include "XML/XmlDocument.h"

#include "Components/SpriteSheetSprite.h"
#include "Components/SpriteSheetSpriteInfo.h"
#include "Components/SpriteSheetAnimationFrame.h"
#include "Components/OverworldRenderable.h"
#include "Components/TileLocator.h"
#include "Components/RegionMapBridge.h"
#include "Components/RegionMap.h"
#include "Components/MapBorderBlock.h"
#include "Components/RegionInaccessibleArea.h"
#include "Components/InaccessibleAreaCollector.h"
#include "Components/MapBorderGenerator.h"
#include "Components/Occupants/OccupantBlock.h"
#include "Components/Occupants/NamedObjectOccupantBlock.h"
#include "Components/Occupants/BuildingOccupantBlock.h"

#include "Reports/References/Reference.h"
#include "Reports/References/TextureReference.h"

#include "Reports/Actions/ReportAction.h"
#include "Reports/Actions/DomClassTogglerReportAction.h"
#include "Reports/Actions/OverflowTogglerReportAction.h"

#include "Reports/ReportInfo.h"
#include "Reports/Report.h"
#include "Reports/RegionLayoutReport.h"
#include "Reports/OverworldRenderableReport.h"

#include "Util/RegionMaps.h"

class KristallBin;

#include "FileTypes/File.h"
#include "FileTypes/GymBadgeFile.h"
#include "FileTypes/ItemFile.h"
#include "FileTypes/PokemonMoveFile.h"
#include "FileTypes/PokemonSpeciesFile.h"
#include "FileTypes/SpriteSheetFile.h"
#include "FileTypes/RegionFile.h"
#include "FileTypes/MapFile.h"
#include "FileTypes/TextureFile.h"
#include "FileTypes/FrameFile.h"

#include "Managers/TextureManager.h"

#include "Components/Occupants/OverworldOccupantBase.h"
#include "Components/Occupants/NamedOverworldObject.h"
#include "Components/Occupants/BuildingOverworldObject.h"
#include "Components/Occupants/OverworldOccupantManager.h"

#include "KristallBin.h"

#define KRISTALLCOMPILER_LOG_FILE "KristallCompiler.htm"

#include "KristallCompilerLogColor.h"
#include "KristallCompilerLog.h"

#endif

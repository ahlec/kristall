# Pokémon Kristall #
##### (Desktop-native dedicated Pokémon game engine) #####

This is the primary, official repository for the Pokémon Kristall game engine, an initiative I originally started probably nine years ago now (spiritually, at least). The primary goals of this project are straightforward: to design an engine that is capable of fully imitating the game engine of the [main series Pokémon games](http://bulbapedia.bulbagarden.net/wiki/Core_series), using code specifically dedicated to this task. In addition to serving as an ambition project and product designed for ultimate consumption by others (either in engine format, as an implemented game, or in some middle stage), Kristall has served as a sandbox for me to explore new aspects of coding and to push the boundaries of what I believed I was capable of doing. This repository is for the C++ implementation of the Kristall engine; the previous version -- coded in C# -- can be found [here](https://bitbucket.org/jyavoc/pokemon-kristall). The C# version is no longer under active development, having been ported over to C++ in the desire to move my projects from C# to C++, to ultimately extend portability to non-Windows operating systems, and to have more control over the dependencies of the project.

### Specific goals ###
In addition to the primary objective specified above, these are other project goals to be considered throughout development:

* In regards to gameplay, the goal is to implement as close as possible the latest generation of the main series games ([Generation VI](http://bulbapedia.bulbagarden.net/wiki/Generation_VI)). This means in regards to how items and moves work, type matchups, content, and gameplay elements (among others);
* In regards to graphics, the goal is to emulate the graphics of [Generation III](http://bulbapedia.bulbagarden.net/wiki/Generation_III); while the main series has moved to using full 3D graphics, Kristall is content to remain with the 2D graphics, in the name of what would be easy for users of the engine to be able to produce content for, as well as giving creative liberties to ultimately branch off from what Generation III implemented without needing to continually compete against active development by Nintendo/GameFreak themselves;
* Produce an executable that is extremely lightweight. The ultimate goal here is to have a deployment that is just a few files: the main executable, the game content binary, and any binaries related to game settings or player saves, and licensing info as necessary. Part of the goal of this is to emulate how easy it is to get [Guild Wars](http://www.guildwars.com)/[Guild Wars 2](http://www.guildwars2.com) installed and running; additionally, however, is that having everything be statically built allows us to be completely in control of the dependencies being used, and so we can be aware of all issues and how the project is expected to be run.

### Projects ###
Kristall is actually comprised of a few different projects:

##### Engine #####
Engine is the game engine itself. This is the only application designed to be given to the player. This runs the game based off of the compiled game data.

##### Compiler #####
Compiler is the content compiler for Kristall. It uses easy-and-verbose XML input files as input, converting to the streamlined and conjoined binary format expected by Engine, as well as performing as many tasks as possible to save Engine from performing needless work.

### Development Environment ###
The development environment is Windows operating system as a base, but using [MinGW 3.20](http://www.mingw.org/) to compile. GCC is used as the compiler for all projects, and under MinGW 3.20, GCC is version 4.7.1. Code is developed in [Code::Blocks 13.12](http://www.codeblocks.org/).

### Dependencies ###
All libraries and programming dependencies necessary to compile the projects are now local to the repository and are referenced through relative paths, so that there is no additional setup necessary to build the programs. The following are the current dependencies for the different projects:

- [SDL2](https://www.libsdl.org/index.php): Engine, Compiler
- [SDL_ttf 2.0](https://www.libsdl.org/projects/SDL_ttf/): Engine
- [ICU 51.1](http://icu-project.org/apiref/icu4c/): Engine, Compiler
- [Lua 5.2](http://www.lua.org/): Engine, Compiler
- [libpng 1.6.10](http://www.libpng.org/pub/png/libpng.html): Compiler
- [zlib](http://www.zlib.net/): Compiler
- [libcurl 7.37.1](http://curl.haxx.se/libcurl/): Engine
- [RapidXml 1.13](http://curl.haxx.se/libcurl/): Compiler
- [edd-dbg 0.2.1](https://bitbucket.org/edd/dbg/wiki/Home): Engine, Compiler

### Additional notes ###
* This was my first project in C++. One of the reasons, in fact, that I chose to rewrite this into C++ from C# was because I wanted to move *to* C++ and I already had such an 
extensive codebase for Kristall that to port it from C# to C++ would allow me to focus on learning the nuances of the language without needing to additionally generate code from 
scratch. At this stage, my proficiency with C++ is no longer an issue that needs to be considered, having been working in C++ almost every day for over a year now. While I have gone 
through and cleaned up the code considerably, there will still be areas where beginner mistakes were made; these areas are most likely going to be areas which haven't received any 
work or needed any updating in a long time, as I make it a point to go back and update code that I see to be incorrect.